package cn.com.qytx.pushPlat.domain;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 功能:系统配置信息 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月9日
 * 修改日期: 2015年3月9日
 * 修改列表:
 */
public class SysConfig {
	
	/**
	 * mqtt服务器ip
	 */
	private String mqttServerIp;
	
	/**
	 * mqtt服务器端口号
	 */
	private String mqttServerPort;
	
	private Integer threadPoolSize;
	
	/**
	 * apns 生产环境
	 */
	private boolean apnsProduction;
	
	private static SysConfig instance = null;
	
	public SysConfig(){
		
	}
	
	public SysConfig(String mqttServerIp,String mqttServerPort,int threadPoolSize,boolean apnsProduction){
		this.mqttServerIp = mqttServerIp;
		this.mqttServerPort = mqttServerPort;
		this.threadPoolSize = threadPoolSize;
		this.apnsProduction = apnsProduction;
	}
	
	public static SysConfig getInstance() {
		if(instance == null){
			instance = (SysConfig) new ClassPathXmlApplicationContext("applicationContext.xml").getBean("sysConfig");
		}
		return instance;
	}
	
	public String getMqttServerIp() {
		return mqttServerIp;
	}

	public void setMqttServerIp(String mqttServerIp) {
		this.mqttServerIp = mqttServerIp;
	}

	public String getMqttServerPort() {
		return mqttServerPort;
	}

	public void setMqttServerPort(String mqttServerPort) {
		this.mqttServerPort = mqttServerPort;
	}

	public Integer getThreadPoolSize() {
		return threadPoolSize;
	}

	public void setThreadPoolSize(Integer threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
	}

	public boolean isApnsProduction() {
		return apnsProduction;
	}

	public void setApnsProduction(boolean apnsProduction) {
		this.apnsProduction = apnsProduction;
	}
}
