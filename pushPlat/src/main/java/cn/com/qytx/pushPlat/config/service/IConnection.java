package cn.com.qytx.pushPlat.config.service;


public interface IConnection {
	public final String MQTT_PUSH_TYPE="mqtt";
	public final String JPUSH_PUSH_TYPE="jpush";
}
