package cn.com.qytx.cbb.myapply.service.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import cn.com.qytx.cbb.affairs.domain.ReminderInfo;
import cn.com.qytx.cbb.affairs.service.IAffairsBody;
import cn.com.qytx.cbb.myapply.dao.MyStartedDao;
import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.cbb.myapply.domain.MyStarted;
import cn.com.qytx.cbb.myapply.domain.MyWaitProcess;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyWaitProcess;
import cn.com.qytx.cbb.myapply.service.MyApplyService;
import cn.com.qytx.cbb.push.service.PushPlatService;
import cn.com.qytx.cbb.push.service.PushPlatService.IOS_PUSH_TYPE;
import cn.com.qytx.cbb.push.service.PushPlatService.JPUSH_PUSH_TYPE;
import cn.com.qytx.cbb.push.service.PushPlatService.PLAT_TYPE;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.rangecontrol.PushControl;

/**
 * 功能：我的申请/审批 API接口的实现
 * 作者： jiayongqiang
 * 创建时间：2014年8月21日
 */
@Transactional
@Service("myApplyService")
public class MyApplyServiceImpl implements MyApplyService {

	@Resource
	private IMyProcessed myProcessedService;
	@Resource
	private IMyWaitProcess myWaitProcessService;
	@Resource
	private IUser userService;
	@Resource
	private MyStartedDao myStartedDao;
	@Autowired
	private PushPlatService pushPlatService;
	
	
	/**
     * 事务提醒主体Impl
     */
	@Autowired
    IAffairsBody affairsBodyImpl;
	
	@Override
	public void createProcess(UserInfo creater, String categoryName,
			String title, String instanceId,
			ModuleCode moduleCode) {
		//创建我的申请对象
		MyStarted myStarted = new MyStarted();
		myStarted.setCategoryName(categoryName);
		myStarted.setTitle(title);
		myStarted.setInstanceId(instanceId);
		myStarted.setCreaterId(creater.getUserId());
		myStarted.setCreaterName(creater.getUserName());
		myStarted.setCreaterTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		myStarted.setModuleCode(moduleCode.getCode());
		myStarted.setCompanyId(creater.getCompanyId());
		myStartedDao.saveOrUpdate(myStarted);
	}
	
	/**
	 * 功能：构造待审批对象
	 * 作者：jiayongqiang
	 * 参数：
	 * 输出：
	 */
	private MyWaitProcess generateWaitProcess(UserInfo creater,String categoryName,
			String title,String instanceId,UserInfo processer,
			ModuleCode moduleCode){
		MyWaitProcess wait = new MyWaitProcess();
		wait.setCategoryName(categoryName);
		wait.setTitle(title);
		wait.setInstanceId(instanceId);
		if(processer!=null){
			wait.setProcesserId(processer.getUserId());
			wait.setProcesserName(processer.getUserName());
		}
		wait.setStartTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		wait.setModuleCode(moduleCode.getCode());
		wait.setCreaterName(creater.getUserName());
		wait.setCompanyId(creater.getCompanyId());
		return wait;
	}
	/**
	 * 功能：构造待审批对象
	 * 作者：jiayongqiang
	 * 参数：
	 * 输出：
	 */
	private MyWaitProcess generateWaitProcess(UserInfo creater,String categoryName,
			String title,String instanceId,UserInfo processer,
			ModuleCode moduleCode,String taskName){
		MyWaitProcess wait = new MyWaitProcess();
		wait.setCategoryName(categoryName);
		wait.setTitle(title);
		wait.setInstanceId(instanceId);
		if(processer!=null){
			wait.setProcesserId(processer.getUserId());
			wait.setProcesserName(processer.getUserName());
		}
		wait.setStartTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		wait.setModuleCode(moduleCode.getCode());
		wait.setCreaterName(creater.getUserName());
		wait.setCompanyId(creater.getCompanyId());
		wait.setTaskName(taskName);
		return wait;
	}
	@Override
	public void approve(ModuleCode moduleCode, String instanceId,
			UserInfo approver, String advice,String state,String approveResult) {
		MyStarted start = myStartedDao.findByModuleCodeAndInstanceId(moduleCode, instanceId);
		MyWaitProcess wait =  myWaitProcessService.findByModuleCodeAndInstanceId(moduleCode, instanceId, approver.getUserId());
		if(wait!=null){
			//删除待审批记录
//			myWaitProcessService.delete(wait, true);
			myWaitProcessService.del(instanceId, wait.getModuleCode());
		}
		//创建已审批记录
		MyProcessed processed = new MyProcessed();
		processed.setCategoryName(start.getCategoryName());
		processed.setTitle(start.getTitle());
		processed.setInstanceId(instanceId);
		processed.setProcesserId(approver.getUserId());
		processed.setProcesserName(approver.getUserName());
		processed.setEndTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		processed.setModuleCode(moduleCode.getCode());
		processed.setCreaterName(start.getCreaterName());
		processed.setAdvice(advice);
		processed.setCompanyId(approver.getCompanyId());
		processed.setApproveResult(approveResult);
		myProcessedService.saveOrUpdate(processed);
		//更新申请表的状态
		updateState(moduleCode, instanceId, state);
	}
	@Override
	public void approve(ModuleCode moduleCode, String instanceId,
			UserInfo approver, String advice,String state,String approveResult,String taskName,UserInfo nextUser,GroupInfo nextGroup) {
		ModuleCode newmoduleCode = moduleCode; 
		if(taskName.equals("6")||taskName.equals("7")||taskName.equals("8")){
			newmoduleCode = ModuleCode.DEFECT;
		}
		MyStarted start = myStartedDao.findByModuleCodeAndInstanceId(newmoduleCode, instanceId);
		/*MyWaitProcess wait =  myWaitProcessService.findByModuleCodeAndInstanceId(moduleCode, instanceId, approver.getUserId());
		if(wait!=null||(taskName.equals("1")&&approveResult.equals("1"))){
			myWaitProcessService.del(instanceId, moduleCode.getCode());
		}*/
		myWaitProcessService.del(instanceId, moduleCode.getCode());
		//创建已审批记录
		MyProcessed processed = new MyProcessed();
		processed.setCategoryName(start.getCategoryName());
		processed.setTitle(start.getTitle());
		processed.setInstanceId(instanceId);
		processed.setProcesserId(approver.getUserId());
		processed.setProcesserName(approver.getUserName());
		processed.setEndTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		processed.setModuleCode(moduleCode.getCode());
		processed.setCreaterName(start.getCreaterName());
		processed.setAdvice(advice);
		processed.setCompanyId(approver.getCompanyId());
		processed.setApproveResult(approveResult);
		processed.setTaskName(taskName);
		float hours=0;
		if("3".equals(taskName)){//处理完成 计算时间
			MyProcessed processedBegin = myProcessedService.findByInstanceIdAndSate(instanceId);
			Timestamp beginTime=processedBegin.getEndTime();
			DecimalFormat   d  =   new  DecimalFormat("0.00"); 
			long from = beginTime.getTime();
			long to = System.currentTimeMillis(); 
			long thisTime=to-from;
			Double time= ((double)thisTime/3600000);
			hours= Float.parseFloat(d.format(time)) ;
			    
		}
		processed.setTimelong(hours);
		if(nextUser!=null){
			processed.setNextProcesserId(nextUser.getUserId());
			processed.setNextProcessType(1);
		}
		if(nextGroup!=null){
			processed.setNextProcesserId(nextGroup.getGroupId());
			processed.setNextProcessType(2);
		}
		myProcessedService.saveOrUpdate(processed);
		//更新申请表的状态
		updateState(moduleCode, instanceId, state);
	}
	@Override
	public void addApprover(String categoryName, String title,
			String instanceId, String nextAssigner, ModuleCode moduleCode) {
		// TODO Auto-generated method stub
		MyStarted started = myStartedDao.findByModuleCodeAndInstanceId(moduleCode, instanceId);
		if(started!=null){
			Integer userId = started.getCreaterId();
			UserInfo createrUser = userService.findOne(userId);
			if(nextAssigner == null){
				MyWaitProcess wait = generateWaitProcess(createrUser, categoryName, title, instanceId, null, moduleCode);
				myWaitProcessService.saveOrUpdate(wait);
			}else{
				List<MyWaitProcess> waits = new ArrayList<MyWaitProcess>();
				List<UserInfo> userlist = userService.findUsersByIds(nextAssigner);
				for(UserInfo u:userlist){
					MyWaitProcess w = generateWaitProcess(createrUser, categoryName, title, instanceId, u, moduleCode);
					waits.add(w);
				}
				myWaitProcessService.saveList(waits);
			}
			
		}
		
	}
	@Override
	public void addApprover(String categoryName, String title,
			String instanceId, String nextAssigner, ModuleCode moduleCode,String taskName,Integer processType,Integer cclType) {
		ModuleCode newmoduleCode = moduleCode; 
		if(taskName.equals("7")){
			newmoduleCode = ModuleCode.DEFECT;
		}
		// TODO Auto-generated method stub
		MyStarted started = myStartedDao.findByModuleCodeAndInstanceId(newmoduleCode, instanceId);
		if(started!=null){
			String content = "";
			String operType = "approve";
			String processName = "";
			if(!taskName.equals("1")&&!taskName.equals("5")||(taskName.equals("-1")&&processType.intValue()==2)){
				processName = "待接收";
				if("-1".equals(taskName)){
					processName = "待调度";
					operType = "dispatch";
				}
				if("3".equals(taskName)||"9".equals(taskName)){
					processName = "待处理";
					operType = "defect";
				}
				if("4".equals(taskName)){
					processName = "待验收";
					operType = "accept";
				}
				if("5".equals(taskName)){
					processName = "待归档";
					operType = "archive";
				}
				if("6".equals(taskName)){
					processName = "待延期";
					operType = "approveDelay";
				}
				content="您有一条"+processName+"的事件,请及时处理!";
				if("10".equals(taskName)){
					content = "您有一条事件验收未通过,请点击查看!";
				}
			}
			Integer userId = started.getCreaterId();
			UserInfo createrUser = userService.findOne(userId);
			
			Map<String,Object> reminderMap = new HashMap<String, Object>();
			reminderMap.put("sendType", "1_0_0");
			reminderMap.put("recordId", instanceId);
			reminderMap.put("moduleName", processName);
			reminderMap.put("content", content);
			reminderMap.put("fromUserInfo", createrUser);
			reminderMap.put("subjcet", "消息通知");
			String affairUrl = "logined/defect/view.jsp?oper="+operType+"&instanceId="+instanceId;
			if(cclType.intValue() != 0){
				affairUrl = "logined/defect/view2.jsp?oper="+operType+"&instanceId="+instanceId;
			}
			reminderMap.put("affairUrl", affairUrl);
			
			if(processType!= null&&processType.intValue()==1){
				UserInfo nextUser = userService.findOne(Integer.parseInt(nextAssigner));
				reminderMap.put("toIds", nextUser.getUserId()+"");
				MyWaitProcess wait = generateWaitProcess(createrUser, categoryName, title, instanceId, nextUser, moduleCode,taskName);
				myWaitProcessService.saveOrUpdate(wait);
				if(StringUtils.isNoneBlank(content)&&!"-1".equals(taskName)){//发送推送
					sendReminder(reminderMap);//发送提醒
					publishPush(nextUser.getUserId(),taskName,instanceId);
				}
				
			}else{
				List<UserInfo> userlist = userService.findUsersByGroupId(nextAssigner);
				List<MyWaitProcess> waits = new ArrayList<MyWaitProcess>();
				//List<UserInfo> userlist = userService.findUsersByIds(nextAssigner);
				String toIds = "";
				List<Integer> userIdCodeList = null;
				if("待调度".equals(processName)||"待接收".equals(processName)){
					if("待调度".equals(processName)){
						userIdCodeList= myStartedDao.getModuleByRoleCode(createrUser.getCompanyId(), "waitDispatch");
					}else{
						userIdCodeList = myStartedDao.getModuleByRoleCode(createrUser.getCompanyId(), "waitAccept");
					}
				}
				for(UserInfo u:userlist){
					if(userIdCodeList!=null&&!userIdCodeList.contains(u.getUserId())){
						continue;
					}
					MyWaitProcess w = generateWaitProcess(createrUser, categoryName, title, instanceId, u, moduleCode,taskName);
					waits.add(w);
					toIds +=u.getUserId()+",";
					if(StringUtils.isNoneBlank(content)){
						publishPush(u.getUserId(),taskName,instanceId);
					}
				}
				if(StringUtils.isNoneBlank(content)&&StringUtils.isNotBlank(toIds)){
					reminderMap.put("toIds",toIds);
					sendReminder(reminderMap);//发送提醒
				}
				
				myWaitProcessService.saveList(waits);
			}
			
		}
		
	}
	
	
	@Override
	public void carApprove(ModuleCode moduleCode, String instanceId,
			UserInfo approver, String advice,String state,String approveResult,String taskName,UserInfo nextUser,GroupInfo nextGroup,Integer carId) {
		MyStarted start = myStartedDao.findByModuleCodeAndInstanceId(moduleCode, instanceId);
		myWaitProcessService.del(instanceId, moduleCode.getCode());
		//创建已审批记录
		MyProcessed processed = new MyProcessed();
		processed.setCategoryName(start.getCategoryName());
		processed.setTitle(start.getTitle());
		processed.setInstanceId(instanceId);
		processed.setProcesserId(approver.getUserId());
		processed.setProcesserName(approver.getUserName());
		processed.setEndTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		processed.setModuleCode(moduleCode.getCode());
		processed.setCreaterName(start.getCreaterName());
		processed.setAdvice(advice);
		processed.setCarId(carId);
		processed.setCompanyId(approver.getCompanyId());
		processed.setApproveResult(approveResult);
		processed.setTaskName(taskName);
		if(nextUser!=null){
			processed.setNextProcesserId(nextUser.getUserId());
			processed.setNextProcessType(1);
		}
		if(nextGroup!=null){
			processed.setNextProcesserId(nextGroup.getGroupId());
			processed.setNextProcessType(2);
		}
		myProcessedService.saveOrUpdate(processed);
		//更新申请表的状态
		if(start!=null){
			start.setState(state);
			myStartedDao.saveOrUpdate(start);
			if("6".equals(taskName)){
				carDispatchPublishPush(start.getCompanyId(),start.getCreaterId()+"","6",instanceId);
			}
		}
	}
	
	@Override
	public void addCarApprover(String categoryName, String title,
			String instanceId, String nextAssigner, ModuleCode moduleCode,String taskName,Integer processType) {
		MyStarted started = myStartedDao.findByModuleCodeAndInstanceId(ModuleCode.CARDISPATCH, instanceId);
		if(started!=null){
			String content = "";
			String operType = "";
			String processName = "";
			if("0".equals(taskName)){//已上报
				processName = "已上报";
				operType = "apply";
			}
			if("1".equals(taskName)){//待派遣
				processName = "待派遣";
				operType = "dispatch";
			}
			
			if("2".equals(taskName)){//待发车
				processName = "待发车";
				operType = "startCar";
			}
			
			if("3".equals(taskName)){//待回场
				processName = "待派遣";
				operType = "backFactory";
			}
			
			if("1".equals(taskName)||"2".equals(taskName)){
				content="您有一条"+processName+"的事件,请及时处理!";
			}
			Integer userId = started.getCreaterId();
			UserInfo createrUser = userService.findOne(userId);
			Map<String,Object> reminderMap = new HashMap<String, Object>();
			reminderMap.put("sendType", "1_0_0");
			reminderMap.put("recordId", instanceId);
			reminderMap.put("moduleName", processName);
			reminderMap.put("content", content);
			reminderMap.put("fromUserInfo", createrUser);
			reminderMap.put("subjcet", "消息通知");
			String affairUrl = "logined/defect/viewCar.jsp?oper="+operType+"&instanceId="+instanceId;
			reminderMap.put("affairUrl", affairUrl);
			
			if(processType!= null&&processType.intValue()==1){
				UserInfo nextUser = userService.findOne(Integer.parseInt(nextAssigner));
				reminderMap.put("toIds", nextUser.getUserId()+"");
				MyWaitProcess wait = generateWaitProcess(createrUser, categoryName, title, instanceId, nextUser, moduleCode,taskName);
				myWaitProcessService.saveOrUpdate(wait);
				if(StringUtils.isNoneBlank(content)){//发送推送
					sendReminder(reminderMap);//发送提醒
					carDispatchPublishPush(nextUser.getCompanyId(),nextUser.getUserId()+"",taskName,instanceId);
				}
				
			}else{
				List<UserInfo> userlist = userService.findUsersByGroupId(nextAssigner);
				List<MyWaitProcess> waits = new ArrayList<MyWaitProcess>();
				String toIds = "";
				for(UserInfo u:userlist){
					MyWaitProcess w = generateWaitProcess(createrUser, categoryName, title, instanceId, u, moduleCode,taskName);
					waits.add(w);
					toIds +=u.getUserId()+",";
					
				}
				if(StringUtils.isNoneBlank(content)){
					carDispatchPublishPush(started.getCompanyId(),toIds,taskName,instanceId);
				}
				if(StringUtils.isNoneBlank(content)&&StringUtils.isNotBlank(toIds)){
					reminderMap.put("toIds",toIds);
					sendReminder(reminderMap);//发送提醒
				}
				
				myWaitProcessService.saveList(waits);
			}
			
		}
		
	}
	
	public void publishPush(final Integer userId,final String taskName,final String instanceId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String ids = userId+"";
				if(StringUtils.isNotEmpty(ids)){
					String[] idsArr = ids.split(",");
					String iosTags="";
					String androidTags="";
					for(String id:idsArr){
						if(StringUtils.isNotEmpty(id)){
							String iosTag = "ios_user_"+id;
							String androidTag = "android_user_"+id;
							iosTags+=(checkTest(iosTag)+",");
							androidTags+=(checkTest(androidTag)+",");
						}
					}
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("recordId", instanceId);
					map.put("moduleName", "事件通知");
					map.put("mt", "hm");
					Map<String, Object> extraSub=new HashMap<String, Object>();
					extraSub.put("sound", "default");
					extraSub.put("content-available", "1");
					extraSub.put("badge", "");
					map.put("ios",extraSub);
					String title="接收";
					String processName = "待接收";
					if("3".equals(taskName)){
						processName = "待处理";
					}
					if("4".equals(taskName)){
						processName = "待验收";
					}
					if("5".equals(taskName)){
						processName = "待归档";
					}
					if("6".equals(taskName)){
						processName = "待延期";
					}
					String content="您有一条"+processName+"的事件,请及时处理!";
					if("10".equals(taskName)){
						content = "您有一条事件验收未通过,请点击查看!";
					}
					System.out.println("=========push content:"+content);
					Gson json = new Gson();
					pushPlatService.pushJpushToPlat(userId, androidTags,PLAT_TYPE.ANDROID, content,title, json.toJson(map), JPUSH_PUSH_TYPE.TAG,null);
			    	pushPlatService.pushJpushToPlat(userId, iosTags, PLAT_TYPE.IOS, content,content, json.toJson(map), JPUSH_PUSH_TYPE.TAG,IOS_PUSH_TYPE.NOTIFY,null);
				}
			}
		}).start();
		
	}
	
	
	public void carDispatchPublishPush(final Integer companyId,final String userIds,final String taskName,final String instanceId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String ids = userIds;
				if(StringUtils.isNotEmpty(ids)){
					if(ids.endsWith(",")){
						ids = ids.substring(0, ids.length()-1);
					}
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("recordId", instanceId);
					map.put("moduleName", "事件通知");
					map.put("mt", "hm");
					Map<String, Object> extraSub=new HashMap<String, Object>();
					extraSub.put("sound", "default");
					extraSub.put("content-available", "1");
					extraSub.put("badge", "");
					map.put("ios",extraSub);
					String title="";
					String processName = "";
					if("1".equals(taskName)){
						processName = "待派遣";
					}
					if("2".equals(taskName)){
						processName = "待发车";
					}
					String content="您有一条"+processName+"的事件,请及时处理!";
					if("6".equals(taskName)){
						content = "您有一条待派遣事件被驳回,请点击查看!";
					}
					if("7".equals(taskName)){
						content = "有一条车辆调度工单,转诊到本科室,请点击查看!";
					}
					System.out.println("=========push content:"+content);
					Gson json = new Gson();
					String jsonMap = json.toJson(map);
					String[] idsArr = ids.split(",");
					String iosTags="";
					String androidTags="";
					for(String id:idsArr){
						if(StringUtils.isNotEmpty(id)){
							String iosTag = "ios_user_"+id;
							String androidTag = "android_user_"+id;
							iosTags=(checkTest(iosTag)+",");
							androidTags=(checkTest(androidTag)+",");
							pushPlatService.pushJpushToPlat(companyId, androidTags,PLAT_TYPE.ANDROID, content,title, jsonMap, JPUSH_PUSH_TYPE.TAG,null);
						}
					}
					
				}
			}
		}).start();
		
	}
	
	
	public String sendReminder(Map<String,Object> reminderMap){
		ReminderInfo reminderInfo = new ReminderInfo();
		reminderInfo.setSendType((String)reminderMap.get("sendType"));//"0_1_1"
        reminderInfo.setAffairContent((String)reminderMap.get("content"));
        reminderInfo.setAffairUrl("");
        reminderInfo.setToids((String)reminderMap.get("toIds"));
        reminderInfo.setFromUserInfo((UserInfo)reminderMap.get("fromUserInfo"));
        reminderInfo.setModuleName((String)reminderMap.get("moduleName"));
        reminderInfo.setSmsContent((String)reminderMap.get("content"));
        reminderInfo.setRecordId((String)reminderMap.get("recordId"));
        reminderInfo.setPushSubjcet((String)reminderMap.get("subject"));
        reminderInfo.setPushContent((String)reminderMap.get("content"));
        reminderInfo.setAffairUrl((String)reminderMap.get("affairUrl"));
        try {
			affairsBodyImpl.sendReminder(reminderInfo);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
     * 功能：判断是否是测试环境
     * @param tag
     * @return
     */
    private String checkTest(String tag){
    	if (PushControl.isTest()) {
			tag="test_"+tag;
		}
    	return tag;
    }
	@Override
	public void updateState(ModuleCode moduleCode, String instanceId,
			String state) {
		// TODO Auto-generated method stub
		MyStarted started = myStartedDao.findByModuleCodeAndInstanceId(moduleCode, instanceId);
		if(started!=null){
			started.setState(state);
			myStartedDao.saveOrUpdate(started);
			if("7".equals(state)){//验收未通过
				publishPush(started.getCreaterId(),"10",instanceId);
			}
		}
	}
	
	public void delMyStarted(String instanceIds,ModuleCode moduleCode){
		myProcessedService.del(instanceIds,moduleCode.getCode());
		myWaitProcessService.del(instanceIds,moduleCode.getCode());
		myStartedDao.del(instanceIds,moduleCode.getCode());
	}
	@Override
	public void rollback(ModuleCode moduleCode, String instanceId,
			UserInfo approver, String advice,String state,String rollbackType) {
		// TODO Auto-generated method stub
		MyStarted start = myStartedDao.findByModuleCodeAndInstanceId(moduleCode, instanceId);
		myWaitProcessService.del(instanceId, moduleCode.getCode());
		//创建已审批记录
		if(start!=null){
			MyProcessed processed = new MyProcessed();
			processed.setCategoryName(start.getCategoryName());
			processed.setTitle(start.getTitle());
			processed.setInstanceId(start.getInstanceId());
			processed.setProcesserId(approver.getUserId());
			processed.setProcesserName(approver.getUserName());
			processed.setEndTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			processed.setModuleCode(moduleCode.getCode());
			processed.setCreaterName(start.getCreaterName());
			processed.setCompanyId(approver.getCompanyId());
			processed.setAdvice(advice);
			if(rollbackType.equals("endnoagree")){
				processed.setApproveResult("1");
			}else if(rollbackType.equals("repeal")){
				processed.setApproveResult("2");
			}
			myProcessedService.saveOrUpdate(processed);
		}
		//更新申请表的状态
		updateState(moduleCode, instanceId, state);
	}

	@Override
	public List<MyProcessed> findHistoryByInstanceId(String instanceId) {
		// TODO Auto-generated method stub
		return myProcessedService.findByInstanceId(instanceId);
	}

	@Override
	public Integer getPreviousProcessId(String instanceId, String taskName) {
		return myProcessedService.getPreviousProcessId(instanceId,taskName);
	}

	@Override
	public void pushReminder(final String state,final String instanceId) {
		final List<Integer> userWaitIds = myWaitProcessService.getProcesserByInstanceIds(instanceId,ModuleCode.DEFECT.getCode());
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(userWaitIds!=null&&!userWaitIds.isEmpty()){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("recordId", instanceId);
					map.put("moduleName", "事件通知");
					map.put("mt", "hm");
					Map<String, Object> extraSub=new HashMap<String, Object>();
					extraSub.put("sound", "default");
					extraSub.put("content-available", "1");
					extraSub.put("badge", "");
					map.put("ios",extraSub);
					String title="";
					String processName = "";
					
					if("-1".equals(state)){//待调度
						processName = "待调度";
					}
					if("0".equals(state)){
						processName = "待接收";
					}
					if("1".equals(state)){
						processName = "待处理";
					}
					if("2".equals(state)){
						processName = "待验收";
					}
					String content="您有一条"+processName+"的事件被【催单】,请及时处理!";
					Gson json = new Gson();
					String androidTag = "";
					for(Integer userId:userWaitIds){
						androidTag = "android_user_"+userId;
						pushPlatService.pushJpushToPlat(userId, checkTest(androidTag)+",",PLAT_TYPE.ANDROID, content,title, json.toJson(map), JPUSH_PUSH_TYPE.TAG,null);
					}
			    	//pushPlatService.pushJpushToPlat(1, iosTags, PLAT_TYPE.IOS, content,content, json.toJson(map), JPUSH_PUSH_TYPE.TAG,IOS_PUSH_TYPE.NOTIFY,null);
				}
			}
		}).start();
	}
	
	
	@Override
	public void pushReminderNew(final String content,final List<Integer> userIds) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(userIds!=null&&!userIds.isEmpty()){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("recordId", "100013");
					map.put("moduleName", "事件通知");
					map.put("mt", "hm");
					Map<String, Object> extraSub=new HashMap<String, Object>();
					extraSub.put("sound", "default");
					extraSub.put("content-available", "1");
					extraSub.put("badge", "");
					map.put("ios",extraSub);
					Gson json = new Gson();
					String androidTag = "";
					for(Integer userId:userIds){
						androidTag = "android_user_"+userId;
						pushPlatService.pushJpushToPlat(userId, checkTest(androidTag)+",",PLAT_TYPE.ANDROID, content,"", json.toJson(map), JPUSH_PUSH_TYPE.TAG,null);
					}
			    	//pushPlatService.pushJpushToPlat(1, iosTags, PLAT_TYPE.IOS, content,content, json.toJson(map), JPUSH_PUSH_TYPE.TAG,IOS_PUSH_TYPE.NOTIFY,null);
				}
			}
		}).start();
	}
	
}
	
	
