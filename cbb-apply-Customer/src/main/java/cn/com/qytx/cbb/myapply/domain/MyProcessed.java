package cn.com.qytx.cbb.myapply.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;

@Entity
@Table(name="tb_cbb_my_processed")
public class MyProcessed extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -259049212410819968L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="my_started_id")
	private Integer myStartedId;
	
	/**
	 * 分类(流程类型/栏目)
	 */
	@Column(name="category_name",length=50)
    private String categoryName;
	
	/**
	 * 标题
	 */
	@Column(name="title",length=50)
    private String title;
	
	/**
	 * 业务ID
	 */
	@Column(name="instance_id",length=50)
    private String instanceId;
	
	/**
	 * 处理人ID
	 */
	@Column(name="processer_id")
    private Integer processerId;
	
	/**
	 * 处理人姓名
	 */
	@Column(name="processer_name",length=50)
    private String processerName;
	
	/**
	 * 任务处理时间
	 */
	@Column(name="end_time")
    private Timestamp endTime;
	@Transient
	private String endTimeStr;
	
	/**
	 * 所属模块
	 */
	@Column(name="module_code",length=50)
    private String moduleCode;
    
	/**
	 * 发起人
	 */
	@Column(name="creater_name",length=50)
    private String createrName;
	
	/**
	 * 审批意见
	 */
	@Column(name="advice",length=200)
    private String advice;
	/**
	 * 0驳回 1通过 2转交
	 */
	@Column(name="approveResult")
	private String approveResult;
	/**
	 * task_name=1,上报，对应的处理人叫上报人
     * task_name=2,审批，对应的处理人叫审批人
     * task_name=3,消缺，对应的处理人叫消缺人
     * task_name=4,验收，对应的处理人叫验收人
     * task_name=5,归档，对应的处理人叫归档人
	 */
	@Column(name="task_name",length=50)
	private String taskName;
	/**
	 * 下一步处理人
	 */
	@Column(name="next_processer_id")
	private Integer nextProcesserId;
	
	/**
	 * 下一步处理人类型
	 */
	@Column(name="next_process_type")
	private Integer nextProcessType;
	
	@Transient
	private String processerPhone;
	
	@Transient
	private String nextProcesserName;
	@Transient
	private String nextProcesserPhone;
	@Column(name="time_long")
	private Float timelong;//消耗时长
	@Transient
	private String workNo;//处理人工号
	@Transient
	private String statusName;//
	
	@Column(name="car_id")
	private Integer carId;//车辆Id;
	
	@Transient
	private String processerDealName;
	
	@Transient
	private String carNo;//车牌号
	@Transient
	private String driverName;
	@Transient
	private String driverPhone;
	@Transient
	private String driverWorkNo;
	@Transient
	private String carTypeStr;
	
	@Transient
	private String carPhone;
	
	
	public String getApproveResult() {
		return approveResult;
	}

	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}

	public Integer getMyStartedId() {
		return myStartedId;
	}

	public void setMyStartedId(Integer myStartedId) {
		this.myStartedId = myStartedId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getProcesserId() {
		return processerId;
	}

	public void setProcesserId(Integer processerId) {
		this.processerId = processerId;
	}

	public String getProcesserName() {
		return processerName;
	}

	public void setProcesserName(String processerName) {
		this.processerName = processerName;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getProcesserPhone() {
		return processerPhone;
	}

	public void setProcesserPhone(String processerPhone) {
		this.processerPhone = processerPhone;
	}

	public Integer getNextProcesserId() {
		return nextProcesserId;
	}

	public void setNextProcesserId(Integer nextProcesserId) {
		this.nextProcesserId = nextProcesserId;
	}

	public String getNextProcesserName() {
		return nextProcesserName;
	}

	public void setNextProcesserName(String nextProcesserName) {
		this.nextProcesserName = nextProcesserName;
	}

	public String getNextProcesserPhone() {
		return nextProcesserPhone;
	}

	public void setNextProcesserPhone(String nextProcesserPhone) {
		this.nextProcesserPhone = nextProcesserPhone;
	}

	public Integer getNextProcessType() {
		return nextProcessType;
	}

	public void setNextProcessType(Integer nextProcessType) {
		this.nextProcessType = nextProcessType;
	}

	public Float getTimelong() {
		return timelong;
	}

	public void setTimelong(Float timelong) {
		this.timelong = timelong;
	}

	public String getProcesserDealName() {
		return processerDealName;
	}

	public void setProcesserDealName(String processerDealName) {
		this.processerDealName = processerDealName;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	public String getDriverWorkNo() {
		return driverWorkNo;
	}

	public void setDriverWorkNo(String driverWorkNo) {
		this.driverWorkNo = driverWorkNo;
	}

	public String getCarTypeStr() {
		return carTypeStr;
	}

	public void setCarTypeStr(String carTypeStr) {
		this.carTypeStr = carTypeStr;
	}

	public String getCarPhone() {
		return carPhone;
	}

	public void setCarPhone(String carPhone) {
		this.carPhone = carPhone;
	}
   
	
	
	
}
