<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<meta charset="utf-8">
<title>选择设备</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/tree/skins/tree_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/datatable/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/treeNode.js"></script>
<script type="text/javascript" src="${ctx}js/logined/capital/selectCapital.js"></script>
<script type="text/javascript">
	$(function(){
		var treeHeight = $(window).height()-90;
		$(".zTreeDemoBackground").height(treeHeight);
		$("#myTree").height(treeHeight);
		/* $(".center_content").height(treeHeight); */
	})
</script>
<body>
<div class="mainpage_r" style="padding-right: 0px;">
<input id="type" type="hidden" value="${param.type}">
<input id="selectGroupId" type="hidden">
	<!-- 左侧树 -->
	<div class="leftMenu" style="left:0px">
		<div class="service-menu">
			<h1>资产组</h1>
			 <div class="zTreeDemoBackground">
				<ul id="myTree" class="ztree"></ul>
			</div> 
		</div>
	</div>
	<!-- 右侧列表 -->
	<div class="list overf" style="overflow:visible;">
		<div class="searchArea overf" style="overflow:visible;">
			<table cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td class="right"  >
						     <input style="float:right;" onClick="serch()" value="查询" class="searchButton" type="button" />
			                <div style="float:right;">
								<label>关键字	：</label>
			                	<input id="keyWord" type="text" class="formText" style="width: 220px;border-radius: 3px;height: 30px;" placeholder="名称/设备编号"/>
							</div>
							<div style="float:right;display: none">
			                	<label>设备状态：</label>
				                <select id="capitalStatus" style="height: 30px;width: 130px;border-radius:3px;" >
				                	<option value="1">在库</option>
				                </select>
			                </div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="center_content" style="width: 747px; ">
			<table id="capitalTable" cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px; width:auto;" >
				<thead>
					<tr>
			       	 	<th style="width:42px;">序号</th>
			        	<th style="width:80px;">设备编号</th>
			        	<th style="width:120px;">名称</th>
			        	<th style="width:120px;">设备型号</th>
			        	<th style="width:120px;">设备组</th>
			        	<th style="width:90px;">当前位置</th>
			        	<th style="width:50px;">操作</th>
			     	</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
</body>
</html>