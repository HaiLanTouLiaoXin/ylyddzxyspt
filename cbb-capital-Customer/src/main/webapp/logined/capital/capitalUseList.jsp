<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<meta charset="utf-8">
<title>使用记录</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/tree/skins/tree_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/datatable/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/treeNode.js"></script>
<script type="text/javascript" src="${ctx}js/logined/capital/capitalUseList.js"></script>
<script type="text/javascript">
	$(function(){
		var treeHeight = $(window).height()-90;
		$(".zTreeDemoBackground").height(treeHeight);
		$("#myTree").height(treeHeight);
		$(".center_content").height(treeHeight);
	})
</script>
<style>
	.searchArea table td.right label{
		padding-left:12px;
	}
</style>
<body>
<div class="mainpage_r">
<input id="selectGroupId" type="hidden">
	<!-- 左侧树 -->
	<div class="leftMenu">
		<div class="service-menu">
			<h1>资产组</h1>
			<div class="zTreeDemoBackground">
				<ul id="myTree" class="ztree"></ul>
			</div>
		</div>
	</div>
	<!-- 右侧列表 -->
	<div class="list overf list-max1160" style="overflow:visible;min-width:1160px">
		<div class="searchArea overf" style="overflow:visible;">
			<table cellspacing="0" cellpadding="0" >
				<tbody>
					<tr>
						<td class="right"  >
						   		<label>设备状态：</label>
				                <select id="capitalStatus" style="height: 30px;width: 130px;border-radius:3px;" >
				                	<option value="">全部</option>
				                	<option value="1">在库</option>
				                	<option value="2">使用中</option>
				                	<option value="3">已停用</option>
				                	<option value="4">待归库</option>
				                	<option value="5">已禁用</option>
				                	<option value="6">已送达</option>
				                </select>
						    
						    	 <label style="line-height:28px;" id="title">使用科室：</label>
							  	<input id="userGroupName" type="text" class="formText" style="width: 150px;border-radius: 3px;height: 30px;" placeholder="使用科室"/>
							  	
							  	<label>使用时间	：</label>
								<input id="useStartTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',maxDate:'#F{$dp.$D(\'useEndTime\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
								<input id="useEndTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'useStartTime\')}'})" class="in_area Wdate" style="width:170px"/>
								
						</td>
						 <!-- <td style="width:95px;">  
         					          
         				</td> -->
					</tr>
					<tr>
					<td class="right" style="padding-top: 10px;">
							  		<label>停用时间	：</label>
									<input id="disableStartTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',maxDate:'#F{$dp.$D(\'disableEndTime\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
									<input id="disableEndTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'disableStartTime\')}'})" class="in_area Wdate" style="width:170px"/>
						
							  	
							  	
									<label>关键字	：</label>
			                		<input id="keyWord" type="text" class="formText" style="width: 150px;border-radius: 3px;height: 30px;" placeholder="名称/设备编号"/>
			                		<div class="fButton blueBtn" onClick="exportUseLog()" style="display: block; float: right;"> <span class="export" id="userExport">导出</span> </div>
			                		<input style="float:right;" onClick="getTable()" value="查询" class="searchButton" type="button" />
			                		
								</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="center_content">
			<table id="capitalTable" cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" >
				<thead>
					<tr>
			       	 	<th style="width:42px;">序号</th>
			        	<th style="width:120px;">名称</th>
			        	<th style="width:80px;">设备编号</th>
			       		<th style="width:90px;">设备组</th>
			        	<th style="width:90px;">设备状态</th> 
			        	<th style="width:80px;">使用科室</th>       	
			        	<th style="width:90px;">当前位置</th>
			        	<th style="width:120px;">使用时间</th>
			        	<th style="width:120px;">停用时间</th>
			        	<th style="width:120px;">使用时长</th>
			     	</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
</body>
</html>