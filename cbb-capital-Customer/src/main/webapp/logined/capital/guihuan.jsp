<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>资产归还</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<style >
	 body{background-color: #fff;}
  	.formPage{width: 100%;height: 100%;padding-top: 0;} 
</style>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/guihuan.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/commonDetail.js"></script>
</head>
<body>
<input id="type" type="hidden" value="${param.type}">
<div class="formPage">
  <div class="formbg" >
    <div class="content_form">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><label>资产名称：</label></th>
          <td id="goodName"></td>
        </tr>
        <tr>
          <th><label>资产组：</label></th>
          <td id="capitalGroup"></td>
        </tr>
        <tr>
          <th><label>资产编号：</label></th>
          <td id="capitalNo"></td>
        </tr>
        <tr>
          <th><label>入库日期：</label></th>
          <td id="storageTime"></td>
        </tr>
        <tr>
          <th><label>资产品牌：</label></th>
          <td id="capitalBrand"></td>
        </tr>
        <tr>
          <th><label>资产型号：</label></th>
          <td id="capitalModel"></td>
        </tr>
        <tr>
          <th><label>资产类型：</label></th>
          <td id="capitalTypeStr">固定资产</td>
        </tr>
        <tr>
          <th><label>使用部门：</label></th>
          <td id="useGroup"></td>
        </tr>
        <tr>
          <th><label>使用人：</label></th>
          <td id="usePeople"></td>
        </tr>
        <tr>
          <th><label>存放位置：</label></th>
          <td>
            <select id="position" style="height: 26px;width: 150px;" >
            </select>
          </td>
        </tr>
        </table>
    </div>
</div>
</div>
</body>
</html>