<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>入库设备</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
	<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/inStore.js"></script>
</head>
<input  type="hidden" id="capitalId" value="${param.id}"/>
<body>
  <div class="formbg" style="height:100px;">
    <div class="content_form">
      <table  border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><label>库存地点：</label></th>
          <td> 
            <input type="hidden" id="repaireId"/>
		    <input type="hidden" id="showType_repair" value="5"/>
 			<input class="in_area" type="text" id="describe"  style="width: 258px" errmsg="capital.describe_not_null" valid="required">
 			<span class="addMember " ><a class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="describe" showType="showType_repair">选择</a></span>
          </td>
        </tr>
        </table>
    </div>
</div>
</body>
</html>