<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>资产入库</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
	<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<style >
    .new{border-radius: 3px;height: 32px;}
 	.textarea{width:100% ; height: 74px;border: 1px solid #e4e4e4;}
  	.formPage .textarea:hover , .formPage .formTextarea:hover{border:1px solid #cecece;}
	.formPage .textarea:focus , .formPage .formTextarea:focus{border:1px solid #459fd2;} 
	.formPage .divselect12:hover , .formPage .divselect12:hover{border:1px solid #459fd2;}
	.formPage .divselect12:focus , .formPage .divselect12:focus{border:1px solid #cecece;}
	.location{float: left;display: block;} 
</style>
<script type="text/javascript" src="${ctx}js/common/validate.js?version=${version}"></script>
<!-- tree -->
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/capitalAdd.js"></script>
</head>
<input  type="hidden" value="${param.type}" id="type"/>
<input  type="hidden" id="capitalId" value="${param.id}"/>
<body>
<form id="form1">
<div class="formPage" style="padding-top:0px">
  <div class="formbg" style="height: 510px;">
    <div class="big_title">设备信息</div>  
    <div class="content_form">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><em class="requireField">*</em><label>设备名称：</label></th>
          <td><input id="capitalName" type="text" class="formText " style="display:block;border-radius: 3px;height: 32px;" placeholder="请输入设备名称（最多50字）" maxlength="50" name="capitalName" 
           valid="required" errmsg="capital.capitalName_not_null"/>
          </td>
	      <th><em class="requireField">*</em><label>设备编号：</label></th>
          <td><input type="text" class="formText" style="border-radius: 3px;height: 32px;" id="capitalNo" maxlength="50" name="capitalNo" valid="required" errmsg="capital.capitalNo_not_null"/></td>
        </tr>
        <tr>
          <th><em class="requireField">*</em><label>当前位置：</label></th>
          <td> 
            <input type="hidden" id="repaireId"/>
		    <input type="hidden" id="showType_repair" value="5"/>
 			<input class="in_area" type="text" id="describe"  style="width: 258px" errmsg="capital.describe_not_null" valid="required">
 			<span class="addMember " ><a class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="describe" showType="showType_repair">选择</a></span>
          </td>
          <th><em class="requireField">*</em><label>设备组：</label></th>
           <td >
            <div class="divselect12  divselect13 " onmouseleave="javascript:$('#capitalGroupTree').hide()">
		      	<cite class="cite12 text_ellipsis cite13"  onClick="toggleSelectGroup()" id="capitalGroupText"></cite>
	      		<ul id="capitalGroupTree" class="ztree leibie"  style="display: none;height:200px !important;width:200px !important;position: relative;z-index: 999;">
				</ul>
		  	</div>
		  	<input type="hidden" id="capitalGroupId" errmsg="capital.capitalGroupId_not_null" valid="required"/>
           </td>
        </tr>
        <tr>
           <th><label>设备型号：</label></th>
          <td><input type="text" class="formText " style="border-radius: 3px;height: 32px;" id="capitalModel" maxlength="50"/></td> 
           <th><label>品牌信息：</label></th>
          <td><input type="text" id="capitalBrand" style="border-radius: 3px;height: 32px;" class="formText" maxlength="50" /></td>
        </tr>
        <tr>
           <th><label>厂商信息：</label></th>
          <td><input type="text" id="supplier" style="border-radius: 3px;height: 32px;" class="formText" maxlength="50" /></td>
        </tr>
        <tr>
        <th><label>备注：</label></th>
        <td colspan="3"><textarea id="remark"  class="textarea" style="color:#555555;border-radius: 3px;" name="remark"  maxlength="500"></textarea></td>    
        </tr>
        </table>
    </div>
</div>
  <div class="buttonArea" id="isShowOper">
    <input type="button" style="height: 38px;" class="formButton_grey" value="取消" onClick="goBack()"/>
    <input type="button" style="height: 38px;" class="formButton_green" id="addOrUpdate" onClick="addCapital()" />
  </div>
</div>
</form>
</body>
</html>