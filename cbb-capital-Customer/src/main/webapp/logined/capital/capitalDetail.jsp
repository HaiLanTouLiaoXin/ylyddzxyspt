<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>设备详情</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/form/skins/form_default.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/datatable/skins/datatable_default.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
<script type="text/javascript" src="${ctx}js/logined/log/logSumup.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/commonDetail.js"></script>
<style>
.inputTable th {
	width: 105px;
}
.big_title {
	border-bottom: 1px solid #017a8f;
	color: #017a8f;
	font-size: 16px;
	height: 40px;
	line-height: 45px;
	overflow: hidden;
	padding: 0 10px;
	text-align: left;
}
</style>
</head>
<body style=" background: #fff">
<input type="hidden" id="capNo" >
	<div class="formPage" style="padding-top:0px;width:auto;">
		<div class="formbg">
			<div class="content_form">
				<table width="100%" cellspacing="0" cellpadding="0" border="0"
					class="inputTable" style="font-size:14px;padding-top: 0px">
					<tbody>
						<tr>
							<th>设备名称：</th>
							<td id="name"></td>
							<th>设备编号：</th>
							<td id="capitalNo"></td>
							
						</tr>
						<tr>
						    <th>设备型号：</th>
							<td id="capitalModel"></td>
							<th>设备组：</th>
							<td id="capitalGroup"></td>
							
						</tr>
						<tr>
						    <th>当前位置：</th>
							<td id="location"></td>
							<th>设备状态：</th>
							<td id="statusStr"></td>
						</tr>
						<tr>
							<th>品牌信息：</th>
							<td id="capitalBrand"></td>
							<th>厂商信息：</th>
							<td id="supplier"></td>
						</tr>
						<tr>
							<th>操作日志：</th>
							<td onclick="findLog()" style="color:blue ;cursor:pointer">点击查看</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>