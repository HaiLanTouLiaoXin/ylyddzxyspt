var parent = art.dialog.parent;
var api = art.dialog.open.api;	

$(function(){
	
	var nowDate = getFormatNowDate();
	$("#endUseTime").val(nowDate);
	
	$("#repaireSelect").click(function(){
		selectLocation(this);
	});
	
	capitalDetail();
	
});

function capitalDetail(){
	var capitalId = $("#capitalId").val();
	var param={
			"capitalId":capitalId
	};
	$.ajax({
		url:basePath+"capital/getCapitalDetail.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			$("#name").html(data.name);
			$("#currUser").html(data.currUser);
			$("#capitalGroup").html(data.capitalGroup);
			$("#storageTime").html(data.createTime);
			$("#capitalNo").html(data.capitalNo);
			$("#capitalModel").html(data.capitalModel);
			$("#location").html(data.storeLocation);
			$("#repaireId").val(data.useGroupId);
			$("#useGroupName").html(data.useGroupName);
			$("#startUseTime").val(data.startUseTime);
		}
	});
}

/**
 * 初始化按钮
 */
function initButton(){
	api.button(
			{
				name: '确定',
				callback: function () {
					disableCapital();
					return false;
				},
				focus: true
			},
			{
				name: '取消',
				callback:function(){
					return true;
				}
			}
		);
}
initButton();

function disableCapital(){
	var capitalId = $("#capitalId").val();
	/*var storeLocation = $("#location").val();*/
	var endUseTime = $("#endUseTime").val();
	if(endUseTime){
		endUseTime += ":00";
	}
	var param={
			"capital.id":capitalId,
			/*"capital.storeLocation":storeLocation,*/
			"endUseTime":endUseTime
	};
	$.ajax({
		url:basePath+"capital/capitalDisable.action",
		type:'post',
		data:param,
		dataType:'text',
		success:function(data){
			if(data=="1"){
				art.dialog.data("result","success");
			}else{
				art.dialog.data("result","fail");
			}
			art.dialog.close();
		}
	})
}

/**
 * 人员选择
 * REN
 **/
function selectLocation(obj)
{
    var groupId=$("#"+$(obj).attr("fieldId")).val();
	/**userId = "gid_"+userId;*/
    userObj = obj;
    qytx.app.tree.alertLocationTree({
    	type:"radio",
    	showType:1,
    	userId:"",
    	groupId:groupId,
    	callback:callBackUser
    });
}


function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    }
}


//时间格式转换
function getFormatNowDate() {
	var now = new Date();
    var formatDate = "";
    // 初始化时间
    var Year = now.getFullYear();// ie火狐下都可以
    var Month = now.getMonth() + 1;
    formatDate += Year;
    if (Month >= 10) {
        formatDate += "-" + Month;
    } else {
        formatDate += "-0" + Month;
    }

    var Day = now.getDate();
    if (Day >= 10) {
        formatDate += "-" + Day;
    } else {
        formatDate += "-0" + Day;
    }
    var Hour = now.getHours();
    var minute = now.getMinutes();
    if (Hour >= 10) {
        formatDate += " " + Hour;
    } else {
        formatDate += " 0" + Hour;
    }
    if (minute >= 10) {
    	formatDate += ":" + minute;
    } else {
    	formatDate += ":0" + minute;
    }
    return formatDate;
}
