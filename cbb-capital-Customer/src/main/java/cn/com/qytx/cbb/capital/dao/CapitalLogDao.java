package cn.com.qytx.cbb.capital.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 功能:资产日志持久层操作
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Repository("capitalLogDao")
public class CapitalLogDao extends BaseDao<CapitalLog, Integer> {

	public Page<CapitalLog> findCapitalLogPage(Pageable pageable, CapitalVo capitalVo) {
		StringBuffer sbHql = new StringBuffer();
		sbHql.append("select c from CapitalLog c where 1=1");
		if(capitalVo.getCompanyId()!=null){
			sbHql.append(" and c.companyId="+capitalVo.getCompanyId());
		}
		if(capitalVo.getLogType()!=null){
			sbHql.append(" and c.type="+capitalVo.getLogType());
		}
		if(capitalVo.getCapitalNo()!=null){
			sbHql.append(" and c.capitalNo='"+capitalVo.getCapitalNo()+"'");
		}
		if(capitalVo.getCapitalType()!=null){
			sbHql.append(" and c.capitalType="+capitalVo.getCapitalType());
		}
		if(capitalVo.getCapitalGroup()!=null&&capitalVo.getCapitalGroup().intValue()!=0){
			sbHql.append(" and (c.capitalGroup.pathId like '%,"+capitalVo.getCapitalGroup()+",%')");
		}
		if(StringUtils.isNotBlank(capitalVo.getStartTime())){
			sbHql.append(" and c.createTime>='"+capitalVo.getStartTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getEndTime())){
			sbHql.append(" and c.createTime<='"+capitalVo.getEndTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getKeyWord())){
			sbHql.append(" and (c.capitalName like '%"+capitalVo.getKeyWord()+"%' or c.capitalNo like '%"+capitalVo.getKeyWord()+"%')");
		}
		sbHql.append(" order by c.createTime desc");
		return super.findByPage(pageable, sbHql.toString());
	}


	
	
}
