package cn.com.qytx.cbb.capital.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产日志
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月9日
 * 修改日期: 2016年8月9日
 * 修改列表: 
 */
@Entity
@Table(name="tb_cbb_capital_log")
public class CapitalLog implements java.io.Serializable{
	private static final long serialVersionUID = -3806018446633238202L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	/**
	 * 资产组
	 */
	@JoinColumn(name="capital_group_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private CapitalGroup capitalGroup;
	
	/**
	 * 操作类型  1 修改 2 新增 3.启用 4.禁用 5 入库 6 借用 7 停用 8 送达 9 待归库
	 */
	@Column(name="type")
	private Integer type;
	
	/**
	 * 资产名称
	 */
	@Column(name="capital_name")
	private String capitalName;
	
	/**
	 * 资产名称
	 */
	@Column(name="capital_type")
	private Integer capitalType;
	

	/**
	 * 资产编号
	 */
	@Column(name="capital_no")
	private String capitalNo;
	
	/**
	 * ip地址
	 */
	@Column(name="ip_address")
	private String ipAddress;
	
	/**
	 * 操作时间
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	/**
	 *  创建人
	 */
	@JoinColumn(name="create_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo createUser;
	
	
	/**
	 * 单位Id
	 */
	@Column(name="company_id")
	private Integer companyId;
	
	/**
	 * 备注
	 */
	@Column(name="remark")
	private String remark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public CapitalGroup getCapitalGroup() {
		return capitalGroup;
	}

	public void setCapitalGroup(CapitalGroup capitalGroup) {
		this.capitalGroup = capitalGroup;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCapitalName() {
		return capitalName;
	}

	public void setCapitalName(String capitalName) {
		this.capitalName = capitalName;
	}

	public String getCapitalNo() {
		return capitalNo;
	}

	public void setCapitalNo(String capitalNo) {
		this.capitalNo = capitalNo;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	

	public UserInfo getCreateUser() {
		return createUser;
	}

	public void setCreateUser(UserInfo createUser) {
		this.createUser = createUser;
	}

	

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getCapitalType() {
		return capitalType;
	}

	public void setCapitalType(Integer capitalType) {
		this.capitalType = capitalType;
	}
	
	
}
