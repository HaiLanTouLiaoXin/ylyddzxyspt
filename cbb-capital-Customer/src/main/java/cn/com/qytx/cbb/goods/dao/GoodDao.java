package cn.com.qytx.cbb.goods.dao;


import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
@Repository
public class GoodDao extends BaseDao<Good, Integer> {
  
	
	public Page<Good>findGoodPage(Pageable pageable,String name,String goodNo,Integer type,Integer companyId,Integer goodGroupId){
		String hql=" isDelete=0";
		if(StringUtils.isNoneBlank(name)){
			hql+=" and name like '%"+name+"%'";
		}
		if(StringUtils.isNoneBlank(goodNo)){
			hql+=" and goodNo like'%"+goodNo+"%'";
		}
		if(type!=null){
			hql+=" and type="+type;
		}
		if(companyId!=null){
			hql+=" and companyId="+companyId;
		}
		if(goodGroupId!=null){
			hql+=" and goodGroup.id="+goodGroupId;
		}
		return super.findAll(hql, pageable);
	}
	
	
	
	public void deleteGoodByIds(Integer companyId,String ids){
        if (ids == null || ids.equals(""))
            return;
        if (ids.endsWith(",")) {
            ids = ids.substring(0, ids.length() - 1);
        }
        if (ids.startsWith(",")) {
            ids = ids.substring(1, ids.length());
        }
        String hql = "update Good" 
                + " set isDelete=1 where id in (" + ids + ") and companyId="+companyId;
        executeQuery(hql);
	}
	/**
	 * 根据 类型 物资组 名称 查询
	 * @param type
	 * @param name
	 * @param goodGroupId
	 * @return
	 */
	public Good findModle(Integer type,String name,Integer goodGroupId){
		String hql =" type="+type+" and name="+name+" and goodGroup.id="+goodGroupId;
		
		return super.findOne(hql);
	}
	
	
	
	
}
