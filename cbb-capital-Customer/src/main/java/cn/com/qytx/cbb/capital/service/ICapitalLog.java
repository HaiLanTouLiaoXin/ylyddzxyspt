package cn.com.qytx.cbb.capital.service;


import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:资产日志接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public interface ICapitalLog extends BaseService<CapitalLog>{

	/**
	 * 功能：资产日志列表分页
	 * @param pageable
	 * @param capitalVo
	 * @return
	 */
	public Page<CapitalLog> findCapitalLogPage(Pageable pageable,CapitalVo capitalVo);
	
	
	
	
}
