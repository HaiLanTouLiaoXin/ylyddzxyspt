package cn.com.qytx.cbb.goods.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.qytx.platform.base.domain.DeleteState;

/**
 * 功能:资产组
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月9日
 * 修改日期: 2016年8月9日
 * 修改列表: 
 */
@Entity
@Table(name="tb_cbb_good_group")
public class GoodGroup implements java.io.Serializable{
	private static final long serialVersionUID = -3806018446633238202L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	/**
	 * 级别
	 */
	@Column(name="grade")
	private Integer grade;
	
	/**
	 * 名称
	 */
	@Column(name="group_name")
	private String groupName;
	
	
	/**
	 * 父ID
	 */
	@Column(name="parent_id")
	private Integer parentId;
		
	/**
	 * 排序
	 */
	@Column(name="order_index")
	private Integer orderIndex;
	
	/**
	 * 资产编码
	 */
	@Column(name="good_code")
	private String goodCode;
		

	@DeleteState
	@Column(name="is_delete")
	private Integer isDelete;
	
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 修改人
	 */
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 单位Id
	 */
	@Column(name="company_id")
	private Integer companyId;
	
	/**
	 * Id路径
	 */
	@Column(name="path_id")
	private String pathId;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getGoodCode() {
		return goodCode;
	}

	public void setGoodCode(String goodCode) {
		this.goodCode = goodCode;
	}

	public String getPathId() {
		return pathId;
	}

	public void setPathId(String pathId) {
		this.pathId = pathId;
	}


	
	
	
	
}
