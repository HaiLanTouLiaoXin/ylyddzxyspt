package cn.com.qytx.cbb.goods.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;

/**
 * 功能:会议持久层 版本: 1.0 开发人员: panbo 创建日期: 2016年8月2日 修改日期: 2016年8月2日 修改列表:
 */
@Repository("goodGroupDao")
public class GoodGroupDao extends BaseDao<GoodGroup, Integer> {

	public List<GoodGroup> findGoodGroupList(Integer companyId) {
		String hql = "isDelete=0 and companyId=?";
		return super.findAll(hql, new Sort(Direction.ASC, "orderIndex"),
				companyId);
	}

	/**
	 * 在指定ID下面是否有相同的资产组名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameGroupName(Integer parentId,String goodName,int companyId){
		String sql="parentId=? and groupName=?  and isDelete=0  and companyId = ?";
		Object object=super.findOne(sql, parentId,goodName,companyId);
		if(object==null){
			return false;
		}else{
			return true;
		}
	}


	public boolean isHasSameGoodCode(Integer parentId, String goodCode,
			int companyId) {
		String sql="parentId=? and goodCode=?  and isDelete=0  and companyId = ?";
		Object object=super.findOne(sql, parentId,goodCode,companyId);
		if(object==null){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * 功能：判断是否存在子资产组
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildGoodGroup(Integer id, int companyId) {
		String hql = "isDelete=0 and parentId=? and companyId=?";
		List list = super.findAll(hql, id,companyId);
		if(list!=null&&list.size()>0){
			return true;
		}
		return false;
	}

	public List<GoodGroup> getGoodGroupListByIds(String ids) {
		String hql = "1=0";
		if(StringUtils.isNotBlank(ids)){
			if(ids.startsWith(",")){
				ids = ids.substring(1);
			}
			if(ids.endsWith(",")){
				ids = ids.substring(0,ids.length()-1);
			}
			hql += "or id in("+ids+")";
		}
		return super.findAll(hql,new Sort(Direction.ASC,"id"));
	}
	
	/**
	 * 功能：判断是否存在资产组
	 * @param name
	 * @param companyId
	 * @return
	 */
	public GoodGroup findModel(String name, int companyId) {
		String hql = "isDelete=0 and groupName=? and companyId=?";
		return super.findOne(hql, name,companyId);
	}
	
	
	
}
