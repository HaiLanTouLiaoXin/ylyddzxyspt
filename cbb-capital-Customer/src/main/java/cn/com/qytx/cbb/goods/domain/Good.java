package cn.com.qytx.cbb.goods.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;
@Entity
@Table(name="tb_cbb_good")
public class Good extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6577973889403790226L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	/**
	 * 修改人
	 */
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 1 已删除 0 未删除
	 */
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete;
	
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 创建日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	
	@Column(name="name")
	private String name;
	
	@Column(name="good_no")
	private String goodNo;
	
	/**
	 * 资产组Id
	 */
	@JoinColumn(name="good_group_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private GoodGroup goodGroup;
	//状态 1备件、2材料、3设备、4仪表 、5其他
	@Column(name="type")
	private Integer type;
	
	//规格
	@Column(name="specifications")
	private String specifications;
	
	
	@Column(name="unit")
	private Integer unit;
	
	
	@Column(name="price")
	private float price;

	@Column(name="num")
	private Integer num;
	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getUpdateUserId() {
		return updateUserId;
	}


	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}


	public Timestamp getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}


	public Integer getIsDelete() {
		return isDelete;
	}


	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}


	public Integer getCreateUserId() {
		return createUserId;
	}


	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}


	public Timestamp getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getGoodNo() {
		return goodNo;
	}


	public void setGoodNo(String goodNo) {
		this.goodNo = goodNo;
	}


	

	public GoodGroup getGoodGroup() {
		return goodGroup;
	}


	public void setGoodGroup(GoodGroup goodGroup) {
		this.goodGroup = goodGroup;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public String getSpecifications() {
		return specifications;
	}


	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}


	public Integer getUnit() {
		return unit;
	}


	public void setUnit(Integer unit) {
		this.unit = unit;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public Integer getNum() {
		return num;
	}


	public void setNum(Integer num) {
		this.num = num;
	}
	
	
	
    
}
