package cn.com.qytx.cbb.capital.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;



import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.service.ICapitalLog;
import cn.com.qytx.cbb.capital.util.Tool;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产日志
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月16日
 * 修改日期: 2016年8月16日
 * 修改列表: 
 */
public class CapitalLogAction extends BaseActionSupport{
	private static final long serialVersionUID = -6075563344226356473L;
	
	/**
	 * 资产日志接口
	 */
	@Resource(name="capitalLogService")
	private ICapitalLog capitalLogService;
	
	/**
	 * 数据字典接口
	 */
	@Resource(name = "dictService")
	private  IDict dictService;
	
	private CapitalVo capitalVo;
	
	/**
	 * 功能：日志列表
	 * @return
	 */
	public String capitalLogList(){
		UserInfo userInfo = this.getLoginUser();
		if(userInfo!=null){
			capitalVo.setCompanyId(userInfo.getCompanyId());
			Pageable pageable = getPageable();
			Page<CapitalLog> pageInfo =  capitalLogService.findCapitalLogPage(pageable, capitalVo);
			List<CapitalLog> capitalLogList = pageInfo.getContent();
			List<Map<String,Object>> capitalList=analyzeResult(capitalLogList, pageable);
            ajaxPage(pageInfo, capitalList);
		}
		return null;
	}

	private List<Map<String, Object>> analyzeResult(
			List<CapitalLog> capitalLogList, Pageable pageable) {
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
		if(capitalLogList!=null&&capitalLogList.size()>0){
		    int i = 1;
			if(pageable!=null){
    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
    		}
			for(CapitalLog capitalLog:capitalLogList){
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("no", i);
				map.put("createTime", capitalLog.getCreateTime()!=null?Tool.formatDate(capitalLog.getCreateTime()):"--");//操作时间
				String userName = "--";//操作人
				UserInfo operUser = capitalLog.getCreateUser();
				if(operUser!=null){
					userName = operUser.getUserName();
				}
				map.put("userName", userName);
				map.put("capitalName", capitalLog.getCapitalName());
				map.put("capitalNo", capitalLog.getCapitalNo());
				map.put("typeStr", getOperTypeStr(capitalLog.getType()));//日志类型
				map.put("ipAddress", StringUtils.isNoneBlank(capitalLog.getIpAddress())?capitalLog.getIpAddress():"--");
				map.put("remark", StringUtils.isNoneBlank(capitalLog.getRemark())?capitalLog.getRemark():"--");
				mapList.add(map);
				i++;
			}
	   }
	   return  mapList;
	}
	
	/**
	 * 功能：获取日志类型字符串
	 * @param type
	 * @return
	 */
	private static String getOperTypeStr(Integer type){
		String operTypeStr = "";
		switch (type) {
		case 1:
			operTypeStr = "修改设备";
			break;
		case 2:
			operTypeStr = "新增设备";
			break;
		case 3:
			operTypeStr = "启用设备";
			break;
		case 4:
			operTypeStr = "禁用设备";
			break;
		case 5:
			operTypeStr = "设备归库";
			break;
		case 6:
			operTypeStr = "设备使用";
			break;
		case 7:
			operTypeStr = "停用设备";
			break;
		case 8:
			operTypeStr = "设备已送达";
			break;
		case 9:
			operTypeStr = "设备待归库";
			break;
		}
		return operTypeStr;
	}

	public CapitalVo getCapitalVo() {
		return capitalVo;
	}

	public void setCapitalVo(CapitalVo capitalVo) {
		this.capitalVo = capitalVo;
	}

}
