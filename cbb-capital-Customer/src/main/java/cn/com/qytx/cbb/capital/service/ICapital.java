package cn.com.qytx.cbb.capital.service;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public interface ICapital extends BaseService<Capital>{

	
	/**
	 * 功能：资产列表分页
	 * @param pageable
	 * @param capitalVo
	 * @return
	 */
	public Page<Capital> findCapitalPage(Pageable pageable,CapitalVo capitalVo);
	
	
	/**
	 * 功能：获取当前物品编码物品数
	 * @param capitalCode
	 * @param companyId
	 * @return
	 */
	public int getCurrCodeMaxCapitalNum(String capitalCode,int companyId);
	
	
	public int getCurrNoMaxCapitalNum(String capitalCode, int companyId);
	
	public void capitalUse(String ipAddress,UserInfo userInfo,Capital capital,Integer status,String startUseTime,String endtartUseTime);
}
