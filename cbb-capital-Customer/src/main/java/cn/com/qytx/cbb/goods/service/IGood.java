package cn.com.qytx.cbb.goods.service;



import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

public interface IGood extends BaseService<Good> {
  
	public Page<Good> findGoodPage(Pageable pageable,String name,String goodNo,Integer type,Integer companyId,Integer goodGroupId);
	
	public void deleteGoodByIds(Integer companyId,String ids);
	
	/**
	 * 根据 类型 物资组 名称 查询
	 * @param type
	 * @param name
	 * @param goodGroupId
	 * @return
	 */
	public Good findModle(Integer type,String name,Integer goodGroupId);
}
