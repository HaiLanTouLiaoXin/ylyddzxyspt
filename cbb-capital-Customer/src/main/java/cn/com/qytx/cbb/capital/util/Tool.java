package cn.com.qytx.cbb.capital.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

/**
 * @author llp
 * 工具类
 *
 */
public class Tool {

	/**
	 * 把 yyyy-MM-dd HH:mm:ss 格式的字符串转成 Timestamp（默认表示时间的字符串表示的时间中东八区区时）
	 * @param timeString 表示时间的字符串
	 * @param timeFormat 时间字符串格式，如：yyyy-MM-dd HH:mm:ss
	 * @return Timestamp 返回字符串表示的东八区区时。当传的表示时间的字符串为空字符串或NULL时，返回NULL。
	 * @throws ParseException 解析字符串时出错
	 */
	public static Timestamp getTimestampByString(String timeString,String timeFormat) throws ParseException{
		if(timeString==null || "".equals(timeString)){
			return null;
		}
		Timestamp ts=null;
		SimpleDateFormat sdf= new SimpleDateFormat(timeFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
		java.util.Date date=sdf.parse(timeString);
		ts=new Timestamp(date.getTime());
		return ts;
	}
	
	/**
	 * 去除前后的逗号
	 * @param str
	 * @return
	 */
	public static String trimComma(String str){
		return StringUtils.isBlank(str) ? null : (str.startsWith(",") ? (str.endsWith(",")?(str.length() == 1 ? "" :str.substring(1, str.length()-1)):str.substring(1)) : (str.endsWith(",")?str.substring(0, str.length()-1):str) );
	}
	
	/**
     * 读取properties配置文件中的value值
     * @param srcFile  properties配置文件的路径
     * @param key  properties配置文件中的key
     * @return  properties配置文件中的value
     */
    public static String getPropertiesValue(String srcFile,String key){
    	String result = "";
    	try {
    		Properties properties = new Properties();
    		String path = Thread.currentThread().getContextClassLoader().getResource(srcFile).getPath();  
    		path=URLDecoder.decode(path, "utf-8");
    		InputStream is = new FileInputStream(path);  
    		properties.load(is);
    		result = (String) properties.get(key);
    		is.close();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return result;
    }
    
    
    /** 
     * 通过年设定一个星期的第一天是星期一  
     * @param year 
     * @return 
     */  
    public static Calendar getCalendarFormYear(int year){  
        Calendar cal = Calendar.getInstance();  
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);        
        cal.set(Calendar.YEAR, year);  
        return cal;  
    }  
    
    /**
     * 功能：获取某天是第几周
     * @param date
     * @return
     */
    public static int getCurrDateWeekNo(Date date ){
    	Calendar c = Calendar.getInstance();
		c.setTime(date);
		int week = c.get(Calendar.WEEK_OF_YEAR);
		return week;
    }
    
    
    /**
	 * 格式化时间 当天：今天xx:xx 跨天: xx月xx日 xx：xx 跨年: xxxx年xx月xx日 xx：xx
	 * 
	 * @param d
	 *            java.util.Date
	 * @return
	 */
	public static String formatDate(java.util.Date d) {
		if (d == null) {
			throw new RuntimeException("传入的时间为NULL");
		}

		String result = null;
		Calendar cal = Calendar.getInstance();
		int thisyear = cal.get(Calendar.YEAR);
		// int thismonth = cal.get(Calendar.MONTH);
		int today = cal.get(Calendar.DAY_OF_YEAR);
		// 修改时间
		cal.setTimeInMillis(d.getTime());
		// cal.set(Calendar.YEAR, 2014);
		// cal.set(Calendar.MONTH, 5);
		// cal.set(Calendar.DAY_OF_MONTH, 5);
		// cal.set(Calendar.HOUR_OF_DAY, 5);

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int yearday = cal.get(Calendar.DAY_OF_YEAR);
		int monthday = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		//int weekday = cal.get(Calendar.DAY_OF_WEEK);

		if (thisyear == year) {
			if (yearday == today) {
				result = "今天"
						+ (hour < 10 ? ("0" + hour) : hour) + ":"
						+ (minute < 10 ? ("0" + minute) : minute);
			} else {
				// 跨天: x月x日 xx：xx
				result = ((month + 1) < 10 ? ("0" + (month + 1))
						: (month + 1))
						+ "-"
						+ (monthday < 10 ? ("0" + monthday) : monthday)
						+ " "
						+ (hour < 10 ? ("0" + hour) : hour)
						+ ":"
						+ (minute < 10 ? ("0" + minute) : minute);
			}
		} else {
			result = year
					+ "-"
					+ ((month + 1) < 10 ? ("0" + (month + 1)) : (month + 1))
					+ "-" + (monthday < 10 ? ("0" + monthday) : monthday);
		}
		return result;
	}
	
	
	/**
	 * 功能：格式化时间 当天：今天 上午/下午 跨天: xx月xx日 上午/下午 跨年: xxxx年xx月xx日 上午/下午
	 * @param d
	 * @return
	 */
	public static String formatAMAndPMDate(java.util.Date d) {
		if (d == null) {
			throw new RuntimeException("传入的时间为NULL");
		}

		String result = null;
		Calendar cal = Calendar.getInstance();
		int thisyear = cal.get(Calendar.YEAR);
		// int thismonth = cal.get(Calendar.MONTH);
		int today = cal.get(Calendar.DAY_OF_YEAR);
		// 修改时间
		cal.setTimeInMillis(d.getTime());
		// cal.set(Calendar.YEAR, 2014);
		// cal.set(Calendar.MONTH, 5);
		// cal.set(Calendar.DAY_OF_MONTH, 5);
		// cal.set(Calendar.HOUR_OF_DAY, 5);

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int yearday = cal.get(Calendar.DAY_OF_YEAR);
		int monthday = cal.get(Calendar.DAY_OF_MONTH);
		//int weekday = cal.get(Calendar.DAY_OF_WEEK);
		int r = cal.get(Calendar.AM_PM);//询上午还是下午
		String am_pm_str = "";
		if(r==Calendar.AM){//如果上午
			am_pm_str = "上午";
		}else{
			am_pm_str = "下午";
		}
		
		if (thisyear == year) {
			if (yearday == today) {
				result = "今天 ";
			} else {
				// 跨天: x月x日 xx：xx
				result = ((month + 1) < 10 ? ("0" + (month + 1))
						: (month + 1))
						+ "-"
						+ (monthday < 10 ? ("0" + monthday) : monthday);
			}
		} else {
			result = year
					+ "-"
					+ ((month + 1) < 10 ? ("0" + (month + 1)) : (month + 1))
					+ "-" + (monthday < 10 ? ("0" + monthday) : monthday);
		}
		result+=" "+am_pm_str;
		return result;
	}
/**
 * 功能：格式化时间 当天：今天   跨天: xx月xx日  跨年: xxxx年xx月xx日 
 * @param d
 * @return
 */
public static String formatDateOnly(java.util.Date d) {
	if (d == null) {
		throw new RuntimeException("传入的时间为NULL");
	}

	String result = null;
	Calendar cal = Calendar.getInstance();
	int thisyear = cal.get(Calendar.YEAR);
	// int thismonth = cal.get(Calendar.MONTH);
	int today = cal.get(Calendar.DAY_OF_YEAR);
	// 修改时间
	cal.setTimeInMillis(d.getTime());
	// cal.set(Calendar.YEAR, 2014);
	// cal.set(Calendar.MONTH, 5);
	// cal.set(Calendar.DAY_OF_MONTH, 5);
	// cal.set(Calendar.HOUR_OF_DAY, 5);

	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH);
	int yearday = cal.get(Calendar.DAY_OF_YEAR);
	int monthday = cal.get(Calendar.DAY_OF_MONTH);
	if (thisyear == year) {
		if (yearday == today) {
			result = "今天 ";
		} else {
			// 跨天: x月x日 xx：xx
			result = ((month + 1) < 10 ? ("0" + (month + 1))
					: (month + 1))
					+ "-"
					+ (monthday < 10 ? ("0" + monthday) : monthday);
		}
	} else {
		result = year
				+ "-"
				+ ((month + 1) < 10 ? ("0" + (month + 1)) : (month + 1))
				+ "-" + (monthday < 10 ? ("0" + monthday) : monthday);
	}
	return result;
}
}
