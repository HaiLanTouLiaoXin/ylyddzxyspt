package cn.com.qytx.cbb.capital.vo;

/**
 * 功能:资产Vo类 
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public class CapitalVo {
	
	private String capitalStatus;//资产状态
	
	private String startTime;//开始入库时间
	
	private String endTime;//结束入库时间
	
	private Integer storeLocation;//存放位置
	
	private String useUserIds;//使用人
	
	private Integer capitalType;//资产类型
	
	private String keyWord;//关键字
	
	private Integer capitalGroup;//资产组
	
	private String capitalGroupIds;//资产组Id串
	
	private Integer companyId;//公司Id
	
	private Integer logType;//日志类型
	private String capitalNo;//编号
	
	
	private Integer userGroupId;//使用人部门
	
	
	
	private Integer type;//2 查看本部门借用的设备
	
	/**
	 * 当前登录人部门ID
	 */
	private Integer currentGroupId;
	
	//使用开始时间
	private String useStartTime;
	//使用结束时间
	private String useEndTime;
	//停用开始时间
	private String disableStartTime;
	//停用结束时间
	private String disableEndTime;
	
	
	private String userGroupName;

	
	private String locationName;
	
	public String getCapitalStatus() {
		return capitalStatus;
	}

	public void setCapitalStatus(String capitalStatus) {
		this.capitalStatus = capitalStatus;
	}

	public Integer getStoreLocation() {
		return storeLocation;
	}

	public void setStoreLocation(Integer storeLocation) {
		this.storeLocation = storeLocation;
	}


	public String getStartTime() {
		return startTime;
	}


	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getUseUserIds() {
		return useUserIds;
	}

	public void setUseUserIds(String useUserIds) {
		this.useUserIds = useUserIds;
	}

	public Integer getCapitalType() {
		return capitalType;
	}

	public void setCapitalType(Integer capitalType) {
		this.capitalType = capitalType;
	}

	public Integer getCapitalGroup() {
		return capitalGroup;
	}

	public void setCapitalGroup(Integer capitalGroup) {
		this.capitalGroup = capitalGroup;
	}

	public Integer getLogType() {
		return logType;
	}

	public void setLogType(Integer logType) {
		this.logType = logType;
	}

	public String getCapitalGroupIds() {
		return capitalGroupIds;
	}

	public void setCapitalGroupIds(String capitalGroupIds) {
		this.capitalGroupIds = capitalGroupIds;
	}

	public String getCapitalNo() {
		return capitalNo;
	}

	public void setCapitalNo(String capitalNo) {
		this.capitalNo = capitalNo;
	}

	public Integer getCurrentGroupId() {
		return currentGroupId;
	}

	public void setCurrentGroupId(Integer currentGroupId) {
		this.currentGroupId = currentGroupId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getUseStartTime() {
		return useStartTime;
	}

	public String getUseEndTime() {
		return useEndTime;
	}

	public String getDisableStartTime() {
		return disableStartTime;
	}

	public String getDisableEndTime() {
		return disableEndTime;
	}

	public void setUseStartTime(String useStartTime) {
		this.useStartTime = useStartTime;
	}

	public void setUseEndTime(String useEndTime) {
		this.useEndTime = useEndTime;
	}

	public void setDisableStartTime(String disableStartTime) {
		this.disableStartTime = disableStartTime;
	}

	public void setDisableEndTime(String disableEndTime) {
		this.disableEndTime = disableEndTime;
	}

	public String getUserGroupName() {
		return userGroupName;
	}

	public void setUserGroupName(String userGroupName) {
		this.userGroupName = userGroupName;
	}

	public Integer getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Integer userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
  
	
	
}
