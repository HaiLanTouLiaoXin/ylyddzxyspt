package cn.com.qytx.cbb.capital.service;

import java.util.List;

import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:资产接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public interface ICapitalGroup extends BaseService<CapitalGroup>{

	
	/**
	 * 功能：获取资产组列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<CapitalGroup> findCapitalGroupList(Integer companyId,String capitalGroupName);
	
	
	/**
	 * 功能：获取资产组树列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<TreeNode> getTreeCapitalGroupList(String path,Integer companyId);
	
	
	public List<TreeNode> getTreeCapitalGroupList_Select(String path,Integer companyId);
	
	/**
	 * 在指定ID下面是否有相同的资产组名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameGroupName(Integer parentId,String groupName,int companyId);
	
	
	/**
	 * 在指定ID下面是否有相同的资产组编码
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameCapitalCode(Integer parentId,String capitalCode,int companyId);
	
	
	/**
	 * 功能：判断是否存在子资产组
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildCapitalGroup(Integer id,int companyId);
	
	/**
	 * 功能：通过Id传获取资产组列表
	 * @param Ids
	 * @return
	 */
	public List<CapitalGroup> getCapitalGroupListByIds(String Ids);
	
	/**
	 * 功能：判断是否存在资产组
	 * @param name
	 * @param companyId
	 * @return
	 */
	public CapitalGroup findModel(String name, int companyId);
	
}
