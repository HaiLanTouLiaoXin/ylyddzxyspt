/**
 * 人员选择
 * REN
 **/
/**
 * 部门主管
 **/
function selectUserDirector()
{
    var directorId=$("#directorId").val();
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	defaultSelectIds:directorId,
    	callback:callBackDirector
    });
}
function callBackDirector(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		userNames.push(item.name);
    	});
        $("#directorId").val(userIds[0]);
        $("#director").val(userNames[0]);
    }
}
/**
 * 部门助理
 **/
function selectUserAssistant()
{
    var assistantId=$("#assistantId").val();
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	defaultSelectIds:assistantId,
    	callback:callBackAssistant
    });
}
function callBackAssistant(data)
{
	if(data!=undefined) {
        var userIds = [];
        var userNames = [];
        $(data).each(function(i,item){
    		userIds.push(item.id);
    		userNames.push(item.name);
    	});
        $("#assistantId").val(userIds[0]);
        $("#assistant").val(userNames[0]);
    }
}
/**
 * 上级主管领导
 **/
function selectUserTopDirector()
{
     var topDirectorId=$("#topDirectorId").val();
     qytx.app.tree.alertUserTree({
     	type:"radio",
     	defaultSelectIds:topDirectorId,
     	callback:callBackTopDirector
     });
}
function callBackTopDirector(data)
{
    if(data){
        var userIds = [];
        var userNames = [];
        $(data).each(function(i,item){
    		userIds.push(item.id);
    		userNames.push(item.name);
    	});
        $("#topDirectorId").val(userIds[0]);
        $("#topDirector").val(userNames[0]);
    }
}
/**
 * 上级分管领导
 **/
function selectUserTopChange()
{
    var topChangeId=$("#topChangeId").val();
    qytx.app.tree.alertUserTree({
     	type:"radio",
     	defaultSelectIds:topChangeId,
     	callback:callBackTopChange
     });
}
function callBackTopChange(data)
{
    if(data){
        var userIds = [];
        var userNames = [];
        $(data).each(function(i,item){
    		userIds.push(item.id);
    		userNames.push(item.name);
    	});
        $("#topChangeId").val(userIds[0]);
        $("#topChange").val(userNames[0]);
    }
}
/**
 * 上级部门
 **/
function selectGroupParent()
{
    openSelectUser(1,callBackParent);
}
function callBackParent(data)
{
    if(data){
        var userIds = [];
        var userNames = [];
        data.forEach(function(value) {
           userIds.push(value.Id);
           userNames.push(value.Name);
        });
        $("#parentId").val(userIds[0]);
        $("#parent").val(userNames[0]);
    }
}



function locationSelect()
{
    var locatrionId=$("#locatrionId").val();
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	showType:5,
    	defaultSelectIds:locatrionId,
    	callback:callBackLaction
    });
}

var locationIds= new Array();

function callBackLaction(data)
{
    if(data){
    	var locationIdArray = [];
    	var locationNames = [];
    	$(data).each(function(i,item){
    		locationIdArray.push(item.id);
    		if(item.pathName){
    			locationNames.push(item.pathName);
    		}else{
    			locationNames.push(item.name);
    		}
    		
    	});
    	var i=contains(locationIds,locationIdArray[0]);//i true 存在 ，否则 不存在
    	if(!i){
    		locationIds.push(locationIdArray[0]);
            $("#locatrionId").val(locationIdArray[0]);
            if(locationNames[0]){
            	$("#locationDiv").append("<div><span>"+locationNames[0]+"</span><span style='padding-left: 20px;color: red;' onclick='delectSelect("+locationIdArray[0]+",this)'>删除</span></div>");
            }
    	}
    	
    }
}

function contains(arr, obj) {
  var i = arr.length;
  while (i--) {
    if (arr[i] === obj) {
      return true;
    }
  }
  return false;
}
/**
 * 删除选中的
 */
function delectSelect(vid,obj){
	obj.parentNode.parentNode.removeChild(obj.parentNode); 
	removeByValue(locationIds, vid);
}
/**
 * 移除添加的元素
 * @param arr
 * @param val
 */
function removeByValue(arr, val) {
  for(var i=0; i<arr.length; i++) {
    if(arr[i] == val) {
      arr.splice(i, 1);
      break;
    }
  }
}

	

/**
 * 打开人员选择对话框
 * callback 回调函数，会把选择的人员以json格式返回
 * @showType 显示类型 1 显示部门 2显示角色 3显示人员
 * @callback 回调方法
 */
function openSelectUser(showType,callback,defaultSelectId) {
    var url = basePath + "/logined/user/selectuserSign.jsp?showType="+showType+"&defaultSelectId="+defaultSelectId;
    var title="选择人员";
    if(showType==1)
    {
        title="选择部门";
    }
    else if(showType==2)
    {
        title="选择角色";
    }
    art.dialog.open(url,
        {
            title:title,
            width : 360,
			height : 407,
			lock : true,
		    opacity: 0.08,
            button:[
                {
                    name:'确定',
                    focus:true,
                    callback:function () {
                        var userMap =art.dialog.data("userMap");
                        callback(userMap);
                        return true;
                    }
                },
                {
                    name:'取消',
                    callback:function () {
                        return true;
                    }
                }
            ]
        }, false);

}