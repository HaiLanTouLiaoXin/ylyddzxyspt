/**
 * 坐席下拉选 获取选中name值
 * */
app.filter("selectUserNames",function(){
	return function(nodes){
		if(!nodes || nodes.length==0) {
			return "请选择";
		}
		names="";
        for(var i = 0; i < nodes.length; i++){
        	if(nodes[i].id.indexOf("uid_") != -1){
        		names+=nodes[i].name + ",";
        	}
        }
        if(names) {
        	names = names.substring(0, names.length-1);
        }
        if(names == "") {
        	return "请选择";
        }
		return names;
	}
});


/**
 * 坐席下拉选 获取选中id值
 * */
app.filter("selectUserIds",function(){
	return function(nodes){
		if(!nodes) {
			return "";
		}
		ids="";
        for(var i = 0; i < nodes.length; i++){
        	if(nodes[i].id.indexOf("uid_") != -1){
        		ids+=nodes[i].id.substring(nodes[i].id.indexOf("uid_")+"uid_".length) + ",";
        	}
        }
        if(ids) {
        	ids = ids.substring(0, ids.length-1);
        }
        if(ids == "") {
        	ids = "";
        }
		return ids;
	}
});



/**
 * 资产组下拉选 获取选中name值
 * */
app.filter("selectGroupNames",function(){
	return function(nodes){
		if(!nodes) {
			return "";
		}
		names="";
        for(var i = 0; i < nodes.length; i++){
        	if(nodes[i].id.indexOf("uid_") != -1){
        		names+=nodes[i].name + ",";
        	}
        }
        if(names) {
        	names = names.substring(0, names.length-1);
        }
        if(names == "") {
        	names = "";
        }
		return names;
	}
});


/**
 * 资产组下拉选 获取选中id值
 * */
app.filter("selectGroupIds",function(){
	return function(nodes){
		if(!nodes) {
			return "";
		}
		ids="";
        for(var i = 0; i < nodes.length; i++){
        	if(nodes[i].id.indexOf("uid_") != -1){
        		ids+=nodes[i].id.substring(nodes[i].id.indexOf("uid_")+"uid_".length) + ",";
        	}
        }
        if(ids) {
        	ids = ids.substring(0, ids.length-1);
        }
        if(ids == "") {
        	ids = "";
        }
		return ids;
	}
});

/**
 * 资产组树 获取选中id值
 * */
app.filter("selectGroupId",function(){
	return function(node){
		if(!node) {
			return "";
		}
		return node.id.replace("gid_","");
	}
});

app.filter("getHistoryTitle",function(){
	return function(taskName){
		
		if(taskName=="-1"){
			return "调度人";
		}else if(taskName=="1"){
			return "上报人";
		}else if(taskName=="2"){
			return "接收人";
		}else if(taskName=="3"){
			return "处理人";
		}else if(taskName=="4"){
			return "验收人";
		}else if(taskName=="5"){
			return "归档人";
		}
	}
});

app.filter("getHistoryDetail",function(){
	return function(taskName,approveResult,nextProcesserName,nextProcesserPhone,advice){
		var outCallHtml = "";
		/*if(isOut=="1"&&nextProcesserPhone){
			outCallHtml = "<span class=\"tel\"  ng-click=\"outCall("+nextProcesserPhone+",'','')\"></span>";
		}*/
		nextProcesserPhone = (nextProcesserPhone?nextProcesserPhone:'电话暂无');
		if(taskName=="-1"&&approveResult=="-1"){
			var operTitle = "调度";
			if(advice&&advice!="undefined"){
				return "上报事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle+":\""+advice+"\"";
			}else{
				return "上报事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle;
			}
		}else if(taskName=="1"&&approveResult=="-1"){
			var operTitle = "接收";
			if(advice&&advice!="undefined"){
				return "上报事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle+":\""+advice+"\"";
			}else{
				return "上报事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle+"接收";
			}
		}else if(taskName=="0"&&approveResult=="4"){
			var operTitle = "接收";
			if(advice&&advice!="undefined"){
				return "调度事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle+":\""+advice+"\"";
			}else{
				return "调度事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")"+operTitle;
			}
		}else if(taskName=="1"&&approveResult=="0"){
			if(advice&&advice!="undefined"){
				return "拒绝作废:\""+advice+"\"";
			}else{
				return "拒绝作废";
			}
		}else if(taskName=="1"&&approveResult=="1"){
			if(advice&&advice!="undefined"){
				return "作废事件:\""+advice+"\"";
			}else{
				return "作废事件";
			}
		}else if(taskName=="2"&&approveResult=="1"){
			if(advice&&advice!="undefined"){
				return "接收事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")处理:\""+advice+"\"";
			}else{
				return "接收事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")处理";
			}
		}else if(taskName=="9"&&approveResult=="5"){
			if(advice&&advice!="undefined"){
				return "转交事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")处理:\""+advice+"\"";
			}else{
				return "转交事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")处理";
			}
		}else if(taskName=="2"&&approveResult=="0"){
			if(advice&&advice!="undefined"){
				return "请求作废:\""+advice+"\"";
			}else{
				return "请求作废";
			}
		}else if(taskName=="2"&&approveResult=="2"){
			if(advice&&advice!="undefined"){
				return "转交给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")接收:\""+advice+"\"";
			}else{
				return "转交给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")接收";
			}
		}else if(taskName=="3"&&approveResult=="1"){
			if(advice&&advice!="undefined"){
				return "处理事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")验收:\""+advice+"\"";
			}else{
				return "处理事件，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")验收";
			}
		}else if(taskName=="4"&&approveResult=="0"){
			if(advice&&advice!="undefined"){
				return "验收驳回:\""+advice+"\"";
			}else{
				return "验收驳回";
			}
		}else if(taskName=="4"&&approveResult=="1"){
			/*if(advice&&advice!="undefined"){
				return "验收通过，指派给 "+nextProcesserName+" ("+nextProcesserPhone+")归档:\""+advice+"\"";
			}else{
				return "验收通过，指派给 "+nextProcesserName+" ("+nextProcesserPhone+")归档";
			}*/
			if(advice&&advice!="undefined"){
				return "验收通过，意见：\""+advice+"\"";
			}else{
				return "验收通过";
			}
		}else if(taskName=="5"&&approveResult=="0"){
			if(advice&&advice!="undefined"){
				return "归档驳回:\""+advice+"\"";
			}else{
				return "归档驳回";
			}
		}else if(taskName=="5"&&approveResult=="1"){
			if(advice&&advice!="undefined"){
				return "归档完成:\""+advice+"\"";
			}else{
				return "归档完成";
			}
		}else if(taskName=="6"&&approveResult=="3"){
			if(advice&&advice!="undefined"){
				return "申请延期，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")延期:\""+advice+"\"";
			}else{
				return "申请延期，指派给 "+nextProcesserName+" ("+nextProcesserPhone+outCallHtml+")延期";
			}
		}else if(taskName=="7"&&approveResult=="3"){
			return "批准延期通过";
		}else if(taskName=="8"&&approveResult=="3"){
			if(advice&&advice!="undefined"){
				return "申请延期驳回:\""+advice+"\"";
			}else{
				return "申请延期驳回";
			}
		}else{
			return "";
		}
	}
});


app.filter("getHistoryDetail2",function(){
	return function(taskName,approveResult){
		var outCallHtml = "";
		/*if(isOut=="1"&&nextProcesserPhone){
			outCallHtml = "<span class=\"tel\"  ng-click=\"outCall("+nextProcesserPhone+",'','')\"></span>";
		}*/
		//nextProcesserPhone = (nextProcesserPhone?nextProcesserPhone:'电话暂无');
		if(taskName=="-1"&&approveResult=="-1"){
			return "调度";
		}else if(taskName=="1"&&approveResult=="-1"){
			return "上报";
		}else if(taskName=="0"&&approveResult=="4"){
			return "接收";
		}else if(taskName=="1"&&approveResult=="0"){
			return "拒绝作废";
		}else if(taskName=="1"&&approveResult=="1"){
			return "作废事件";
		}else if(taskName=="2"&&approveResult=="1"){
			return "接收";
		}else if(taskName=="9"&&approveResult=="5"){
			return "转交处理";
		}else if(taskName=="2"&&approveResult=="0"){
			return "请求作废";
		}else if(taskName=="2"&&approveResult=="2"){
			return "转交接收";
		}else if(taskName=="3"&&approveResult=="1"){
			return "处理";
		}else if(taskName=="4"&&approveResult=="0"){
			return "验收驳回";
		}else if(taskName=="4"&&approveResult=="1"){
			/*if(advice&&advice!="undefined"){
				return "验收通过，指派给 "+nextProcesserName+" ("+nextProcesserPhone+")归档:\""+advice+"\"";
			}else{
				return "验收通过，指派给 "+nextProcesserName+" ("+nextProcesserPhone+")归档";
			}*/
			return "验收";
		}else if(taskName=="5"&&approveResult=="0"){
			return "归档驳回";
		}else if(taskName=="5"&&approveResult=="1"){
			if(advice&&advice!="undefined"){
				return "归档完成:\""+advice+"\"";
			}else{
				return "归档完成";
			}
		}else if(taskName=="6"&&approveResult=="3"){
			return "申请延期";
		}else if(taskName=="7"&&approveResult=="3"){
			return "延期通过";
		}else if(taskName=="8"&&approveResult=="3"){
			return "延期驳回";
		}else{
			return "";
		}
	}
});

app.filter("stateColor",function(){
	return function(state){
		if(state=="待接收"||state=="待处理"||state=="待作废"||state=="待调度"){
			return "stateColor1";
		}else if(state=="待验收"){
			return "stateColor2";
		}else if(state=="待归档"){
			return "stateColor3";
		}else if(state=="已归档"||state=="已作废"||state=="已完结"){
			return "stateColor4";
		}else{
			return "";
		}
	}
});



app.filter("getLength",function(){
	return function(str){
		var realLength = 0, len = str.length, charCode = -1;
	    for (var i = 0; i < len; i++) {
	        charCode = str.charCodeAt(i);
	        if (charCode >= 0 && charCode <= 128) realLength += 1;
	        else realLength += 2;
	    }
	    return realLength;
	}
});


app.filter("cutstr",function(){
	return function(str,len){
		var str_length = 0;
	    var str_len = 0;
	    str_cut = new String();
	    str_len = str.length;
	    for (var i = 0; i < str_len; i++) {
	        a = str.charAt(i);
	        str_length++;
	        if (escape(a).length > 4) {
	            //中文字符的长度经编码之后大于4  
	            str_length++;
	        }
	        str_cut = str_cut.concat(a);
	        if (str_length >= len) {
	            //str_cut = str_cut.concat("...");
	            return str_cut;
	        }
	    }
	    //如果给定字符串小于指定长度，则返回源字符串；  
	    if (str_length < len) {
	        return str;
	    }
	}
});
