var instanceId = "";
$(function(){
	instanceId = $("#instanceId").val();
	findPeopleList(instanceId);
});



var userObj;
/**
 * 选择参与人
 * REN
 **/
function selectPeople()
{
    var userIds="";
    var peopleNum = 0;
    $("input[name='peopleId']").each(function(){
    	peopleNum += 1;
    	userIds+=$(this).val()+",";
    });
    if(peopleNum>0){
    	userIds = userIds.substring(0,userIds.length-1);
    }
    qytx.app.tree.alertUserTree({
    	type:"checkbox",
    	showType:3,
    	defaultSelectIds:userIds,
    	extension:"1,2",
    	callback:callBackPeople
    });
}

function callBackPeople(data)
{
    if(data){
    	var userIds = [];
    	var userIdStr = "";
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    	});
    	//遍历
    	//将处理人去除
    	var processUserId = $("#processUserId").val();
		for(var i =0;i<userIds.length;i++){
			if(userIds[i]!=processUserId){
				userIdStr += userIds[i]+",";
			}
		}
		userIdStr = userIdStr.substring(0,userIdStr.length-1);
		addPeople(userIdStr);
    }
}



//添加参与人
function addPeople(userIds){
	var url = basePath+"defect/saveSelectPeople.action";
	var param = {"userIds":userIds,"instanceId":instanceId};
	$.ajax({
		url:url,
		type:'POST',
		data:param,
		dataType:'text',
		success:function(data){
			if(data=="0"){
				art.dialog.tips("添加成功!");
				findPeopleList(instanceId);
			}else{
				art.dialog.tips("添加失败,请稍后重试!");
			}
		}
	});
	
	
}
		




/**
 * 获得选择的列表
 */
function findPeopleList(instanceId){
	$.ajax({
		url:basePath+"defect/findSelectPeopleList.action",
		type:"post",
		data:{"instanceId":instanceId},
		dataType:'json',
		success:function(data){
			var operType = $("#oper").val();
			var html="";
			if(data.length>0){
				for(var i=0;i<data.length;i++){
					html += '<tr><input type="hidden" value="'+data[i].peopleId+'" name="peopleId"/><td width="25%">'+data[i].userName+'</td><td width="20%">'+data[i].workNo+'</td><td width="20%">'+data[i].groupName+'</td><td width="25%">'+data[i].phone+'</td>';
					if(operType=="dispatch"||operType=="approve"||operType=="defect"){
						html += '<td width="10%"><a href="javascript:;" onClick="deletePeople('+data[i].id+')" class="color_red">删除</a></td></tr>';
					}else{
						html += '<td width="10%">--</td></tr>';
					}
				}
			}else{
				html += '<tr><td colspan="5">您还没有参与人</td></tr>';
			}
			$("#peopleTable").html(html);
		}
	})
}

/**
 * 删除操作
 */
function deletePeople(id){
	$.ajax({
		url:basePath+"defect/deleteSelectPeople.action",
		type:"post",
		data:{"id":id},
		dataType:'text',
		success:function(data){
			if(data==0){
				art.dialog.tips("删除成功！")
				findPeopleList(instanceId);
			}else{
				art.dialog.alert("删除失败！")
			}
		}
	})
}