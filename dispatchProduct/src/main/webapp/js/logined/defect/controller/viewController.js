app.controller("viewController",["$rootScope","$scope","defectService","$filter","ngDialog",function($rootScope,$scope,defectService,$filter,ngDialog){
	var instanceId=document.getElementById("instanceId").value;
	var oper=document.getElementById("oper").value;
	var isOut=document.getElementById("isOut").value;
	var isForkGroup=document.getElementById("isForkGroup").value;
	var iDisplayStart=document.getElementById("iDisplayStart").value;
	$scope.isOut = isOut;
	/**
	 * apply申请详情
	 * approve待接收
	 * defect 待消缺
	 * accept 待验收
	 * archive 待归档
	 * view 不可操作
	 */
	$scope.oper=oper;
	$rootScope.dialogInfo = {
		theme:"",
		userTitle:"",
		adviceTitle:""
	};
	
	//获取页面详情信息
	defectService.getView({"instanceId":instanceId},function(result){
		$scope.apply=result.apply;
		$scope.historyList=$scope.apply.historyList;
		$scope.turnUser=result.turnUser;
		$scope.rejectNum=result.rejectNum;
		$scope.turnNum=result.turnNum;
		var attachmentIds=new Array();
		if($scope.apply.attachmentIds){
			attachmentIds=$scope.apply.attachmentIds.split(",");
		}
		$scope.attachmentIds=attachmentIds;
		
		var checkAttachmentIds=new Array();
		if($scope.apply.checkAttachmentIds){
			checkAttachmentIds=$scope.apply.checkAttachmentIds.split(",");
		}
		$scope.checkAttachmentIds=checkAttachmentIds;
		
		$scope.audioSrc = baseUrl+"fileServer/download?fileId="+$scope.apply.audioIds;
		
		$scope.hour=result.hour;
		$scope.outTimeState=result.outTimeState;
		$scope.processUserId=result.processUserId;
		$scope.processGroupName=result.processGroupName;
		$scope.processUserName=result.processUserName;
		$scope.processTime=result.processTime;
		
		$scope.dispatchWorkNo=result.dispatchWorkNo;
		$scope.dispatchUserName=result.dispatchUserName;
		
	});
	
	//返回
	$scope.goBack=function(){
		var url = document.referrer;
		url = url.replace(/iDisplayStart/g,"20")
		if(document.referrer.indexOf('?')>0){
			url += "&iDisplayStart="+iDisplayStart;
		}else{
			url += "iDisplayStart="+iDisplayStart;
		}
		if(document.referrer.indexOf("index.jsp")>-1){
			window.top.location.href = url;
		}else{
			window.history.go(-1);
		}
		
	}
	//编辑缺陷
	$scope.editDefect=function(){
		window.location.href=basePath+"logined/defect/add.jsp?instanceId="+$scope.apply.instanceId+"&isForkGroup="+isForkGroup;
	}
	//作废缺陷
	$scope.draftDefect=function(){
		art.dialog.confirm("确定要作废该事件上报吗？",function(){
			$scope.approve(1,1,'同意作废','');
		});
	}
	//同意作废
	$scope.agree=function(){
		art.dialog.confirm("确定要同意吗？",function(){
			$scope.approve(1,1,'同意作废','');
		});
	}
	//拒绝作废
	$scope.rejuest=function(){
		art.dialog.confirm("确定要拒绝吗？",function(){
			$scope.approve(1,0,'拒绝作废','');
		});
	}
	
	
	//同意延期
	$scope.agreeDelay=function(){
		art.dialog.confirm("确定要同意吗？",function(){
			$scope.approve(7,3,'同意延期','');
		});
	}
	//拒绝延期
	$scope.rejuestDelay=function(){
		$scope.openDialog(8);
	}
	
	//请求作废
	$scope.applyDefect=function(){
		art.dialog.confirm("确定要请求作废吗？",function(){
			$scope.approve(2,0,'请求作废','');
		});
	}
	//删除缺陷
	$scope.deleteDefect=function(){
		art.dialog.confirm("您确定要删除该上报吗？",function(){
			defectService.deleteDefect({"instanceId":instanceId},function(data){
				if(data==1){
					window.location.href=basePath+"logined/defect/myStartList.jsp?&isForkGroup="+isForkGroup;
				}else if(data==3){
					art.dialog.alert("上报申请已接收不可删除");
				}else{
					art.dialog.alert("删除失败");
				}
			});
		});
	}
	
	$scope.pushReminder=function(){
		art.dialog.confirm("确定要进行催单吗？",function(){
			defectService.pushReminder({"instanceId":instanceId},function(data){
				if(data=="0"){
					art.dialog.tips("催单成功!");
					setTimeout(function(){
						window.location.reload();
					},1000);
				}else{
					art.dialog.alert("操作失败,请稍后重试!");
				}
			});
		});
	}
	
	$scope.openApprove = function(theme,userTitle,adviceTitle,taskName,approveResult,adviceDef,index){
		var url = basePath+"logined/defect/approve.jsp";
		var param = {"userTitle":userTitle,"adviceTitle":adviceTitle,"taskName":taskName,"approveResult":approveResult,"adviceDef":adviceDef};
		art.dialog.data("dialogInfo",param);
		art.dialog.data("apply",$scope.apply);
		art.dialog.data("turnNum",$scope.turnNum);
		art.dialog.data("turnUser",$scope.turnUser);
		art.dialog.data("instanceId",instanceId);
		art.dialog.data("oper",oper);
		art.dialog.data("index",index);
		var type="approve";
		if(taskName==0){
			type="dispatch";
		}else if(taskName==1){
			type="apply";
		}else if(taskName==2){
			type="approve";
		}else if(taskName==3||taskName==6||taskName==9){
			type="defect";
		}else if(taskName==4){
			type="accept";
		}else if(taskName==5){
			type="archive";
		}
		art.dialog.open(url,{
			title : theme,
			width : 480,
			height : 220,
			lock : true,
			drag : true,
			opacity : 0.08,//透明度
			close:function(){
				if(art.dialog.data("result")=='success1'){
					if(type=="apply"){
						window.location.href=basePath+"/logined/defect/myStartList.jsp?isForkGroup="+isForkGroup;
					}else if(oper=="delay"){
						window.location.href=basePath+"/logined/defect/myStartList.jsp?type=delay&isForkGroup="+isForkGroup;
					}else if(oper=="approveDelay"){
						window.location.href=basePath+"/logined/defect/myWaitList.jsp?type=approveDelay";
					}else{
						window.location.href=basePath+"logined/defect/myWaitList.jsp?type="+type;
					}
					
				}
				return true;
		   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
		});
	}
	
	
	//选择下一步操作人，及输入处理意见
	$scope.adviceDef="";
	$scope.openDialog=function(index){
		var theme="";
		var userTitle="";
		var adviceTitle="";
		var taskName="";
		var approveResult="";
		var adviceDef="";
		$scope.index=index;
		if(index==0){
			$rootScope.dialogInfo.theme="转交事件";
			$rootScope.dialogInfo.userTitle="转交人";
			$rootScope.dialogInfo.adviceTitle="转交意见";
			taskName="2";
			approveResult="2";
			$scope.adviceDef="已转交";
			
			theme="转交事件";
			userTitle="转交人";
			adviceTitle="转交意见";
			adviceDef="已转交";
		}else if(index==1){
			$rootScope.dialogInfo.theme="处理完成";
			$rootScope.dialogInfo.userTitle="验收人";
			$rootScope.dialogInfo.adviceTitle="处理意见";
			taskName="3";
			approveResult="1";
			$scope.adviceDef="已处理";
			
			theme="处理完成";
			userTitle="验收人";
			adviceTitle="处理意见";
			adviceDef="已处理";
			
			
		}else if(index==2){
			$rootScope.dialogInfo.theme="验收通过";
//			$rootScope.dialogInfo.userTitle="归档人";
			$rootScope.dialogInfo.userTitle="";
			$rootScope.dialogInfo.adviceTitle="验收意见";
			taskName="4";
			approveResult="1";
			$scope.adviceDef="已验收";
			
			theme="验收通过";
			//userTitle="归档人";
			adviceTitle="验收意见";
			adviceDef="已验收";
			
		}else if(index==3){
			$rootScope.dialogInfo.theme="验收不过";
			$rootScope.dialogInfo.userTitle="";
			$rootScope.dialogInfo.adviceTitle="驳回理由";
			taskName="4";
			approveResult="0";
			$scope.adviceDef="验收驳回";
			
			theme="验收不过";
			userTitle="";
			adviceTitle="驳回理由";
			adviceDef="验收驳回";
		}else if(index==4){
			$rootScope.dialogInfo.theme="归档完成";
			$rootScope.dialogInfo.userTitle="";
			$rootScope.dialogInfo.adviceTitle="归档意见";
			taskName="5";
			approveResult="1";
			$scope.adviceDef="已归档";
			
			theme="归档完成";
			userTitle="";
			adviceTitle="归档意见";
			adviceDef="已归档";
		}else if(index==5){
			$rootScope.dialogInfo.theme="驳回验收";
			$rootScope.dialogInfo.userTitle="";
			$rootScope.dialogInfo.adviceTitle="驳回理由";
			taskName="5";
			approveResult="0";
			$scope.adviceDef="验收驳回";
			
			theme="驳回验收";
			userTitle="";
			adviceTitle="驳回理由";
			adviceDef="验收驳回";
		}else if(index==6){
			$rootScope.dialogInfo.theme="接收事件";
			$rootScope.dialogInfo.userTitle="处理人";
			$rootScope.dialogInfo.adviceTitle="接收意见";
			taskName="2";
			approveResult="1";
			$scope.adviceDef="已接收";
			
			theme="接收事件"
			userTitle="处理人";
			adviceTitle="接收意见";
			adviceDef="已接收";
			
		}else if(index==7){
			$rootScope.dialogInfo.theme="申请延期";
			$rootScope.dialogInfo.userTitle="批准领导";
			$rootScope.dialogInfo.adviceTitle="延期原因";
			taskName="6";
			approveResult="3";
			$scope.adviceDef="待延期";
			
			theme="申请延期";
			userTitle="批准领导";
			adviceTitle="延期原因";
			adviceDef="待延期";
		}else if(index==8){
			$rootScope.dialogInfo.theme="拒绝延期";
			$rootScope.dialogInfo.adviceTitle="拒绝原因";
			taskName="8";
			approveResult="3";
			$scope.adviceDef="拒绝延期";
			
			theme="拒绝延期";
			userTitle="";
			adviceTitle="拒绝原因";
			adviceDef="拒绝延期";
		}else if(index==9){
			$rootScope.dialogInfo.theme="调度事件";
			$rootScope.dialogInfo.userTitle="接收人";
			$rootScope.dialogInfo.adviceTitle="调度意见";
			taskName="0";
			approveResult="4";
			$scope.adviceDef="已调度";
			
			theme="调度事件"
			userTitle="接收人/部门";
			adviceTitle="调度意见";
			adviceDef="已调度";
			
		}else if(index==10){
			$rootScope.dialogInfo.theme="转交事件";
			$rootScope.dialogInfo.userTitle="转交人";
			$rootScope.dialogInfo.adviceTitle="转交意见";
			taskName="9";
			approveResult="5";
			$scope.adviceDef="已转交处理";
			
			theme="转交事件"
			userTitle="处理人";
			adviceTitle="转交意见";
			adviceDef="已转交处理";
			
		}
		
		$scope.openApprove(theme,userTitle,adviceTitle,taskName,approveResult,adviceDef,index);
		/*ngDialog.openConfirm({
             template: 'modalDialogId',
             className: 'ngdialog-theme-default',
             controller: 'dialogCtrl',
             scope: $scope
         }).then(function (value) {
        	 var advice=value.advice;
        	 var userId=value.userId;
        	 if($rootScope.dialogInfo.userTitle){
        		 if(!userId){
        			 art.dialog.alert("请选择人员");
        			 return;
        		 }
        	 }else{
        		 userId="";
        	 }
        	 if(!advice){
        		 advice=$scope.adviceDef;
        	 }
        	$scope.approve(taskName, approveResult, advice, userId);
         }, function (reason) {
             
         });*/
	}
	
	//处理结果提交
	$scope.approve=function(taskName,approveResult,advice,nextUserId){
		var params={
			instanceId:instanceId,
			taskName:taskName,
			approveResult:approveResult,
			advice:advice,
			nextUserId:nextUserId
		}
		var type="approve";
		if(taskName==0){
			type="dispatch";
		}else if(taskName==1){
			type="apply";
		}else if(taskName==2){
			type="approve";
		}else if(taskName==3||taskName==6||taskName==9){
			type="defect";
		}else if(taskName==4){
			type="accept";
		}else if(taskName==5){
			type="archive";
		}
		defectService.approve(params,function(result){
			if(result&&result==1){
				if(type=="apply"){
					window.location.href=basePath+"/logined/defect/myStartList.jsp?isForkGroup="+isForkGroup;
				}else if($scope.oper=="delay"){
					window.location.href=basePath+"/logined/defect/myStartList.jsp?type=delay";
				}else if($scope.oper=="approveDelay"){
					window.location.href=basePath+"/logined/defect/myWaitList.jsp?type=approveDelay";
				}else{
					window.location.href=basePath+"logined/defect/myWaitList.jsp?type="+type;
				}
			}
		});
	}
	
	
	
	
	$scope.outCall = function(called,hotline,source){
		 if (typeof(exec_obj) == 'undefined') {
	            exec_obj = document.createElement('iframe');
	            exec_obj.name = 'tmp_frame';
	            exec_obj.src = 'http://10.10.1.30:8081/zxyy/outCall.html?called=' + called +
	                "&hotline=" + hotline + "&source=" + source + "&token=" + Math.random();
	            exec_obj.style.display = 'none';
	            document.body.appendChild(exec_obj);
	        } else {
	            exec_obj.src = 'http://10.10.1.30:8081/zxyy/outCall.html?called=' + called +
	                "&hotline=" + hotline + "&source=" + source + "&token=" + Math.random();
	        }
	}
	
	
}]).controller("dialogCtrl",["$scope","defectService","$filter",function($scope,defectService,$filter){
	$scope.showSelectUserValue=2;
	$scope.flag=false;
	/**
	 * 获取useuser下拉树
	 */
	$scope.getUserSelect = function(){
		$scope.set1 = {
			isRadio: true,//true单选，false复选,默认true
			enable: true
		};
		defectService.getSelectUser({"showUser":3},function(result){
			$scope.data1 = result;
		});
	}
	//初始化人员树
	$scope.getUserSelect();
	//选择人员
	$scope.toggleSelectUser = function (){
		if($scope.showSelectUserValue== 1){
			$scope.showSelectUserValue=2;
		}else {
			$scope.showSelectUserValue=1;
		}
		this.showSelectUserValue = $scope.showSelectUserValue;
	}
	if($scope.turnNum-$scope.apply.turnNum<=1&&$scope.index==0){
		$scope.defaultSelectUser="uid_"+$scope.turnUser.userId;
		$scope.node1=[{"obj":1,"id":$scope.defaultSelectUser,"name":$scope.turnUser.userName}];
		$scope.flag=true;
	}
	
	
	
	
	
}]);