app.controller("distributionAddController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	var instanceId=document.getElementById("instanceId").value;
	var eventLoginName = $("#eventLoginName").val();
	$scope.loginName = eventLoginName;
	var eventUserId = $("#eventUserId").val();
	
	
	$scope.refresh = function () {
		$("#divselect3"+" ul").show();
         var param = {
             "capitalGroupName":$scope.maintenanceItems
         };
         defectService.findCapitalGroupList(param, function(result){
             $scope.capitalGroupList = result;
         });
     }
	
	//自定义下拉2
	$scope.selectP = function (id,name) {
        $("#maintenanceItems").val(name);
        $scope.capitalGroupId = id;
    }
	
	//上报信息对象
	var apply={
			isRepair:1
	};
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"grade",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.gradeList=result;
	});
	if(instanceId&&instanceId!='null'){
		//获取页面详情信息
		defectService.getView({"instanceId":instanceId},function(result){
			$scope.apply=result.apply;
			$("#maintenanceItems").val(result.apply.equipmentName);
			$("#processId").val(result.apply.processId);
			$("#processName").val(result.apply.processName);
			$("#processType").val(result.apply.processType);
			$("#equipmentNumber").val(result.apply.equipmentNumber);
			$("#processName").attr("disabled",true);
			$("#userSelect").hide();
			$("#createGroupId").val(result.apply.createGroupId);
			$("#createGroupName").val(result.apply.defectDepartmentVo);
			$("#defectTime").val(defectService.formatDate(new Date(result.apply.defectTime)));
			if($scope.apply.processId&&$scope.apply.processName){
				$scope.defaultSelectUser="uid_"+$scope.apply.processId;
				$scope.node1=[{"obj":1,"id":$scope.defaultSelectUser,"name":$scope.apply.processName}];
			}
			if(result.attList){
				var fs=new Array();
				var outfile=new Array();
				for(var i=0;i<result.attList.length;i++){
					var attachment=result.attList[i];
					fs.push({
						name:attachment.attachName
					});
					outfile.push({
						fileData: [{
							id:attachment.id
						}]
					});
				}
				$scope.fs=fs;
				$scope.outfile=outfile;
			}
			
		});
	}else{
		$scope.apply=apply;
	}
	//上传图片
	var uploadConfig = {
		fileSize:'200KB',	
		fileTypeExts:'*.jpg;*.jpge;*.gif;*.png',	
		isBenci:1,//本次上傳	
		url:baseUrl+'fileServer/upload?moduleCode=defect&companyId=1',// * 必填，上传路径
		//queueID:"queue", 
		buttonImg:basePath+'flat/images/upload.png',// * 必填，上传按钮的背景图片，各个项目的路径不同
		swf:basePath+'plugins/upload/uploadify.swf',// *必填 *flash文件的路径，各个项目的路径也不同
	}
	$scope.uploadConfig = uploadConfig;
	//根据缺陷等级，自动生成期望消缺时间
	$scope.changeGrade=function(){
		var newValue=$scope.apply.grade;
		if(newValue){
			var endTime = null;
			if(newValue == 1){
				$scope.apply.defectTimeStr = "24小时";
				endTime = defectService.addDate(new Date(),1);
			}else if(newValue == 2){
				$scope.apply.defectTimeStr = "7天";
				endTime = defectService.addDate(new Date(),7);
			}else if(newValue == 3){
				$scope.apply.defectTimeStr = "1个月";
				endTime = defectService.addDate(new Date(),30);
			}
			$("#defectTime").val(defectService.formatDateYYYYMMDD(endTime));
		}
	}
	//根据紧急程度，自动生成时间
	$scope.chageRadio=function(num){
		if(num){
			var date =  new Date();
			var endTime = null;
			if(num == 1){
				endTime=date.setTime(date.getTime()+2*1000*60*60);
			}else if(num == 2){
				endTime=date.setTime(date.getTime()+24*1000*60*60);
			}else if(num == 3){
				endTime=date.setTime(date.getTime()+7*24*1000*60*60);
			}
			
			$("#defectTime").val(defectService.formatDate(new Date(endTime)));
		}
	}
	
	
	
	if(!instanceId||instanceId=="null"){
		$scope.chageRadio(2);
		$scope.apply={};
		$scope.apply.grade=2;
	}
	$scope.submitNum = 0;//验证二次提交
	//保存信息
	$scope.addApply=function(isValid){
		$scope.submitNum += 1;
		if(!isValid){
			$scope.submitted=true;
			$scope.isSubmitted=true;
			$scope.submitNum = 0;
			return false;
		}
		if($("#userId").attr("id")){
			$scope.apply.createUserId = $("#userId").val()
			if(!$scope.apply.createUserId){
				art.dialog.alert("请输入上报人工号!");
				$scope.submitNum = 0;
				return false;
			}
		}
		$scope.apply.equipmentName = $("#maintenanceItems").val();
		if(!$scope.apply.equipmentName){
			art.dialog.alert("请选择设备名称!");
			$scope.submitNum = 0;
			return false;
		}
		var isSelectCapitalGroup = isExistCapitalGroup($scope.apply.equipmentName);
		if(!isSelectCapitalGroup){
			art.dialog.alert("设备名称不存在,请选择!");
			$scope.submitNum = 0;
			return false;
		}
		
		/*if(!$scope.capitalGroupId){
			art.dialog.alert("请选择设备名称!");
			$scope.submitNum = 0;
			return false;
		}*/
		$scope.apply.equipmentNumber = $("#equipmentNumber").val();
		if(!$scope.apply.equipmentNumber){
			art.dialog.alert("请输入设备数量!");
			$scope.submitNum = 0;
			return false;
		}
		
		$scope.apply.processId = $("#processId").val();
		if(!$scope.apply.processId){
			art.dialog.alert("请选择配送部门!");
			$scope.submitNum = 0;
			return false;
		}
		
		$scope.apply.createGroupId =  $("#createGroupId").val();
		if(!$scope.apply.createGroupId){
			art.dialog.alert("请选择上报科室!");
			$scope.submitNum = 0;
			return;
		}
		
		$scope.apply.processType =  $("#processType").val();
		var defectTime = $("#defectTime").val();
		if(!defectTime){
			art.dialog.alert("请选择配送完成时间!");
			$scope.submitNum = 0;
			return;
		}
		$scope.apply.defectTime = defectTime;
		
		$scope.apply.describe =  $("#describe").val();
		if(!$scope.apply.describe){
			art.dialog.alert("请输入或选择配送地点!");
			$scope.submitNum = 0;
			return;
		}
		
		/*var attachmentIds="";
		for(var i=0;i<$scope.outfile.length;i++){
			attachmentIds+=($scope.outfile)[i].fileData[0].id+",";
		}
		if(attachmentIds){
			attachmentIds=attachmentIds.substring(0,attachmentIds.length-1);
			$scope.apply.attachmentIds=attachmentIds;
		}*/
		//$scope.apply.grade=$("input[name='eventLevel']:checked").val();
		$scope.apply.equipmentUnit = 1;
		$scope.apply.defectProfessional = 1;
		$scope.apply.equipmentState = 1;
		$scope.apply.type=$("#type").val();
		$scope.apply.applyPhone=$("#applyPhone").val();
		if($scope.submitNum == 1){
			defectService.addApply($scope.apply,function(data){
				if(data==1){
					if($("#from").val()=='dispatch'){
						art.dialog.tips("上报成功!",1000);
						setTimeout(function(){window.parent.location.href = document.referrer;},1000);
					}else{
						//发送提醒,通知话务平台
						defectService.sendOnlineReminder({"type":(parseInt($("#type").val())+1),"_clientType":"wap"},function(result){});
						if(instanceId&&instanceId!='null'){
							window.location.href=basePath+"logined/defect/myStartList.jsp?type=apply";
						}else{
							window.parent.location.href=basePath+"logined/defect/myStartList.jsp?type=apply";
						}
					}
				}else if(data==2){
					art.dialog.alert("您还有未验收的工单,请验收后上报!");
					$scope.submitNum = 0;
				}else if(data==3){
					art.dialog.alert("配送申请已接收不可编辑");
					$scope.submitNum = 0;
				}else{
					art.dialog.alert("保存失败");
					$scope.submitNum = 0;
				}
			});
		}
		
	}
	
	
	/*$scope.userLocationName = function () {
		defectService.userLocationName({}, function(result){
			if(result!=null&&result!=""){
				$("#describe").val(result.pathName);
			}
		});
	}*/
	$scope.userLocationName = function (userId) {
		defectService.userLocationName({"userId":userId}, function(result){
			if(result!=null&&result!=""){
				$("#describe").val(result.pathName);
			}
		});
	}

	$scope.isExistPhone = false;
	$scope.getUserAndLocation = function (applyPhone) {
		defectService.getUserAndLocation({"workNo":applyPhone}, function(result){
			if(result!=null&&result!=""){
				if(result.isExist=="1"){
					$scope.isExistPhone = true;
				}
				$("#describe").val(result.pathName);
				$("#createGroupId").val(result.createGroupId);
				$("#createGroupName").val(result.createGroupName);
				$("#applyPhoneName").val(result.applyPhoneName);
			}
		});
	}
	var formStr = $("#from").val();
	if(formStr=="dispatch"){
		$scope.getUserAndLocation($("#applyPhone").val());
	}else{
		$scope.userLocationName(eventUserId);
	}
	
	$scope.dispatchRefresh = function () {
		if($scope.loginName.length>2){
			 $("#divselect_user"+" ul").show();
			 $("#userId").val("");
	        $("#dispatchUserName").text("--");
	        $("#dispatchGroupName").text("--");
	        var param = {
	            "loginName":$scope.loginName
	        };
	        defectService.findUserList(param, function(result){
	            $scope.userList = result;
	        });
		}
    }
	$scope.hideUserUrl = function () {
		$("#divselect_user"+" ul").hide();
	}
	//自定义下拉2
	$scope.selectUser = function (id,loginName,userName,groupId,groupName) {
		//if(!$scope.isExistPhone){
			$scope.userLocationName(id);
			$("#createGroupId").val(groupId);
			$("#createGroupName").val(groupName);
		//}
        $("#userId").val(id);
        $("#loginName").val(loginName);
        $("#dispatchUserName").text(userName);
        $("#dispatchGroupName").text(groupName);
        $("#divselect_user"+" ul").hide();
    }
}]);


function isExistCapitalGroup(capitalGroupName){
	var isExist = false;
	var url = basePath+"capital/capitalGroupList.action";
	$.ajax({
		url:url,
		type:'POST',
		data:{"capitalGroupName":capitalGroupName},
		dataType:'json',
		async:false,
		success:function(data){
			if(data!=null && data.length > 0){
				isExist = true;
			}
		}
	});
	return isExist;
}