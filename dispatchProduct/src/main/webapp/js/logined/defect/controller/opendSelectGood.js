$(function(){
	var instanceId=document.getElementById("instanceId").value;
	art.dialog.data("findGoodList",findGoodList);
	findGoodList(instanceId);
})
//添加物资
function addGoods(){
	var instanceId=document.getElementById("instanceId").value;
	art.dialog.data("instanceId",instanceId);
	var url=basePath+"logined/defect/goodAddView.jsp";
	var artWindow=art.dialog.open(url,{
		title : "选择物资",
		width : 1450,
		height : 500,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button : [{
			name : '确定',
			focus:true,
			callback : function() {
					var iframe = this.iframe.contentWindow;
					iframe.opened();
					return false;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
	});
  art.dialog.data("artWindow",artWindow);
}
		




/**
 * 获得选择的列表
 */
function findGoodList(instanceId){
	$.ajax({
		url:basePath+"defect/findSelectGoodList.action",
		type:"post",
		data:{"instanceId":instanceId},
		dataType:'json',
		success:function(data){
			var operType = $("#oper").val();
			var html="";
			if(data.length>0){
				for(var i=0;i<data.length;i++){
					html += '<tr><td width="20%">'+data[i].name+'</td><td width="15%">'+data[i].groupName+'</td><td width="15%">'+data[i].goodNo+'</td><td width="10%">'+data[i].type+'</td><td width="10%">'+data[i].price+'</td><td width="10%">'+data[i].num+'</td><td width="10%">'+data[i].money+'</td>';
					if(operType=="dispatch"||operType=="approve"||operType=="defect"){
						html += '<td width="10%"><a href="javascript:;" onClick="deleteGood('+data[i].id+',\''+instanceId+'\')" class="color_red">删除</a></td></tr>';
					}else{
						html += '<td width="10%">--</td></tr>';
					}
				}
			}else{
				html += '<tr><td colspan="8">您还没有选择物资</td></tr>';
			}
			$("#goodsTable").html(html);
		}
	})
}

/**
 * 删除操作
 */
function deleteGood(id,instanceId){
	$.ajax({
		url:basePath+"defect/delectSelectGoods.action",
		type:"post",
		data:{"id":id},
		dataType:'json',
		success:function(data){
			if(data==0){
				art.dialog.tips("删除成功！")
				findGoodList(instanceId);
			}else{
				art.dialog.alert("删除失败！")
			}
		}
	})
}