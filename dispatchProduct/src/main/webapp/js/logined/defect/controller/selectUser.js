$(function(){
	$("#userSelect").click(function(){
		selectUser(this);
	});
	$("#repaireSelect").click(function(){
		selectLocation(this);
	});
	
	$("#createGroupSelect").click(function(){
		selectCreateGroup(this);
	});
	
	var type=$("#type").val();
	findDepartment(type);
});
var userObj;
/**
 * 人员选择
 * REN
 **/
function selectUser(obj)
{
	var processType = $("#processType").val();
    var userId=$("#"+$(obj).attr("fieldId")).val();
   /* if(processType&&processType==2){
    	userId = "gid_"+userId;
    }*/
    var showType = $("#"+$(obj).attr("showType"));
    if(!showType){
    	showType = 3;
    }else{
    	showType = showType.val();
    }
    var extension =$("#"+$(obj).attr("extension"));
    if(!extension){
    	extension="";
    }else{
    	extension=extension.val();
    }
    userObj = obj;
    var moduleName = "";
    var index = $("#index").val();
    if(index&&(index==6||index==10)){
    	moduleName = "defect";
    }
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	showType:showType,
    	defaultSelectIds:userId,
    	extension:extension,
    	moduleName:moduleName,
    	callback:callBackUser
    });
}

/**
 * 人员选择
 * REN
 **/
function selectLocation(obj)
{
	var tempUserId = $("#userId").val();
	if(!tempUserId){
		tempUserId = "";
	}
    var userId=$("#"+$(obj).attr("fieldId")).val();
	/**userId = "gid_"+userId;*/
    userObj = obj;
    qytx.app.tree.alertLocationTree({
    	type:"radio",
    	showType:1,
    	defaultSelectIds:userId,
    	userId:tempUserId,
    	groupId:"",
    	callback:callBackUser
    });
}


/**
 * 人员选择
 * REN
 **/
function selectCreateGroup(obj)
{
	var processType = $("#processType").val();
    var userId=$("#"+$(obj).attr("fieldId")).val();
   /* if(processType&&processType==2){
    	userId = "gid_"+userId;
    }*/
    var showType = $("#"+$(obj).attr("showType"));
    if(!showType){
    	showType = 3;
    }else{
    	showType = showType.val();
    }
    var extension =$("#"+$(obj).attr("extension"));
    if(!extension){
    	extension="";
    }else{
    	extension=extension.val();
    }
    userObj = obj;
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	showType:showType,
    	defaultSelectIds:userId,
    	extension:extension,
    	callback:callBackCreateGroup
    });
}
function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    }
}

function callBackCreateGroup(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    	findLocation(userIds[0]);
    	
    }
}
/**
 * 获得原始部门
 */
function findDepartment(type){
	var param={
		"defaultDepartment.type":type
	}
	$.ajax({
		url:basePath+"defaultDepartmentSet/setDepartment_findDepartment.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			$("#processId").val(data.groupId);
	    	$("#processName").val(data.groupName);
		}
		
	})
}

/**
 * 获得原始部门
 */
function findLocation(groupId){
	
	var param={
		"groupId":groupId
	}
	$.ajax({
		url:basePath+"location/location_getUserLocationName.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			if(data!=null){
				$("#describe").val(data.pathName);
			}else{
				$("#describe").val("");
			}
		}
		
	})
}

