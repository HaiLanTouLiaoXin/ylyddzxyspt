app.controller("dispatchAddController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	var instanceId=document.getElementById("instanceId").value;
	$scope.instanceId = instanceId;
	//上报信息对象
	var apply={
			isRepair:1
	};
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"grade",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.gradeList=result;
	});
	if(instanceId&&instanceId!='null'){
		//获取页面详情信息
		defectService.getView({"instanceId":instanceId},function(result){
			$scope.apply=result.apply;
			$("#maintenanceItems").val(result.apply.equipmentName);
			$("#processId").val(result.apply.processId);
			$("#processName").val(result.apply.processName);
			$("#processType").val(result.apply.processType);
			$scope.changeGrade();
			if($scope.apply.processId&&$scope.apply.processName){
				$scope.defaultSelectUser="uid_"+$scope.apply.processId;
				$scope.node1=[{"obj":1,"id":$scope.defaultSelectUser,"name":$scope.apply.processName}];
			}
			if(result.attList){
				var fs=new Array();
				var outfile=new Array();
				for(var i=0;i<result.attList.length;i++){
					var attachment=result.attList[i];
					fs.push({
						name:attachment.attachName
					});
					outfile.push({
						fileData: [{
							id:attachment.id
						}]
					});
				}
				$scope.fs=fs;
				$scope.outfile=outfile;
			}
			
		});
	}else{
		$scope.apply=apply;
	}
	//上传图片
	var uploadConfig = {
		fileSize:'200KB',	
		fileTypeExts:'*.jpg;*.jpge;*.gif;*.png',	
		isBenci:1,//本次上傳	
		url:baseUrl+'fileServer/upload?moduleCode=defect&companyId=1',// * 必填，上传路径
		//queueID:"queue", 
		buttonImg:basePath+'flat/images/upload.png',// * 必填，上传按钮的背景图片，各个项目的路径不同
		swf:basePath+'plugins/upload/uploadify.swf',// *必填 *flash文件的路径，各个项目的路径也不同
	}
	$scope.uploadConfig = uploadConfig;
	
	//根据缺陷等级，自动生成期望消缺时间
	$scope.changeGrade=function(){
		var newValue=$scope.apply.grade;
		if(newValue){
			var endTime = null;
			if(newValue == 1){
				$scope.apply.defectTimeStr = "24小时";
				endTime = defectService.addDate(new Date(),1);
			}else if(newValue == 2){
				$scope.apply.defectTimeStr = "7天";
				endTime = defectService.addDate(new Date(),7);
			}else if(newValue == 3){
				$scope.apply.defectTimeStr = "1个月";
				endTime = defectService.addDate(new Date(),30);
			}
			$("#defectTime").val(defectService.formatDateYYYYMMDD(endTime));
		}
	}
	//根据紧急程度，自动生成时间
	$scope.chageRadio=function(num){
		if(num){
			var date =  new Date();
			var endTime = null;
			if(num == 1){
				endTime=date.setTime(date.getTime()+2*1000*60*60);
			}else if(num == 2){
				endTime=date.setTime(date.getTime()+24*1000*60*60);
			}else if(num == 3){
				endTime=date.setTime(date.getTime()+7*24*1000*60*60);
			}
			
			$("#defectTime").val(defectService.formatDate(new Date(endTime)));
		}
	}
	
	$scope.chageRadio(2);
	$scope.apply={};
	$scope.apply.grade=2;
	
	$scope.submitNum = 0;//验证二次提交
	
	//保存信息
	$scope.addApply=function(isValid){
		$scope.submitNum += 1;
		if(!isValid){
			$scope.submitted=true;
			$scope.isSubmitted=true;
			$scope.submitNum = 0;
			return false;
		}
		
		$scope.apply.createUserId = $("#userId").val()
		if(!$scope.apply.createUserId){
			art.dialog.alert("请输入上报人工号!");
			$scope.submitNum = 0;
			return false;
		}
		
		$scope.apply.processId = $("#processId").val();
		if(!$scope.apply.processId){
			art.dialog.alert("请选择维修部门或人员!");
			$scope.submitNum = 0;
			return false;
		}
		$scope.apply.processType =  $("#processType").val();
		var defectTime = $("#defectTime").val();
		if(!defectTime){
			art.dialog.alert("请选择维修时间!");
			$scope.submitNum = 0;
			return;
		}
		$scope.apply.defectTime = defectTime;
		
		$scope.apply.describe =  $("#describe").val();
		if(!$scope.apply.describe){
			art.dialog.alert("请输入或选择维修地点!");
			$scope.submitNum = 0;
			return;
		}
		
		var attachmentIds="";
		for(var i=0;i<$scope.outfile.length;i++){
			attachmentIds+=($scope.outfile)[i].fileData[0].id+",";
		}
		if(attachmentIds){
			attachmentIds=attachmentIds.substring(0,attachmentIds.length-1);
			$scope.apply.attachmentIds=attachmentIds;
		}
		//$scope.apply.grade=$("input[name='eventLevel']:checked").val();
		$scope.apply.equipmentUnit = 1;
		$scope.apply.defectProfessional = 1;
		$scope.apply.equipmentNumber = "0001";
		$scope.apply.equipmentState = 1;
		$scope.apply.type=0;
		if($scope.submitNum == 1){
			defectService.addApply($scope.apply,function(data){
				if(data==1){
					window.location.href=basePath+"logined/defect/myStartList.jsp";
				}else if(data==3){
					art.dialog.alert("上报申请已接收不可编辑");
					$scope.submitNum = 0;
				}else{
					art.dialog.alert("保存失败");
					$scope.submitNum = 0;
				}
			});
		}
		
	}
	
	$scope.refresh = function () {
		$("#divselect3"+" ul").show();
         var param = {
             "event.eventName":$scope.maintenanceItems
         };
         defectService.findEventList(param, function(result){
             $scope.eventList = result;
         });
     }
	$scope.hideLocationUrl = function () {
		$("#divselect3"+" ul").hide();
	}
	
	
	
	//自定义下拉2
	$scope.selectP = function (id,name,urgencyLevel,completeTimeType,groupId,groupName) {
        $("#maintenanceItems").val(name);
        $("#processId").val(groupId);
        $("#processName").val(groupName);
        $("input[type='radio'][value='"+urgencyLevel+"']").prop("checked",true);
        $("#divselect3"+" ul").hide();
        $scope.chageRadio(urgencyLevel);
    }
	
	
	
	
	$scope.dispatchRefresh = function () {
		 $("#divselect_user"+" ul").show();
		 $("#userId").val("");
         $("#dispatchUserName").text("--");
         $("#dispatchGroupName").text("--");
         var param = {
             "loginName":$scope.loginName
         };
         defectService.findUserList(param, function(result){
             $scope.userList = result;
         });
     }
	$scope.hideUserUrl = function () {
		$("#divselect_user"+" ul").hide();
	}
	
	
	//自定义下拉2
	$scope.selectUser = function (id,loginName,userName,groupId,groupName) {
		$scope.userLocationName(id);
        $("#userId").val(id);
        $("#loginName").val(loginName);
        $("#dispatchUserName").text(userName);
        $("#dispatchGroupName").text(groupName);
        $("#divselect_user"+" ul").hide();
    }
	
	$scope.userLocationName = function (userId) {
		defectService.userLocationName({"userId":userId}, function(result){
			if(result!=null&&result!=""){
				$("#describe").val(result.pathName);
			}
		});
	}
	
	
}]);