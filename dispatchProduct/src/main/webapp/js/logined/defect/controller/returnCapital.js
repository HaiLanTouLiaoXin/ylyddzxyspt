$(function(){
	getCapitalDetail();
});




var submitNum = 0;//验证二次提交
//保存信息
function returnCapital(){
	if(submitNum > 0 ){
		return false;
	}
	submitNum = 1;
	var equipmentName = $("#maintenanceItems").val();
	var equipmentNumber = $("#equipmentNumber").val();
	var processId = $("#processId").val();
	if(!processId){
		art.dialog.alert("请选择配送部门!");
		submitNum = 0;
		return false;
	}
	
	var createGroupId =  $("#createGroupId").val();
	/*if(!createGroupId){
		art.dialog.alert("请选择上报科室!");
		submitNum = 0;
		return;
	}*/
	var processType =  $("#processType").val();
	var describe =  $("#describe_temp").val();
	if(!describe){
		art.dialog.alert("地点不能为空!");
		submitNum = 0;
		return;
	}
	var grade=$("input[name='eventLevel']:checked").val();
	var endTime = "";
	if(grade == 1){
		endTime = addDate(new Date(),1);
	}else if(grade == 2){
		endTime = addDate(new Date(),7);
	}
	var defectTime = formatDateYYYYMMDD(endTime);
	var equipmentUnit = 1;
	var defectProfessional = 1;
	var equipmentState = 1;
	var type=$("#type").val();
	var applyPhone=$("#applyPhone").val();
	var memo=$("#memo").val();
	if(submitNum == 1){
		var url = basePath+"defect/returnCapital.action";
		var param = {
				"apply.processId":processId,
				"apply.processType":processType,
				"apply.grade":grade,
				"apply.isRepair":0,
				"apply.describe":describe,
				"apply.equipmentNumber":equipmentNumber,
				"apply.equipmentName":$("#maintenanceItems").val(),
				"apply.equipmentState":equipmentState,
				"apply.equipmentUnit":equipmentUnit,
				"apply.defectDepartment":1,
				"apply.defectProfessional":defectProfessional,
				"apply.defectTime":defectTime,
				"apply.memo":memo,
				"apply.type":type,
				"apply.applyPhone":applyPhone,
				"apply.createGroupId":createGroupId,
				"equipments":JSON.stringify(capitalArr)
			};
		var url = basePath+"defect/apply.action";
		$.ajax({
			url : url,
			type:'POST',
			data:param,
			dataType:'text',
			success:function(data){
				if(data==1){
					art.dialog.tips("操作成功!",1000);
					var getTable = art.dialog.data("getTable");
					getTable(1);
					art.dialog.close();
				}else if(data==2){
					art.dialog.alert("您还有未验收的工单,请验收后归还!");
					submitNum = 0;
				}else{
					art.dialog.alert("保存失败");
					submitNum = 0;
				}
			}
		})
	}
	
}

var capitalArr = [];

function getCapitalDetail(){
		var param={
				"capitalId":$("#capitalId").val()
		};
		$.ajax({
			url:basePath+"capital/getCapitalDetail.action",
			type:'post',
			data:param,
			dataType:'json',
			success:function(data){
				$("#maintenanceItemsText").html(data.name);
				$("#maintenanceItems").val(data.name);
				$("#describe_temp").val(data.storeLocation);
				capitalArr.push(data.id);
			}
		})
	}


function addDate(beginDate,addNums){
	var m = beginDate.valueOf();
	m+=addNums*24*60*60*1000;
	return new Date(m);
}

function formatDateYYYYMMDD(d){
	var year = d.getFullYear();
	var month = d.getMonth()+1;
	if(month<10){
		month = "0"+month;
	}
	var date = d.getDate();
	if(date<10){
		date = "0"+date;
	}
	return year+"-"+month+"-"+date;
}