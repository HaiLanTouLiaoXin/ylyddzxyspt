app.factory("defectService",["$http",function($http){
	return {
		addDate:function(beginDate,addNums){
			var m = beginDate.valueOf();
			m+=addNums*24*60*60*1000;
			return new Date(m);
		},
		//格式化时间
		formatDate:function(d){
			var year = d.getFullYear();
			var month = d.getMonth()+1;
			var date = d.getDate();
			var hour = d.getHours();
			var minute = d.getMinutes();
			var second = d.getSeconds();
			if(month<10){
				month = "0"+month;
			}
			if(date<10){
				date = "0"+date;
			}
			if(hour<10){
				hour = "0"+hour;
			}
			if(minute<10){
				minute = "0"+minute;
			}
			return year+"-"+month+"-"+date+" "+hour+":"+minute+":00";
		},
		formatDateYYYYMMDD:function(d){
			var year = d.getFullYear();
			var month = d.getMonth()+1;
			if(month<10){
				month = "0"+month;
			}
			var date = d.getDate();
			if(date<10){
				date = "0"+date;
			}
			return year+"-"+month+"-"+date;
		},
		//选择人员
		getSelectUser:function(param, callback){
			var showType = param.showUser?3:2;
			var search = param.search?'':param.search;
			var type = param.search?5:1;
			$http.get(basePath+"user/selectUser.action?searchName="+search+"&showType="+showType+"&type="+type)
			.success(function(result) {
				callback(result);
			});
		},
		//保存上报信息
		addApply:function(apply,callback){
			var param = {
					"apply.instanceId":apply.instanceId,
					"apply.processId":apply.processId,
					"apply.processName":apply.processName,
					"apply.processType":apply.processType,
					"apply.grade":apply.grade,
					"apply.isRepair":apply.isRepair,
					"apply.describe":apply.describe,
					"apply.equipmentNumber":apply.equipmentNumber,
					"apply.equipmentName":$("#maintenanceItems").val(),
					"apply.equipmentState":apply.equipmentState,
					"apply.equipmentUnit":apply.equipmentUnit,
					"apply.defectDepartment":1,
					"apply.defectProfessional":apply.defectProfessional,
					"apply.defectTime":apply.defectTime,
					"apply.attachmentIds":apply.attachmentIds,
					"apply.memo":apply.memo,
					"apply.createUserId":apply.createUserId,
					"apply.type":apply.type,
					"apply.applyPhone":apply.applyPhone,
					"apply.createGroupId":apply.createGroupId,
					"apply.applyPhoneName":$("#applyPhoneName").val()
				};
			$http.post(basePath+"defect/apply.action",param)
				.success(function(result){
					callback(result);
				});
		},
		//获取我上报的列表
		getMyStartList:function(params,callback){
			$http.post(basePath+"defect/myStarted.action",params)
			.success(function(result){
				callback(result);
			});
		},
		getMydList:function(params,callback){
			$http.post(basePath+"defect/mydList.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取事件列表
		findEventList:function(params,callback){
			$http.post(basePath+"event/event_eventList.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取资产组列表
		findCapitalGroupList:function(params,callback){
			$http.post(basePath+"capital/capitalGroupList.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取用户列表
		findUserList:function(params,callback){
			$http.post(basePath+"event/event_userList.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取当前人默认地点
		userLocationName:function(params,callback){
			$http.post(basePath+"location/location_getUserLocationName.action",params)
			.success(function(result){
				callback(result);
			});
		},//获取用户和地址
		getUserAndLocation:function(params,callback){
			$http.post(basePath+"location/location_getUserInfoByPhone.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取当前事件上报人信息
		getApplyUser:function(params,callback){
			$http.post(basePath+"defect/getApplyUser.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取待我处理的列表
		getMyWaitList:function(params,callback){
			$http.post(basePath+"defect/myWaitProcess.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//获取我已处理的列表
		getMyProcessList:function(params,callback){
			$http.post(basePath+"defect/myProcessed.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//详情
		getView:function(params,callback){
			$http.post(basePath+"defect/view.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//删除
		deleteDefect:function(params,callback){
			$http.post(basePath+"defect/delete.action",params)
			.success(function(result){
				callback(result);
			});
		},
		//处理
		approve:function(params,callback){
			$http.post(basePath+"defect/approve.action",params)
			.success(function(result){
				callback(result);
			});
		},
		// 查询数据字典
		getDicts:function(param,callback){
			$http.post(basePath+"dict/getDicts.action", param)
				.success(function(result){
					callback(result);
				})
		},
		//待处理数量
		getUnHandleCount:function(param,callback){
			$http.post(basePath+"defect/getUnHandleCount.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//图表
		getReport:function(param,callback){
			$http.post(basePath+"defect/report.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//值班信息
		getDutyList:function(param,callback){
			$http.post(basePath+"duty/getDutyList.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//查询今日值班
		todayWorkDuty:function(param,callback){
			$http.post(basePath+"duty/todayWorkDuty.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//签到
		saveWorkDuty:function(param,callback){
			$http.post(basePath+"duty/saveWorkDuty.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//签退
		deleteWorkDuty:function(param,callback){
			$http.post(basePath+"duty/deleteWorkDuty.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//最新公告
		viewList:function(param,callback){
			$http.post(basePath+"notify/notify_viewList.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//催单
		pushReminder:function(param,callback){
			$http.post(basePath+"defect/pushReminder.action", param)
			.success(function(result){
				callback(result);
			})
		},
		//发送提醒,通知话务平台
		sendOnlineReminder:function(param,callback){
			$http.post(zxyyPath+"/onlineReminer/sendOnlineReminder.action", param)
			.success(function(result){
				callback(result);
			})
		},
		updateEvaluate:function(param,callback){
			$http.post(basePath+"/defect/updateEvaluate.action", param)
			.success(function(result){
				callback(result);
			})
		}
	};
}]);