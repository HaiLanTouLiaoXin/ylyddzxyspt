$(document).ready(function() {
	getGroupTree();
	
	getEventTypeTree();
	
	$("#groupSel").click(function() {
		showGroup();
		return false;
	});
	
	

    $("#groupSel").keydown(function(e) {
        if(e.which==8){  
            return false;       
        }
    });
    
    $("#groupType").click(function() {
    	showEventTypeGroup();
		return false;
	});
    $("#groupType").keydown(function(e) {
        if(e.which==8){  
            return false;       
        }
    });

});

/**
 * 群组树调用群组树
 */
var showOrHide = true;
function showGroup(){
	$('#menuContent').toggle(showOrHide);
	//相当于
	if (showOrHide) {
		showOrHide=false;
		var groupObj = $("#groupSel");
		var groupOffset = $("#groupSel").position();
		$("#menuContent").css({left:groupOffset.left + "px", top:groupOffset.top + groupObj.outerHeight() - 1 + "px"}).show();
		$("#treeContent").one("mouseleave",function(){
			$("#menuContent").hide();
			showOrHide=true;
			return false;
        });
	} else {
	   $("#menuContent").hide();
	   showOrHide=true;
	}
}

var  isShow =true;
function showEventTypeGroup(){
	$('#menuDiv').toggle(isShow);
	//相当于
	if (isShow) {
		isShow=false;
		var groupObj = $("#groupType");
		var groupOffset = $("#groupType").position();
		$("#menuDiv").css({left:groupOffset.left + "px", top:groupOffset.top + groupObj.outerHeight() - 1 + "px"}).show();
		$("#treeDiv").one("mouseleave",function(){
			$("#menuDiv").hide();
			isShow=true;
			return false;
        });
	} else {
	   $("#menuDiv").hide();
	   isShow=true;
	}
}





/**
 * 获取群组的树结构
 */
function getGroupTree() {
	// 设置树样式
	// 开始隐藏
	$("#menuContent").hide();
	$("#menuDiv").hide();
    // 组织机构树参数设置
    var setting = {
        view: {dblClickExpand: false},
        data: { simpleData: {enable: true }},
        callback: {onClick: onTreeNodeClick}
    };
    var dataParam = {
            'type' : 1,
            'showType' :1
	};
    qytx.app.ajax({
		url : basePath+ "user/selectUser.action",
		type : 'post',
		dataType :'json',
		data:dataParam,
		success : function(data) {
            $.fn.zTree.init($("#groupTree"), setting, data);
			//选择部门
		}
	});
}  
    
    /**
     * 获取群组的树结构
     */
    function getEventTypeTree() {
    	// 设置树样式
    	// 开始隐藏
    	$("#menuDiv").hide();
        // 组织机构树参数设置
        var setting = {
            view: {dblClickExpand: false},
            data: { simpleData: {enable: true }},
            callback: {onClick: onTreeNodeClickEvent}
        };
        var param={
        	'treeType':1	
        };
        qytx.app.ajax({
    		url : basePath+"event/eventType_eventTypeTreeList.action",
    		type : 'post',
    		dataType :'json',
    		data:param,
    		success : function(data) {
                $.fn.zTree.init($("#eventTypeTree"), setting, data);
    			//选择部门
    		}
    	});
    
    }
    
    
    
    
	/**
	 * 群组树调用点击事件
	 */
	function onTreeNodeClick(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("groupTree");
		var nodes = zTree.getSelectedNodes();

		if (nodes.length > 0) {
			var groupName = nodes[nodes.length - 1].name;
			var groupId = nodes[nodes.length - 1].id;
			var parentId =groupId.substring(4, groupId.length);
			if(parentId==0){
				art.dialog.alert("只能选择部门！");
				return;
			}
			$("#parentId").val(groupId.substring(4, groupId.length));
			$("#groupSel").val(groupName);
			$("#menuContent").hide();
		}
		//根据部门名称得到当前节点
        var groupId=$("#groupId").val();
        var parentId=$("#parentId").val();
        if(parentId){
            if(groupId==parentId){
                $("#parentId").val("");
                $("#groupSel").val("");
                qytx.app.dialog.alert("不能选择本部门作为上级部门！");
                return;
            }
        }

        
        var currentNode=zTree.getNodesByParam("id","gid_"+groupId, null);
        if(currentNode.length>0){
            var childrenArr=getAllChildrenNodes(currentNode[0],[]);
            //不能选择子节点
            if($.inArray(parentId, childrenArr)!=-1){
                $("#parentId").val("");
                $("#groupSel").val("");
                qytx.app.dialog.alert("不能选择子部门作为上级部门！");
                return;
            }
        }
	}
    /**
     * 得到所有子节点
     */
    function getAllChildrenNodes(treeNode,result) {
        if (treeNode.isParent) {
            var childrenNodes = treeNode.children;
            if (childrenNodes) {
                for (var i = 0; i < childrenNodes.length; i++) {
                    result.push((childrenNodes[i].id).substring(4));
                    result = getAllChildrenNodes(childrenNodes[i], result);
                }
            }
        }
        return result;
    }


    
    
    /**
	 * 群组树调用点击事件
	 */
	function onTreeNodeClickEvent(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("eventTypeTree");
		var nodes = zTree.getSelectedNodes();

		if (nodes.length > 0) {
			var groupName = nodes[nodes.length - 1].name;
			var groupId = nodes[nodes.length - 1].id;
			var parentId =groupId.substring(4, groupId.length);
			if(parentId==0){
				art.dialog.alert("只能选择类别！");
				return;
			}
			$("#eventType").val(groupId.substring(4, groupId.length));
			$("#groupType").val(groupName);
			$("#menuDiv").hide();
		}
		//根据部门名称得到当前节点
        var groupId=$("#groupId").val();
        var parentId=$("#eventType").val();
        if(parentId){
            if(groupId==parentId){
                $("#eventType").val("");
                $("#groupType").val("");
                qytx.app.dialog.alert("不能选择本类别作为上级类别！");
                return;
            }
        }

        
        var currentNode=zTree.getNodesByParam("id","gid_"+groupId, null);
        if(currentNode.length>0){
            var childrenArr=getAllChildrenNodes(currentNode[0],[]);
            //不能选择子节点
            if($.inArray(parentId, childrenArr)!=-1){
                $("#eventType").val("");
                $("#groupType").val("");
                qytx.app.dialog.alert("不能选择子部门作为上级部门！");
                return;
            }
        }
	}  
