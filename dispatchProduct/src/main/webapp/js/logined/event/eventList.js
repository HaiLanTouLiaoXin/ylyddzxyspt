/**
 * @author REN

 */
var pd=0;
var _eventCount=0;
$(document).ready(function() {
	
	$("#importEvent").click(function(){
        uploadEvent();
        return false;
    });
	
	// 清除cookie中的分页信息
	$.removeTableCookie('SpryMedia_DataTables_eventTable_eventList.jsp');
	// 获取人员信息列表
	getInfo();
	$("#searchButton").click(function(){
		$.removeTableCookie('SpryMedia_DataTables_eventTable_eventList.jsp');
		pd=1;
		_checkedIds="";
		getInfo();
	});
	
	// 头部全选复选框
	$("#eventTable").delegate("#total", "click", function(event) {
				checkTotal();
				event.stopPropagation();
			});
	// 第一列复选按钮
	$("#eventTable").delegate("input:checkbox[name='chk']", "click",
			function(event) {
				check();
				event.stopPropagation();
			});

	// 单击导出
	$("#eventExport").click(function() {
		var eventName= $.trim($("#searchName").val())?$.trim($("#searchName").val()):"";
		var eventType =$.trim($("#eventType").val())?$.trim($("#eventType").val()):"";
	    var data="";
		 data +="&event.eventName="+eventName;
		 data +="&event.eventType="+eventType;
		 data=encodeURI(encodeURI(data));
		 document.location.href=basePath + "event/event_exportEvent.action?" + data;
		return false;
		 
	});
	
	// 捕获回车事件
    $("html").die().live("keydown", function(event) {
        if (event.keyCode == 13) {
        	$.removeTableCookie('SpryMedia_DataTables_eventTable_eventList.jsp');
        	pd=1;
    		// 获取人员信息列表
    		_checkedIds="";	
    		getInfo();
        }
    });
});

/**
 * 导入
 */
function uploadEvent() {
	art.dialog.data("getInfo",getInfo);
	var eventType=$("#eventType").val();
	if(!eventType||eventType==0){
		art.dialog.alert("请选择服务项目分类");
		return;
	}
	art.dialog.open(basePath +"logined/event/importPage.jsp?eventType="+eventType,{
				id : 'importEvent',
				title : '事件导入',
				lock :true,
				opacity: 0.08, 
				width : 600,
				height : 300,
				button : [{
					name : '导 入',
					callback : function() {
						var iframe = this.iframe.contentWindow;
						iframe.startAjaxFileUpload();
						return false;
					},
					focus:true
				},
				{
					name : '取 消',
					callback : function() {
						return true; 
					}
				}]
			});

}



function getInfo(){
	grid();
}
var grid=function(){
	qytx.app.grid({
		id:'eventTable',
		url:basePath + "event/event_eventPage.action",
		selectParam:{
                'event.eventName':$.trim($("#searchName").val()),
                'event.eventType':$.trim($("#eventType").val())
				},
		valuesFn:[{
			"aTargets" : [0],
			"fnRender" : function(oObj) {
				return '<input name="chk" value="'+ oObj.aData.eventId+'" type="checkbox" />';
			 }},

			{
			"aTargets" : [6],
			"fnRender" : function(oObj) {
				var managerId = oObj.aData.eventId;
				  return "<a href='#' class='view_url' onclick='toAddOrUpdate("+managerId+")'>修改</a>";
				}
		}]
});
}

/**
 * 头部全选记录
 */
function checkTotal() {
	var isTotalChecked = $("input:checkbox[id='total']").prop("checked");
	var listCheckbox = $("input:checkbox[name='chk']");
	if (isTotalChecked) {
		listCheckbox.prop("checked", function(i, val) {
					if (!$(this).prop("disabled")) {
						return true;
					}
				});
	} else {
		listCheckbox.prop("checked", false);
	}
}
/**
 * 选择记录
 */
function check() {
	var checkTotalNum = $("input:checkbox[name='chk']");
	var checkNum = 0;
	var checkNumAll = true;
	checkTotalNum.each(function(i, val) {
				if ($(checkTotalNum[i]).prop("checked")) {
					checkNum++;
				} else {
					checkNumAll = false;
				}
			});
	if (!checkNumAll) {
		$("#total").prop("checked", false);
	} else {
		$("#total").prop("checked", true);
	}
}
/**
 * 删除
 */
function deleteEvent() {
	// 获取选择管理员id
	var chkbox = document.getElementsByName("chk");
	var eventIds = _checkedIds;
	var selLen = _checkedIds.length;
	if (selLen <1) {
		qytx.app.dialog.alert("请至少选择一项");
		return;
	}
	var groupId=$("#eventType").val();
	var paramData = {
		'eventIds' : eventIds
	};
	// 删除管理员
	qytx.app.dialog.confirm('确定要删除吗？', function() {
		    $.ajax({
		    	url : basePath + "event/event_deleteEvent.action",
				type : "post",
				data : paramData,
				success : function(data) {
					if (data == "success") {
							window.parent.refreshTree("gid_" + $("#eventType").val());
							_checkedIds="";
							getInfo();
					}else{
						art.dialog.alert("操作失败");
					}
				}
		    })
	});
}

/**
 * 新增或修改
 */
function toAddOrUpdate(id){
	art.dialog.data("grid",grid);
	var eventType=$("#eventType").val();
	var url;
	if(id){
		url = basePath+"logined/event/addOrUpdateEvent.jsp?eventId="+id;
	}else{
		if(eventType==0){
			art.dialog.alert("请选择具体分类！");
			return;
		}
		url = basePath+"logined/event/addOrUpdateEvent.jsp?eventType="+eventType;
	}
	art.dialog.open(  url,{
		title : "新增事件",
		width : 480,
		height : 300,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button :[{
			name : '确定',
			focus:true,
			callback : function() {
				var iframe = this.iframe.contentWindow;
				iframe.addOrUpdateEvent();
				return false;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
}, false);
}


//删除
function delLocation(){
	var groupId = $("#id").val();
	if(groupId!=undefined&&groupId!=""){
		art.dialog.confirm("确定要删除吗?",function(){
			param = {"location.id":groupId};
			$.ajax({
				url:basePath+"location/location_deleteLocation.action",
				type:"POST",
				data:param,
				dataType:"text",
				success:function(result){
					if(result=="0"){
						art.dialog.tips("删除成功!");
						$("#id").val("");
						$(".formPage").hide();
						$("#noData").show();
						getLocationTree("");
					}else if(result=="1"){
						art.dialog.alert("该地点下存在子地点,不能删除!");
					}else{
						art.dialog.alert("操作失败,请稍后重试!");
					}
				}
			});
		});
	}else{
		art.dialog.alert("请选择要删除的地点!");
	}
};