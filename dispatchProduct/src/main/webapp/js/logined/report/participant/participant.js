$(document).ready(function() {
	findParticipantList();
	//查询列表
	$("#search").click(function(){
		findParticipantList();
	});
	

});
    

//导出
function export_list(){
	var beginTime=$("#begDate").val();
	var endTime=$("#endDate").val();
    var url = basePath + "report/participant_exportReport.action?beginTime="+beginTime+"&endTime="+endTime;	
	url=encodeURI(url);
	window.open(url);
}

/**
 * 
 * 获得视图
 */
function findParticipantList(){
	var param={
			'beginTime':$("#begDate").val(),
            'endTime':$("#endDate").val()	
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/participant_findParticipantReport.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].userName+"</td>";
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].total+"</td>";
						html+="<td>"+data[i].completeNum+"</td>";
						html+="<td>"+data[i].noCompleteNum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='6'>暂无数据</td></tr>";
				}
				$("#participantList").html(html);
			
		}
	});
}
