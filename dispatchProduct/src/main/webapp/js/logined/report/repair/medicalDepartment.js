$(document).ready(function() {
	findRepairMedicalDepartment();
	//查询列表
	$("#search_medicalDepartment").click(function(){
		findRepairMedicalDepartment();
		
	});
	//导出
	$("#report_medicalDepartment").click(
			function(){
				report_MedicalDepartment();
			}
	);
});
//导出
function report_MedicalDepartment(){
	var beginTime=$("#begDate_medicalDepartment").val();
	var endTime=$("#endDate_medicalDepartment").val();
    var url = basePath + "report/medicalDepartment_exportMedicalDepartmen.action?beginTime="+beginTime+"&endTime="+endTime;	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findRepairMedicalDepartment(){
	var param={
			'beginTime':$("#begDate_medicalDepartment").val(),
            'endTime':$("#endDate_medicalDepartment").val(),
            'departmentName':$("#department_name").val()
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/medicalDepartment_findMedicalDepartmenList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].reportNum+"</td>";
						html+="<td>"+data[i].finishNum+"</td>";
						html+="<td>"+(data[i].noFinishNum<0?0:data[i].noFinishNum)+"</td>";
						html+="<td>"+data[i].longTime+"</td>";
						html+="<td>"+data[i].workTime+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='6'>暂无数据</td></tr>";
				}
				$("#medicalDepartmentList").html(html);
		}
	});
}


