
$(function(){
	var beginTimeStr = getNowDate();
    var endTimeStr = getNowDate();
    //初始化各页面的时间为当前系统时间
    $("#begDate_topTen").val(beginTimeStr);
    $("#endDate_topTen").val(endTimeStr);
    $("#begDate_EventType").val(beginTimeStr);
    $("#endDate_EventType").val(endTimeStr);
    $("#begDate_Satisfaction").val(beginTimeStr);
    $("#endDate_Satisfaction").val(endTimeStr);
    $("#begDate_Department").val(beginTimeStr);
    $("#endDate_Department").val(endTimeStr);
    $("#begDate_medicalDepartment").val(beginTimeStr);
    $("#endDate_medicalDepartment").val(endTimeStr);
    $("#begDate_repairPeople").val(beginTimeStr);
    $("#endDate_repairPeople").val(endTimeStr);
    
    //设置标签的active属性
	$(".canverse_right_tap").children("span").removeClass("active");
	$("#callLogDetailSpan").addClass("active");
	//默认显示第一个标签
	$(".canverse_tap_area .canverse_tap_box").eq(0).removeClass('hide');
	//点击事件
	$(".canverse_right_tap span").click(function(){
		$(this).addClass('active');
		$(this).siblings('span').removeClass('active');
		 index = $(this).index();
		$(".canverse_tap_area .canverse_tap_box").eq(index).removeClass('hide');
		$(".canverse_tap_area .canverse_tap_box").eq(index).siblings('.canverse_tap_box').addClass('hide');
	    $(".IllegalsNum").addClass("hide");
		 $(".list_IllegalsNum").removeClass("hide");
		 $("#showList_IllNum").addClass("hide");
		 $("#showView_IllNum").removeClass("hide");
		 $("#showList_Satisfaction").addClass("hide");
		 $("#showView_Satisfaction").removeClass("hide");
		 $("#showList_repairTopTen").addClass("hide");
		 $("#showView_repairTopTen").removeClass("hide");
	});
     	
});


var index;

//获取当前日期
function getNowDate() {
	var now = new Date();
	return getFormatDate(now);
}
//时间格式转换
function getFormatDate(srcdate) {

	var formatDate = "";
	// 初始化时间
	var Year = srcdate.getFullYear();// ie火狐下都可以
	var Month = srcdate.getMonth() + 1;
	formatDate += Year;
	if (Month >= 10) {
		formatDate += "-" + Month;
	} else {
		formatDate += "-0" + Month;
	}

	var Day = srcdate.getDate();
	if (Day >= 10) {
		formatDate += "-" + Day;
	} else {
		formatDate += "-0" + Day;
	}
	return formatDate;
}


