$(document).ready(function() {
	findRepairTopTen(1);
	//查询列表
	$("#search_repairTopTen").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			findRepairTopTen(1);
		}else{
			findRepairTopTen(2);
		}
	});
	//导出
	$("#report_repairTopTen").click(
			function(){
				export_RepairTopTen();
			}
	);
	//查看视图
	$("#showView_repairTopTen").click(
			function(){
				if($("#showList_repairTopTen").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_repairTopTen").removeClass("hide");
					$("#showView_repairTopTen").addClass("hide");
					findRepairTopTen(2);
				}
			});
	//查看列表
	$("#showList_repairTopTen").click(
			function(){
				if($("#showView_repairTopTen").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_repairTopTen").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_repairTopTen").removeClass("hide");
					findRepairTopTen(1);
				}
});
    
});
//导出
function export_RepairTopTen(){
	var beginTime=$("#begDate_topTen").val();
	var endTime=$("#endDate_topTen").val();
    var url = basePath + "report/repair_exportRepairTopTen.action?beginTime="+beginTime+"&endTime="+endTime;	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findRepairTopTen(type){
	var param={
			'beginTime':$("#begDate_topTen").val(),
            'endTime':$("#endDate_topTen").val()	
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/repair_findRepairTopTen.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			if(type==1){
				var html="";
				if(data.aData.length>0){
					for(var i=0;i<data.aData.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data.aData[i].no+"</td>" ;
						html+="<td>"+data.aData[i].name+"</td>";
						html+="<td>"+data.aData[i].num+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='3'>暂无数据</td></tr>";
				}
				$("#repairTopTenList").html(html);
			}else{
				eachartRepairTopTen(data);
			}
		}
	});
}
//加载柱状图
function eachartRepairTopTen(data){
	 var myChart = echarts.init(document.getElementById("main_RepairTopTen"));
	option = {
			tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        }
		    },
		    grid: { 
		        x: 30,
		        x2: 10,
		        y2: 80,
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : data.nameArray
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		        {
		            name:'维修事项',
		            type:'bar',
		            data:data.numArray
		        }
		    ]
		};
	
	 myChart.setOption(option);
}

