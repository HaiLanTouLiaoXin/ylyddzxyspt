$(document).ready(function() {
	findequipmentUseList();
	//查询列表
	$("#search_equipmentUse").click(function(){
		findequipmentUseList()
	});
	//导出
	$("#export_equipmentUse").click(
			function(){
				export_equipmentUse();
			}
	);

    
});
//导出
function export_equipmentUse(){
	var beginTime=$("#begDate_equipmentUse").val();
	var endTime=$("#endDate_equipmentUse").val();
    var url = basePath + "report/equipment_exportReportEquipmentUseList.action?beginTime="+beginTime+"&endTime="+endTime;	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findequipmentUseList(){
	var param={
			'beginTime':$("#begDate_equipmentUse").val(),
            'endTime':$("#endDate_equipmentUse").val()
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/equipment_findCapitalUseList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].capitalGroupName+"</td>";
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].capitalNum+"</td>";
						html+="<td>"+data[i].useNum+"</td>";
						html+="<td>"+data[i].returnNum+"</td>";
						html+="<td>"+data[i].timeLong+"</td>";
						html+="<td>"+data[i].avgTime+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='8'>暂无数据</td></tr>";
				}
				$("#equipmentUseList").html(html);
		}
	});
}
