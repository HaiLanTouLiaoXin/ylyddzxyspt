$(function(){
	var beginTimeStr = getNowDate();
    var endTimeStr = getNowDate();
    //初始化各页面的时间为当前系统时间
    $("#begDate_EventType").val(beginTimeStr);
    $("#endDate_EventType").val(endTimeStr);
    $("#begDate_equipmentReturn").val(beginTimeStr);
    $("#endDate_equipmentReturn").val(endTimeStr);
    $("#begDate_equipmentUse").val(beginTimeStr);
    $("#endDate_equipmentUse").val(endTimeStr);
    
    //设置标签的active属性
	$(".canverse_right_tap").children("span").removeClass("active");
	$("#callLogDetailSpan").addClass("active");
	//默认显示第一个标签
	$(".canverse_tap_area .canverse_tap_box").eq(0).removeClass('hide');
	//点击事件
	$(".canverse_right_tap span").click(function(){
		$(this).addClass('active');
		$(this).siblings('span').removeClass('active');
		 index = $(this).index();
		$(".canverse_tap_area .canverse_tap_box").eq(index).removeClass('hide');
		$(".canverse_tap_area .canverse_tap_box").eq(index).siblings('.canverse_tap_box').addClass('hide');
		$(".IllegalsNum").addClass("hide");
		$(".list_IllegalsNum").removeClass("hide");
	});
	$(document).keydown(function(event){
		var code=event.which;
		if(index==0){
			if (code == 13) {
				getDataTable_Squadron();//要触发的方法
	        }
		}
		//违法号码分类
		if(index==1){
			if (code == 13) {
				getTable();//要触发的方法
	        }
		}
		if(index==2){
			if (code == 13) {
				getDataTable_staffsWork();//要触发的方法
	        }
		}
		//质检员工作量
		if(index==3){
			if (code == 13) {
				getDataTable_Inspectors();//要触发的方法
	        }
		}
		//追呼号码统计
		if(index==4){
			if (code == 13) {
				getDataTable_callNum();//要触发的方法
	        }
		}
	});
     	
});


var index;

//获取当前日期
function getNowDate() {
	var now = new Date();
	return getFormatDate(now);
}
//时间格式转换
function getFormatDate(srcdate) {

	var formatDate = "";
	// 初始化时间
	var Year = srcdate.getFullYear();// ie火狐下都可以
	var Month = srcdate.getMonth() + 1;
	formatDate += Year;
	if (Month >= 10) {
		formatDate += "-" + Month;
	} else {
		formatDate += "-0" + Month;
	}

	var Day = srcdate.getDate();
	if (Day >= 10) {
		formatDate += "-" + Day;
	} else {
		formatDate += "-0" + Day;
	}
	return formatDate;
}


