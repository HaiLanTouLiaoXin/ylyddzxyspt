$(document).ready(function() {
	getTable();
	//查询列表
	$("#search_IllegalsNum").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			getTable();
		}else{
			findChart();
		}
	});
	//导出
	$("#report_IllegalsNum").click(
			function(){
				report_export();
			}
	);
	//查看视图
	$("#showView_IllNum").click(
			function(){
				if($("#showList_IllNum").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_IllNum").removeClass("hide");
					$("#showView_IllNum").addClass("hide");
					findChart();
				}
			});
	//查看列表
	$("#showList_IllNum").click(
			function(){
				if($("#showView_IllNum").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_IllNum").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_IllNum").removeClass("hide");
					getTable();
				}
});
    
});
//导出
function report_export(){
	var beginTime=$("#begDate_Department").val();
	var endTime=$("#endDate_Department").val();
    var url = basePath + "report/send_export.action?beginTime="+beginTime+"&endTime="+endTime+"&type=1";	
	url=encodeURI(url);
	window.open(url);
}
var getTable=function(){
	var param={
			'beginTime':$("#begDate_Department").val(),
            'endTime':$("#endDate_Department").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/send_findGroupSend.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].sumTakeWorkNum+"</td>" ;
						html+="<td>"+data[i].sumNum+"</td>";
						html+="<td>"+data[i].finish+"</td>";
						html+="<td>"+data[i].equipmentNumber+"</td>";
						html+="<td>"+data[i].noFinish+"</td>";
						html+="<td>"+data[i].finishPer+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='8'>暂无数据</td></tr>";
				}
				$("#teamWorkList").html(html);
		}
	});
}
/**
 * 获得视图
 */
function findChart(){
	var param={
			'beginTime':$("#begDate_Department").val(),
            'endTime':$("#endDate_Department").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/send_findGroupChart.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			eachart(data);
		}
	});
}
//加载柱状图
function eachart(data){
	 var myChart = echarts.init(document.getElementById("main"));
	option = {
			tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        }
		    },
		    legend: {
		        data:data.completeName
		    },
		    grid: { 
		        x: 30,
		        x2: 10,
		        y2: 80,
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : data.name
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [{
                name:'应接工量',
                type:'bar',
                data:data.takeWorkArray
            },
		        {
		            name:'接工量',
		            type:'bar',
		            data:data.sumArray
		        },
		        {
		            name:'完工量',
		            type:'bar',
		            data:data.finishArray
		        },{
		            name:'未完工量',
		            type:'bar',
		            data:data.noFinishArray
		        }
		    ]
		};
	
	 myChart.setOption(option);
}

