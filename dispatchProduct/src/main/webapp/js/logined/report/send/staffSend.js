$(document).ready(function() {
	    findStaffSend();
	//查询列表
	$("#search_repairPeople").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			findStaffSend();
		}else{
			findCharts();
		}
		
	});
	//导出
	$("#report_repairPeople").click(
			function(){
				report_repairPeople();
			}
	);
	//查看视图
	$("#showView_staffSend").click(
			function(){
				if($("#showList_staffSend").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_staffSend").removeClass("hide");
					$("#showView_staffSend").addClass("hide");
					findCharts();
				}
			});
	//查看列表
	$("#showList_staffSend").click(
			function(){
				if($("#showView_staffSend").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_staffSend").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_staffSend").removeClass("hide");
					findStaffSend();
				}
	});
	
});
//导出
function report_repairPeople(){
	var beginTime=$("#begDate_repairPeople").val()?$("#begDate_repairPeople").val():"";
	var endTime=$("#endDate_repairPeople").val()?$("#endDate_repairPeople").val():"";
	var userName=$("#employee").val()?$("#employee").val():"";
	var departmentName=$("#department").val()?$("#department").val():"";
    var url = basePath + "report/staff_export.action?beginTime="+beginTime+"&endTime="+endTime+"&userName="+userName+"&departmentName="+departmentName+"&type=1";	
	url=encodeURI(encodeURI(url));
	window.open(url);
}

/**
 * 
 * 
 */
function findStaffSend(){
	var param={
			'beginTime':$("#begDate_repairPeople").val(),
            'endTime':$("#endDate_repairPeople").val(),
            'userName':$("#employee").val(),
            'departmentName':$("#department").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/staff_findStaffSend.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].departmentName+"</td>";
						html+="<td>"+data[i].receiveNum+"</td>";
						html+="<td>"+data[i].finishNum+"</td>";
						html+="<td>"+data[i].equipmentNumber+"</td>";
						html+="<td>"+data[i].noFinishNum+"</td>";
						html+="<td>"+data[i].longTimeSum+"</td>";
						html+="<td>"+data[i].avgTime+"</td>";
						//html+="<td>"+data[i].workTimeSum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='9'>暂无数据</td></tr>";
				}
				$("#staffSendList").html(html);
		}
	});
}
/**
 * 获得视图
 */
function findCharts(){
	var param={
		'beginTime':$("#begDate_repairPeople").val(),
        'endTime':$("#endDate_repairPeople").val(),
        'userName':$("#employee").val(),
        'departmentName':$("#department").val(),
        'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/staff_findStaffChart.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			eacharts(data);
		}
	});
}
//加载柱状图
function eacharts(data){
	 var myChart = echarts.init(document.getElementById("staffMain"));
	option = {
			tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        }
		    },
		    legend: {
		        data:data.completeName
		    },
		    grid: { 
		        x: 30,
		        x2: 10,
		        y2: 80,
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : data.name
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		        {
		            name:'接工量',
		            type:'bar',
		            data:data.sumArray
		        },
		        {
		            name:'完工量',
		            type:'bar',
		            data:data.finishArray
		        },{
		            name:'未完工量',
		            type:'bar',
		            data:data.noFinishArray
		        }
		    ]
		};
	
	 myChart.setOption(option);
}

