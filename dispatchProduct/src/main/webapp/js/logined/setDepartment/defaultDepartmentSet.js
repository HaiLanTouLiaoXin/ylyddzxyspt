$(function(){
	findDepartment(0);
	findDepartment(1);
	findDepartment(2);
	$("#saveOne").click(function(){
		var processId=$("#processId").val();
		var processName=$("#processName").val();
		var id=$("#idOne").val();
		if(!processId||!processName){
			art.dialog.alert("请选择部门！");
			return;
		}
		setDefaultDepartment(id,processId,processName,0);
	})
	$("#saveTwo").click(function(){
		var processId=$("#distributionId").val();
		var processName=$("#distributionName").val();
		var id=$("#idTwo").val();
		if(!processId||!processName){
			art.dialog.alert("请选择部门！");
			return;
		}
		setDefaultDepartment(id,processId,processName,1);
	})
	$("#saveThree").click(function(){
		var processId=$("#returnId").val();
		var processName=$("#returnName").val();
		var id=$("#idThree").val();
		if(!processId||!processName){
			art.dialog.alert("请选择部门！");
			return;
		}
		setDefaultDepartment(id,processId,processName,2);
	})
	
});

/**
 * 设置默认部门
 */
function setDefaultDepartment(id,groupId,name,type){
	var param={
		"defaultDepartment.groupId":groupId,
		"defaultDepartment.groupName":	name,
		"defaultDepartment.id":id,
		"defaultDepartment.type":type
	}
	$.ajax({
		url:basePath+"defaultDepartmentSet/setDepartment_addOrUpdateDepartment.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			if(data==0){
				art.dialog.tips("设置成功！");
			}else{
				art.dialog.alert("设置失败！");
			}
		}
		
	})
}

/**
 * 获得原始部门
 */
function findDepartment(type){
	var param={
		"defaultDepartment.type":type
	}
	$.ajax({
		url:basePath+"defaultDepartmentSet/setDepartment_findDepartment.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			if(type==0){
				$("#processId").val(data.groupId);
				$("#processName").val(data.groupName);
				$("#idOne").val(data.id);
			}else if(type==1){
				$("#distributionId").val(data.groupId);
				$("#distributionName").val(data.groupName);
				$("#idTwo").val(data.id);
			}else{
				$("#returnId").val(data.groupId);
				$("#returnName").val(data.groupName);
				$("#idThree").val(data.id);
			}
		}
		
	})
}
