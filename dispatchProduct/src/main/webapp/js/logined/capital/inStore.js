
$(function(){
	$("#repaireSelect").click(function(){
		selectLocation(this);
	});
});

/**
 * 入库信息
 */
function addStore(){
	var describe = $("#describe").val();
	if(!describe){
		art.dialog.alert("库存地点不能为空");
		return;
	}
	
	var param={
		"capitalId"	:$("#capitalId").val(),
		"describe":describe
	}
	$.ajax({
		url:basePath+"capital/inStore.action",
		type:'post',
		data:param,
		dataType:'text',
		success:function(data){
			if(data==1){
				var getTable = art.dialog.data("getTable");
				getTable(2);
				art.dialog.close();
			}else{
				art.dialog.alert("入库失败！");
			}
		}
	}) 
}


function selectLocation(obj)
{
    var groupId=$("#"+$(obj).attr("fieldId")).val();
	/**userId = "gid_"+userId;*/
    userObj = obj;
    qytx.app.tree.alertLocationTree({
    	type:"radio",
    	showType:1,
    	userId:"",
    	groupId:groupId,
    	callback:callBackUser
    });
}


function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    }
}