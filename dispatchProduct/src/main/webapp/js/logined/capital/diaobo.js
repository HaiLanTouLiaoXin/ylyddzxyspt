$(function(){
	initButton();
	initUserTree();
})

/**
	 * 调拨
	 * */
	function change(){
		var id=art.dialog.data("capitalId");
		var useUserId = $("#useUserIds").val();
		if(!useUserId){//没有选择人员
			art.dialog.alert("请选择人员！");
			return;
		}
		var param = {
				"capitalChangesVo.type":3,
				"capitalChangesVo.id":id,
				"capitalChangesVo.useUserId":useUserId
		}
		$.ajax({
			url:basePath+"capital/changes.action",
			type:'post',
			data:param,
			dataType:'text',
			success:function(result){
				if(result==1){
					art.dialog.tips("调拨成功");
					setTimeout(function(){
						art.dialog.data("result","success");
						art.dialog.close();
					},500);
				}else{
					art.dialog.alert("调拨失败，请稍后重试！");
				}
			}
		});
	}

function initUserTree(){
    var useUserIds=$("#useUserIds").val();
    qytx.app.tree.user({
    	id:"userUserTree",
    	type:"radio",
    	click:callBackUser
    });
}

function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("uid_");
    		if(item.id.indexOf("uid_") != -1){
    			userIds.push(item.id.substring(4));
    			userNames.push(item.name);
        	}
    	});
        $("#useUserText").text(userNames.join(","));
        $("#useUserIds").val(userIds.join(","));
    }
}

function toggleSelectUser(){
	$("#userUserTree").show();
}


/**
 * 初始化按钮
 */
function initButton(){
	var api = art.dialog.open.api;
	api.button(
			{
				name: '确定',
				callback: function () {
					change();
					return false;
				},
				focus: true
			},
			{
				name: '取消'
			}
		);
}