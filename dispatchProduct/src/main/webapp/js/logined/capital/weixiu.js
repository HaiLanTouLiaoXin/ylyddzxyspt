$(function(){
	initButton();
})


/**
	 * 维修
	 * */
function change(){
	var id=art.dialog.data("capitalId");
	var repairMoney=$("#repairMoney").val();
	var reg=/^[0-9]\d*$/;
    if(!reg.test(repairMoney)){
    	art.dialog.alert("维修费用必须为正整数！");
    	return;
    }
    var repairTime =$("#repairTime").val();
	var param = {
			"capitalChangesVo.type":4,
			"capitalChangesVo.id":id,
			"capitalChangesVo.repairTime":repairTime?repairTime+':00.000':null,
			"capitalChangesVo.repairMoney":repairMoney,
			"capitalChangesVo.repairCompany":$("#repairCompany").val(),
			"capitalChangesVo.repairResult":$("#repairResult").val()
	}
		$.ajax({
			url:basePath+"capital/changes.action",
			type:'post',
			data:param,
			dataType:'text',
			success:function(result){
				if(result==1){
					art.dialog.tips("维修成功");
					setTimeout(function(){
						art.dialog.data("result","success");
						art.dialog.close();
					},500);
				}else{
					art.dialog.alert("维修失败，请稍后重试！");
				}
			}
		});
	}

/**
 * 初始化按钮
 */
function initButton(){
	var api = art.dialog.open.api;
	api.button(
			{
				name: '确定',
				callback: function () {
					change();
					return false;
				},
				focus: true
			},
			{
				name: '取消'
			}
		);
}