var parent = art.dialog.parent;
var api = art.dialog.open.api;	

$(function(){
	
	$("#repaireSelect").click(function(){
		selectLocation(this);
	});
	
	$("#createGroupSelect").click(function(){
		selectCreateGroup(this);
	});
	
	capitalDetail();
	
});

function capitalDetail(){
	var capitalId = $("#capitalId").val();
	var param={
			"capitalId":capitalId
	};
	$.ajax({
		url:basePath+"capital/getCapitalDetail.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			$("#name").html(data.name);
			$("#currUser").html(data.currUser);
			$("#capitalGroup").html(data.capitalGroup);
			$("#storageTime").html(data.createTime);
			$("#capitalNo").html(data.capitalNo);
			$("#capitalModel").html(data.capitalModel);
			$("#location").val(data.storeLocation);
		}
	});
}

/**
 * 初始化按钮
 */
function initButton(){
	api.button(
			{
				name: '确定',
				callback: function () {
					borrowService();
					return false;
				},
				focus: true
			},
			{
				name: '取消',
				callback:function(){
					return true;
				}
			}
		);
}
initButton();

function borrowService(){
	var capitalId = $("#capitalId").val();
	var storeLocation = $("#location").val();
	var useGroupId = $("#useGroupId").val();
	if(!useGroupId){
		art.dialog.alert("请选择使用科室!");
		return;
	}
	if(!storeLocation){
		art.dialog.alert("请选择使用地点");
		return;
	}
	var param={
			"capital.id":capitalId,
			"capital.storeLocation":storeLocation,
			"capital.useGroup.groupId":useGroupId
	};
	$.ajax({
		url:basePath+"capital/capitalService.action",
		type:'post',
		data:param,
		dataType:'text',
		success:function(data){
			if(data=="1"){
				art.dialog.data("result","success");
			}else{
				art.dialog.data("result","fail");
			}
			art.dialog.close();
		}
	})
}

/**
 * 人员选择
 * REN
 **/
function selectLocation(obj)
{
    var groupId=$("#"+$(obj).attr("fieldId")).val(); 
	/**userId = "gid_"+userId;*/
    userObj = obj;
    qytx.app.tree.alertLocationTree({
    	type:"radio",
    	showType:1,
    	userId:"",
    	groupId:groupId,
    	callback:callBackUser
    });
}


function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    }
}

/**
 * 人员选择
 * REN
 **/
function selectCreateGroup(obj)
{
	var processType = $("#processType").val();
    var userId=$("#"+$(obj).attr("fieldId")).val();
   /* if(processType&&processType==2){
    	userId = "gid_"+userId;
    }*/
    var showType = $("#"+$(obj).attr("showType"));
    if(!showType){
    	showType = 3;
    }else{
    	showType = showType.val();
    }
    var extension =$("#"+$(obj).attr("extension"));
    if(!extension){
    	extension="";
    }else{
    	extension=extension.val();
    }
    userObj = obj;
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	showType:showType,
    	defaultSelectIds:userId,
    	extension:extension,
    	callback:callBackCreateGroup
    });
}


function callBackCreateGroup(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    	findLocation(userIds[0]);
    }
}


/**
 * 获得原始部门
 */
function findLocation(groupId){
	var param={
		"groupId":groupId
	}
	$.ajax({
		url:basePath+"location/location_getUserLocationName.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			if(data!=null){
				$("#location").val(data.pathName);
			}else{
				$("#location").val("");
			}
		}
		
	})
}
