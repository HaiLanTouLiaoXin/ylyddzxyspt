var parent = art.dialog.parent;
var api = art.dialog.open.api;	
/**
 * 初始化按钮
 */
function initButton(){
	api.button(
			{
				name: '确定',
				callback: function () {
					selectCapital();
					return false;
				},
				focus: true
			},
			{
				name: '取消',
				callback:function(){
					return true;
				}
			}
		);
}
initButton();

var capitalMap;

$(function(){
	
	capitalMap=art.dialog.data("capitalMap");
	if(capitalMap.size()>0){
		showCapital();
	}else{
		capitalMap = new Map();
	}
	
	
	var treeHeight = $(window).height()-38;
	$(".center_content").height(treeHeight);
	getCapitalGroupTree("gid_0");
	//加载存放位置下拉选
	// 捕获回车事件
    $("html").die().live("keydown", function(event) {
        if (event.keyCode == 13) {
        	getTable();
        }
    });
	serch();
});



var Capital= function(){
	this.id = "";
	this.name = "";
	this.capitalNo = "";
	this.capitalModel = "";
}

//搜索
function serch(){
	getTable();
}


function getTable(){
   var capitalStatus=$("#capitalStatus").val();
   var keyWord=$.trim($("#keyWord").val());
   var capitalType = $("#type").val();
   if(capitalType==2){
	   capitalStatus = "2,3";
   }
   if(capitalType==6){
	   capitalStatus = "3,6";
   }
	$('#capitalTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
						"name" : "capitalVo.capitalStatus",
						"value" : capitalStatus
					},{
						"name":"capitalVo.keyWord",
						"value":keyWord
					},{
						"name":"capitalVo.capitalGroup",
						"value":$("#selectGroupId").val()
					},{
						"name":"capitalVo.type",
						"value":$("#type").val()
					}
				);
		},
		"sAjaxSource" : basePath+"capital/getCapitalList.action",// 获取管理员列表
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,
		"iDisplayLength" : 10, // 每页显示多少行
		"aoColumns" : [
			    {
					"mDataProp" : "no",
				}, {
					"mDataProp" : "capitalNo",
					"sClass" : "longTxt"
				}, {
					"mDataProp" : "goodName",
				    "sClass" : "longTxt"
				}, {
					"mDataProp" :"capitalModel"
				}, {
					"mDataProp" : "capitalGroup"
				},{
					"mDataProp" : "location"
				},{
					"mDataProp" : null
				}],
		"oLanguage" : {
			"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
		},
		"fnDrawCallback" : function(oSettings) {
			$('#capitalTable tbody  tr td').each(function() {
				this.setAttribute('title', $(this).text());
			});
			
			$('#capitalTable tbody  tr').each(function(){
				var obj1 = $(this);
				$(obj1).click(function(){
					var obj = $(this);
					var tempClass = $(obj).attr("tempClass");
					if(tempClass){
						$(obj).addClass(tempClass);
						$(obj).removeAttr("tempClass");
						$(obj).css("background","");
						$(obj).find("input").prop("checked",false);
					}else{
						$(obj).find("input").prop("checked",true);
						/*$(obj).siblings().each(function(){
							$(this).css("background","");
							var temp1Class = $(this).attr("tempClass");
							if(temp1Class){
								$(this).addClass(temp1Class);
								$(this).removeAttr("tempClass");
							}
						});*/
						$(obj).attr("tempClass",$(obj).attr("class"));
						$(obj).removeAttr("class");
						$(obj).css("background","#FBEC88");
					}
				});
			});
              
		},
		"fnInitComplete" : function() {
			// 重置iframe高度
		
		},
		"aoColumnDefs" : [{
			"aTargets" : [6],
			"fnRender" : function(oObj) {
				var vid = oObj.aData.id;
				var goodName = oObj.aData.goodName;
				var capitalNo = oObj.aData.capitalNo;
				var capitalModel = oObj.aData.capitalModel;
				var html = '<input type="checkbox" id="'+vid+'" name="'+goodName+'" capitalNo="'+capitalNo+'" capitalModel="'+capitalModel+'"/>';
				return html;
			}
		}]
	});
}	

function getCapitalGroupTree(defaultSelectId){
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	if(vid.substring(4)!="0"){
		$("#selectGroupId").val(vid.substring(4));
	}else{
		$("#selectGroupId").val("");
	}
	getTable();
}


function selectCapital(){
	/*var selectObjArr = $("input[type='checkbox']:checked");
	if(selectObjArr.length>0){}
	var selectArr = [];
	selectObjArr.each(function(i,val){
		var equipment = new Equipment();
		equipment.id=$(this).attr("id");
		equipment.name=$(this).attr("name");
		equipment.capitalNo=$(this).attr("capitalNo");
		equipment.capitalModel=$(this).attr("capitalModel");
		selectArr.push(equipment);
	});
	art.dialog.data("selectArr",selectArr);
	art.dialog.close();*/
	
	var len=$("#capitalDiv").find("div").length
	if(len<=0){
		art.dialog.alert("请添加设备");
	}
	if(capitalMap.size()>0){
		art.dialog.data("capitalMap",capitalMap);
		art.dialog.close();
	}else{
		art.dialog.alert("请添加设备");
		return false;
	}
	
	
}



function delCapital(id){
	if(capitalMap.get(id+"")){
		capitalMap.remove(id+"");
	}
	if(capitalMap.size()>0){
		showCapital();
	}else{
		$("#capitalDiv").html("");
	}
}


function addCapital(){
	var checkBox= $("input[type='checkBox']");
	for(var i=0;i<checkBox.length;i++){
		if(checkBox[i].checked){
			 var id=$(checkBox[i]).attr("id");
			 if(!capitalMap.get(id+"")){
				 if(capitalMap.size()>19){
					art.dialog.alert("最多添加20个资产");
					return;
				 }
				 var name=$(checkBox[i]).attr("name");
		    	 var capitalNo=$(checkBox[i]).attr("capitalNo");
		    	 var capitalModel=$(checkBox[i]).attr("capitalModel");
		    	 var id=$(checkBox[i]).attr("id");
		    	 var capital = new Capital();
				 capital.id=id;
				 capital.name=name;
				 capital.capitalNo=capitalNo;
				 capital.capitalModel=capitalModel;
				 capitalMap.put(capital.id+"",capital);
			 }
		}
	}
	showCapital();
}


function showCapital(){
	if(capitalMap.size()>0){
		var html="";
		var arr = capitalMap.arr;
		for( var i=0 ;i<capitalMap.size();i++){
			var temp = arr[i].value;
			var id=temp.id;
			var name=temp.name;
			var capitalNo=temp.capitalNo;
			var capitalModel = temp.capitalModel;
			html+="<div class='col'><span class='name' >"+temp.name;
			if(capitalNo){
				html+="("+capitalNo+")";
			}
			html+="</span><span class='icon-close' onclick='delCapital("+temp.id+")'></span></div>";
		}
		$("#capitalDiv").html(html);
	}else{
		art.dialog.alert("请选择设备");
		return;
	}
}


function xuanRan(obj){
	var tempClass = $(obj).attr("tempClass");
	if(tempClass){
		$(obj).addClass(tempClass);
		$(obj).removeAttr("tempClass");
	}else{
		$(obj).siblings().each(function(){
			$(this).css("background","");
			var temp1Class = $(this).attr("tempClass");
			if(temp1Class){
				$(this).addClass(temp1Class);
				$(this).removeAttr("tempClass");
			}
		});
		$(obj).attr("tempClass",$(obj).attr("class"));
		$(obj).removeAttr("class");
		$(obj).css("background","#FBEC88");
	}
	
}
