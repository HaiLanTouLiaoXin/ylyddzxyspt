$(function(){
	var treeHeight = $(window).height()-69;
	$(".center_content").height(treeHeight);
	getList();
});

//获取列表
function getList(){
	var capitalNo=$("#capitalNo").val();
	var param = {
			"capitalVo.capitalNo":capitalNo,
			
		};
	qytx.app.grid({
		id	:	"myTable",
		url	:	basePath + "capital/capitalLogList.action",
		iDisplayLength:	15,
		selectParam:param,
		valuesFn:	[]
	});
	
	
}


function getCapitalGroupTree(defaultSelectId){
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	$("#selectGroupId").val(vid.substring(4));
	getList();
}