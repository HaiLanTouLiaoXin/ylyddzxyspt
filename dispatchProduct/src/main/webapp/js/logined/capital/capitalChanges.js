$(function(){
	//1.部门设备  2.管理员管理设备 3.查看设备 4 待归库 5 入库
	var type=$("#type").val();
	var treeHeight = $(window).height()-38;
	$(".center_content").height(treeHeight);
	if(type==2){
		$("#isShowAdd").show();
		$("#isShowImport").show();
	}else if(type==4){
		$("#isShowAdd").hide();
		$("#isShowImport").hide();
		$("#title").html("配送人：");
		$("#statuDiv").hide();
		$("#chageGroupName").html("配送科室");
		$("#chageUserName").html("配送人");
	}else if(type==1){
		$("#isShowAdd").hide();
		$("#isShowImport").hide();
		var statusHtml = '<option value="">全部</option><option value="2">使用中</option><option value="3">已停用</option><option value="6">已送达</option>';
		$("#capitalStatus").html(statusHtml);
		$("#userGroupName").hide();
		$("#userGroupNameLabel").hide();
		
		
	}else{
		$("#isShowAdd").hide();
		$("#isShowImport").hide();
	}
	
	getCapitalGroupTree("gid_0");
	$(document).keydown(function(event){
		var code=event.which;
		if (code == 13) {
			serch();
			return false;
		}
	}); 
	initUserTree();
	serch();
	initGroupTree();
	
	
});

function toUpdateOrAdd(id){
	var type=$("#type").val();
	if(id){
		window.location.href = basePath + "logined/capital/capitalAdd.jsp?id="+id+"&type="+type;
	}else{
		window.location.href = basePath + "logined/capital/capitalAdd.jsp?type="+type;
	}
	return false;
}
/**
 * 弹出导入页面
 */
function toImport(){
	art.dialog.data("getTable",getTable);
	var type=$("#type").val();
	if(!type||type==0){
		art.dialog.alert("请设备组");
		return;
	}
	art.dialog.open(basePath +"logined/capital/importCapital.jsp?type="+type,{
				id : 'import',
				title : '设备导入',
				lock :true,
				opacity: 0.08, 
				width : 600,
				height : 300,
				button : [{
					name : '导 入',
					callback : function() {
						var iframe = this.iframe.contentWindow;
						iframe.startAjaxFileUpload();
						return false;
					},
					focus:true
				},
				{
					name : '取 消',
					callback : function() {
						return true; 
					}
				}]
		});
}




//搜索
function serch(){
	var type=$("#type").val();
	getTable(type);
}

function initUserTree(){
    var useUserIds=$("#useUserIds").val();
    qytx.app.tree.user({
    	id:"userUserTree",
    	type:"check",
    	click:callBackUser
    });
}

function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("uid_");
    		if(item.id.indexOf("uid_") != -1){
    			userIds.push(item.id.substring(4));
    			userNames.push(item.name);
        	}
    	});
        $("#useUserText").text(userNames.join(","));
        $("#useUserIds").val(userIds.join(","));
        $("#userUserTree").hide();
    }
}

function toggleSelectUser(){
	$("#userUserTree").show();
}



/**
 * 部门树
 */

function initGroupTree(){
    qytx.app.tree.user({
    	id:"groupTree",
    	type:"check",
    	showType:1,
    	click:callBackGroup
    });
}

function callBackGroup(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("gid_");
    		if(item.id.indexOf("gid_") != -1){
    			userIds.push(item.id.substring(4));
    			userNames.push(item.name);
        	}
    	});
        $("#groupText").text(userNames.join(","));
        $("#groupId").val(userIds.join(","));
        $("#groupTree").hide();
    }
}

function toggleSelectGroup(){
	$("#groupTree").show();
}


function getTable(type){
	var capitalStatus=$("#capitalStatus").val();
	if(type==4){
		capitalStatus=4;
	}
	if(type==5){
		capitalStatus=1;
	}
	if(type==1&&!capitalStatus){
		capitalStatus="2,3,6";
	}
   var keyWord=$.trim($("#keyWord").val());
	$('#capitalTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
						"name" : "capitalVo.capitalStatus",
						"value" : capitalStatus
					},{
						"name":"capitalVo.keyWord",
						"value":keyWord
					},{
						"name":"capitalVo.userGroupName",
						"value":$("#userGroupName").val()
					},{
						"name":"capitalVo.type",
						"value":type
					},{
						"name":"capitalVo.capitalGroup",
						"value":$("#selectGroupId").val()
					}
				);
		},
		"sAjaxSource" : basePath+"capital/getCapitalList.action",// 获取管理员列表
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [
			    {
					"mDataProp" : "no",
				}, {
					"mDataProp" : "goodName",
				    "sClass" : "longTxt"
				}, {
					"mDataProp" : "capitalNo",
					"sClass" : "longTxt"
				}, {
					"mDataProp" :"capitalModel"
				}, {
					"mDataProp" : "capitalGroup"
				},{
					"mDataProp" : "statusStr"
				},{
					"mDataProp" : "useGroupName"
				},{
					"mDataProp" : "location"
				},{
					"mDataProp" : null,
					"sClass" : "data_l"
				}],
		"oLanguage" : {
			"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
		},
		"fnDrawCallback" : function(oSettings) {
			$('#knowlegeTable tbody  tr td').each(function() {
				this.setAttribute('title', $(this).text());
			});
              
		},
		"fnInitComplete" : function() {
			// 重置iframe高度
		
		},
		"aoColumnDefs" : [{
			"aTargets" : [8],
			"fnRender" : function(oObj) {
				var vid = oObj.aData.id;
				var status=oObj.aData.status;
				var html='<a href="javascript:void(0);" onclick="capitalView('+vid+')">查看</a>';
				/*if(type==1){//本部门设备借用
					if(status==2){
						html+='<a href="javascript:void(0);" onclick="capitalDisabled('+vid+',0)">停用</a>';
					}else{
						html+='<a href="javascript:void(0);" onclick="capitalBorrow('+vid+',0)">使用</a>';
					}
				}*/
				if(type==2){//管理员功能
					 html+='<a href="javascript:void(0);" onclick="toUpdateOrAdd('+vid+')">修改</a>';
					// html+='<a href="javascript:void(0);" onclick="setState('+vid+')">设置状态</a>';
					if(status!=5){
						html+='<a href="javascript:void(0);" onclick="enableOrUnable('+vid+',5)">禁用</a>';
					}
					/*if(status==2){
						html+='<a href="javascript:void(0);" onclick="capitalDisabled('+vid+',1)">停用</a>';
					}*/
					if(status==1){
						html+='<a href="javascript:void(0);" onclick="capitalService('+vid+')">送达</a>';
					}
					/*if(status!=2){
						html+='<a href="javascript:void(0);" onclick="capitalBorrow('+vid+',1)">使用</a>';
					}*/
					if(status==5){
						html+='<a href="javascript:void(0);" onclick="enableOrUnable('+vid+',1)">启用</a>';
					}
					/*if(status==3||status==4){
						html+='<a href="javascript:void(0);" onclick="inStore('+vid+')">入库</a>';
					}*/
					if(status!=2){
						html+='<a href="javascript:void(0);" onclick="delCapital('+vid+')">删除</a>';
					}
					if(status!=1 && status!=4 && status!=5){
						html+='<a href="javascript:void(0);" onclick="returnCapital('+vid+',2)">归还</a>';
					}
				}else{
					if(status!=1 && status!=4 && status!=5){
						html+='<a href="javascript:void(0);" onclick="returnCapital('+vid+')">归还</a>';
					}
				}
					/*if(type==4){
						html+='<a href="javascript:void(0);" onclick="inStore('+vid+')">入库</a>';
					}*/
				
				return html;
			}
			
		}]
	});
}	
/**
 * 设置状态
 */
function setState(id){
	art.dialog.data("getTable",getTable);
	var url=basePath+"logined/capital/chageState.jsp?id="+id;
	art.dialog.open(url, {
		title : '状态管理',
		width : 600,
		height :400,
		lock : true,
		opacity : 0.08,
		 button : [{
				name : '确定',
				focus:true,
				callback : function() {
					var iframe = this.iframe.contentWindow;
					iframe.addStore();
					return false;
				}
			}, {
				name : '取消',
				callback : function() {
					return true;
				}
			}]
	}, false);
	
}
	
/**
 * 归还
 */
function returnCapital(id,operType){
	if(operType == "2"){//管理员归还
		var title="确定要归还吗?";
		art.dialog.confirm(title, function () {
			$.ajax({
				url:basePath+"capital/returnCapital.action",
			    data:{"capitalId":id},
			    type:'post',
			    dataType:'text',
			    success:function(data){
			    	if(data==1){
			    		art.dialog.tips("归还成功!");
			    		serch();
			    	}else{
			    		art.dialog.alert("操作失败,请稍候重试!");
			    	}
			    }
			 })
	        
	    })
	}else{
		art.dialog.data("getTable",getTable);
		var url=basePath+"logined/defect/returnCapital.jsp?capitalId="+id;
		art.dialog.open(url, {
			title : '设备归还',
			width : 630,
			height :500,
			lock : true,
			opacity : 0.08,
			button : [{
				name : '确定',
				focus:true,
				callback : function() {
					var iframe = this.iframe.contentWindow;
					iframe.returnCapital();
					return false;
				}
			}, {
				name : '取消',
				callback : function() {
					return true;
				}
			}]
		}, false);
	}
	
}


function delCapital(capitalId){
	art.dialog.confirm("确定要删除吗?",function(){
		param = {"capitalId":capitalId};
		$.ajax({
			url:basePath+"capital/deleteCapital.action",
			type:"POST",
			data:param,
			dataType:"text",
			success:function(result){
				if(result=="0"){
					art.dialog.tips("删除成功!");
					serch();
				}else{
					art.dialog.alert("操作失败,请稍后重试!");
				}
			}
		});
	});
};

	
	
	
	
/**
 * num 5 禁用  1 启用
 * @param id
 * @param num
 */
function enableOrUnable(id,num){
	var title="";
	if(num==5){
		title="确定要将设备置禁用吗？禁用的设备将不能被使用。";
	}else if(num==3){
		title="确定要将设备停用吗？";
	}else{
		title="确定要启用设备吗？启用的设备将回归到在库状态。";
	}
    
	art.dialog.confirm(title, function () {
		$.ajax({
			url:basePath+"capital/chageStatu.action",
		    data:{"capitalId":id,"num":num},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		art.dialog.tips("操作成功!");
		    		serch();
		    	}else{
		    		art.dialog.alert("操作失败,请稍候重试!");
		    	}
		    }
		 })
        
    })
}


/**
 * 查看资产详情
 */
function capitalView(id) {
	var capitalViewUrl = basePath+ "logined/capital/capitalDetail.jsp";
	art.dialog.data("capitalId", id);
	art.dialog.open(capitalViewUrl, {
		title : '设备详情',
		width : 780,
		height :500,
		lock : true,
		opacity : 0.08,
		 button : [{
					name : '关闭',
					callback : function() {
						art.dialog.data("id", "");
						return true;
					}
				}]
	});

}
/**
 *进入资产添加界面
 */
function toAddCapitalView(){
	window.location.href=basePath+"logined/capital/capitalAdd.jsp";
}





function getCapitalGroupTree(defaultSelectId){
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	var type=$("#type").val();
	if(vid.substring(4)!="0"){
		$("#selectGroupId").val(vid.substring(4));
	}else{
		$("#selectGroupId").val("");
	}
	getTable(type);
}

/**
 * 入库操作
 */
function inStore(id){
	art.dialog.data("getTable",getTable);
	var url = basePath+ "logined/capital/inStore.jsp?id="+id;
	art.dialog.open(url, {
		title : '入库设备',
		width : 600,
		height :100,
		lock : true,
		opacity : 0.08,
		 button : [{
				name : '确定',
				focus:true,
				callback : function() {
					var iframe = this.iframe.contentWindow;
					iframe.addStore();
					return false;
				}
			}, {
				name : '取消',
				callback : function() {
					return true;
				}
			}]
	}, false);
}


function capitalService(capitalId){
	art.dialog.data("result","");
	var url = basePath+"logined/capital/capitalService.jsp?capitalId="+capitalId;
	art.dialog.open(url,{
		title : "设备送达",
		width : 610,
		height : 500,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		close:function(){
			var result = art.dialog.data("result");
			if(result=="success"){
				art.dialog.tips("送达成功!");
				serch();
			}
			if(result=="fail"){
				art.dialog.alert("系统繁忙,请稍后重试!");
			}
			return true;
	   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
	});
	
	
}

function capitalBorrow(capitalId,isManager){
	art.dialog.data("result","");
	var url = basePath+"logined/capital/capitalBorrow.jsp?capitalId="+capitalId+"&isManager="+isManager;
	art.dialog.open(url,{
		title : "使用设备",
		width : 610,
		height : 500,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		close:function(){
			var result = art.dialog.data("result");
			if(result=="success"){
				art.dialog.tips("操作成功!");
				serch();
			}
			if(result=="fail"){
				art.dialog.alert("系统繁忙,请稍后重试!");
			}
			return true;
	   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
	});
}


function capitalDisabled(capitalId,isManager){
	art.dialog.data("result","");
	var url = basePath+"logined/capital/capitalDisable.jsp?capitalId="+capitalId+"&isManager="+isManager;
	art.dialog.open(url,{
		title : "停用设备",
		width : 610,
		height : 500,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		close:function(){
			var result = art.dialog.data("result");
			if(result=="success"){
				art.dialog.tips("停用成功!");
				serch();
			}
			if(result=="fail"){
				art.dialog.alert("系统繁忙,请稍后重试!");
			}
			return true;
	   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
	});
}
