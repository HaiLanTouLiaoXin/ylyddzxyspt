$(document).ready(function() {
	getCarList();
});

/**
 * 车辆列表
 */
function getCarList(){
	var carType=$("#carType").val();
	var iDisplayStart = 0;
	$('#myTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carType",
				"value":carType
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findCarList.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" :10, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : null, //选择
				}, {
					"mDataProp" : "no" //序号
				}, {
					"mDataProp" : "carNo", //车牌号
				},  {
					"mDataProp" : "carPhone" //车辆电话号
				}, {
					"mDataProp" : null, //车辆类型
				}, {
					"mDataProp" : "statusStr", //车辆状态
					"sClass" : "tdCenter"
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myTable tbody  tr td,#myTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
			   
			   $('#myTable tbody  tr').each(function(){
				   var obj1 = $(this);
					$(obj1).click(function(){
						var obj = $(this);
						var tempClass = obj.attr("tempClass");
						if(tempClass){
							obj.addClass(tempClass);
							obj.removeAttr("tempClass");
							obj.css("background","");
							obj.find("input").prop("checked",false);
						}else{
							obj.siblings().css("background","");
							obj.siblings().removeAttr("tempClass");
							obj.find("input").prop("checked",true);
							obj.attr("tempClass",obj.attr("class"));
							obj.removeAttr("class");
							obj.css("background","#FBEC88");
						}
					});
				});
			   
			   
			   
			   
			   
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [0], //选择
			"fnRender" : function(oObj) {
				var html = "<label><input name='car' type='radio' carNo='"+oObj.aData.carNo+"' carBrand='"+oObj.aData.carBrand+"' carPhone='"+oObj.aData.carPhone+"' carType='"+oObj.aData.carType+"' status='"+oObj.aData.status+"' id='"+oObj.aData.id+"'); /> </label>"
//				var html = "";
//				if(oObj.aData.statusStr != "空闲"){
//					html += "<label><input name='car' type='radio' disabled /> </label>"
//				}else{
//					html += "<label><input name='car' type='radio'  onclick=toParentParams('"+oObj.aData.carNo+"','"+oObj.aData.carBrand+"','"+oObj.aData.carModel+"','"+oObj.aData.carType+"','"+oObj.aData.status+"','"+oObj.aData.id+"'); /> </label>"
//				}
				return html;
			}
		},{
			"aTargets" : [4], //操作
			"fnRender" : function(oObj) {
				if(oObj.aData.carType == 1){
					return " 转诊车";
				}else if(oObj.aData.carType == 2){
					return " 急诊车";
				}else{
					return "--";
				}
			}
		},{
			"aTargets" : [5], //状态
			"fnRender" : function(oObj) {
				if(oObj.aData.statusStr == "空闲"){
					return "<span style='color:green;'> 空闲</span>";
				}else if(oObj.aData.statusStr == "忙碌"){
					return " <span style='color:red;'> 忙碌</span>";
				}
			}
		}]
	}); 
}

var Car = function(){
	this.id = "";
	this.carNo = "";
	//this.carBrand="";
	this.carPhone="";
	this.carType="";
	this.status="";
};


function toParentParams(){
	var selectObjArr = $("input[type='radio']:checked");
	var car =new Car();
	car.id=selectObjArr.attr("id");
	car.carNo=selectObjArr.attr("carNo");
	//car.carBrand=selectObjArr.attr("carBrand");
	car.carPhone=selectObjArr.attr("carPhone");
	car.carType=selectObjArr.attr("carType");
	car.status=selectObjArr.attr("status");
	return car;
	/*$("#carNo").val(carNo);
	$("#carBrand").val(carBrand);
	$("#carModel").val(carModel);
	$("#carType").val(carType);
	$("#carStatus").val(carStatus);
	$("#carId").val(carId);*/
//	artDialog.data("carNo", carNo);
//	artDialog.data("carBrand", carBrand);
//	artDialog.data("carModel", carModel);
//	artDialog.data("carType", carType);
//	artDialog.data("carStatus", status);
//	artDialog.data("carId", id);
}