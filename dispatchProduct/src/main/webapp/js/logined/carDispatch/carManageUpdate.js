$(document).ready(function() {
	//保存
	$("#saveCars").click(function(){
		var carNo=$.trim($("#carNo").val());
		if (carNo == "") {
			art.dialog.alert('请输入车牌号');
			return;
		}
		var carPhone=$.trim($("#carPhone").val());
	    if(carPhone){
	    	if(!(/^1[34578]\d{9}$/.test(carPhone))){ 
	    		art.dialog.alert("手机号码有误，请重填");  
	            return; 
	        } 
	    }
	    //车辆类别
	    var carType = $('input:radio[name="carType"]:checked').val();
		 //是否有担架
	    var havaStretcher = $('input:radio[name="havaStretcher"]:checked').val();
	    //车辆座位数
	    var seatNum=$.trim($("#seatNum").val());
		var paramData = {
				'carVo.id':$("#id").val(),
			    'carVo.carNo':carNo,
				//'carVo.carBrand':carBrand,
				'carVo.carPhone':carPhone,
				'carVo.carType':carType,
				'carVo.havaStretcher':havaStretcher,
				'carVo.seatNum':seatNum
			};
		$.ajax({
			url: basePath+"cars/updateCars.action",
			type : "post",
			dataType :'json',
			data:paramData,
			success : function(data) {
				if(data == 0) {
					var getDataTable=art.dialog.data('getDataTable');
					getDataTable();
					art.dialog.close();
					art.dialog.tips('修改车辆成功！');
	            }else {
	            	art.dialog.tips('修改车辆失败！');
	            }
		}});
	})
	
	//取消
	$("#cancelCars").click(function(){
		 artDialog.close();
	});
	
})