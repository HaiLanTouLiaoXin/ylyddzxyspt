$(document).ready(function() {
	getDriverList();
});

/**
 * 司机列表
 */
function getDriverList(){
	var iDisplayStart = 0;
	$('#myTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
//			aoData.push();
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findDriverList.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : null, //选择
				}, {
					"mDataProp" : "no" //序号
				}, {
					"mDataProp" : "userName", //驾驶人
				}, {
					"mDataProp" : "phone" //驾驶人手机
				}, {
					"mDataProp" : "statuStr" //状态
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myTable tbody  tr td,#myTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
			   
			   $('#myTable tbody  tr').each(function(){
				   var obj1 = $(this);
					$(obj1).click(function(){
						var obj = $(this);
						var tempClass = obj.attr("tempClass");
						if(tempClass){
							obj.addClass(tempClass);
							obj.removeAttr("tempClass");
							obj.css("background","");
							obj.find("input").prop("checked",false);
						}else{
							obj.siblings().css("background","");
							obj.siblings().removeAttr("tempClass");
							obj.find("input").prop("checked",true);
							obj.attr("tempClass",obj.attr("class"));
							obj.removeAttr("class");
							obj.css("background","#FBEC88");
						}
					});
				});
			   
			   
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [0], //选择
			"fnRender" : function(oObj) {
				var html = "<label><input name='car' type='radio'  userName='"+oObj.aData.userName+"' phone='"+oObj.aData.phone+"' userId='"+oObj.aData.userId+"' status='"+oObj.aData.status+"' /> </label>"
//				var html = "";
//				if(oObj.aData.statuStr != "空闲"){
//					html += "<label><input name='car' type='radio' disabled /> </label>"
//				}else{
//					html += "<label><input name='car' type='radio' onclick=toParentParams('"+oObj.aData.userName+"','"+oObj.aData.phone+"','"+oObj.aData.userId+"','"+oObj.aData.status+"'); /> </label>"
//				}
				return html;
			}
		},{
			"aTargets" : [4], //状态
			"fnRender" : function(oObj) {
				if(oObj.aData.statuStr == "空闲"){
					return "<span style='color:green;'> 空闲</span>";
				}else if(oObj.aData.statuStr == "忙碌"){
					return " <span style='color:red;'> 忙碌</span>";
				}
			}
		}]
	}); 
}


var Driver = function(){
	this.userName = "";
	this.phone = "";
	this.userId="";
	this.status="";
};


function toParentParams(){
	var selectObjArr = $("input[type='radio']:checked");
	var driver =new Driver();
	driver.userName=selectObjArr.attr("userName");
	driver.phone=selectObjArr.attr("phone");
	driver.userId=selectObjArr.attr("userId");
	driver.status=selectObjArr.attr("status");
	return driver;
	
//	artDialog.data("driverName", userName);
//	artDialog.data("driverPhone", phone);
//	artDialog.data("driverId", userId);
//	artDialog.data("driverStatus", status);
}

