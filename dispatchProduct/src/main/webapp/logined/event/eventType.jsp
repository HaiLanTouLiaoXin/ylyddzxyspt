<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html >
<meta charset="utf-8">
<title>事件类型管理</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}/flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/annex/skins/annex_default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/event/eventType.js"></script>
<script type="text/javascript">
	$(function(){
		var treeHeight = $(window).height()-90;
		$(".zTreeDemoBackground").height(treeHeight);
		$("#myTree").height(treeHeight);
	})
</script>
<style type="text/css">
/*文字隐藏*/
.text_hide{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;}
</style>
<body  >
<input type="hidden" id="eventType"/>
<div class="mainpage_r">
	<!-- 左侧树 -->
	<div class="leftMenu" ng-cloak>
		<div class="service-menu">
			<h1>事件类型管理</h1>
			 <div class="workes" style="text-align: center"><a style="cursor: pointer;" onClick="addLocation()"><em class="em1"></em>新增</a>
			 <a style="cursor: pointer;" onClick="delLocation()"><em class="em3"></em>删除</a></div>
			<div class="zTreeDemoBackground">
				<ul id="groupUserTree" class="ztree">
				</ul>
			</div>
		</div>
	</div>
	<!-- 右侧新增/修改 -->
	<div class="formPage" id="formPage" style="display: none;width: 800px;margin-top:20px">
  			<div class="formbg" style="height:200px;">
				<div class="big_title" id="operationTitle">{{operationTitle}}</div>
				<input type="hidden" id="id">
				<input type="hidden"  id="parentId">
				<input type="hidden" id="operationType">
    			<div class="content_form">
			<form action="#" id="groupForm">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="inputTable" style="font-size:14px">
					<tr>
						<th style="width:100px"><label>事件类型名称：</label>
						</th>
						<td><input type="text" class="formText" maxlength="25" id="eventTypeName"/>
						</td>
						<th>&nbsp;
						</th>
						<td>&nbsp;
						</td>
					</tr>
					<tr>
						<th><label>排序号：</label>
						</th>
						<td>
						    <input name="input2" type="text" class="formText"  maxlength="3" id="orderIndex"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
						</td>
						<th>&nbsp;
						</th>
						<td>&nbsp;
						</td>
					</tr>
				</table>				
			</form>
			</div>
			</div>
			<div class="buttonArea">
					<input value="保 存" type="button" class="formButton_green"  onClick="saveOrUpdate()"/>					
			</div>		
	</div>
	<div class="none_wrap" id="noData" style="margin-top:40px">
		<h3 class="none_title"><em></em><span>请选择地点</span></h3>
	</div>
</div>
</body>
</html>