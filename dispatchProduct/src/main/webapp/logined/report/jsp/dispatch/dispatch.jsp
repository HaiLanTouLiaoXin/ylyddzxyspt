<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <jsp:include page="../../../../common/flatHead.jsp" />
    <link rel="stylesheet" href="../../css/style.css">
     <link href="../../css/reset.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
    <link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
    <link href="../../css/datatable_default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .inputTable th{width:82px;}
        .searchArea{padding:19px 0 12px 0;}
        .dataTables_wrapper{
            padding: 0 20px;
        }
        .canverse_right_tap{
            height:60px;
        }
        .canverse_right_tap span{
            padding:6px 10px;
        }
        /* .canverse_tap_area{
            margin:12px 10px;
        } */
    </style>

    <script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="${ctx}js/placeholder.js"></script>
    <script type="text/javascript" src="${ctx}flat/js/base.js"></script>
    <script type="text/javascript" src="${ctx}js/logined/report/dispatch/dispatch.js"></script>
</head>
<body>
<div class="converse_right">
    <div class="canverse_right_two">
        <div class="canverse_right_tap">
            <span id="callLogDetailSpan" >调度员工工作量统计</span>
        </div>
        <div class="canverse_tap_area">
            <div class="canverse_tap_box" >
					<div class="searchArea">
						<table cellspacing="0" cellpadding="0">
								<tbody>
								<tr>
						            <td class="right">
							            <label style="padding-left:20px;color:#999;font-size:14px;">时间：</label>
							            <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="begDate" class="in_something2 pr Wdate" name=""/>
							            -
							            <input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'begDate\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="endDate" class="in_something2 pr Wdate" name=""/>
							           <label style="padding-left:20px;color:#999;font-size:14px;">员工：</label>
						               <input type="text"  id="employee" class="input_css" name=""/>
							           <button class="btn_jinhu_add" onclick="getTable()">查询</button>
						             	<button class="btn_jinhu_add" onclick="report_export()">导出</button>
						             </td>
		         			 </tr>
							</tbody>
						</table>
					</div>
				  <div class="list_IllegalsNum">
					  <div id="table_wrapper" class="dataTables_wrapper" role="grid">
					    <table id="table" class="pretty dataTable" width="100%" cellspacing="0" cellpadding="0" border="0">
					   			<thead>
									<tr>
										<th id="no">序号</th>
										<th id="name">姓名</th>
										<th id="phoneReceive">电话调度量</th>
										<th id="receive">自主调度量</th>
										<th id="sum">调度总量</th>
									</tr>
								</thead>
								<tbody id="dispatchList">
								     
								</tbody>
						</table>
					  </div>
				  </div>
			  </div>
        </div>
    </div>
</div>
</body>
</html>