<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		 <jsp:include page="../../../../common/flatHead.jsp" />
		<link rel="stylesheet" href="../../css/style.css">			
		<link href="../../css/reset.css" rel="stylesheet" type="text/css" />
		<link href="../../css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
		<link href="../../css/datatable_default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.inputTable th{width:82px;}
			.searchArea{padding:19px 0 12px 0;}
  			.dataTables_wrapper{
			 padding: 0 20px;
			}
			.canverse_right_tap{
			 height:59px;
			}
			.canverse_right_tap span{
			 padding:6px 10px;
			}
			/* .canverse_tap_area{
			 margin:12px 10px;
			} */
		</style>
			
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/selecedForDatatablePagination.js"></script>
		<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
		<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
		<script type="text/javascript" src="${ctx}/plugins/workview/echarts.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/equipment/equipment.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/equipment/equipmentReturn.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/equipment/equipmentSend.js"></script>
                <script type="text/javascript" src="${ctx}js/logined/report/equipment/useEquipment.js"></script>
	
	</head>
	<body> 
			<div class="converse_right">
				<div class="canverse_right_two">
					<div class="canverse_right_tap">
						 <span id="callLogDetailSpan" class="active">临床科室设备使用报表</span> 
						<span>后勤人员配送报表</span> 
						<span>后勤科室归还报表</span>
					</div>					
					<div class="canverse_tap_area">
					     <jsp:include page="useEquipment.jsp"/> 
						 <jsp:include page="equipmentSend.jsp"/>  
						 <jsp:include page="equipmentReturn.jsp"/>
					</div>	
				</div>
			</div>
	</body>
</html>