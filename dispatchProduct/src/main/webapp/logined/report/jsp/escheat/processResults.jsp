<%--
  Created by IntelliJ IDEA.
  User: lilipo
  Date: 2017/5/13
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--处理结果满意度统计--%>
<div class="canverse_tap_box hide" >
     <div class="searchArea">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td class="right">
                    <label style="padding-left:20px;color:#999;font-size:14px;">时间：</label>
                    <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate_process\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="begDate_process" class="in_something2 pr Wdate" name=""/>
                    -
                    <input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'begDate_process\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="endDate_process" class="in_something2 pr Wdate" name=""/>
                    <button class="btn_jinhu_add" onclick="query_process()">查询</button>
                   <!--  <button class="btn_jinhu_add" onclick="report_export_process()">导出</button> -->
                    <button class="btn_jinhu_add" onclick="change_process(this,0)">图表视图</button>
                    <button class="btn_jinhu_add" id="list_button_p" onclick="change_process(this,-1)">列表视图</button>
                </td>
                <!-- <td style="width:110px;">
                    
                </td> -->
            </tr>
            </tbody>
        </table>
    </div>

    <div class="IllegalsNum" id="view_process" style="width: 70%;height:60%; margin-left:120px;"></div>
    <div class="list_IllegalsNum" id="list_process">
        <div id="table_wrapper" class="dataTables_wrapper" role="grid">
            <div id="myTable_Squadron_processing" class="dataTables_processing" style="visibility: hidden;">正在加载数据...</div>
            <table id="tables" class="pretty dataTable" width="100%" cellspacing="0" cellpadding="0" border="0">
                <colgroup>
	   			   <col width="80px">
	   			   <col width="120px">
	   			   <col>
	   			  <!--  <col width="100px"> -->
	   			   <col width="150px">
	   			   <!-- <col width="80px"> -->
	   			   <col width="150px">
	   			</colgroup>
                <thead>
                <tr>
                    <th >序号</th>
                    <th >姓名</th>
                    <th >所属部门</th>
                    <!-- <th >非常满意</th> -->
                    <th >满意</th>
                   <!--  <th >一般</th> -->
                    <th >不满意</th>
                </tr>
                </thead>
                <tbody id="tbody_process">

                </tbody>
            </table>
        </div>
    </div>
</div>