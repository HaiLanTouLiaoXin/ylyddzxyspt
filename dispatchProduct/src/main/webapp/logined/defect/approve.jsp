<%@page pageEncoding="UTF-8" %>
<%@ page import="cn.com.qytx.platform.org.domain.UserInfo" %>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("adminUser");
	String currUserId = "";
	String currUserName = "";
	if(userInfo!=null){
		currUserId = userInfo.getUserId().toString();
		currUserName = userInfo.getUserName();
	}
	request.setAttribute("currUserId", currUserId);
	request.setAttribute("currUserName", currUserName);
%>
<!DOCTYPE html>
<html ng-app="approveApp">
	<head>
		<meta charset="UTF-8">
		<title>值班签到缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/approveApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/approveController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
	</head>
	<body ng-controller="approveController" style="background-color:#fff;">
	<input type="hidden"  id="currUserId"  value="<%=currUserId%>"/>
	<input type="hidden"  id="index"  value="{{index}}"/>
		<div class="elasticFrame formPage">
			<form name="myForm" novalidate="novalidate">
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable">
			    <tbody>
			      <tr ng-if="dialogInfo.userTitle!=''">
			        <th><label>{{dialogInfo.userTitle}}：</label></th>
			        <td>
			        	<input type="hidden" id="processType" value="{{processType}}"/>
			        	<input type="hidden" id="userId" value="{{userId}}"/>
			        	<input type="hidden" id="showType" value="{{showType}}"/>
			        	<input type="hidden" id="extension" value="{{extension}}"/>
			        	<input class="formText" type="text"  id="userName" value="{{userName}}" style="width:250px" readonly="readonly">
			        	<span class="addMember" ng-show="!flag"><a
							class="icon_add" href="#" id="userSelect" fieldId="userId" fieldName="userName" fileType="processType" showType="showType" extension="extension">选择</a></span>
			        	<!-- <div class="divselect12  divselect13" style="width: 250px">
			        		<input type="hidden" value="{{node1 | selectUserIds}}" id="userId"/>
				      		<cite class="cite12 text_ellipsis cite13" style="width:220px"  ng-click="toggleSelectUser()" ng-show="!flag">{{node1 | selectUserNames}}</cite>
							<cite class="cite12 text_ellipsis cite13" style="width:220px;{{flag?'background:url()':''}}"  ng-show="flag">{{node1 | selectUserNames}}</cite>
				      		<ul id="tree2" class="ztree leibie" basetree tree-data="data1" tree-set="set1" check-node="node1" select-node="node" default-select="defaultSelectUser" ng-show="showSelectUserValue==1" style="display: block;height:150px !important;width:250px !important;z-index:20000;position:absolute;top:28px;left:0px;border:1px solid #d5d5d5;" ng-mouseleave="showSelectUserValue=2">
							</ul>
		  				</div> -->
			        </td>
			      </tr>
			      <tr ng-if="dialogInfo.adviceTitle!=''">
			        <th><label>{{dialogInfo.adviceTitle}}：</label></th>
			        <td>
			        	<!-- <input class="formText" type="text" name="" value="" placeholder="" id="advice" style="width:220px"> -->
			        	<textarea style="width: 250px; height: 70px; border: 1px solid #d9d9d9; outline: none;" id="advice"></textarea>
			        </td>
			      </tr>
			      <tr ng-if="index==2">
			        <th><label>满意度评价：</label></th>
			        <td>
			            <input type="radio" name="achievement" checked="checked" value="1"> 非常满意
			            <input type="radio" name="achievement"  value="2"> 满意
			            <input type="radio" name="achievement"  value="3"> 一般
			            <input type="radio" name="achievement"  value="4"> 不满意
			        	<!-- <select class="defect_select wd258" id="achievement">
							<option value="1">非常满意</option>
							<option value="2">满意</option>
							<option value="3">一般</option>
							<option value="4">不满意</option>
						</select> -->
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
	
	<script type="text/javascript">
	$(function(){
		var currUserId = "${currUserId}";
		var currUserName = "${currUserName}";
		var index = art.dialog.data("index");
		if(index==6){
			$("#userId").val(currUserId);
			$("#userName").val(currUserName);
			$("#extension").val("1,2");
			$("#processType").val(1);
		}
		if(index==10){
			$("#extension").val("1,2");
			$("#processType").val(1);
		}
	});
</script>
</html>
