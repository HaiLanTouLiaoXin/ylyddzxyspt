<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="myProcessApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>待我处理的</title>
<link href="${ctx}css/author_main.css" rel="stylesheet" type="text/css" />
<jsp:include page="../../common/flatHead.jsp" />
<script type="text/javascript">
	var basePath = "${ctx}";
	$(function(){
		$(".new_right_content").css({"height":$(window).height()});
	});
</script>
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-animate.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-sanitize.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basepage.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/myProcessApp.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/myProcessController.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
<style>
	.ng-cloak{display:none;}
</style>
</head>
<body ng-controller="myProcessController">
<input type="hidden" id="type" value="<%=request.getParameter("type") %>" />

<div class="new_right_content">
 	<div class="index_detail pb20">
		<p class="new_index_title">
			{{title}}
		</p>

<div class="list">
	<!-- <div class="searchArea">
				<table cellspacing="0" cellpadding="0">
						<tbody>
								<tr>
                                	<td class="right">&nbsp;</td>
									<td style="width:954px;">
									<span class="des_info_title">状态：</span>
									<select name="state" class="defect_select percent30" ng-model="searchVo.state">
										<option value="">全部</option>
										<option value="0,6">待接收</option>
										<option value="5">待作废</option>
										<option value="1,7">待消缺</option>
										<option value="2,8">待验收</option>
										<option value="3">待归档</option>
										<option value="4">已归档</option>
										<option value="9">已作废</option>
									</select>
									<span class="des_info_title">机组：</span>
									<select name="equipmentUnit" class="defect_select percent30" ng-model="searchVo.equipmentUnit">
										<option value="">全部</option>
										<option value="{{equipmentUnit.value}}" ng-repeat="equipmentUnit in equipmentUnitList">{{equipmentUnit.name}}</option>
									</select>
									<span class="des_info_title">部门：</span>
									<select name="defectDepartment" class="defect_select percent30" ng-model="searchVo.defectDepartment">
										<option value="">全部</option>
										<option value="{{defectDepartment.value}}" ng-repeat="defectDepartment in defectDepartmentList">{{defectDepartment.name}}</option>
									</select>
									<span class="des_info_title">专业：</span>
									<select name="defectProfessional" class="defect_select percent30" ng-model="searchVo.defectProfessional">
										<option value="">全部</option>
										<option value="{{defectProfessional.value}}" ng-repeat="defectProfessional in defectProfessionalList">{{defectProfessional.name}}</option>
									</select>
									<label>关键字：</label>
						              <span class="position:relative;">
						              <input type="text"  maxlength="30" class="formText searchkey" placeholder="请输入设备名称" ng-model="searchVo.searchName"/>
						              </span>
						              <input type="button" value="查询" class="searchButton" style="float:right" ng-click="searchList();"/>
                                    </td>
								</tr>
						</tbody>
				</table>
		</div> -->
		
		
		
		<div class="overf mb20">
				<ul class="select_list fl">
					<li>
					<span class="select_list_title">工单类型：</span>
						<select name="type" class="select_style" ng-model="searchVo.type">
							<option value="">全部</option>
							<!-- <option value="0">维修</option> -->
							<option value="1">配送</option>
							<option value="2">归还</option>
						</select>
					</li>
				
					<li style="padding-left: 55px;">
						<span class="select_list_title">状态：</span>
						<select name="state" class="select_style" ng-model="searchVo.state">
							<option value="">全部</option>
							<option value="-1">待调度</option>
							<option value="0,6">待接收</option>
							<!-- <option value="5">待作废</option> -->
							<option value="1,7">待处理</option>
							<option value="2,8">待验收</option>
							<option value="3">已完结</option>
							<!-- <option value="4">已归档</option> -->
							<option value="9">已作废</option>
						</select>
					</li>
					<!-- <li>
						<span class="select_list_title">机组：</span>
						<select name="equipmentUnit" class="select_style" ng-model="searchVo.equipmentUnit">
							<option value="">全部</option>
							<option value="{{equipmentUnit.value}}" ng-repeat="equipmentUnit in equipmentUnitList">{{equipmentUnit.name}}</option>
						</select>
					</li> -->
					<!-- <li>
						<span class="select_list_title">所属部门：</span>
						<select name="defectDepartment" class="select_style" ng-model="searchVo.defectDepartment">
							<option value="">全部</option>
							<option value="{{defectDepartment.value}}" ng-repeat="defectDepartment in defectDepartmentList">{{defectDepartment.name}}</option>
						</select>
					</li> -->
					<!-- <li>
						<span class="select_list_title">专业：</span>
						<select name="defectProfessional" class="select_style" ng-model="searchVo.defectProfessional">
							<option value="">全部</option>
							<option value="{{defectProfessional.value}}" ng-repeat="defectProfessional in defectProfessionalList">{{defectProfessional.name}}</option>
						</select>
					</li> -->
					<li  style="width: 436px;">
						<span class="select_list_title">上报时间：</span>
						<input id="startTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',maxDate:'#F{$dp.$D(\'endTime\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
						<input id="endTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'startTime\')}'})" class="in_area Wdate" style="width:170px"/>
					</li>
					<li style="padding-left: 70px;">
						<span class="select_list_title">关键字：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入名称" ng-model="searchVo.searchName"/>
					</li>
				</ul>
				<div class="fl pd4">					
					<span class="btn_inline01 btn_search_new" ng-click="searchList();">查找</span>
				</div>
			</div>
		
		
		<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">
		<thead>
		<tr>
        	<th  style="width:100px;">工单</th>
        	<th style="width:50px">类型</th>
        	<th>处理事项</th>
        	<th style="width:60px">是否紧急</th>
        	<th style="width:50px">上报人</th>
        	<th style="width:100px">上报部门</th>
       		<th  style="width:140px;">地点</th>
       		<th  style="width:150px;">上报时间</th>
			<!-- <th  style="width:100px;">完工时间</th> -->
        	<th  style="width:70px;" class="data_r">工单状态</th>        	
        	<th  style="width:110px;" class="data_r">当前操作人/部门</th>
        	<th  style="width:50px;" class="data_r">操作</th>
     	 </tr>
		</thead>
		<tbody>
	     		<tr ng-repeat="apply in aaData" ng-class-odd="'odd'" ng-class-even="'even'"
	     		style="font-size: 14px;" onClick="xuanRan(this)">
		        	<td class="tdCenter" ng-cloak>{{apply.defectNumber}}</td>
		        	<td class="tdCenter" ng-cloak>{{apply.type==2?"归还":(apply.type==1?"配送":"维修")}}</td>
		        	<!-- <td class="data_r" ng-cloak>{{apply.equipmentUnit}}</td>
		        	<td class="data_r" ng-cloak>{{apply.defectProfessional}}</td> -->
		        	<td class="longTxt" title="{{apply.equipmentName}}{{apply.type==2||apply.type==1?'&nbsp;(数量:'+apply.equipmentNumber+')':''}}" ng-cloak>{{apply.equipmentName}}{{apply.type==2||apply.type==1?"&nbsp;(数量:"+apply.equipmentNumber+")":""}}</td>
		        	<td class="tdCenter" ng-cloak style="{{apply.grade==1?'color:red':''}}">{{apply.grade==1?'紧急':'一般'}}</td>
		        	<!-- <td class="tdCenter">{{apply.equipmentState}}</td> -->
		        	<td class="tdCenter" ng-cloak>{{apply.createUserName}}</td>
		        	<td class="longTxt" title="{{apply.groupName}}" ng-cloak>{{apply.groupName}}</td>
		        	<td class="longTxt" title="{{apply.describe}}" ng-cloak>{{apply.describe}} </td>
		        	<td class="tdCenter"  ng-cloak>{{apply.createTime}} </td>
			        <!-- <td class="tdCenter"  ng-cloak style="{{apply.outTimeState==1?'color:red':''}}">{{apply.outTimeState==0?'剩余'+apply.hour+'小时':(apply.outTimeState==1?('逾期'+apply.hour+'小时'):apply.defectTime)}} </td> -->
		        	<!-- <td class="data_r" ng-cloak>{{apply.defectDepartment}}</td> -->
		        	<td class="tdCenter" ng-cloak><span class="{{apply.state|stateColor}}">{{apply.state}}{{apply.delayState}}</span></td>
		        	<!-- <td class="tdCenter"  ng-cloak>{{apply.updateTime}}</td> -->
		        	<td class="tdCenter" ng-cloak>{{apply.processName}}</td>
		        	<td class="data_r" ng-cloak><a href="javascript:void(0);" ng-click="goView(apply.instanceId,apply.applyState,apply.type)">查看</a></td>
		         </tr>
		      	<tr class="odd" ng-show="result==0"><td valign="top" colspan="11" class="dataTables_empty">没有检索到数据</td></tr>
		    </tbody>
	</table>
	<!-- 分页 -->
	<page page-search="getList()" i-total-display-records="result" i-display-length="iDisplayLength" i-display-start = "iDisplayStart" ></page>
	</div>
 </div>
</div>
</body>
</html>