<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<meta charset="utf-8">
<title>物资管理</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/tree/skins/tree_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/datatable/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/datatable/selecedForDatatablePagination.js"></script>
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/treeNode.js"></script>
<script type="text/javascript" src="${ctx}js/logined/defect/controller/goodAddView.js"></script>
<script type="text/javascript">
	$(function(){
		var treeHeight = $(window).height()-90;
		$(".zTreeDemoBackground").height(treeHeight);
		$("#myTree").height(treeHeight);
		$(".center_content").height(treeHeight);
	})
</script>
<body>
<div class="mainpage_r">
<input id="selectGroupId" type="hidden">
	<!-- 左侧树 -->
	<div class="leftMenu">
		<div class="service-menu">
			<h1>物资组</h1>
			<div class="zTreeDemoBackground">
				<ul id="myTree" class="ztree"></ul>
			</div>
		</div>
	</div>
	<!-- 右侧列表 -->
	<div class="list overf list-max1160" style="overflow:visible;min-width:1160px">
		<div class="searchArea overf" style="overflow:visible;">
			<table cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td class="right"  >
						     <input style="float:right;" onClick="serch()" value="查询" class="searchButton" type="button" />
							<div style="float:right;">
			                	<label>类型：</label>
				                <select id="goodType" style="height: 30px;width: 130px;border-radius:3px;" >
				                	<option value="">全部</option>
				                	<option value="1">备件</option>
				                	<option value="2">材料</option>
				                	<option value="3">设备</option>
				                	<option value="4">仪表</option>
				                	<option value="5">其他</option>
				                </select>
			                </div>
			                <div style="float:right;">
								<label>编号：</label>
			                	<input id="goodNo" type="text" class="formText" style="width: 220px;border-radius: 3px;height: 30px;" placeholder="请输入编号"/>
							</div>
			                <div style="float:right;">
								<label>物资名称：</label>
			                	<input id="goodName" type="text" class="formText" style="width: 220px;border-radius: 3px;height: 30px;" placeholder="请输入物资名称"/>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="center_content">
			<table id="goodTable" cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" >
				<thead>
					<tr>
			       	 	<th style="width:42px;">序号</th>
			        	<th style="width:120px;">物资名称</th>
			        	<th style="width:80px;">编号</th>
			        	<th style="width:120px;">物资组</th>
			       		<th style="width:90px;">类型</th>
			        	<th style="width:90px;">规格</th>        	
			        	<th style="width:90px;">单位</th>
			        	<th style="width:80px;">单价</th>
			        	<th style="width:120px;">操作</th>
			     	</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
</body>
</html>