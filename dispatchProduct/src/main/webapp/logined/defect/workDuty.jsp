<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="workDutyApp">
	<head>
		<meta charset="UTF-8">
		<title>值班签到缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/workDutyApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/workDutyController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
	</head>
	<body ng-controller="workDutyController" style="background-color:#fff;">
		<div class="elasticFrame formPage">
			<form name="myForm" novalidate="novalidate">
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable">
			    <tbody>
			      <tr>
			        <th><em class="requireField">*</em><label>姓名：</label></th>
			        <td>{{userName}}</td>
			      </tr>
			      <tr>
			        <th><em class="requireField">*</em><label>值班岗位：</label></th>
			        <td>
			        		<select name="dutyDepartment" class="defect_select" ng-model="dutyDepartment" required>
								<option value="">请选择</option>
								<option value="{{dutyDepartment.value}}" ng-repeat="dutyDepartment in dutyDepartmentList">{{dutyDepartment.name}}</option>
							</select>
							<p style="color:red;" class="error" ng-show="myForm.dutyDepartment.$error.required&&(!myForm.dutyDepartment.$pristine||submitted)">请选择值班岗位</p>
			        	
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
</html>
