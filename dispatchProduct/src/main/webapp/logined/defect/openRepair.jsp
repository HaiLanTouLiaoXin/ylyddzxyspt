<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html >
  <head>
    <base href="<%=basePath%>">
    <title>事件上报</title>
    <jsp:include page="../../common/flatHead.jsp" />
    <meta charset="UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript">
	    document.domain = '10.10.90.30';
		var url = basePath+"login/loginSmp.action";
		var loginName = "${adminUser.workNo}";
		var isLogin = false;
		var errorMessage = "";
		$.ajax({
		    url : url,
		    type : "post",
		    dataType : 'json',
		    data : {
			    "loginName" : loginName,
			    "loginType":"eventAdd",
			    "_clientType":"wap"
		    },
		    success : function(data) {
		    	if(data.success){
		    		isLogin = true;
		    		window.open(data.loginUrl+"&sysOpen=true&telNum=","_self");
		    		/* art.dialog.open(data.loginUrl+"&sysOpen=true&telNum="+phone, {
						id : 'zwwx',
						title : '总务维修',
						width : 1250,
						height : 680,
						lock : true,
						opacity : 0.08,
						button : [ {
							name : '关闭',
							callback : function() {
								return true;
							}
						} ]
					}, false); */
		    	}else{
		    		errorMessage = data.message;
		    		art.dialog.alert(errorMessage);
		    		window.open("http://117.159.26.230:6060/smp/loginController.do?login","_self");
		    	}
		    }

		});
		
	</script>
	
  </head>
</html>
