<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="reportApp">
	<head>
		<meta charset="UTF-8">
		<title>上报缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.hello{display:block;background-color: red}
			.ech_box{width:100%;overflow:hidden;}
			.ech_box div{width:50%;float:left;overflow:hidden;height:400px;}
		</style>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script TYPE="text/javascript" src="${ctx}js/common/echarts.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/reportApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/reportController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
	</head>
	<body ng-controller="reportController">
		<div class="list">
		<div class="searchArea">
				<table cellspacing="0" cellpadding="0">
						<tbody>
								<tr>
                                	<td class="right" style="text-align:left;text-indent:10px;font-size:16px;color:#25b6ed;">缺陷图表</td>
									<td style="width:657px;">
									<span class="des_info_title">时间：</span>
									<input type="text"  class="formText Wdate" id="startTime"  ng-model="searchVo.startTime" onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})"/>-
									<input type="text"  class="formText Wdate" id="endTime"  ng-model="searchVo.endTime" onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})"/>
									<!-- <span class="des_info_title">部门：</span>
									<select name="defectDepartment" class="defect_select percent30" ng-model="searchVo.defectDepartment">
										<option value="">全部</option> 
										<option value="{{defectDepartment.value}}" ng-repeat="defectDepartment in defectDepartmentList">{{defectDepartment.name}}</option>
									</select> -->
									
									<!-- <span class="des_info_title">专业：</span>
									<select name="defectProfessional" class="defect_select percent30" ng-model="searchVo.defectProfessional">
										<option value="">全部</option>
										<option value="{{defectProfessional.value}}" ng-repeat="defectProfessional in defectProfessionalList">{{defectProfessional.name}}</option>
									</select>
									<span class="des_info_title">机组：</span>
									<select name="equipmentUnit" class="defect_select percent30" ng-model="searchVo.equipmentUnit">
										<option value="">全部</option>
										<option value="{{equipmentUnit.value}}" ng-repeat="equipmentUnit in equipmentUnitList">{{equipmentUnit.name}}</option>
									</select> -->
						              <input type="button" value="查询" class="searchButton" ng-click="getReport();"/>
                                    </td>
								</tr>
						</tbody>
				</table>
		</div>
		</div>
		<div class="ech_box">
			<div id="main1"></div>
			<div id="main2"></div>
			<div id="main3"></div>
			<div id="main4"></div>
		</div>
		
	</body>
</html>
