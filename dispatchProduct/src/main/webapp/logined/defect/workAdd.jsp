<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html >
  <head>
    <base href="<%=basePath%>">
    <title>事件上报</title>
    <jsp:include page="../../common/flatHead.jsp" />
    <meta charset="UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript">
	    document.domain = '10.10.90.30';
		var basePath = "${ctx}";
		var baseUrl="${baseUrl}";
		$(function(){
			$(".new_right_content").css({"height":$(window).height()});
		});
	</script>
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
	<link rel="stylesheet" href="${ctx}/css/jquery.autocomplete.css" />
	
	<link href="${ctx}logined/defect/css/hm_style.css" rel="stylesheet">
	<link href="${ctx}logined/defect/css/form_default.css" rel="stylesheet">
	<link href="${ctx}logined/defect/css/style.css" rel="stylesheet">
  </head>
  
<body >
		
		<div class="new_right_content" style="height:690px">
			<div class="index_detail pdb8">
				<div class="tab-area report-tab-area">
					<ul>
						 <li class="fl" style="width: 50%">
							<a href="javascript:;" class="current" >维修登记</a>
						</li> 
						<li class="fl" style="width: 50%">
							<a href="javascript:;">配送登记</a>
						</li>
						<!-- <li class="fl" >
							<a href="javascript:;">归还登记</a>
						</li> -->
					</ul>
				</div>
				<!--end .tab-area-->
				
				
				<div  class="tab-content">
					<iframe src="${ctx}logined/defect/openRepair.jsp" id="content" frameborder="0" width="100%" height="100%"></iframe>
					
				</div>
				<!--end .tab-content-->
				
				<%-- <div   <%if("1".equals(type)||"2".equals(type)){%> class="tab-content"<%}else{%>class="tab-content hide"<%} %>>
					<iframe src="${ctx}logined/defect/eventDistribution.jsp" id="content" frameborder="0" width="100%" height="100%"></iframe>
				</div> --%>
				<!--end .tab-content-->
				
			</div>
			<!--end .index_detail-->
		</div>
		<!--end .new_right_content-->
		
		<script>
			//click on tab 
			$(".tab-area ul li a").on("click",function(){
				if($(this).hasClass("current"))
				return;
				 
				//var type =$("#type").val();
				var index = $(".tab-area ul li a").index($(this));
				$(".tab-area ul li a").removeClass("current");
				$(".tab-area ul li a").eq(index).addClass("current");
				 /*  if(type&&index==null){
					  index = type;
				  } */
				  /* if(index==2){
					  index = 1;
				  } */
				/* $(".tab-content").fadeIn(500);
				$(".tab-content").addClass("hide");
				$(".tab-content").eq(index).removeClass("hide"); */
				 if(index==0){
					document.getElementById("content").src="${ctx}logined/defect/openRepair.jsp";
					$("#content").attr("height","100%");
			  	}else if(index==1){
			  		document.getElementById("content").src="${ctx}logined/defect/distributionAdd.jsp?type=1";
			  		$("#content").attr("height","75%");
			  	}else if(index==2){
			  		document.getElementById("content").src="${ctx}logined/defect/distributionAdd.jsp?type=2";
			  		$("#content").attr("height","75%");
			  	} 
				/* if(index==0){
			  		document.getElementById("content").src="${ctx}logined/defect/distributionAdd.jsp?type=1";
			  	}else if(index==1){
			  		document.getElementById("content").src="${ctx}logined/defect/distributionAdd.jsp?type=2";
			  	} */
			});
		</script>
	</body>
</html>
