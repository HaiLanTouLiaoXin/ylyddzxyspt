<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="myProcessApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>维护</title>
<link href="${ctx}css/author_main.css" rel="stylesheet" type="text/css" />
<jsp:include page="../../common/flatHead.jsp" />
<script type="text/javascript">
	var basePath = "${ctx}";
	$(function(){
		$(".new_right_content").css({"height":$(window).height()});
	});
</script>
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-animate.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-sanitize.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basepage.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/myProcessApp.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/myProcessController.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
<style>
	.ng-cloak{display:none;}
</style>
<script type="text/javascript">

function addMaintenance(){
	url = basePath+"logined/defect/addMaintenance.jsp";
	art.dialog.open(  url,{
		title : "新增维护标题",
		width : 480,
		height : 220,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		close:function(){
			return true;
	   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
	});
}

</script>
</head>
<body ng-controller="myProcessController">
<div class="new_right_content">
 	<div class="index_detail wd1362 pb20">
		<p class="new_index_title mb20">
			维护信息
		</p>

<div class="list">
		<div class="overf mb20">
				<ul class="select_list fl">
					<li>
						<span class="select_list_title">紧急程度：</span>
						<select name="state" class="select_style" >
							<option value="1">紧急</option>
							<option value="2">一般</option>
							<option value="3">普通</option>
						</select>
					</li>
					<li>
						<span class="select_list_title">维修部门：</span>
						<select name="state" class="select_style" >
							<option value="1">调度中心</option>
						</select>
					</li>
					<li>
						<span class="select_list_title">维修标题：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入维修标题" ng-model="searchVo.searchName"/>
					    
					</li>
				</ul>
				<div class="fl pd4">					
					<span class="btn_inline01 btn_search_new" ng-click="searchList();">查找</span>
					   <span class="btn_inline01 btn_add_new" onclick="addMaintenance()">新增</span>
					    <span class="btn_inline01 btn_output_new" ng-click="exportApply();" style="cursor: pointer">导出</span>
				</div>
			</div>
		
		
		<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">
		<thead>
		<tr>
        	<th  style="width:100px;">序号</th>
        	<!-- <th  style="width:90px;" class="data_r">所属机组</th>
        	<th  style="width:90px;" class="data_r">所属专业</th> -->
        	<th style="width:35%">维修标题</th>
        	<!-- <th  style="width:40%;" class="data_r">设备状态</th> -->
       		<th  style="width:200px;">维修部门</th>
        	<!-- <th  style="width:120px;" class="data_r">所属部门</th> -->
        	<th  style="width:90px;" class="data_r">紧急程度</th>        	
        	<th  style="width:150px;" class="data_r">创建时间</th>        	
        	<th  style="width:120px;" class="data_r">创建人</th>
        	<th  style="width:90px;" class="data_r">操作</th>
     	 </tr>
		</thead>
		<tbody>
	     		<tr  class="'even'"style="font-size: 14px;">
		        	<td class="tdCenter" ng-cloak>00001</td>
		        	<!-- <td class="data_r" ng-cloak>{{apply.equipmentUnit}}</td>
		        	<td class="data_r" ng-cloak>{{apply.defectProfessional}}</td> -->
		        	<td class="longTxt" ng-cloak title="{{apply.equipmentName}}">电灯不亮</td>
		        	<!-- <td class="tdCenter">{{apply.equipmentState}}</td> -->
		        	<td class="longTxt" title="{{apply.describe}}" ng-cloak>调度中心 </td>
		        	<!-- <td class="data_r" ng-cloak>{{apply.defectDepartment}}</td> -->
		        	<td class="tdCenter" ng-cloak><span class="{{apply.state|stateColor}}">紧急</span></td>
		        	<td class="tdCenter"  ng-cloak>2017-03-07</td>
		        	<td class="data_r" ng-cloak>调度中心</td>
		        	<td class="data_r" ng-cloak><a href="javascript:void(0);" >修改</a><a href="javascript:void(0);" >删除</a></td>
		         </tr>
		      	<tr  class="'even'"style="font-size: 14px;">
		        	<td class="tdCenter" ng-cloak>00001</td>
		        	<!-- <td class="data_r" ng-cloak>{{apply.equipmentUnit}}</td>
		        	<td class="data_r" ng-cloak>{{apply.defectProfessional}}</td> -->
		        	<td class="longTxt" ng-cloak title="{{apply.equipmentName}}">灯不亮</td>
		        	<!-- <td class="tdCenter">{{apply.equipmentState}}</td> -->
		        	<td class="longTxt" title="{{apply.describe}}" ng-cloak>调度中心 </td>
		        	<!-- <td class="data_r" ng-cloak>{{apply.defectDepartment}}</td> -->
		        	<td class="tdCenter" ng-cloak><span class="{{apply.state|stateColor}}">一般</span></td>
		        	<td class="tdCenter"  ng-cloak>2017-03-07</td>
		        	<td class="data_r" ng-cloak>调度中心</td>
		        	<td class="data_r" ng-cloak><a href="javascript:void(0);" >修改</a><a href="javascript:void(0);" >删除</a></td>
		         </tr>
		    </tbody>
	</table>
	<!-- 分页 -->
	   <div class='converse_page1 bgf2' ng-show='iTotalDisplayRecords>0'>
				<span class='date_num'>共<span>2</span>条数据</span>
				<div class='page_num'>
				<span class='lt' ><em></em></span><a >1</a>
				<span class='gt'><em></em></span>
				</div>
				</div>
	</div>
 </div>
</div>
</body>
</html>