<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>设备详情</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/form/skins/form_default.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/datatable/skins/datatable_default.css"
	rel="stylesheet" type="text/css" />
	<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/capitalService.js"></script>
<style>
.inputTable th {
	width: 105px;
}
.big_title {
	border-bottom: 1px solid #017a8f;
	color: #017a8f;
	font-size: 16px;
	height: 40px;
	line-height: 45px;
	overflow: hidden;
	padding: 0 10px;
	text-align: left;
}
</style>
</head>
<body>
<input type="hidden" id="capitalId" value="${param.capitalId }">
	<div class="formPage" style="padding-top:0px;width:605px">
		<div class="formbg">
			<div class="content_form">
				<table width="100%" cellspacing="0" cellpadding="0" border="0"
					class="inputTable" style="font-size:14px;padding-top: 0px">
					<tbody>
						<tr>
							<th>设备名称：</th>
							<td id="name"></td>
							<th>设备编号：</th>
							<td id="capitalNo"></td>
						</tr>
						<tr>
						    <th>设备型号：</th>
							<td id="capitalModel"></td>
							<th>设备组：</th>
							<td id="capitalGroup"></td>
							
						</tr>
						<tr id = "managerUserGroup" >
						    <th>使用科室：</th>
						    <td colspan="3">
						    	<input type="hidden" id="useGroupId" />
								<input type="hidden" id="showType" value="1"/>
					  			<input class="in_area" type="text" id="useGroupName2" readonly="readonly" style="width: 258px" >
					  			<span class="addMember"><a
									class="icon_add" href="#" id="createGroupSelect" fieldId="useGroupId" fieldName="useGroupName2"  showType="showType" >选择</a></span>
							</td>
						</tr>
						<tr>
							<th>使用地点：</th>
							<td colspan="3">
								<input type="hidden" id="repaireId"/>
							    <input type="hidden" id="showType_repair" value="5"/>
					 			<input class="in_area" type="text" id="location"  style="width: 258px" errmsg="capital.location_not_null" valid="required">
					 			<span class="addMember " ><a class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="location" showType="showType_repair">选择</a></span>
							</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>