<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html ng-app="meetingstatisticsApp">
<meta charset="utf-8">
<title>资产列表</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}/flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_page.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />

<!--angularjs类库-->
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular.min.js"></script>
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular-animate.min.js"></script>
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular-sanitize.min.js"></script>
<!-- My97 -->
<script type="text/javascript" src="${ctx }angularframe/directives/ui-datePicker.js"></script>
<!-- 分页 -->
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basepage.js"></script>
<!--bootstrapUI控件的angularjs版本-->
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/ui-bootstrap-tpls-1.3.3.min.js"></script>
<!-- tree -->
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>

<script TYPE="text/javascript" src="${ctx }js/logined/capital/test_app.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/meeting/service/meetingstatisticsService.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/controller/test.js"></script>

<body ng-controller="mainCtrl">
	<!-- 左侧树 -->
	<div class="leftMenu" ng-controller="treeCtrl" ng-cloak>
		<div class="service-menu">
			<h1>资产组</h1>
			<div class="zTreeDemoBackground">
				<ul id="myTree" basetree tree-data="data" select-node="node"  class="ztree"></ul>
			</div>
		</div>
	</div>
	<!-- 右侧列表 -->
 	<div class="list" ng-controller="meetingstatisticsCtrl" ng-cloak>
	<div class="searchArea">
			<table cellspacing="0" cellpadding="0">
			<tbody>
			   <tr>
				<td class="right" >
				  <label>会议室：</label>
                  <select ng-model="meetingId"  ng-options="meetingRoom.id as meetingRoom.name for meetingRoom in meetingRoomList" style="height: 26px;" >
                     <option value="">全部</option>                
                  </select>
                  <label>申请日期：</label>
                  <input type="text" ng-model="startTime" date-picker config="config" style="width:150px;" class="Wdate formText"/>&nbsp;-&nbsp;
                  <input type="text" ng-model="endTime" date-picker config="config" style="width:150px;" class="Wdate formText"/>
				<input hideFocus="" ng-click="serch()" value="查询" class="searchButton" type="button" id="searchButton" />                               
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">
		<thead>
		<tr>
		    <th  class="chk"><input type='checkbox' id='total'/></th>
       	 	<th  class="num">序号</th>
        	<th  style="width:100px;">资产名称</th>
        	<th style="width:60%;">资产组</th>
        	<th  style="width:40%;" class="data_r">入库日期</th>
       		<th  style="width:255px;">资产编号</th>
        	<th  style="width:60px;" class="data_r">资产状态</th>        	
        	<th  style="width:90px;" class="data_r">存放位置</th>
        	<th  style="width:90px;" class="data_r">使用人</th>
        	<th  style="width:90px;" class="data_r">操作</th>
     	 </tr>
		</thead>
		<tbody>
	     		<tr ng-repeat="applyRecord in aaData" ng-class-odd="'odd'" ng-class-even="'even'"
	     		style="font-size: 14px;">
	     		    <td  class="chk"><input type='checkbox'/></td>
		        	<td >{{applyRecord.no}}</td>
		        	<td class="tdCenter">{{applyRecord.createTime}}</td>
		        	<td class="longTxt" title="{{applyRecord.name}}">{{applyRecord.name}}</td>
		        	<td class="longTxt" title="{{applyRecord.meetingRoomName}}">{{applyRecord.meetingRoomName}} </td>
		        	<td class="longTxt">{{applyRecord.meetTime}}</td>
		        	<td class="data_r">{{applyRecord.occupancyNumber}}</td>
		        	<td class="data_r">{{applyRecord.fee}}</td>
		         </tr>
		      	<tr class="odd" ng-show="result==0"><td valign="top" colspan="9" class="dataTables_empty">没有检索到数据</td></tr>
		    </tbody>
	</table>
	<!-- 分页 -->
	<page page-search="getList()" i-total-display-records="result" i-display-length="iDisplayLength" i-display-start = "iDisplayStart" ></page>
</div>
</body>
</html>