	<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>默认部门设置</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<style type="text/css">
			.index_detail.no-border {
				width: 550px;
			    margin-left: 120px;
			    border-bottom: 1px dashed #e5e5e5;
			    padding: 0 0 20px 0;
			}
			.index_detail .title-change {
			    font-weight: bold;
			    padding-left: 21px;
			    margin-bottom: 5px;
			}
			.index_detail.no-border .describle_info {
				padding-left: 0;
			}
			.addMember.width120 {
				width: 120px;
			}
			.addMember .save {
				float: right;
			}
			.hello{display:block;background-color: red}
			.uploadify-button {background-repeat:no-repeat;background-position:center left;}
			.annex ul li{float:left;margin-right:10px;}
			.txt p a{color: #ff5454 !important;}
			.selectLocation:hover{background-color: #e6e6e6}
			.new_right_content.no-padding {
					padding:0;
					background: none;
			}
		</style>
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl="${baseUrl}";
			$(function(){
				$(".new_right_content").css({"height":$(window).height()});
			});
		</script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/setDepartment/selectUser.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/setDepartment/defaultDepartmentSet.js"></script>
	</head>
	<body>
		<input type="hidden" id="showType" value="1"/>
		<input type="hidden" id="processType" value="2">
		<input type="hidden" id="extension" value="1,2"/>
		<form novalidate="novalidate" name="myForm">
		
		<div class="new_right_content no-padding">
			<p class="new_index_title mb20" style="padding-left: 20px;">
				默认部门
			</p>
			<div class="index_detail no-border">
				<p class="title-change">维修事件</p>
				<ul class="describle_info" style="overflow:initial;">
					<li class="pr" style="border: none;">
						<span class="des_info_title" style="width: 120px;" >默认处理部门：</span>
						<input type="hidden" id="processId" value=""/>
						<input type="hidden" id="idOne" value=""/>
			  			<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px" value="">
			  			<span class="addMember width120">
			  				<a class="icon_add" href="#" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType" showType="showType">选择</a>
			  				<a href="javascript:;" class="save" id="saveOne">保存</a>
			  			</span>
					</li>
				</ul>
		   </div>
		   <div class="index_detail no-border">
				<p class="title-change">维修事件</p>
				<ul class="describle_info" style="overflow:initial;">
					<li class="pr" style="border: none;">
			  			<span class="des_info_title" style="width: 120px;" >默认处理部门：</span>
						<input type="hidden" id="distributionId" value=""/>
						<input type="hidden" id="idTwo" value=""/>
			  			<input class="in_area" type="text" id="distributionName" readonly="readonly" style="width: 258px" value="">
			  			<span class="addMember width120">
			  				<a class="icon_add" href="#" id="distributionSelect" fieldId="distributionId" fieldName="distributionName" fileType="processType" showType="showType">选择</a>
			  				<a href="javascript:;" class="save" id="saveTwo">保存</a>
			  			</span>
					</li>
				</ul>
		   </div>
		   <div class="index_detail no-border">
				<p class="title-change">维修事件</p>
				<ul class="describle_info" style="overflow:initial;">
					<li class="pr" style="border: none;">
						<span class="des_info_title" style="width: 120px;" >默认处理部门：</span>
						<input type="hidden" id="returnId" value=""/>
						<input type="hidden" id="idThree" value=""/>
			  			<input class="in_area" type="text" id="returnName" readonly="readonly" style="width: 258px" value="">
			  			<span class="addMember width120">
			  				<a class="icon_add" href="#" id="returnSelect" fieldId="returnId" fieldName="returnName" fileType="processType" showType="showType">选择</a>
			  				<a href="javascript:;" class="save" id="saveThree">保存</a>
			  			</span>
					</li>
				</ul>
		   </div>
		</div>
		</form>
	</body>
</html>
