<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="viewApp">
	<head>
		<meta charset="UTF-8">
		<title>工单详情</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="css/screen.css" />
		<link rel="stylesheet" href="css/step.css" />
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl = "${baseUrl}";
		</script>
		<script src="${ctx}plugins/ngDialog/js/ngDialog.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/carDispatch/view.js"></script>
		<style>
			.ngdialog.ngdialog-theme-default .ngdialog-content{padding:0px;padding-bottom:1em;}
			.describle_title{border-bottom:1px solid #ff9933;}
			.in_area{width:100%;}
			.ngdialog-buttons{padding:0 10px;}
			.des_photos_box{position:relative;overflow:initial;}
			.des_photos_box img:hover{transform:scale(10,10);position:absolute;top:0px;left:150px;background:#fff;z-index:999;box-shadow: 0 0 5px #333;}
			.paizhao{overflow:initial !important;min-height:76px;}
			.ng-cloak{display:none;}
			
			.ScoreTableBoxContainer {padding-left: 30px; margin-top: 20px;}
			.ScoreTableBoxContainer .score_table_box {padding: 0 120px;}
			.ScoreTableBoxContainer .TableTitle {
				font-size: 14px;
				position: absolute;
			    top: -8px;
			    left: 0;
			    width: 129px;
			    text-align: right;
			    line-height: 30px;
			}
		</style>
	</head>
	<body >
		<input type="hidden" id="instanceId" value="<%=request.getParameter("instanceId") %>">
		<input type="hidden" id="currentTab" value="${param.currentTab}">
		<input id="viewType" type="hidden" value="${param.viewType}">
		<!--右侧描述信息标题及列表-->
		<!-- <div>
			<p class="describle_title">缺陷信息</p> -->
			
		<div class="new_right_content">
			<div class="index_detail pdb8" >
			<div class="new_index_title mb20 fixed">
					工单详情
					<div class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
						<span class="btn_inline_new hm_btn_white ml15" onClick="backToList();"> &nbsp;返回&nbsp; </span>
					</div>
			</div>
			<iframe style="width: 100%;display: none;border:none;height:567px;" id="yltView" src="http://10.10.90.30:8081/zxyy/cloud/callLog/carDispatch/jsp/carDispatchView.jsp?carOrderNo=${param.instanceId}&status=${param.currentTab}&_clientType=wap"></iframe>
			
			
		<!--startprint-->
	<ul class="describle_info special" id="carDispatchView" >
		<li class="pr percent30 fl" id="">
			<span class="des_info_title">工单编号：</span>
			<span class="des_info_decration" ><%=request.getParameter("instanceId") %></span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">工单状态：</span>
			<span class="des_info_decration" id="stateStr">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">工单类别：</span>
			<span class="des_info_decration" id="orderType">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">派遣类别：</span>
			<span class="des_info_decration" id="businessType">--</span>
		</li>
	</ul>
	<ul class="describle_info special" style="margin-top: 15px;">
		<li class="pr" style="text-align: center; line-height: 35px;background-color: #fafafa; padding-left: 0;">
			<span>车辆派遣</span>
		</li>
		<li class="pr percent30 fl" id="">
			<span class="des_info_title">车辆类型：</span>
			<span class="des_info_decration" id="carTypeStr">--</span>
		</li>
		<li class="pr percent30 fl" id="">
			<span class="des_info_title">用车内容：</span>
			<span class="des_info_decration" id="carContent">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">处理科室：</span>
			<span class="des_info_decration" id="processDepartment">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">出发地点：</span>
			<span class="des_info_decration" id="startLocation">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">到达地点：</span>
			<span class="des_info_decration" id="arriveLocation">--</span>
		</li>
		<li class="pr percent30 fl">
			<span class="des_info_title">计划发车时间：</span>
			<span class="des_info_decration" id="planStartTime">--</span>
		</li>
	</ul>
				
	<div class="TabReviseContainer">
	    <div class="tab-box">
	        <ul>
	        	<li><span class="tab current">处理流程</span></li>
	        </ul>
	    </div>
	    <!-- end .tab-box -->
	    <div class="tab-revise-content">
			<ul class="event-status-list" id="history">
				<li >
					<div class="info-box">
						<p class="title">
							<span class="time">11</span>
							<span class="style">2222</span>
						 	
						</p>
					</div>
					<p class="prompt" >备注：3333</p>
					<!--end .info-box-->
				</li>
			</ul>
	    </div>
	    <!-- end .tab-revise-content -->
	</div>
				<!-- end .TabReviseContainer -->
			</div>
		</div>
    	<script>
	    	$(".tab-box ul li .tab").click(function(){
	    	    if($(this).hasClass("current"))
	    	    return;
	    			 
	    	    var index = $(".tab-box ul li .tab").index($(this));
	    	    $(".tab-box ul li .tab").removeClass("current");
	    	    $(".tab-box ul li .tab").eq(index).addClass("current");
	    			  
	    	    $(".tab-revise-content").addClass("hide");
	    	    $(".tab-revise-content").eq(index).removeClass("hide");
	    	})
    	</script>
	</body>
	
	
</html>
