<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="css/style.css">			
		<link href="css/reset.css" rel="stylesheet" type="text/css" />
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
		<link href="css/datatable_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<style type="text/css">
			.inputTable th{width:82px;}
			.searchArea{padding:19px 0 12px 0;}
  			.dataTables_wrapper{
			 padding: 0 20px;
			}
			.canverse_right_tap{
			 height:59px;
			}
			.canverse_right_tap span{
			 padding:6px 10px;
			}
			.canverse_tap_area{
			 /* margin:12px 10px; */
			}
			.paging_full_numbers {
				width: auto;
				margin-top: 17px;
			}
		</style>
			
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/selecedForDatatablePagination.js"></script>
		<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
		<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
		<script type="text/javascript" src="${ctx}/plugins/workview/echarts.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/carDispatch/driverList.js"></script>
	
	</head>
<body> 
<input type="hidden" id="driverId" value="" />
<input type="hidden" id="driverName" value="" /><!--  -->
<input type="hidden" id="driverPhone" value="" />
<input type="hidden" id="driverStatus" value="" />
<div class="canverse_tap_box" id="waitDispatchDiv">
	<div class="list">
		<table width="100%" border="0" cellpadding="0" cellspacing="0"  class="pretty dataTable" id="myTable" >
		<thead>
			<tr>
				<th style="width:60px;"></th>
				<th style="width:60px;">序号</th>
				<th style="width:100px;">驾驶人</th>
				<th style="width:120px;">驾驶人手机</th>
				<th style="width:100px;">驾驶人状态</th>
			</tr>
		</thead>
		<tbody>
		<!--数据区-->
		</tbody>
		</table>
	</div>
</div>
</body>

