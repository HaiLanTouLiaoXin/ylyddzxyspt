<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<div class="canverse_tap_box" id="carBackdiv">
	<div class="list">
		<div class="overf mb20" style="min-height: 80px;margin-top:10px;padding-left:20px;">
				<ul class="select_list fl">
					<li>
					<span class="select_list_title">派遣类型：</span>
						<select name="type" class="select_style" id="businessType">
							<option value="">全部</option>
							<option value="1">上转住院</option>
							<option value="2">上转检查</option>
							<option value="3">上转门诊</option>
							<option value="4">下转住院</option>
							<option value="5">急危重抢救</option>
							<option value="6">标本收取 </option>
							<option value="7">转运病人 </option>
							<option value="8">会诊安排 </option>
							<option value="9">物资调配</option>
							<option value="10">其他</option>
						</select>
					</li>
					<li style="width: 213px;">
					<span class="select_list_title">车辆类型：</span>
						<select name="type" class="select_style" id="carType">
							<option value="">全部</option>
							<option value="1">转诊</option>
							<option value="2">急诊</option>
						</select>
					</li>
					<li style="padding-left: 105px; width: 250px;"> 
						<span class="select_list_title" style="width: 100px;">实际到达地点：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入实际到达地点" id="arriveLocation"/>
					</li>
					<li style="width: 460px;padding-left: 105px;">
						<span class="select_list_title" style="width: 100px;">实际发车时间：</span>
						<input id="StartTime_begin" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'StartTime_end\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
						<input id="StartTime_end" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'StartTime_begin\')}'})" class="in_area Wdate" style="width:170px"/>
					</li>
					<li style="padding-left: 80px;">
						<span class="select_list_title" style="width: 70px; text-align: right;">车牌号：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入车牌号" id="carNo"/>
					</li>
					<li style="padding-left:80px;">
						<span class="select_list_title" style="width: 70px; text-align: right;">驾驶人：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入驾驶人名称" id="driver"/>
					</li>
					<li  style="width: 458px;">
						<span class="select_list_title" style="width: 97px; text-align: right;">回场时间：</span>
						<input id="returnTime_bengin" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'returnTime_end\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
						<input id="returnTime_end" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'returnTime_bengin\')}'})" class="in_area Wdate" style="width:170px"/>
					</li>
					<li style="width: 85px;text-align: center;padding-left: 0;">
						<span class="btn_inline01 btn_search_new" id="searchCarBackList" onclick="searchCarBackList();" style="margin-left: 0;">查询</span>
					</li>
				</ul>
			</div>
			<table width="100%" border="0" cellpadding="0" cellspacing="0"  class="pretty dataTable" id="myCarBackTable" >
			<thead>
				<tr>
					<th style="width:30px;">序号</th>
					<th style="width:100px;">工单编号</th>
					<th style="width:70px;">派遣类型</th>
					<th style="width:70px;">车辆类型</th>
					<th style="width:100px;">出发地点</th>
					<th style="width:100px;">到达地点</th>
					<th style="width:180px;">车牌号</th>
					<th style="width:80px;">驾驶人</th>
					<th style="width:150px;">实际发车时间</th>
					<th style="width:150px;">回场时间</th>
					<th style="width:100px;">任务耗时</th>
					<th style="width:50px;">操作</th>
				</tr>
			</thead>
			<tbody>
			<!--数据区-->
			</tbody>
</table>
	<div class="list">

</div>

