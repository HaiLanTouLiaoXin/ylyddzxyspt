

/**
 * 登陆后首页
 */
var _title="银杏健康信息服务平台";
var newAffairsTitleTime = null;
var loadAffairsTitleTime = null;
var loadWaitAcceptTime = null;
var _second=3;
$(document).ready(function() {
	loadAffairs();
});



//定期访问事务提醒信息
getAffairs();
newAffairsTitleTime = setInterval("getAffairs()", 6000);
loadAffairsTitleTime = setInterval("loadAffairs()", 8000);

loadWaitAcceptTime = setInterval("loadWaitAcceptAffairs()", 30000);

function loadAffairs(){
	$.ajax({
	    url : basePath + "affairs/setup_getNewAffairsNum.action",
	    type : "post",
	    dataType : 'text',
	    success : function(data) {
		    if (data != null && data > 0) {
		    	$("#affairsNum").html("事务提醒（"+data+"）<a href='javascript:void(0);' class='close' onclick='closeLittleDiv(event)'></a>");
		    	audioVoicePlay();
 			    $("#newAffairsAlert").show();
 			  /*  setTimeout(function() {
 			    	$("#newAffairsAlert").hide();
 			    }, 5000);*/
		    }
	    }
	});
}

function loadWaitAcceptAffairs(){
	$.ajax({
	    url : basePath + "defect/waitProcessNum.action",
	    data:{"taskName":2},
	    type : "post",
	    dataType : 'text',
	    success : function(data) {
		    if (data != null && data != "0") {
		    	audioPlay();
		    }
	    }
	});
}

function getAffairs() {
	 
	$.ajax({
	    url : basePath + "affairs/setup_getNewAffairs.action",
	    type : "post",
	    dataType : 'text',
	    success : function(data) {
		    if (data == "success") {
		        $("#affairsEm").addClass("icon_work_on");
		    }else{
		    	$("#affairsEm").removeClass("icon_work_on");
		    }
	    }
	});
}



/**
 * 打开事务提醒页面
 */
function openAffairs() {
	document.title = _title;
    // 同时更新此信息为已接受
    $.ajax({
	    url : basePath + "affairs/setup_updateReceivedAffairs.action",
	    type : "post",
	    dataType : 'text',
	    success : function(data) {
	    }
	});
      
    $("#affairsEm").removeClass("icon_work_on"); //取消提醒
	art.dialog.open(basePath + "affairs/getShortcutMenu.action", {
	    id : "openAffairs",
	    title : "事务提醒",
	    width : 500,
	    height : 380,
	    lock : true,
	    opacity: 0.1,// 透明度
	    button: [{
	    	name: '全部已阅',
	    	focus: true,
	    	callback:function(){
	    		var iframe = this.iframe.contentWindow;
	    		iframe.updateAllReaded();
	    		return false;
	    	}
	    	},
	        {name: '关闭'}]
	});
}


function closeLittleDiv(event){
	$("#newAffairsAlert").hide();
	if ($.browser.msie){
		event.cancelBubble = true ;
	}else{
		event.stopPropagation();
	}
}


function openNewAffairs(){
	openAffairs();
	$("#newAffairsAlert").hide();
}

function audioPlay(){
	if($.browser.msie && $.browser.version=='8.0'){
		//本来这里用的是<bgsound src="system.wav"/>,结果IE8不播放声音,于是换成了embed
		$('#newMessageDIV').html('<embed src="'+basePath+'/down/sound.mp3"/>');
		}else{
		//IE9+,Firefox,Chrome均支持<audio/>
		$('#newMessageDIV').html('<audio autoplay="autoplay"><source src="'+basePath+'/down/sound.mp3"'
		+ 'type="audio/wav"/><source src="system.mp3" type="audio/mpeg"/></audio>');
		}
}

function audioVoicePlay(){
	if($.browser.msie && $.browser.version=='8.0'){
		//本来这里用的是<bgsound src="system.wav"/>,结果IE8不播放声音,于是换成了embed
		$('#newMessageDIV').html('<embed src="'+basePath+'/down/soundVoice.mp3"/>');
		}else{
		//IE9+,Firefox,Chrome均支持<audio/>
		$('#newMessageDIV').html('<audio autoplay="autoplay"><source src="'+basePath+'/down/soundVoice.mp3"'
		+ 'type="audio/wav"/><source src="system.mp3" type="audio/mpeg"/></audio>');
		}
}