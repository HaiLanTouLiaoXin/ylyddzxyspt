jQuery.divselect = function(divselectid,inputselectid) {
	var inputselect = $(inputselectid);
	$(divselectid+" cite").click(function(event){
		event.stopPropagation();
		var ul = $(divselectid+" ul");
		$.each($(".divselect4"),function(i,obj){
			if(divselectid!=("#"+$(obj).attr("id"))){
				$(obj).find("ul").hide();
			}
		});
		if(!ul.is(":visible")){
			ul.slideDown("fast");
			
		}else{
			ul.slideUp("fast");
		}
		
	});
	$(divselectid+" ul li a").click(function(){
		var txt = $(this).text();
		$(divselectid+" cite").html(txt);
		var value = $(this).attr("selectid");
		inputselect.val(value);
		$(divselectid+" ul").hide();
		
	});
	$(document).click(function(){
		$(divselectid+" ul").hide();
	});
};