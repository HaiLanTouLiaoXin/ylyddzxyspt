(function() {
	"use strict"
	angular.module("ui-select", []).directive('commonselect', function() {
    return {
        restrict : 'E',
        template: '<div class="{{divClass}}"  ng-mouseleave="show = 2" >'+
    	          '<cite class="{{citeClass}}"  ng-click="show = 1">{{selectedName}}</cite>'+
    	          '<ul ng-show="show == 1" style="display: block; margin-top: 0;" >'+
    		      '<li ng-repeat="each in datalist"><a href="javascript:;" ng-click="selectOne (each.name, each.value)" >{{each.name}}</a></li>'+
                  '</ul></div>',
        
        
        
       // templateUrl : 'commonselect.html',
        scope:{
        	datalist:"=",
        	defaultSelect:"=",
        	divClass:"=",
        	citeClass:"=",
        	outValue:"="
        },
        link : function(scope, element, attrs) { 
        	if (!scope.divClass){
        		scope.divClass="divselect7";
        	}
        	if (!scope.citeClass){
        		scope.citeClass="cite7";
        	}
        	
        	scope.init = function(){
        		scope.outValue = scope.defaultSelect;
        		if (scope.datalist){
        			for(var i = 0; i < scope.datalist.length; i++){
                		if(scope.defaultSelect == scope.datalist[i].value){
                			scope.selectedName = scope.datalist[i].name;
                			break;
                		}
                	}
        		}
        	}
        	
        	scope.$watch("defaultSelect", function(newVal, oldVal) {
        		scope.init(); 
			});
        	
        	/**
        	 * 选择一个选项
        	 */
        	scope.selectOne = function(name, value){
        		scope.outValue = value;
        		scope.defaultSelect = value;
        		scope.selectedName = name;
        		scope.show = 0;
        	};
        }
    }
});
})();