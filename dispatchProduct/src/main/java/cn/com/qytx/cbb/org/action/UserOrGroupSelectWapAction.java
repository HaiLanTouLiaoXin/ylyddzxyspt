/**
 * 
 */
package cn.com.qytx.cbb.org.action;

import java.util.List;

import javax.annotation.Resource;

import com.google.gson.Gson;

import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.tree.TreeNode;

/**
 * 功能: 手机选怎部门人员接口
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月23日
 * 修改日期: 2017年5月23日
 * 修改列表: 
 */
public class UserOrGroupSelectWapAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 /** 用户信息 */
    @Resource(name = "userService")
    IUser userService;
	
	private Integer type;
	
	private Integer showType;// 选择类型 1只显示部门 2 显示角色 3 显示人员
	
	private String extension;//1 调度 2后勤 3 临床
	
	private Integer userId;
	
	
	private String moduleName;
	 /**
	  * 获得部门
	  */
	 public String findSelectGroup(){
		 if(userId==null){
        	ajax("101||参数不能为空!");
        	return null;
         }
		 UserInfo user=userService.findOne(userId);
	     String contextPath = getRequest().getContextPath();
	     List<TreeNode> list = userService.selectUserByGroup(user, null, moduleName, showType, 0, contextPath, type,extension,"mobile");
	     Gson json = new Gson();
	     ajax("100||"+json.toJson(list));
		 return null;
	 }
	public Integer getShowType() {
		return showType;
	}

	public void setShowType(Integer showType) {
		this.showType = showType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	 
	 
	 
	 
	 
	 

}
