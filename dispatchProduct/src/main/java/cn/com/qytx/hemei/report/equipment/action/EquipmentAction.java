package cn.com.qytx.hemei.report.equipment.action;

import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.equipment.impl.service.IEquipment;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

public class EquipmentAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7259743315470024497L;
	
	@Autowired
	private IEquipment equipmentService;
	
    private String groupName;
	
	private String userName;
	
	private String beginTime;
	
	private String endTime;

	private Integer type;//1 配送 2 归还
	
	/**
	 * 归还设备报表
	 * @return
	 */
	public String findReturnEquipmentList(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				List<Object[]> list=equipmentService.findReturnEquipmentList(groupName, userName,beginTime,endTime,type);
				List<Map<String,Object>> listMap= getMapList(list);
				ajax(listMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int num=1;
			for(Object[] obj:list){
				Map<String,Object> map= new HashMap<String, Object>();
				    map.put("no",num);
					map.put("equipmentGroupName", obj[0]==null?"--":(String)obj[0]);
					map.put("userName",  obj[2]==null?"--":(String)obj[2]);
					map.put("groupName",  obj[1]==null?"--":(String)obj[1]);
					map.put("equipmentNum",obj[3]==null?"--":(Integer)obj[3]);
					num++;
					listMap.add(map);
			}
		}
		return listMap;
	}
	
	
	/*
	 * 
	 * 导出
	 */
		public void exportReport(){
			HttpServletResponse response = this.getResponse();
	        response.setContentType("application/vnd.ms-excel");
	        OutputStream outp = null;
			try {
				UserInfo user= getLoginUser();
				if(user!=null){
					if(StringUtils.isNoneBlank(userName)){
						userName=URLDecoder.decode(userName, "UTF-8");
					}
					if(StringUtils.isNoneBlank(groupName)){
						groupName=URLDecoder.decode(groupName, "UTF-8");
					}
					List<Object[]> list=equipmentService.findReturnEquipmentList(groupName, userName,beginTime,endTime,type);
					List<Map<String,Object>> listMap= getMapList(list);
					String name="设备配送报表";
					if(type==2){
						name="设备归还报表";
					}
					String fileName = URLEncoder.encode(name+".xls", "UTF-8");
			        // 把联系人信息填充到map里面
			        response.addHeader("Content-Disposition",
			                "attachment;filename=" + fileName);// 解决中文
					outp = response.getOutputStream();
		            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(type), listMap, getExportKeyList());
		            exportExcel.exportWithSheetName(name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	   private List<String> getExportHeadList(Integer type){
	        List<String> headList = new ArrayList<String>();
	        headList.add("序号");
	        headList.add("设备组");
	        headList.add("配送人员");
	        headList.add("所属科室");
	        String name="配送设备数";
	        if(type==2){
	        	name="归还设备数量";
	        }
	        headList.add(name);
	        return headList;
	    }
		    
	    private List<String> getExportKeyList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("no");
	        headList.add("equipmentGroupName");
	        headList.add("userName");
	        headList.add("groupName");
	        headList.add("equipmentNum");
	        return headList;
	    }
	
	    /**
	     * 查询设备使用报表
	     */
	    public void findCapitalUseList(){
	    	try {
	    		UserInfo user=getLoginUser();
	    		if(user!=null){
	    			List<Object[]> list=equipmentService.findUseEquipmentList(beginTime, endTime);
	    			Map<String,Integer> mapNum=equipmentService.findEquipmentNum(beginTime, endTime);
					List<Map<String,Object>> listMap= getEquipmentUseMap(list,mapNum);
					ajax(listMap);
	    		}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
	    
		/**
		 * 封装数据
		 * @param list
		 * @return
		 */
		public List<Map<String,Object>> getEquipmentUseMap(List<Object[]> list,Map<String,Integer> mapNum){
			List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
			if(list!=null && list.size()>0){
				int num=1;
				for(Object[] obj:list){
					Map<String,Object> map= new HashMap<String, Object>();
					    Integer capitalNum=(Integer)obj[0];//设备数
					    Integer useNum=(Integer)obj[1];//使用次数
					    Integer capitalGroupId=(Integer)obj[2];//资产组Id
					    Integer groupId=(Integer)obj[3];//部门Id
					    Integer timeLong=(Integer)obj[4];
					    String groupName=(String)obj[5];
					    Integer avgTime=0;
					    if(useNum!=0){
					      avgTime=(int)timeLong/useNum;
					    }
					    String capitalGroupName=(String)obj[6];
					    String key=capitalGroupId+"_"+groupId;
					    map.put("no",num);
						map.put("capitalNum", capitalNum);
						map.put("useNum",  useNum);
						Integer returnNum=0;
						if(mapNum.containsKey(key)){
							returnNum=mapNum.get(key);
						}
						map.put("returnNum",returnNum);
						map.put("timeLong", timeLong);
						map.put("groupName", groupName);
						map.put("capitalGroupName", capitalGroupName);
						map.put("avgTime", avgTime);
						num++;
						listMap.add(map);
				}
			}
			return listMap;
		}
	    
		/*
		 * 
		 * 导出设备使用报表
		 */
			public void exportReportEquipmentUseList(){
				HttpServletResponse response = this.getResponse();
		        response.setContentType("application/vnd.ms-excel");
		        OutputStream outp = null;
				try {
					UserInfo user= getLoginUser();
					if(user!=null){
						List<Object[]> list=equipmentService.findUseEquipmentList(beginTime, endTime);
		    			Map<String,Integer> mapNum=equipmentService.findEquipmentNum(beginTime, endTime);
						List<Map<String,Object>> listMap= getEquipmentUseMap(list,mapNum);
						String fileName = URLEncoder.encode("临床科室设备使用报表.xls", "UTF-8");
				        // 把联系人信息填充到map里面
				        response.addHeader("Content-Disposition",
				                "attachment;filename=" + fileName);// 解决中文
						outp = response.getOutputStream();
			            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadEquipmentList(), listMap, getExportKeyEquipmentList());
			            exportExcel.exportWithSheetName("临床科室设备使用报表");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
	
			
			 private List<String> getExportHeadEquipmentList(){
			        List<String> headList = new ArrayList<String>();
			        headList.add("序号");
			        headList.add("设备组");
			        headList.add("使用科室");
			        headList.add("使用设备数");
			        headList.add("使用次数");
			        headList.add("归还设备数");
			        headList.add("使用总时长");
			        headList.add("使用平均时长");
			        return headList;
			    }
				    
			    private List<String> getExportKeyEquipmentList(){
			        List<String> headList = new ArrayList<String>();
			        headList.add("no");
			        headList.add("capitalGroupName");
			        headList.add("groupName");
			        headList.add("capitalNum");
			        headList.add("useNum");
			        headList.add("returnNum");
			        headList.add("timeLong");
			        headList.add("avgTime");
			        return headList;
			    }		
			
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}
	
	
	

}
