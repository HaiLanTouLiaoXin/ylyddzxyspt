package cn.com.qytx.hemei.cardispatch.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.CarVo;
import cn.com.qytx.hemei.cardispatch.service.ICar;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 车辆管理
 * @author xs
 *
 */
public class CarListAction extends BaseActionSupport{
	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 5955923361018578332L;
	private final static Logger logger = Logger.getLogger(CarListAction.class);
	
	@Autowired
	private ICarOrder  carOrderService;
	
	@Autowired
	private ICar carService;

	public Car car;
	
	public CarVo carVo;
	
	private String ids;
	
	private String carMsg;
	/**
	 * 车辆列表
	 * @return
	 */
	public String carList(){
		try {
			UserInfo userInfo = getLoginUser();
			if (userInfo != null) {
				Page<Car> pageInfo = carService.findPageByVo(getPageable(new Sort(Direction.ASC,
						"status")), carVo);
				List<Car> content = pageInfo.getContent();
				List<Map<String, Object>> result = analyzeResult(content,userInfo.getCompanyId());
				this.ajaxPage(pageInfo, result);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	/**
     * 封装数据
     * @param smsList
     * @return
     */
    private List<Map<String, Object>> analyzeResult(List<Car> smsList,Integer companyId){
    	List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
    	//Map<Integer,Integer> carOrderMap=carOrderService.getCarOrderNum(companyId);
    	if (smsList != null) {
    		int i = getPageable().getPageNumber() * getPageable().getPageSize() + 1;
			for (Car car : smsList) {
				 Map<String, Object> map = new HashMap<String, Object>();
				 map.put("no", i++);//序号
				 map.put("id",car.getId());//id
				 map.put("carNo", StringUtils.isNotEmpty(car.getCarNo())?car.getCarNo():"--");//车牌号
				 map.put("carBrand", StringUtils.isNotEmpty(car.getCarBrand())?car.getCarBrand():"--");//车辆品牌
				 map.put("carPhone", StringUtils.isNotEmpty(car.getCarPhone())?car.getCarPhone():"--");//车辆电话
				 map.put("carType", carTypeStr(car.getCarType()));//车辆类型
				 map.put("historyNum", car.getHistoryNum());
				 map.put("status", car.getStatus()!=null&&car.getStatus().intValue()==1?"空闲":"忙碌");
				 /* Integer num=0;
				 String statuStr="空闲";
				 if(carOrderMap.containsKey(car.getId())){
					 num=carOrderMap.get(car.getId());
					 if(num>0){
						 statuStr="忙碌" ;
					 }
				 }*/
				 
				 map.put("havaStretcher",(car.getHavaStretcher()!=null&&car.getHavaStretcher()==1)?"有":"没有");
				 map.put("seatNum",car.getSeatNum()==null?"--":car.getSeatNum());
				 mapList.add(map);
			}
		}
    	return mapList;
    }
	
    /**
     * 车辆类型转化成字符串
     * @param carType
     * @return
     * 车辆类型（1 转诊车 2 急诊车）
     */
    public String carTypeStr(Integer carType){
    	if (carType != null && carType > 0) {
			if (carType == 1) {
				return "转诊车";
			}
			if (carType == 2) {
				return "急诊车";
			}
		}
    	return "--";
    }
    
    /**
     * 当前状态转化成字符串
     * @param status
     * @return
     * 车辆状态（1 空闲 2 待发车 3 待回场（已发车） ）
     */
    public String statusStr(Integer status){
    	if (status != null && status > 0) {
			if (status == 1) {
				return "空闲";
			}
			if (status == 2) {
				return "待发车";
			}
			if (status == 3) {
				return "待回场";
			}
		}
    	return "--";
    }
	/**
	 * 车辆添加
	 * @return
	 */
	public void addCar(){
		try {
			UserInfo userInfo = getLoginUser();
			if (userInfo != null) {
				Car currentCar = carService.findByCarNo(carVo.getCarNo());
				if (currentCar!=null) {
					ajax("1");
				}else {
					Car cars = new Car();
					cars.setCarNo(carVo.getCarNo());
					cars.setStatus(1);
					cars.setCarType(carVo.getCarType());
					cars.setCarBrand(carVo.getCarBrand());
					//cars.setCarModel(carVo.getCarModel());
					cars.setHistoryNum(0);
					cars.setIsDelete(0);
					cars.setCarPhone(carVo.getCarPhone());
					cars.setHavaStretcher(carVo.getHavaStretcher());//是否有担架  1 有  2 没有
					cars.setSeatNum(carVo.getSeatNum());//座位数
					cars.setOccupy(2);//是否被占用  1 有  2 没有
					cars.setRestSeatNum(carVo.getSeatNum());//剩余座位数
					cars.setCreateUserId(userInfo.getUserId());
					cars.setCompanyId(userInfo.getCompanyId());
					cars.setCreateTime(new Timestamp(System.currentTimeMillis()));
					carService.saveOrUpdate(cars);
					ajax("0");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 车辆修改
	 * @return
	 */
	public String editCar(){
		try {
			UserInfo userInfo = getLoginUser();
			if (userInfo != null) {
				car = carService.findOne(carVo.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	/**
	 * 修改当前
	 */
	public void updateCars(){
		try {
			UserInfo userInfo = getLoginUser();
			if (userInfo != null) {
				Car cars = carService.findOne(carVo.getId());
				cars.setCarNo(carVo.getCarNo());
				cars.setCarType(carVo.getCarType());
				cars.setCarBrand(carVo.getCarBrand());
				//cars.setCarModel(carVo.getCarModel());
				cars.setHavaStretcher(carVo.getHavaStretcher());//是否有担架  1 有  2 没有
				cars.setSeatNum(carVo.getSeatNum());//座位数
				cars.setRestSeatNum(carVo.getSeatNum());//剩余座位数
				cars.setCarPhone(carVo.getCarPhone());
				cars.setUpdateUserId(userInfo.getUserId());
				cars.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				carService.saveOrUpdate(cars);
				ajax("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 修改car的剩余车辆和发车时间和担架占用情况         中心医院工单平台调用的接口
	 */
	public void updateCarSeatWap(){
		//先根据车牌号码判断有没有这个车辆
		try {
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
			Car car = gson.fromJson(carMsg, Car.class);
			Car car_new = carService.findOne(car.getId());
			if(car_new!=null){
				car_new.setRestSeatNum(car.getRestSeatNum());//剩余车辆
				car_new.setDepartureTime(car.getDepartureTime());//发车时间
				if(!(car_new.getOccupy()==1)){
					car_new.setOccupy(car.getOccupy());//是否被占用
				}
				carService.saveOrUpdate(car_new);
				ajax("success");
			}else{
				logger.info("修改车辆信息失败，没有查到此车信息");
				ajax("error");
			}
		} catch (Exception e) {
			logger.info("修改车辆信息失败，没有查到此车信息");
			ajax("error");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 车辆删除
	 * @return
	 */
	public String deleteCar(){
		try {
			UserInfo userInfo = getLoginUser();
			if (userInfo != null&&ids!=null) {
				int num=carService.delete(userInfo.getCompanyId(),ids);
				ajax(num+"");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public CarVo getCarVo() {
		return carVo;
	}

	public void setCarVo(CarVo carVo) {
		this.carVo = carVo;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getCarMsg() {
		return carMsg;
	}
	public void setCarMsg(String carMsg) {
		this.carMsg = carMsg;
	}
	
}
