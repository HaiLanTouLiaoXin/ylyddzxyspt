package cn.com.qytx.hemei.duty.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.hemei.duty.service.IWorkDutyService;
import cn.com.qytx.hemei.duty.util.DateConvertUtils;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.utils.StringUtil;

import common.Logger;

/**
 * 
 * 功能: 版本: 1.0 开发人员: 徐长江 创建日期: 2016年5月17日 修改日期: 2016年5月17日 修改列表:
 */
public class ImportWorkDutyAction extends BaseActionSupport {
	private static final long serialVersionUID = 1L;

	private File fileToUpload; //

	private String uploadFileName;

	private int allNum = 0;// 总个数

	private int successNum = 0;// 成功个数

	private int skipNum = 0;// 跳过个数

	private int overrideNum = 0;// 覆盖个数
	/** 值班impl */
	@Autowired
	IWorkDutyService workDutyImpl;
	Logger logger = Logger.getLogger(ImportWorkDutyAction.class);

	/**
	 * 检查文件是否符合格式action
	 * 
	 * @return
	 * @throws Exception
	 */
	public String check() throws Exception {

		PrintWriter writer = null;
		try {
			if (uploadFile()) {
				String result = checkExcel(uploadFileName);
				writer = new PrintWriter(ServletActionContext.getResponse().getWriter());
				writer.print(result);
				writer.close();
			}

		} catch (Exception e) {
			logger.debug("ImportWorkDutyAction 值班导入类发生异常", e);
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write("对不起！导入文件时出错！");
			response.getWriter().close();
		}
		return null;
	}

	/**
	 * 上传文件
	 */
	@SuppressWarnings("deprecation")
	private boolean uploadFile() {

		String path = ServletActionContext.getRequest().getRealPath("/upload");
		File checkPath = new File(path);
		if (!checkPath.exists()) {
			// 目录不存在，则创建目录
			boolean result = checkPath.mkdirs();
			if (!result) {
				return false;
			}
		}
		try {
			if (fileToUpload != null) {
				String fileName = UUID.randomUUID().toString();
				String ext = ".xls";
				if (uploadFileName != null) {
					ext = uploadFileName.substring(uploadFileName.lastIndexOf("."));
				}
				if (!ext.equals(".xls")) {
					return false;
				}
				String saveFileName = fileName + ext;
				File savefile = new File(new File(path), saveFileName);
				if (!savefile.getParentFile().exists()) {
					boolean result = savefile.getParentFile().mkdirs();
					if (!result) {

					}
				}
				FileUtils.copyFile(fileToUpload, savefile);// 拷贝文件
				uploadFileName = path + "/" + saveFileName;

			}

		} catch (Exception e) {

			return false;
		}
		return true;
	}

	/**
	 * 导入值班action
	 */
	public String importWorkDuty() throws Exception {

		PrintWriter writer = null;
		try {
			if (uploadFile()) {
				// String result = "";
				String result = startImportWorkDuty(uploadFileName);
				writer = new PrintWriter(ServletActionContext.getResponse().getWriter());
				if (allNum > 0) {
					int failNum = allNum - successNum - skipNum - overrideNum;// 失败条数
					if (failNum < 0) {
						failNum = 0;
					}
					String path = ServletActionContext.getRequest().getContextPath();
					String basePath = ServletActionContext.getRequest().getScheme() + "://" + ServletActionContext.getRequest().getServerName() + ":" + ServletActionContext.getRequest().getServerPort() + path + "/";

					result = "共" + allNum + "行数据，导入成功" + (successNum) + "行,失败" + failNum + "行";

				}
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.print(result);
				out.close();
			}

		} catch (Exception e) {
			logger.debug("ImportWorkDutyAction 值班导入类发生异常", e);
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write("对不起！导入文件时出错！");
			response.getWriter().close();
		}
		return null;
	}

	/**
	 * 开始导入值班
	 * 
	 * @param excel
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private String startImportWorkDuty(String excel) throws Exception {

		String check = checkExcel(excel);
		if (!check.equals("")) {
			return check;
		} else {
			List<WorkDuty> list = getWorkDutyList(excel);
			if (list != null && list.size() > 0) {
				allNum = list.size();// 总个数
				successNum = list.size();// 成功数量
			}
			workDutyImpl.saveOrUpdate(list);
		}
		return "";
	}

	/**
	 * 判断文件是否符合要求
	 * 
	 * @param excel
	 * @return 空字符串表示验证成功 其他失败
	 * @throws Exception
	 */
	private String checkExcel(String excel) {

		Workbook wb = null;
		String result = "";
		try {
			InputStream is = new FileInputStream(excel);
			wb = Workbook.getWorkbook(is);
			if (wb == null) {
				result = "要导入的文件不正确！";
				return result;
			}
			Sheet sheet = wb.getSheet("Sheet1"); // 获取工作Excel
			if (sheet == null) {
				result = "要导入的文件不正确！";
				return result;
			}
			boolean flag = true;
			int totalRow = sheet.getRows();
			if (totalRow == 1) {
				result = "请填写要导入的内容！";
				return result;
			}
			// 得到列的长度
			// Cell[] cellsZero = sheet.getRow(0); // 读标题行
			// Integer headLength = cellsZero.length - 1;
			// 第一行不判断
			for (int j = 1; j < sheet.getRows(); j++) {
				Cell[] cells = sheet.getRow(j); // 读取一行
				// 如果这行全部为空,则不处理
				boolean isEmpty = true;
				if (cells != null) {
					for (Cell cell : cells) {
						String val = cell.getContents();// 内容
						if (null != val && !"".equals(val.trim())) {
							isEmpty = false;
							break;
						}
					}
				}
				if (isEmpty) {
					continue;
				}
				/*
				 * if (cells != null && cells.length < 13) { result = "第" + (j +
				 * 1) + "行数据不完整！"; break; }
				 */
				if (cells != null && cells.length > 0) {
					for (int k = 0; k < cells.length; k++) {

						String val = cells[k].getContents();// 内容
						if (val != null) {
							val = val.trim();
						}
						if (k == 0) {
							if (StringUtil.isEmpty(val)) {
								result = "第" + (j + 1) + "行第" + (k + 1) + "列" + "数据不能为空！";
								flag = false;// 退出循环
								break;
							} else {

								/*
								 * if(!isDateCheck(val)) { result = "第" + (j +
								 * 1) + "行第" + (k + 1)+"列" + "数据格式有误！"; flag =
								 * false;// 退出循环 break; }
								 */
								if (DateConvertUtils.createDate1(val) == null) {
									result = "第" + (j + 1) + "行第" + (k + 1) + "列" + "数据格式有误！";
									flag = false;// 退出循环
									break;
								}
							}
						} else if (k == 1) {
							if (StringUtil.isEmpty(val)) {
								result = "第" + (j + 1) + "行第" + (k + 1) + "列" + "数据不能为空！";
								flag = false;// 退出循环
								break;
							}
						} else if (k == 2) {

							if (StringUtil.isEmpty(val)) {
								result = "第" + (j + 1) + "行第" + (k + 1) + "列" + "数据不能为空！";
								flag = false;// 退出循环
								break;
							}
						}
					}// column
				}
				if (!flag) {
					break;
				}
			}// one sheet

		} catch (Exception ex) {
			ex.printStackTrace();
			result = ex.getMessage();
		} finally {
			if (wb != null) {
				wb.close();
			}
		}
		return result;
	}

	/**
	 * 由文件生成值班列表
	 * 
	 * @param excel
	 * @return
	 */
	private List<WorkDuty> getWorkDutyList(String excel) {
		Object userObject = this.getSession().getAttribute("adminUser");
		UserInfo userInfo = (UserInfo) userObject;
		List<WorkDuty> list = new ArrayList<WorkDuty>();
		Workbook wb = null;
		try {
			InputStream is = new FileInputStream(excel);
			wb = Workbook.getWorkbook(is);
			Sheet sheet = wb.getSheet("Sheet1"); // 获取工作Excel,这个名字根据你的excel里面的名字而定
			boolean flag = true;
			// 第一行不判断
			for (int j = 1; j < sheet.getRows(); j++) {

				Cell[] cells = sheet.getRow(j); // 读取一行
				if (cells != null && cells.length > 0) {
					WorkDuty wd = new WorkDuty();
					Date  dutyDate = null;// 值班时间
					String dutyPost = null;// 值班岗位
					String dutyPersons = null;// 值班人员
					String dutyPhone = null;// 值班电话
					for (int k = 0; k < cells.length; k++) {
						String val = cells[k].getContents();// 内容
						if (val != null) {
							val = val.trim();
						}
						if (k == 0) {
							dutyDate = DateConvertUtils.createDate1(val);
						} else if (k == 1) {
							dutyPost = val;
						} else if (k == 2) {
							dutyPersons = val;
						} else if (k == 3) {
							dutyPhone = val;
						}

					}
					//删除当前日期值班信息
					workDutyImpl.deleteWorkDuty(dutyDate,dutyPost);
					wd.setCompanyId(userInfo.getCompanyId());
					wd.setCreateTime(new Timestamp(System.currentTimeMillis()));
					//wd.setCreateUserId(userInfo.getUserId());
					wd.setDutyDate(dutyDate);
					wd.setDutyPersons(dutyPersons);
					wd.setDutyPhone(dutyPhone);
					wd.setDutyPost(dutyPost);
					list.add(wd);
				}
				if (!flag) {
					break;
				}
			}// one sheet

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (wb != null) {
				wb.close();
			}
		}
		return list;
	}

	/**
	 * 字符串转换成日期
	 * 
	 * @param str
	 * @return date
	 */
	public static Date StrToDate(String str) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * 功能：是否是日期的判断
	 * 
	 * @param date
	 * @return
	 */
	public boolean isDateCheck(String date) {
		String eL = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(date);
		boolean dateFlag = m.matches();
		if (dateFlag) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 功能：是否为数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNum(String str) {

		return str.matches("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$");
	}

	/**
	 * 功能：是否为邮编
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isZipCode(String str) {

		return str.matches("^[0-9]{6}$");
	}

	/**
	 * 功能：是否为电话号码
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isPhone(String str) {

		return str.matches("^[0-9]{7,12}$");
	}

	/**
	 * 替换掉str里面的特殊字符
	 * 
	 * @param str
	 * @return
	 */
	private static String StringFilter(String str) {

		String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}

	/**
	 * 功能：是否为手机号码
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isMobilePhone(String str) {

		return str.matches("^(13[0-9]|15[0-9]|18[0-9])[0-9]{8}$");
	}

	/**
	 * 功能：是否为邮箱
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmail(String str) {

		return str.matches("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
	}

	private String getExtension(String fileName) {

		if ((fileName != null) && (fileName.length() > 0)) {
			int i = fileName.lastIndexOf('.');

			if ((i > -1) && (i < fileName.length() - 1)) {
				return fileName.substring(i + 1);
			}
		}
		return "";
	}

	public File getFileToUpload() {

		return fileToUpload;
	}

	public void setFileToUpload(File fileToUpload) {

		this.fileToUpload = fileToUpload;
	}

	public String getUploadFileName() {

		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {

		this.uploadFileName = uploadFileName;
	}

}