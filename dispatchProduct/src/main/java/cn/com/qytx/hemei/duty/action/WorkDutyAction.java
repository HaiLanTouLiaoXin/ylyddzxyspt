package cn.com.qytx.hemei.duty.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.hemei.duty.service.IWorkDutyService;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * 功能:值班 版本: 1.0 开发人员: 徐长江 创建日期: 2016年5月17日 修改日期: 2016年5月17日 修改列表:
 */
public class WorkDutyAction extends BaseActionSupport {
	/**
	 * 描述含义
	 */
	private static final long serialVersionUID = 1L;
	/** 值班impl */
	@Autowired
	IWorkDutyService workDutyImpl;
	
	@Autowired
	private IDict dictService;
	
	private String dutyDate;
	
	private Integer dutyDepartmentId;

	public String getDutyList() {
		try {
			UserInfo userInfo = this.getLoginUser();
			if (userInfo != null) {
				Date workDate=new Date();
				if(StringUtils.isNotBlank(dutyDate)){
					workDate=StrToDate(dutyDate);
				}
				List<Dict> dictList = dictService.findListByInfoType("dutyDepartment");
				List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
				/** 得到结果 */
				List<WorkDuty> workDutyList = workDutyImpl.getWorkDutyByWorkDate(workDate);
				Map<Integer,List<WorkDuty>> workDutyMap = new HashMap<Integer, List<WorkDuty>>();
				if(workDutyList!=null&&!workDutyList.isEmpty()){
					List<WorkDuty> wdList = null;
					for(WorkDuty duty:workDutyList){
						Integer dutyDepId = Integer.parseInt(duty.getDutyPost());
						if(workDutyMap.containsKey(dutyDepId)){
							wdList = workDutyMap.get(dutyDepId);
						}else{
							wdList = new ArrayList<WorkDuty>();
						}
						UserInfo dutyUser = duty.getCreateUser();
						if(dutyUser!=null){
							duty.setDutyPersons(dutyUser.getUserName());
							duty.setDutyPhone(dutyUser.getPhone());
							duty.setCreateUser(null);
							duty.setIsOwn(dutyUser.getUserId().intValue()==userInfo.getUserId().intValue()?1:0);
							duty.setCreateUserId(dutyUser.getUserId());
						}
						wdList.add(duty);
						workDutyMap.put(dutyDepId, wdList);
					}
				}
				for(Dict dict:dictList){
					Map<String,Object> subMap = new HashMap<String, Object>();
					subMap.put("dutyDepartment", dict.getName());
					Integer dictVal = dict.getValue();
					if(workDutyMap.containsKey(dictVal)){
						subMap.put("dutyList", workDutyMap.get(dictVal));
					}
					mapList.add(subMap);
				}
				Gson gson=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").create();
				ajax(gson.toJson(mapList));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	public String todayWorkDuty(){
		try{
			UserInfo userInfo = this.getLoginUser();
			if(userInfo!=null){
				Map<String,Object> map = new HashMap<String, Object>();
				WorkDuty todayWd = workDutyImpl.getWorkDutyByUserId(new Date(), userInfo.getUserId());
				map.put("isDuty", todayWd!=null?"1":"0");
				map.put("userName", userInfo.getUserName());
				map.put("userId", userInfo.getUserId());
				Gson gson = new Gson();
				ajax(gson.toJson(map));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 功能：值班
	 * @return
	 */
	public String saveWorkDuty(){
		try{
			UserInfo userInfo = this.getLoginUser();
			WorkDuty wd = new WorkDuty();
			wd.setCompanyId(userInfo.getCompanyId());
			wd.setCreateUser(userInfo);
			wd.setCreateTime(new Timestamp(System.currentTimeMillis()));
			wd.setDutyDate(new Date());
			wd.setDutyPost(String.valueOf(dutyDepartmentId));
			workDutyImpl.saveOrUpdate(wd);
			ajax("0");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 功能：值班
	 * @return
	 */
	public String deleteWorkDuty(){
		try{
			UserInfo userInfo = this.getLoginUser();
			WorkDuty todayWd = workDutyImpl.getWorkDutyByUserId(new Date(), userInfo.getUserId());
			workDutyImpl.delete(todayWd, true);
			ajax("0");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	

	/**
	 * 导出
	 * @Title: export   
	 */
	public void export(){
		HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				Date workDate=null;
				if(StringUtils.isNotBlank(dutyDate)){
					workDate=StrToDate(dutyDate);
				}
				/** 得到结果 */
				List<WorkDuty> list = workDutyImpl.getWorkDutyByWorkDate(workDate);
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					for(WorkDuty duty:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String dutyDate=duty.getDutyDate()==null?"":sdf.format(duty.getDutyDate());
						String dutyPost=duty.getDutyPost()==null?"":duty.getDutyPost();
						String dutyPhone=duty.getDutyPhone()==null?"":duty.getDutyPhone();
						String dutyPersons=duty.getDutyPersons()==null?"":duty.getDutyPersons();
						map.put("dutyDate", dutyDate);
						map.put("dutyPost", dutyPost);
						map.put("dutyPhone", dutyPhone);
						map.put("dutyPersons", dutyPersons);
						mapList.add(map);
					}
				}
				String fileName = URLEncoder.encode("值班表.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), mapList, getExportKeyList());
	            exportExcel.exportWithSheetName("Sheet1");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("日期");
        headList.add("值班岗位");
        headList.add("值班人员");
        headList.add("值班电话");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("dutyDate");
        headList.add("dutyPost");
        headList.add("dutyPersons");
        headList.add("dutyPhone");
        return headList;
    }

	/**
	 * 字符串转换成日期
	 * 
	 * @param str
	 * @return date
	 */
	public static Date StrToDate(String str) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 日期转换成字符串
	 * 
	 * @param str
	 * @return date
	 */
	public static String DateToStr(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String str = format.format(date);
		return str;
	}



	public String getDutyDate() {
		return dutyDate;
	}



	public void setDutyDate(String dutyDate) {
		this.dutyDate = dutyDate;
	}

	public Integer getDutyDepartmentId() {
		return dutyDepartmentId;
	}

	public void setDutyDepartmentId(Integer dutyDepartmentId) {
		this.dutyDepartmentId = dutyDepartmentId;
	}

	

}
