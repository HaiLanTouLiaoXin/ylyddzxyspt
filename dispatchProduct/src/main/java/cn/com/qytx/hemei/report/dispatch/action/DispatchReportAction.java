package cn.com.qytx.hemei.report.dispatch.action;

import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.IDispatchReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

public class DispatchReportAction extends BaseActionSupport {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7875668958659815299L;

	@Autowired
	private IDispatchReport  dispatchReportService;
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private String userName;
	
	public void findDispatchList(){
		try {
			UserInfo user =getLoginUser();
			if(user!=null){
				List<Object[]> list= dispatchReportService.findList(beginTime, endTime, userName);
				List<Map<String,Object>> listMap= getMapList(list);
				ajax(listMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
/*
 * 
 * 导出
 */
	public void exportReport(){
		HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try {
			UserInfo user= getLoginUser();
			if(user!=null){
				if(StringUtils.isNoneBlank(userName)){
					userName=URLDecoder.decode(userName, "UTF-8");
				}
				List<Object[]> list= dispatchReportService.findList(beginTime, endTime, userName);
				List<Map<String,Object>> listMap= getMapList(list);
				String fileName = URLEncoder.encode("调度人员工作量统计.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("调度人员工作量统计");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	 
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("姓名");
        headList.add("电话调度量");
        headList.add("自主调度量");
        headList.add("调度总量");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("phoneReceive");
        headList.add("receive");
        headList.add("sum");
        return headList;
    }
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int i=1;
			Integer phoneReceiveSum=0;
			Integer receiveSum=0;
			Integer countSum=0;
			for(Object[] obj:list){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("no", i);
				map.put("name", obj[0].toString());
				Integer phoneReceiveNum=Integer.valueOf(obj[1].toString());//电话上报数
				Integer receiveNum=Integer.valueOf(obj[2].toString());//正常上报数
				map.put("phoneReceive",phoneReceiveNum );//接收
				map.put("receive",receiveNum);
				map.put("sum",receiveNum+phoneReceiveNum);
				listMap.add(map);
				phoneReceiveSum+=phoneReceiveNum;
				receiveSum+=receiveNum;
				countSum+=receiveNum+phoneReceiveNum;
				i++;
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("no", "合计");
			map.put("name", "");
			map.put("phoneReceive", phoneReceiveSum);
			map.put("receive",receiveSum);
			map.put("sum",countSum);
			listMap.add(map);
		}
		return listMap;
	}
	
	
	
	
	public IDispatchReport getDispatchReportService() {
		return dispatchReportService;
	}

	public void setDispatchReportService(IDispatchReport dispatchReportService) {
		this.dispatchReportService = dispatchReportService;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
