package cn.com.qytx.hemei.event.service;

import java.util.List;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.event.domain.EventType;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:事件类型接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2017年4月10日
 * 修改日期: 2017年4月10日
 * 修改列表: 
 */
public interface IEventType extends BaseService<EventType>{

	
	/**
	 * 功能：获取事件类型列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<EventType> findEventTypeList(Integer companyId);
	
	
	/**
	 * 功能：获取事件类型树列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<TreeNode> getTreeEventTypeList(String path,Integer companyId);
	
	
	public List<TreeNode> getTreeEventTypeList_Select(String path,Integer companyId);
	
	/**
	 * 在指定ID下面是否有相同的事件类型名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameEventTypeName(Integer parentId,String typeName,int companyId);
	
	/**
	 * 功能：判断是否存在子事件类型
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildEventType(Integer id,int companyId);
	
	/**
	 * 功能：通过Id传获取事件类型列表
	 * @param Ids
	 * @return
	 */
	public List<EventType> getEventTypeListByIds(String Ids);
}
