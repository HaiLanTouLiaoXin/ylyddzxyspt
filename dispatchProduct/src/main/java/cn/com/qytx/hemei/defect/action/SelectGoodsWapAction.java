package cn.com.qytx.hemei.defect.action;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.hemei.defect.service.ISelectGoods;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;

public class SelectGoodsWapAction extends BaseActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3713703487531560694L;

    private String instanceId;
	
	private Integer goodId;
	
	private Integer num;
	
	private Integer id;//选择物资的Id
	
	private Integer userId;
	
	@Resource
	private IUser userService;
	
	@Autowired
	private ISelectGoods selectGoodsService;
	
	@Autowired
	private IGood goodService;
	
	@Autowired
	private IDict dictService;
	
	/**
	 * 保存所选的物资
	 */
	public  void saveSelectGood(){
		
		try {
			if(userId!=null){
				UserInfo userInfo=userService.findOne(userId);
				if(StringUtils.isNoneBlank(instanceId) && goodId!=null){
					Good good= goodService.findOne(goodId);
					SelectGoods selectGoods = selectGoodsService.findByGoodIdAndInstanceId(goodId, instanceId);
					float price=good.getPrice();
			    	DecimalFormat df = new DecimalFormat("0.00");
				    if(selectGoods!=null){//修改
				    	Integer count = selectGoods.getNum()+num;
				    	selectGoods.setMoney(df.format(price*count));
				    	selectGoods.setNum(count);
				    	selectGoods.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setUpdateUserId(userInfo.getUserId());
				    	selectGoodsService.saveOrUpdate(selectGoods);
				    }else{//新增
				    	selectGoods= new SelectGoods();
				    	selectGoods.setCompanyId(userInfo.getCompanyId());
				    	selectGoods.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setUpdateUserId(userInfo.getUserId());
				    	selectGoods.setCreateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setCreateUserId(userInfo.getUserId());
				    	selectGoods.setInstanceId(instanceId);
				    	selectGoods.setGood(good);
				    	selectGoods.setIsDelete(0);
				    	selectGoods.setNum(num);
				    	selectGoods.setMoney(df.format(price*num));
				    	selectGoodsService.saveOrUpdate(selectGoods);
				    }
				}
				    ajax("100||操作成功");
				}else{
					ajax("101||参数缺少");
				}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
		
	}
	
	public void findSelectGoodList(){
		try {
			if(userId!=null){
				UserInfo user=userService.findOne(userId);
				if(user!=null){
					List<SelectGoods> list = selectGoodsService.findList(instanceId);
					List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
					Map<String,Object> jsonMap =new HashMap<String, Object>(); 
					if(list!=null && list.size()>0){
						for(SelectGoods s:list){
							Map<String,Object> map = new HashMap<String, Object>();
							Good g= s.getGood();
							map.put("id", s.getId());
							map.put("name",g.getName());//物资名称
							map.put("groupName",g.getGoodGroup().getGroupName());
							map.put("goodNo",g.getGoodNo());
							map.put("type",getGoodType(g.getType()));
							map.put("price",g.getPrice());
							map.put("num",s.getNum());
							map.put("money", s.getMoney());
							listMap.add(map);
						}
					}
					Gson gson = new Gson();
					jsonMap.put("aaData", listMap);
					ajax("100||"+gson.toJson(jsonMap));
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
	   }
	}
	/*
	 * 获得物资类型	
	 */
	private  String getGoodType(Integer type){
		String typeStr="";
		if(type==null){
			typeStr=" ";
		}else if(type==1){
			typeStr="备件";
		}else if(type==2){
			typeStr="材料";
		}else if(type==3){
			typeStr="设备";
		}else if(type==4){
			typeStr="仪表";
		}else {
			typeStr="其他";
		}
		return typeStr;
	}
	
	
	
	/**
	 * 删除已选择的物资
	 */
	public void delectSelectGoods(){
		try {
			if(userId!=null){
				UserInfo user = userService.findOne(userId);
				if(user!=null){
					selectGoodsService.delete(id, true);
					ajax("100||操作成功");
				}
			}else{
				ajax("101||参数缺少");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
		
	}
	
	
	
	
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getGoodId() {
		return goodId;
	}

	public void setGoodId(Integer goodId) {
		this.goodId = goodId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ISelectGoods getSelectGoodsService() {
		return selectGoodsService;
	}

	public void setSelectGoodsService(ISelectGoods selectGoodsService) {
		this.selectGoodsService = selectGoodsService;
	}

	public IGood getGoodService() {
		return goodService;
	}

	public void setGoodService(IGood goodService) {
		this.goodService = goodService;
	}

	public IDict getDictService() {
		return dictService;
	}

	public void setDictService(IDict dictService) {
		this.dictService = dictService;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
}
