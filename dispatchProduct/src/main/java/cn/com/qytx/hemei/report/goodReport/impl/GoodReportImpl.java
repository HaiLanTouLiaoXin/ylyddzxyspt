package cn.com.qytx.hemei.report.goodReport.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.report.goodReport.dao.GoodReportDao;
import cn.com.qytx.hemei.report.goodReport.service.IGoodReport;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service
@Transactional
public class GoodReportImpl extends BaseServiceImpl<DefectApply> implements IGoodReport {

	@Autowired
	private GoodReportDao goodReportDao;

	@Override
	public List<Object[]> findDepartmentUseGoods(String beginTime,
			String endTime, String repairTeam,  String goodName,
			String groupName) {
		// TODO Auto-generated method stub
		return goodReportDao.findDepartmentUseGoods(beginTime, endTime, repairTeam, goodName, groupName);
	}

	@Override
	public List<Object[]> findRepairPeopleUseGoods(String beginTime,
			String endTime, String repairTeam, String name, String goodName,
			String groupName) {
		// TODO Auto-generated method stub
		return goodReportDao.findRepairPeopleUseGoods(beginTime, endTime, repairTeam, name, goodName, groupName);
	}

	
	

}
