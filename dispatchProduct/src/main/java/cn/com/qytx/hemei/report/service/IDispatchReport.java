package cn.com.qytx.hemei.report.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.service.BaseService;

public interface IDispatchReport extends BaseService<DefectApply> {
  
	/**
	 *查询调度的任务
	 * @return
	 */
	List<Object[]> findList(String beginTime,String endTime,String employeeName);
	
}
