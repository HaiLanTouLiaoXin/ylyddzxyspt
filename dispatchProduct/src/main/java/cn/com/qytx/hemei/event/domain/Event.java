package cn.com.qytx.hemei.event.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

@Entity
@Table(name="tb_event")
public class Event extends BaseDomain{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	/**
	 * 事件名称
	 */
	@Column(name="event_name")
	private String eventName;
	
	/**
	 * 事件类型
	 */
	@Column(name="event_type")
	private Integer eventType;
	
	/**
	 * 紧急程度  1 紧急 2 一般 
	 */
	@Column(name="urgency_level")
	private Integer urgencyLevel;
	
	/**
	 * 完成时间类型 1 (一小时 ) 2 (12小时)   3 (1天)  4 (3天)
	 */
	@Column(name="complete_time_type")
	private Integer completeTimeType;
	
	/**
	 * 期望多少小时 
	
	private Integer completeHour; */
	
	
	/**
	 * 完成时间
	 */
	@Column(name="complete_time")
	private Timestamp completeTime;
	
	/**
	 * 维护部门Id
	 */
	@Column(name="group_id")
	private Integer groupId;
	
	
	/**
	 * 维修工时
	 */
	@Column(name="work_hour")
	private float workHour;
		
	@DeleteState
	@Column(name="is_delete")
	private Integer isDelete;
	
	@Column(name="create_user_id")
	private Integer createUserId;
	
	@Column(name="create_time")
	private Timestamp createTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_time")
	private Timestamp updateTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_user_id")
	private Integer updateUserId;

	@Transient
	private String groupName;
	
	
	
	public Integer getId() {
		return id;
	}

	public String getEventName() {
		return eventName;
	}

	public Integer getEventType() {
		return eventType;
	}

	public Integer getUrgencyLevel() {
		return urgencyLevel;
	}

	public Integer getCompleteTimeType() {
		return completeTimeType;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public float getWorkHour() {
		return workHour;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public void setUrgencyLevel(Integer urgencyLevel) {
		this.urgencyLevel = urgencyLevel;
	}

	public void setCompleteTimeType(Integer completeTimeType) {
		this.completeTimeType = completeTimeType;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public void setWorkHour(float workHour) {
		this.workHour = workHour;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(Timestamp completeTime) {
		this.completeTime = completeTime;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
