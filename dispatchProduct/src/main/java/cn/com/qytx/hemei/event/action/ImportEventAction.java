package cn.com.qytx.hemei.event.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;

import cn.com.qytx.cbb.file.config.FilePathConfig;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.hemei.event.domain.EventImporVo;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.hemei.util.excel.ExcelImport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


public class ImportEventAction extends BaseActionSupport{
	private static final long serialVersionUID = 4508105073089312570L;

	
	@Resource(name="filePathConfig")
    private FilePathConfig filePathConfig;
	
	@Autowired
	private IEvent eventService;
	
	@Autowired
	private IGroup groupService;
	
	
	
	
	protected File fileToUpload;
    private String uploadFileName;//设置上传文件的文件名
    private String file;//上传的文件
    private List<EventImporVo> erroImportEventVoList=new ArrayList<EventImporVo>();
    private String errorFile;
    
    private Integer eventType;
    
    private Integer companyId;
     
    public String importEvent(){
    	try {
    	UserInfo userInfo = this.getLoginUser();
    	if(userInfo != null){
        	String result = "";
            if (uploadFile()) {
            	File tempFile = new File(file);
            	if(!tempFile.exists()){
            		result = "要导入的文件不存在,请重新导入!";
            		return null;
            	}
                ExcelImport<EventImporVo> schoolExcel = new ExcelImport(EventImporVo.class);
        	    List<EventImporVo> eventVoList = (ArrayList) schoolExcel.importExcel(1,tempFile);
        	    int importSuccessNum = 0;
        	    if(eventVoList != null && eventVoList.size() > 0){
        	    	String eventName = "";
        	    	String groupName = "";
        	    	String urgencyLevel = "";
        	    	String workHour = "";
        	    	for(EventImporVo e:eventVoList){
        	    		boolean isErrorRecord = true;
        	    		eventName = e.getEventName();
        	    		groupName = e.getGroupName();
        	    		urgencyLevel = e.getUrgencyLevel();
        	    		workHour = e.getWorkHour();
        	    		StringBuffer errorRecord = new StringBuffer();
        	    		
        	    		if(StringUtils.isNotBlank(eventName)){
        	    			Event event= eventService.findByName(eventName);
        	    			if(event!=null){
        	    				errorRecord.append("<事件名称不能重复!>");
        	    				isErrorRecord = false;
        	    			}
        	    		}else{
        	    			errorRecord.append("<事件名称不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		
        	    	
        	    		if(!StringUtils.isNotBlank(groupName)){
        	    			errorRecord.append("<维修部门不能为空!>");
    	    				isErrorRecord = false;
        	    		}else{
        	    		    GroupInfo g = groupService.findByName(groupName);
        	    		    if(g==null){
        	    		    	errorRecord.append("<维修部门不存在!>");
        	    				isErrorRecord = false;
        	    		    }
        	    		}
        	    		
        	    		if(!StringUtils.isNotBlank(urgencyLevel)){
        	    			errorRecord.append("<事件紧急程度不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		//验证是否是正确的
        	    		if(isErrorRecord){
        	    			Event event = new Event();
        	    			event.setEventName(eventName);
        	    			if("紧急".equals(urgencyLevel)){
        	    				event.setUrgencyLevel(1);
        	    			}else if("一般".equals(urgencyLevel)){
        	    				event.setUrgencyLevel(2);
        	    			}
        	    			if(StringUtils.isNotBlank(workHour)){
        	    				event.setWorkHour(Float.parseFloat(workHour));
        	    			}
        	    			 GroupInfo g = groupService.findByName(groupName);
        	    			 event.setGroupId(g.getGroupId());
        	    			 event.setCompanyId(userInfo.getCompanyId());
        	    			 event.setIsDelete(0);
        	    			 event.setCreateTime(new Timestamp(System.currentTimeMillis()));
        	    			 event.setCreateUserId(userInfo.getUserId());
        	    			 event.setEventType(eventType);
        	    			 eventService.saveOrUpdate(event);
        	    			importSuccessNum ++;
        	    		}else{
        	    			e.setImportErrorRecord(errorRecord.toString());
        	    			erroImportEventVoList.add(e);
        	    		}
        	    	}
        	    	
    	    	}
                String basePath = getRequest().getScheme() + "://" + getRequest().getServerName() + ":" + getRequest().getServerPort()+"/";
        	    if(erroImportEventVoList != null && erroImportEventVoList.size() > 0){
        	    	errorFile= exportResult();
        	    	result = "导入成功"+importSuccessNum+"条记录,"+erroImportEventVoList.size()+"条记录导入失败,请您<a href='"+basePath+"files"+errorFile+"'><font color='blue'>点击下载失败文件</font>"+"</a>";
        	    }else{
        	    	result = "导入成功!";
        	    }
        	    ajax(result);
            }
    	}
    } catch (Exception e) {
    	e.printStackTrace();
            LOGGER.error(e.getMessage());
            ajax("对不起！导入文件时出错！");
        }
        return null;
    	
    	
    }
    
    
    /**
     * 导出结果
     */
    private String exportResult()
    {
        String filePath="";
        try
        {
           if(erroImportEventVoList!=null && erroImportEventVoList.size()>0)
           {
               List<String> headList=new ArrayList<String>();
               headList.add("事件名称");
               headList.add("部门名称");
               headList.add("紧急程度");
               headList.add("维修工时");
               headList.add("失败原因");
               List<Map<String, Object>> mapList=new ArrayList<Map<String, Object>>();  //表内容
               Map<String, Object> map=null;
               int no = 1;
               for(EventImporVo eVo:erroImportEventVoList)
               {
                    map=new HashMap<String, java.lang.Object>();
                    map.put("no",no++);                   
                    map.put("eventName",eVo.getEventName());
	                map.put("groupName", eVo.getGroupName());
	                map.put("urgencyLevel", eVo.getUrgencyLevel());
	                map.put("workHour", eVo.getWorkHour());
	                map.put("failureReason",eVo.getImportErrorRecord());
                    mapList.add(map);
               }
               List<String> keyList=new ArrayList<String>();   //map的key
               keyList.add("eventName");
               keyList.add("groupName");
               keyList.add("urgencyLevel");
               keyList.add("workHour");
               keyList.add("failureReason");//导入错误提示记录
               String fileName="eventImport-"+UUID.randomUUID().toString()+".xls";
               String uploadPath = ""; // 上传的目录
               String catalinaHome = filePathConfig.getFileUploadPath();
               SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
               String datePath = sp.format(new Date()); // 每月一个上传目录
               uploadPath = "/upload/importFail/" + datePath + "/";
               String path = catalinaHome + uploadPath;
               File checkPath = new File(path);
               if (!checkPath.exists()) {
                   //目录不存在，则创建目录
                   checkPath.mkdirs();
               }
               OutputStream os = new FileOutputStream(path+"/"+fileName);
               ExportExcel export=new ExportExcel(os,headList,mapList,keyList);
               export.export();
               filePath = uploadPath+fileName;
           }
        }
        catch (Exception ex)
        {
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
        }
        return filePath;
    }
    
    
    /**
     * 上传文件
     */
    @SuppressWarnings("unused")
	private boolean uploadFile() {
        String uploadPath = ""; // 上传的目录
        String catalinaHome = filePathConfig.getFileUploadPath();
        SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
        String datePath = sp.format(new Date()); // 每月一个上传目录
        uploadPath = "/upload/import/" + datePath + "/";
        String targetDirectory = catalinaHome + uploadPath;
        try {
            if (fileToUpload != null) {
                String fileName = UUID.randomUUID().toString();
                String ext = ".xls";
                if (uploadFileName != null) {
                    ext = uploadFileName.substring(uploadFileName.lastIndexOf("."));
                }
                if (!ext.equals(".xls")) {
                    return false;
                }
                String saveFileName = fileName + ext;
                File savefile = new File(new File(targetDirectory), saveFileName);
                if (!savefile.getParentFile().exists()){
                    boolean result = savefile.getParentFile().mkdirs();
                    if (!result){
                        return false;
                    }
                }
                FileUtils.copyFile(fileToUpload, savefile);//拷贝文件
                file = targetDirectory + "/" + saveFileName;
            }

        } catch (Exception e) {

            return false;
        }
        return true;
    }
    
    /**
     * 判断字符串是否是整数
     */
    public static boolean isInteger(String value) {
     try {
      Integer.parseInt(value);
      return true;
     } catch (NumberFormatException e) {
      return false;
     }
    }

    
 
  


	public File getFileToUpload() {
		return fileToUpload;
	}


	public String getUploadFileName() {
		return uploadFileName;
	}


	public String getFile() {
		return file;
	}


	public List<EventImporVo> getErroImportEventVoList() {
		return erroImportEventVoList;
	}


	public String getErrorFile() {
		return errorFile;
	}


	public void setFileToUpload(File fileToUpload) {
		this.fileToUpload = fileToUpload;
	}


	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}


	public void setFile(String file) {
		this.file = file;
	}


	public void setErroImportPigVoList(List<EventImporVo> erroImportEventVoList) {
		this.erroImportEventVoList = erroImportEventVoList;
	}


	public void setErrorFile(String errorFile) {
		this.errorFile = errorFile;
	}


	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}


	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public Integer getEventType() {
		return eventType;
	}


	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	};
	
	
	
}
