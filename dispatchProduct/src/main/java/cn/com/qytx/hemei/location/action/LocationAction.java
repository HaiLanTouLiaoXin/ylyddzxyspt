package cn.com.qytx.hemei.location.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.hemei.location.service.IGroupLocation;
import cn.com.qytx.hemei.location.service.ILocation;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;

/**
 * 功能：地址
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年4月19日
 * 修改日期：2017年4月19日	
 */
public class LocationAction extends BaseActionSupport{
	private static final long serialVersionUID = -7588771189909689456L;
	
	
	/**
	 * 地点业务接口
	 */
	@Resource(name = "locationService")
	private ILocation locationService;
	
	@Resource(name = "groupLocationService")
	private IGroupLocation groupLocationService;
	
	
	@Resource(name="userService")
	private IUser userService;
	
	@Resource(name="groupService")
	private IGroup groupService;
	/**
	 * 1 代表左侧树 2 代表 下拉树
	 */
	private Integer treeType;

	private Location location;
	
	private Integer userId;
	
	private Integer groupId;
	
	private String workNo;
	
	
	public String getUserLocationName(){
		try{
			UserInfo userInfo = this.getLoginUser();
			if(userId!=null){
				userInfo = userService.findOne(userId);
			}
			if(groupId==null){
				groupId = userInfo.getGroupId();
			}
			List<GroupLocation> glList = groupLocationService.findList(groupId);
			if(glList!=null&&glList.size()>0){
				List<Location> newLocationList = locationService.getLocationPathListByIds(glList.get(0).getLocationId()+"",userInfo.getCompanyId());
				if(newLocationList!=null&&newLocationList.size()>0){
					location = newLocationList.get(0);
					Gson gson = new Gson();
					ajax(gson.toJson(location));
					return null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String getUserInfoByPhone(){
		try{
			UserInfo userInfo = this.getLoginUser();
			GroupInfo userGroupInfo = groupService.findOne(userInfo.getGroupId());
			UserInfo phoneUser = null;
			if(StringUtils.isNoneBlank(workNo)){
				phoneUser = userService.findOne(" workNo=? or officeTel=? or homeTel=?",workNo, workNo,workNo);
			}
			Map<String,Object> resultMap = new HashMap<String,Object>();
			groupId = phoneUser!=null?phoneUser.getGroupId():userInfo.getGroupId();
			GroupInfo groupInfo = groupService.findOne(phoneUser!=null?phoneUser.getGroupId():userInfo.getGroupId());
			resultMap.put("createGroupId", phoneUser!=null?phoneUser.getGroupId():userInfo.getGroupId());
			resultMap.put("applyPhoneName", phoneUser!=null?phoneUser.getUserName():"");
			resultMap.put("createGroupName", groupInfo!=null?groupInfo.getGroupName():userGroupInfo.getGroupName());
			List<GroupLocation> glList = groupLocationService.findList(groupId);
			String pathName = "";
			if(glList!=null&&glList.size()>0){
				List<Location> newLocationList = locationService.getLocationPathListByIds(glList.get(0).getLocationId()+"",userInfo.getCompanyId());
				if(newLocationList!=null&&newLocationList.size()>0){
					location = newLocationList.get(0);
					pathName = location.getPathName();
				}
			}
			resultMap.put("pathName", pathName);
			resultMap.put("isExist", phoneUser!=null?"1":"0");
			Gson gson = new Gson();
			ajax(gson.toJson(resultMap));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
		
	/**
	 * 获取资源组树列表
	 */
	public void capitalGroupTreeList() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				String contextPath = getRequest().getContextPath();
				List<TreeNode> list = null;
				if(treeType!=null&&treeType.intValue()==1){
					list = locationService
							.getTreeLocationList(contextPath,user.getCompanyId(),null);
				}else{
					list = locationService
							.getTreeLocationList_Select(contextPath,user.getCompanyId());
				}
				Gson json = new Gson();
				ajax(json.toJson(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 地点信息修改或新增地点 地点信息修改 成功：1； 地点添加成功：0；
	 */
	public String saveOrUpdateLocation() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				if (location.getId() != null) {//修改
					Location oldLocation = locationService.findOne(location.getId());
					if(oldLocation!=null){
						String oldGroupName = oldLocation.getLocationName();
						//判断是否存在地点名称
				    	if(!oldGroupName.equals(location.getLocationName())){
				    		boolean  isSame=locationService.isHasSameLocationName(oldLocation.getParentId(), location.getLocationName(),user.getCompanyId());
				        	if(isSame){
				                ajax("2");
				                return null;
				        	}
				    	}
				    	oldLocation.setLocationName(location.getLocationName());
				    	oldLocation.setOrderIndex(location.getOrderIndex());
				    	oldLocation.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	oldLocation.setUpdateUserId(user.getUserId());
						locationService.saveOrUpdate(oldLocation);
						ajax("1");
					}
				} else {
						Integer parentId = 0;
						if(location.getParentId()!=null){
							parentId = location.getParentId();
						}
						//判断是否存在地点名称
						boolean isExistGroupName = locationService.isHasSameLocationName(parentId, location.getLocationName(), user.getCompanyId());
						if(isExistGroupName){
							ajax("2");
							return null;
						}
						location.setCompanyId(user.getCompanyId());
						location.setCreateTime(new Timestamp(System.currentTimeMillis()));
						location.setCreateUserId(user.getUserId());
						location.setIsDelete(0);
						location.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						location.setUpdateUserId(user.getUserId());
						locationService.saveOrUpdate(location);
						Location parentLocation = locationService.findOne(parentId);
						if(parentLocation!=null){
							//设置路径
			    			String pathIdParent = parentLocation.getPath();
			    			if(StringUtils.isNotBlank(pathIdParent)){
			    				location.setPath(pathIdParent+location.getId()+",");
			    			}
			            	//设置级别
			    			Integer gradeParent=parentLocation.getGrade();
			    			if(gradeParent!=null){
			    				location.setGrade(gradeParent+1);
			    			}
						}else{
							location.setPath(","+String.valueOf(location.getId())+",");
							location.setGrade(1);
						}
						locationService.saveOrUpdate(location);
						ajax("0_"+location.getId());// 添加成功
					}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
	
	
	/**
	 * 功能：删除地点
	 * @return
	 */
	public String deleteLocation(){
		UserInfo userInfo = this.getLoginUser();
		try{
			if(userInfo!=null){
				boolean isHasChild = locationService.isHasChildLocation(location.getId(),userInfo.getCompanyId());
				if(isHasChild){
					ajax("1");
					return null;
				}
				locationService.delete(location.getId(), false);
				ajax("0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
    

	
	/**
	 * 功能：获取地点树列表,通过部门Id
	 * @return
	 */
	public String locationTreeByGroupId(){
		try{
			UserInfo userInfo = this.getLoginUser();
			if(userId!=null){
				userInfo = userService.findOne(userId);
			}
			if(groupId==null){
				groupId = userInfo.getGroupId();
			}
			List<GroupLocation> groupLocationList = groupLocationService.findList(groupId);
			String contextPath = getRequest().getContextPath();
			List<TreeNode> tnList = new ArrayList<TreeNode>();
			if(groupLocationList!=null&&groupLocationList.size()>0){
				for(GroupLocation gl:groupLocationList){
					TreeNode treeNode = new TreeNode();
					treeNode.setId("gid_"+gl.getLocationId());
					treeNode.setName(gl.getPathName());
					treeNode.setPId("gid_0");
					treeNode.setObj(1);// 排序号
					treeNode.setIcon(contextPath + "/images/group.png");
					tnList.add(treeNode);
				}
			}
			Gson gson = new Gson();
			String gsonStr = gson.toJson(tnList);
			ajax(gsonStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Integer getTreeType() {
		return treeType;
	}

	public void setTreeType(Integer treeType) {
		this.treeType = treeType;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	
}
