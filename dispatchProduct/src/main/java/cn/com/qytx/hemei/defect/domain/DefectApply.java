package cn.com.qytx.hemei.defect.domain;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

@Table(name="tb_defect_apply")
@Entity
public class DefectApply extends BaseDomain{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;

	@Column(name="create_user_id")
	private Integer createUserId;
	
	@Column(name="create_group_id")
	private Integer createGroupId;
	
	@Column(name="create_time")
	private Timestamp createTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_time")
	private Timestamp updateTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	@Column(name="instance_id")
	private String instanceId;//申请实例 规则 userId+yyyyMMddHHmmss
	/**
	 *  0（待接收消缺） 
	 *  1接收通过（待消缺）
	 *  2消缺完成（待验收）
	 *  3验收通过（待归档）
	 *  4归档完成（已归档）
	 *  5接收拒绝（待作废）
	 *  6接收转交（待接收）
	 *  7验收驳回（待消缺）
	 *  8归档驳回（待验收） 
	 *  9上报作废（已作废）
	 *  10 缺陷延期 (待延期)
	 *  11 延期通过 (延期中)
	 *  12 延期驳回 (待消缺)
	 *  
	 *  -1 待调度
	 *  
	 */
	@Column(name="state")
	private String state;//申请状态
	@Column(name="process_id")
	private Integer processId;//当前处理人ID
	@Column(name="process_name")
	private String processName;//当前处理人名字
	@Column(name="process_type")
	private Integer processType;//当前处理权限  1 人员 2 部门 3 角色
	@Column(name="previous_process_id")
	private Integer previousProcessId;//上一步处理人ID
	@Column(name="grade")
	private String grade;//缺陷等级
	@Column(name="is_repair")
	private Integer isRepair;//是否大修（0无1有）
	@Column(name="describe")
	private String describe;//缺陷描述
	@Column(name="equipment_name")
	private String equipmentName;//设备名称
	@Column(name="defect_number")
	private String defectNumber;//缺陷编号
	@Column(name="equipment_number")
	private String equipmentNumber;//设备编号  设备数量
	@Column(name="equipment_state")
	private String equipmentState;//设备状态
	@Column(name="equipment_unit")
	private String equipmentUnit;//所属机组
	@Column(name="defect_department")
	private String defectDepartment;//消缺部门
	@Column(name="defect_professional")
	private String defectProfessional;//消缺专业
	@Column(name="defect_time")
	private Timestamp defectTime;//期望消缺时间
	@Column(name="real_defect_time")
	private Timestamp realDefectTime;//实际消缺时间
	@Column(name="memo")
	private String memo;//备注
	@Column(name="attachment_ids")
	private String attachmentIds;//附件ids
	
	//验收时添加图片
	@Column(name="check_attachment_ids")
	private String checkAttachmentIds;
	
	
	
	@Column(name="turn_num")
	private Integer turnNum;//转交次数
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete=0;
	
	@Column(name="is_delay")
	private Integer isDelay;//0 正常 1 待延期 2 延期中 3延期驳回
	
	@Column(name="delay_process_id") 
	private Integer delayProcessId;//延期处理人ID
	
	@Column(name="delay_user_id")
	private Integer delayUserId;//申请延期人ID
	
	@Column(name="audio_ids")
	private String audioIds;//录音ID串
	
	@Column(name="audio_time")
	private String audioTime;//音频时长 秒数
	
	@Column(name="achievement")
	private Integer achievement;//满意度（1.非常满意。 2.满意。3一般。4.不满意）
	
	@Column(name="evaluate")
	private String evaluate;
	
	private Integer type;//0维修上报  1 配送上报 2 归还上报
	
	@Transient
	private String achievementStr;//满意度
	@Transient
	private String stateVo;
	@Transient
	private List<MyProcessed> historyList;
	@Transient
	private String gradeVo;
	@Transient
	private String equipmentStateVo;
	@Transient
	private String equipmentUnitVo;
	@Transient
	private String defectDepartmentVo;
	@Transient
	private String defectProfessionalVo;
	@Transient
	private Integer isDraft=0;//是否可以作废，0可以 1不可以
	@Transient
	private String createUserName;//上报人
	
	@Transient
	private String createGroupName;
	
	@Transient
	private String createUserPhone;//上报人手机号
	
	@Transient
	private String createUserWorkNo;//上报人工号
	
	
	@Transient
	private String createTimeStr;//上报时间字符串
	
	@Transient
	private String defectTimeStr;//消缺时间字符串
	
	@Transient
	private String delayStateVo;
	
	@Column(name="apply_phone")
	private String applyPhone;//上报工单  的来电号码
	
	@Column(name="apply_phone_name")
	private String applyPhoneName;
	/**
	 * 调度上报 用户ID
	 */
	@Column(name="dispatch_work_no")
	private String dispatchWorkNo;
	
	/**
	 * 催单次数
	 */
	@Column(name="reminder_num")
	private Integer reminderNum;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getProcessId() {
		return processId;
	}
	public void setProcessId(Integer processId) {
		this.processId = processId;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public Integer getIsRepair() {
		return isRepair;
	}
	public void setIsRepair(Integer isRepair) {
		this.isRepair = isRepair;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getEquipmentNumber() {
		return equipmentNumber;
	}
	public void setEquipmentNumber(String equipmentNumber) {
		this.equipmentNumber = equipmentNumber;
	}
	public String getEquipmentState() {
		return equipmentState;
	}
	public void setEquipmentState(String equipmentState) {
		this.equipmentState = equipmentState;
	}
	public String getEquipmentUnit() {
		return equipmentUnit;
	}
	public void setEquipmentUnit(String equipmentUnit) {
		this.equipmentUnit = equipmentUnit;
	}
	public String getDefectDepartment() {
		return defectDepartment;
	}
	public void setDefectDepartment(String defectDepartment) {
		this.defectDepartment = defectDepartment;
	}
	public String getDefectProfessional() {
		return defectProfessional;
	}
	public void setDefectProfessional(String defectProfessional) {
		this.defectProfessional = defectProfessional;
	}
	public Timestamp getDefectTime() {
		return defectTime;
	}
	public void setDefectTime(Timestamp defectTime) {
		this.defectTime = defectTime;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	public Integer getPreviousProcessId() {
		return previousProcessId;
	}
	public void setPreviousProcessId(Integer previousProcessId) {
		this.previousProcessId = previousProcessId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getAttachmentIds() {
		return attachmentIds;
	}
	public void setAttachmentIds(String attachmentIds) {
		this.attachmentIds = attachmentIds;
	}
	public String getStateVo() {
		return stateVo;
	}
	public void setStateVo(String stateVo) {
		this.stateVo = stateVo;
	}
	public List<MyProcessed> getHistoryList() {
		return historyList;
	}
	public void setHistoryList(List<MyProcessed> historyList) {
		this.historyList = historyList;
	}
	public String getEquipmentName() {
		return equipmentName;
	}
	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
	public Timestamp getRealDefectTime() {
		return realDefectTime;
	}
	public void setRealDefectTime(Timestamp realDefectTime) {
		this.realDefectTime = realDefectTime;
	}
	public String getDefectNumber() {
		return defectNumber;
	}
	public void setDefectNumber(String defectNumber) {
		this.defectNumber = defectNumber;
	}
	public String getGradeVo() {
		return gradeVo==null?"":gradeVo;
	}
	public void setGradeVo(String gradeVo) {
		this.gradeVo = gradeVo;
	}
	public String getEquipmentStateVo() {
		return equipmentStateVo==null?"":equipmentStateVo;
	}
	public void setEquipmentStateVo(String equipmentStateVo) {
		this.equipmentStateVo = equipmentStateVo;
	}
	public String getEquipmentUnitVo() {
		return equipmentUnitVo==null?"":equipmentStateVo;
	}
	public void setEquipmentUnitVo(String equipmentUnitVo) {
		this.equipmentUnitVo = equipmentUnitVo;
	}
	public String getDefectDepartmentVo() {
		return defectDepartmentVo==null?"":defectDepartmentVo;
	}
	public void setDefectDepartmentVo(String defectDepartmentVo) {
		this.defectDepartmentVo = defectDepartmentVo;
	}
	public String getDefectProfessionalVo() {
		return defectProfessionalVo==null?"":defectProfessionalVo;
	}
	public void setDefectProfessionalVo(String defectProfessionalVo) {
		this.defectProfessionalVo = defectProfessionalVo;
	}
	public Integer getTurnNum() {
		return turnNum;
	}
	public void setTurnNum(Integer turnNum) {
		this.turnNum = turnNum;
	}
	public Integer getIsDraft() {
		return isDraft;
	}
	public void setIsDraft(Integer isDraft) {
		this.isDraft = isDraft;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateTimeStr() {
		return createTimeStr;
	}
	public String getDefectTimeStr() {
		return defectTimeStr;
	}
	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
	public void setDefectTimeStr(String defectTimeStr) {
		this.defectTimeStr = defectTimeStr;
	}
	public Integer getIsDelay() {
		return isDelay;
	}
	public Integer getDelayProcessId() {
		return delayProcessId;
	}
	public void setIsDelay(Integer isDelay) {
		this.isDelay = isDelay;
	}
	public void setDelayProcessId(Integer delayProcessId) {
		this.delayProcessId = delayProcessId;
	}
	public Integer getDelayUserId() {
		return delayUserId;
	}
	public void setDelayUserId(Integer delayUserId) {
		this.delayUserId = delayUserId;
	}
	public String getDelayStateVo() {
		return delayStateVo;
	}
	public void setDelayStateVo(String delayStateVo) {
		this.delayStateVo = delayStateVo;
	}
	public String getAudioIds() {
		return audioIds;
	}
	public void setAudioIds(String audioIds) {
		this.audioIds = audioIds;
	}
	public String getAudioTime() {
		return audioTime;
	}
	public void setAudioTime(String audioTime) {
		this.audioTime = audioTime;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public Integer getAchievement() {
		return achievement;
	}
	public void setAchievement(Integer achievement) {
		this.achievement = achievement;
	}
	public String getAchievementStr() {
		return achievementStr;
	}
	public void setAchievementStr(String achievementStr) {
		this.achievementStr = achievementStr;
	}
	public String getCreateUserPhone() {
		return createUserPhone;
	}
	public void setCreateUserPhone(String createUserPhone) {
		this.createUserPhone = createUserPhone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getCreateUserWorkNo() {
		return createUserWorkNo;
	}
	public void setCreateUserWorkNo(String createUserWorkNo) {
		this.createUserWorkNo = createUserWorkNo;
	}
	public String getApplyPhone() {
		return applyPhone;
	}
	public void setApplyPhone(String applyPhone) {
		this.applyPhone = applyPhone;
	}
	public Integer getCreateGroupId() {
		return createGroupId;
	}
	public void setCreateGroupId(Integer createGroupId) {
		this.createGroupId = createGroupId;
	}
	public String getApplyPhoneName() {
		return applyPhoneName;
	}
	public void setApplyPhoneName(String applyPhoneName) {
		this.applyPhoneName = applyPhoneName;
	}
	
	public String getDispatchWorkNo() {
		return dispatchWorkNo;
	}
	public void setDispatchWorkNo(String dispatchWorkNo) {
		this.dispatchWorkNo = dispatchWorkNo;
	}
	public String getCreateGroupName() {
		return createGroupName;
	}
	public void setCreateGroupName(String createGroupName) {
		this.createGroupName = createGroupName;
	}
	public Integer getReminderNum() {
		return reminderNum;
	}
	public void setReminderNum(Integer reminderNum) {
		this.reminderNum = reminderNum;
	}
	public String getCheckAttachmentIds() {
		return checkAttachmentIds;
	}
	public void setCheckAttachmentIds(String checkAttachmentIds) {
		this.checkAttachmentIds = checkAttachmentIds;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
	
	
}
