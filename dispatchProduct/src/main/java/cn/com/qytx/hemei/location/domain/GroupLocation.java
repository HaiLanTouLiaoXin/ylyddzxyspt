package cn.com.qytx.hemei.location.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
@Entity
@Table(name="tb_group_location")
public class GroupLocation extends BaseDomain{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	/**
	 * 部门Id
	 */
	@Column(name="group_id")
	private Integer groupId;
	
	/**
	 * 位置Id
	 */
	@Column(name="location_id")
	private Integer locationId;
	
	@Column(name="create_user_id")
	private Integer createUserId;
	
	@Column(name="create_time")
	private Timestamp createTime=new Timestamp(System.currentTimeMillis());

	/**
	 * 位置路径
	 */
	@Column(name="path_name")
	private String pathName;
	
	public Integer getId() {
		return id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}
	
	
	
	
}
