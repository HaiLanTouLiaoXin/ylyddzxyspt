package cn.com.qytx.hemei.location.action;

import java.util.List;

import javax.annotation.Resource;

import com.google.gson.Gson;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.hemei.location.service.IGroupLocation;
import cn.com.qytx.hemei.location.service.ILocation;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;

/**
 * 功能：手机端地点接口
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年4月20日
 * 修改日期：2017年4月20日	
 */
public class LocationWapAction extends BaseActionSupport{
	private static final long serialVersionUID = 1684763121661892141L;

	@Resource(name="locationService")
	private ILocation locationService;
	
	@Resource(name="userService")
	private IUser userService;
	
	
	@Resource(name = "groupLocationService")
	private IGroupLocation groupLocationService;
	
	private Integer userId;
	
	private Integer groupId;//部门Id
	
	/**
	 * 是否默认展开
	 */
	private Integer isOpen;
	
	/**
	 * 获取地点列表
	 */
	public String locationTreeList() {
		try {
			 if(userId==null){
		        	ajax("101||参数不能为空!");
		        	return null;
		        }
			 UserInfo userInfo = userService.findOne(userId);
			String contextPath = getRequest().getContextPath();
			List<TreeNode> list = null;
			list = locationService
							.getTreeLocationList(contextPath,userInfo.getCompanyId(),isOpen);
			Gson json = new Gson();
			ajax("100||"+json.toJson(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public String getUserLocationName(){
		try{
			if(userId==null && groupId==null){
	        	ajax("101||参数不能为空!");
	        	return null;
	        }
			UserInfo userInfo = null;
			if(userId!=null){
				userInfo = userService.findOne(userId);
			}
			if(groupId==null){
				groupId = userInfo.getGroupId();
			}
			List<GroupLocation> glList = groupLocationService.findList(groupId);
			if(glList!=null&&glList.size()>0){
				List<Location> newLocationList = locationService.getLocationPathListByIds(glList.get(0).getLocationId()+"",userInfo.getCompanyId());
				if(newLocationList!=null&&newLocationList.size()>0){
					Location location = newLocationList.get(0);
					Gson gson = new Gson();
					ajax("100||"+gson.toJson(location));
					return null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	
	
}
