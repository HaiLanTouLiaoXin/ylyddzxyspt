package cn.com.qytx.hemei.defect.dao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.defect.domain.SearchVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.PageImpl;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.utils.DateUtils;

@Repository
public class DefectApplyDao extends BaseDao<DefectApply, Serializable> {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 查询详情
	 * @Title: findByInstanceId   
	 * @param instanceId
	 * @return
	 */
	public DefectApply findByInstanceId(String instanceId) {
		String hql="isDelete=0 and instanceId=?";
		List<DefectApply> list=super.findAll(hql, instanceId);
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}
	/**
	 * 我发起的列表
	 * @Title: myStarted   
	 * @param pageable
	 * @param userId
	 * @param searchVo 
	 * @return
	 */
	public Page<DefectApply> myStarted(Pageable pageable, Integer userId, SearchVo searchVo) {
		String hql="isDelete=0 ";
		List<Object> params=new ArrayList<Object>();
		if(userId!=null){
			hql+=" and createUserId=? ";
			params.add(userId);
		}
		if(searchVo!=null){
			if(searchVo.getGroupId()!=null){
				//hql = " and createUserId in (select userId from UserInfo u where u.groupId in (select g.groupId from GroupInfo g where (g.groupId="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%')) )";
				hql += " and createGroupId in (select groupId from GroupInfo g where (g.groupId="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%'))";
			}
			if(StringUtils.isNotBlank(searchVo.getDispatchUserName())){
				hql += " and dispatchWorkNo in (select workNo from UserInfo u where u.userName like '%"+searchVo.getDispatchUserName()+"%')";
			}
			if(StringUtils.isNotBlank(searchVo.getUserName())){
				hql += " and createUserId in (select userId from UserInfo u where u.userName like '%"+searchVo.getUserName()+"%')";
			}
			if(StringUtils.isNotBlank(searchVo.getCreateGroupName())){
				hql += " and createGroupId in (select groupId from GroupInfo g where g.groupName like '%"+searchVo.getCreateGroupName()+"%')";
			}
			if(StringUtils.isNotBlank(searchVo.getProcessUserName())){
				hql += " and ( (state in (2,8) and previousProcessId in (select u.userId from UserInfo u where u.userName like '%"+searchVo.getProcessUserName()+"%')) or (state=3 and instanceId in (select md.instanceId from MyProcessed md  where md.taskName='3' and md.processerId like '%"+searchVo.getProcessUserName()+"%')) or (state not in (2,3,8) and processType=1  and processId in (select u.userId from UserInfo u where u.userName like '%"+searchVo.getProcessUserName()+"%') ) )";
			}
			if(StringUtils.isNotBlank(searchVo.getProcessUserGroup())){
				hql += " and ( (state in (2,8) and previousProcessId in (select u.userId from UserInfo u where u.groupId in(select groupId from GroupInfo g where g.groupName like '%"+searchVo.getProcessUserGroup()+"%') )) or (state not in (2,3,8) and processType=2  and processId in (select g.groupId from GroupInfo g where g.groupName like '%"+searchVo.getProcessUserGroup()+"%') ) or (state=3 and instanceId in (select md.instanceId from MyProcessed md  where md.taskName='3' and md.processerId in (select u.userId from UserInfo u where u.groupId in (select groupId from GroupInfo g where g.groupName like '%"+searchVo.getProcessUserGroup()+"%')))) )";
			}
			/*if(searchVo.getProcessGroupId()!=null){
				hql += " and ( (state in (2,8) and previousProcessId in (select u.userId from UserInfo u where u.groupId in(select groupId from GroupInfo g where g.groupId="+searchVo.getProcessGroupId()+" or g.path like '"+searchVo.getProcessGroupId()+",%') )) or (state not in (2,3,8) and processType=2  and processId in (select g.groupId from GroupInfo g where g.groupId="+searchVo.getProcessGroupId()+" or g.path like '"+searchVo.getProcessGroupId()+",%') ) or (state=3 and instanceId in (select md.instanceId from MyProcessed md  where md.taskName='3' and md.processerId in (select u.userId from UserInfo u where u.groupId in (select groupId from GroupInfo g where g.groupId="+searchVo.getProcessGroupId()+" or g.path like '"+searchVo.getProcessGroupId()+",%')))) )";
			}*/
			if(searchVo.getType()!=null){
				hql+=" and type=?";
				params.add(Integer.valueOf(searchVo.getType()));
			}else{
				
			}
			if(StringUtils.isNotBlank(searchVo.getDelayUserId())){
				hql+=" and delayUserId=?";
				params.add(Integer.valueOf(searchVo.getDelayUserId()));
			}
			
			if(StringUtils.isNotBlank(searchVo.getDelayProcessId())){
				hql+=" and delayProcessId=? and isDelay=2";
				params.add(Integer.valueOf(searchVo.getDelayProcessId()));
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				hql+=" and defectDepartment=? ";
				params.add(searchVo.getDefectDepartment());
			}
			if(searchVo.getIsDelay()!=null){
				hql+=" and isDelay!=0 and state in (1,7)";
				if(StringUtils.isNotBlank(searchVo.getDelayState())){
					hql+=" and isDelay in ("+searchVo.getDelayState()+") ";
				}
			}
			if(StringUtils.isNoneBlank(searchVo.getGrade())){
				hql+=" and grade=?";
				params.add(searchVo.getGrade());
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				hql+=" and defectProfessional=? ";
				params.add(searchVo.getDefectProfessional());
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				hql+=" and equipmentState=? ";
				params.add(searchVo.getEquipmentState());
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				hql+=" and equipmentUnit=? ";
				params.add(searchVo.getEquipmentUnit());
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				hql+=" and equipmentName like '%"+searchVo.getSearchName()+"%' ";
			}
			
			if(searchVo.getTimeType()!=null){//最近一个月
				if(StringUtils.isNotBlank(searchVo.getState())&&!"1,7".equals(searchVo.getState())){
					hql += " and createTime>='"+DateUtils.date2YYYYMMStr(new Date())+"-01"+" 00:00:00'";
					hql+=" and state in ("+searchVo.getState()+") ";
				}else{
					hql += " and ((createTime>='"+DateUtils.date2YYYYMMStr(new Date())+"-01"+" 00:00:00' ";
					if(StringUtils.isNotBlank(searchVo.getState())){
						hql+=" and state in ("+searchVo.getState()+")) ";
					}else{
						hql += ")";
					}
					hql+=" or state in (1,7))";
				}
			}else{
				if(StringUtils.isNotBlank(searchVo.getStartTime())){
					hql += " and createTime>='"+searchVo.getStartTime()+"'";
				}
				if(StringUtils.isNotBlank(searchVo.getEndTime())){
					hql += " and createTime<='"+searchVo.getEndTime()+"'";
				}
				if(StringUtils.isNotBlank(searchVo.getState())){
					hql+=" and state in ("+searchVo.getState()+") ";
				}
			}
			
			
		}
		return super.findAll(hql, pageable, params.toArray());
	}
	/**
	 * 待我处理的
	 * @Title: myWaitProcess   
	 * @param pageable
	 * @param userId
	 * @param taskName
	 * @return
	 */
	public Page<DefectApply> myWaitProcess(Pageable pageable, Integer userId, String taskName, SearchVo searchVo,String beginTime,String endTime) {
		String sql="select DISTINCT a.instance_id,a.grade,a.describe,a.defect_time,a.state,a.process_name,a.equipment_number,a.equipment_state,a.equipment_name,a.equipment_unit,a.defect_department,a.defect_professional,a.defect_number,a.is_delay,a.update_time,a.process_type,a.process_id,a.type,a.create_time,a.create_user_id,a.apply_phone,a.create_group_id,a.reminder_num from tb_cbb_my_wait_process w,tb_defect_apply a "
				+ "where w.instance_id=a.instance_id and a.is_delete=0 ";
		if(userId!=null&&searchVo.getMyJoin()==null){
			if("'3'".equals(taskName)||"3".equals(taskName)||"'7'".equals(taskName)||"7".equals(taskName)){
				sql += " and w.processer_id="+userId+" ";
			}else{
				sql += " and w.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}
		}
		
		if(StringUtils.isNotBlank(taskName)){
			sql+=" and w.task_name in ("+taskName+") ";
		}
		if(searchVo!=null){
			/*if("1".equals(searchVo.getIsOut())){
				sql += " and w.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}*/
			if(searchVo.getType()!=null){
				sql+=" and a.type="+searchVo.getType()+" ";
			}
			if(searchVo.getMyJoin()!=null){//我参与的 （待处理的）
				sql+=" and state not in (2,3,8) and w.instance_id in (select cps.instance_id from tb_cbb_people_select cps where cps.user_id="+userId+")";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				sql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				sql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				sql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				sql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				sql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getState())){
				sql+=" and state in ("+searchVo.getState()+") ";
			}
			if(StringUtils.isNoneBlank(beginTime)){
				sql+=" and a.update_time>='"+beginTime+"'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				sql+=" and a.update_time<='"+endTime+"'";
			}
			
			
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				sql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				sql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
		}
		sql+=" order by a.create_time desc ";
		String countSql="select count(distinct a.instance_id) from tb_cbb_my_wait_process w,tb_defect_apply a "
				+ "where w.instance_id=a.instance_id and a.is_delete=0 ";
		if(userId!=null&&searchVo.getMyJoin()==null){
			if("'3'".equals(taskName)||"3".equals(taskName)){
				countSql += " and w.processer_id="+userId+" ";
			}else{
				countSql += " and w.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}
		}
		if(StringUtils.isNotBlank(taskName)){
			countSql+=" and w.task_name in ("+taskName+") ";
		}
		if(searchVo!=null){
			/*if("1".equals(searchVo.getIsOut())){
				countSql += " and w.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}*/
			if(searchVo.getType()!=null){
				countSql+=" and a.type="+searchVo.getType()+" ";
			}
			if(searchVo.getMyJoin()!=null){//我参与的 （待处理的）
				countSql+=" and state not in (2,3,8) and w.instance_id in (select cps.instance_id from tb_cbb_people_select cps where cps.user_id="+userId+")";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				countSql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				countSql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				countSql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				countSql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				countSql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getState())){
				countSql+=" and state in ("+searchVo.getState()+") ";
			}
			if(StringUtils.isNoneBlank(beginTime)){
				countSql+=" and a.update_time>='"+beginTime+"'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				countSql+=" and a.update_time<='"+endTime+"'";
			}
			
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				countSql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				countSql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
			
			
		}
		Object o=this.entityManager.createNativeQuery(countSql).getSingleResult();
		int count=0;
		if(o!=null){
			count=(Integer)o;
		}
		@SuppressWarnings("unchecked")
		List<Object[]> list=this.entityManager.createNativeQuery(sql).setFirstResult(pageable.getOffset())
			.setMaxResults(pageable.getPageSize()).getResultList();
		List<DefectApply> applyList=new ArrayList<DefectApply>();
		if(list!=null&&list.size()>0){
			for(Object[] obj:list){
				DefectApply apply=new DefectApply();
				String instanceId=obj[0]==null?"":obj[0].toString();
				String grade=obj[1]==null?"":obj[1].toString();
				String describe=obj[2]==null?"":obj[2].toString();
				Timestamp defectTime=obj[3]==null?null:(Timestamp)obj[3];
				String state=obj[4]==null?"":obj[4].toString();
				String processName=obj[5]==null?"":obj[5].toString();
				String equipmentNumber=obj[6]==null?"":obj[6].toString();
				String equipmentState=obj[7]==null?"":obj[7].toString();
				String equipmentName=obj[8]==null?"":obj[8].toString();
				String equipmentUnit=obj[9]==null?"":obj[9].toString();
				String defectDepartment=obj[10]==null?"":obj[10].toString();
				String defectProfessional=obj[11]==null?"":obj[11].toString();
				String defectNumber=obj[12]==null?"":obj[12].toString();
				Timestamp updateTime=obj[14]==null?null:(Timestamp)obj[14];
				Timestamp createTime=obj[18]==null?null:(Timestamp)obj[18];
				apply.setInstanceId(instanceId);
				apply.setGrade(grade);
				apply.setDescribe(describe);
				apply.setDefectTime(defectTime);
				apply.setCreateTime(createTime);
				apply.setState(state);
				apply.setProcessName(processName);
				apply.setEquipmentNumber(equipmentNumber);
				apply.setDefectNumber(defectNumber);
				apply.setEquipmentState(equipmentState);
				apply.setEquipmentName(equipmentName);
				apply.setEquipmentUnit(equipmentUnit);
				apply.setDefectDepartment(defectDepartment);
				apply.setDefectProfessional(defectProfessional);
				apply.setIsDelay((Integer)obj[13]);
				apply.setUpdateTime(updateTime);
				apply.setProcessType((Integer)obj[15]);
				apply.setProcessId((Integer)obj[16]);
				apply.setType((Integer)obj[17]);
				apply.setCreateUserId((Integer)obj[19]);
				apply.setApplyPhone((String)obj[20]);
				apply.setCreateGroupId((Integer)obj[21]);
				apply.setReminderNum((Integer)obj[22]);
				applyList.add(apply);
			}
		}
		return new PageImpl<DefectApply>(applyList,pageable,count);
	}
	/**
	 * 我已处理的
	 * @Title: myProcessed   
	 * @param pageable
	 * @param userId
	 * @param taskName
	 * @return
	 */
	public Page<DefectApply> myProcessed(Pageable pageable, Integer userId, String taskName, SearchVo searchVo) {
		String sql="select DISTINCT s.* from (select top 100000000000 a.instance_id,a.grade,a.describe,a.defect_time,a.state,a.process_name,a.equipment_number,a.equipment_state,a.equipment_name,a.equipment_unit,a.defect_department,a.defect_professional,a.defect_number,a.is_delay,a.update_time,a.process_type,a.process_id,a.type,a.create_time,a.create_user_id,a.create_group_id from tb_cbb_my_processed p,tb_defect_apply a "
				+ " where p.instance_id=a.instance_id and a.is_delete=0 ";
		if(userId!=null&&searchVo.getMyJoin()==null){
			sql+=" and p.processer_id="+userId;
		}
		
		if(StringUtils.isNotBlank(taskName)){
			sql+=" and p.task_name in ("+taskName+") ";
		}else{
			sql+=" and p.task_name <> '1' ";
		}
		if(searchVo!=null){
			if(searchVo.getType()!=null){
				sql+=" and a.type="+searchVo.getType()+" ";
			}
			if("1".equals(searchVo.getIsOut())){
				sql += " and p.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}
			if(searchVo.getMyJoin()!=null){//我参与的 （已处理的）
				sql+=" and state in (2,3,4,8) and p.instance_id in (select cps.instance_id from tb_cbb_people_select cps where cps.user_id="+userId+")";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				sql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				sql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				sql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				sql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				sql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getState())){
				sql+=" and state in ("+searchVo.getState()+") ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				sql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				sql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
		}
		sql+="  ) s order by s.create_time desc";
		String countSql="select count(DISTINCT a.instance_id) from tb_cbb_my_processed p,tb_defect_apply a "
				+ "where p.instance_id=a.instance_id and a.is_delete=0 ";
		if(userId!=null&&searchVo.getMyJoin()==null){
			countSql+=" and p.processer_id="+userId;
		}
		if(StringUtils.isNotBlank(taskName)){
			countSql+=" and p.task_name in ("+taskName+") ";
		}else{
			countSql+=" and p.task_name <> '1' ";
		}
		if(searchVo!=null){
			if(searchVo.getType()!=null){
				countSql+=" and a.type="+searchVo.getType()+" ";
			}
			if("1".equals(searchVo.getIsOut())){
				countSql += " and p.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+searchVo.getGroupId()+" or g.path like '"+searchVo.getGroupId()+",%' )";
			}
			if(searchVo.getMyJoin()!=null){//我参与的 （已处理的）
				countSql+=" and state in (2,3,4,8) and p.instance_id in (select cps.instance_id from tb_cbb_people_select cps where cps.user_id="+userId+")";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				countSql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				countSql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				countSql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				countSql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				countSql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getState())){
				countSql+=" and state in ("+searchVo.getState()+") ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				countSql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				countSql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
		}
		Object o=this.entityManager.createNativeQuery(countSql).getSingleResult();
		int count=0;
		if(o!=null){
			count=(Integer)o;
		}
		@SuppressWarnings("unchecked")
		List<Object[]> list=this.entityManager.createNativeQuery(sql).setFirstResult(pageable.getOffset())
			.setMaxResults(pageable.getPageSize()).getResultList();
		List<DefectApply> applyList=new ArrayList<DefectApply>();
		if(list!=null&&list.size()>0){
			for(Object[] obj:list){
				DefectApply apply=new DefectApply();
				String instanceId=obj[0]==null?"":obj[0].toString();
				String grade=obj[1]==null?"":obj[1].toString();
				String describe=obj[2]==null?"":obj[2].toString();
				Timestamp defectTime=obj[3]==null?null:(Timestamp)obj[3];
				String state=obj[4]==null?"":obj[4].toString();
				String processName=obj[5]==null?"":obj[5].toString();
				String equipmentNumber=obj[6]==null?"":obj[6].toString();
				String equipmentState=obj[7]==null?"":obj[7].toString();
				String equipmentName=obj[8]==null?"":obj[8].toString();
				String equipmentUnit=obj[9]==null?"":obj[9].toString();
				String defectDepartment=obj[10]==null?"":obj[10].toString();
				String defectProfessional=obj[11]==null?"":obj[11].toString();
				String defectNumber=obj[12]==null?"":obj[12].toString();
				Timestamp updateTime=obj[14]==null?null:(Timestamp)obj[14];
				Timestamp createTime=obj[18]==null?null:(Timestamp)obj[18];
				apply.setInstanceId(instanceId);
				apply.setGrade(grade);
				apply.setDescribe(describe);
				apply.setDefectTime(defectTime);
				apply.setCreateTime(createTime);
				apply.setState(state);
				apply.setProcessName(processName);
				apply.setDefectNumber(defectNumber);
				apply.setEquipmentNumber(equipmentNumber);
				apply.setEquipmentState(equipmentState);
				apply.setEquipmentName(equipmentName);
				apply.setEquipmentUnit(equipmentUnit);
				apply.setDefectDepartment(defectDepartment);
				apply.setDefectProfessional(defectProfessional);
				apply.setIsDelay((Integer)obj[13]);
				apply.setUpdateTime(updateTime);
				apply.setProcessType((Integer)obj[15]);
				apply.setProcessId((Integer)obj[16]);
				apply.setType((Integer)obj[17]);
				apply.setCreateUserId((Integer)obj[19]);
				apply.setCreateGroupId((Integer)obj[20]);
				applyList.add(apply);
			}
		}
		return new PageImpl<DefectApply>(applyList,pageable,count);
	}
	
	public Page<DefectApply> mydList(Pageable pageable, Integer userId, SearchVo searchVo) {
		String sql="";
		sql = "select DISTINCT s.* from ( select top 100000000000 a.instance_id,a.grade,a.describe,a.defect_time,a.state,a.process_name,a.equipment_number,a.equipment_state,a.equipment_name,a.equipment_unit,a.defect_department,a.defect_professional,a.defect_number,a.is_delay,a.update_time,a.process_type,a.process_id,a.type,a.create_time,a.create_user_id,a.create_group_id from tb_defect_apply a INNER JOIN tb_cbb_my_processed p on a.instance_id = p.instance_id where p.task_name='3' and p.approveResult = 1 AND a.achievement = 4";
		if(searchVo!=null){
			if(searchVo.getDelayUserId() != null){
				sql += " and p.processer_id = "+searchVo.getDelayUserId();
			}
			if(searchVo.getType()!=null){
				sql+=" and a.type="+searchVo.getType()+" ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				sql += " and p.end_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				sql += " and p.end_time<='"+searchVo.getEndTime()+"'";
			}
		}
		sql+="  ) s order by s.create_time desc";
		String countSql="select count(DISTINCT a.instance_id) from tb_defect_apply a INNER JOIN tb_cbb_my_processed p on a.instance_id = p.instance_id where p.task_name='3' and p.approveResult = 1 AND a.achievement = 4 ";
		if(searchVo!=null){
			if(searchVo.getDelayUserId() != null){
				countSql += " and p.processer_id = "+searchVo.getDelayUserId();
			}
			if(searchVo.getType()!=null){
				countSql+=" and a.type="+searchVo.getType()+" ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				countSql += " and p.end_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				countSql += " and p.end_time<='"+searchVo.getEndTime()+"'";
			}
		}
		Object o=this.entityManager.createNativeQuery(countSql).getSingleResult();
		int count=0;
		if(o!=null){
			count=(Integer)o;
		}
		@SuppressWarnings("unchecked")
		List<Object[]> list=this.entityManager.createNativeQuery(sql).setFirstResult(pageable.getOffset())
		.setMaxResults(pageable.getPageSize()).getResultList();
		List<DefectApply> applyList=new ArrayList<DefectApply>();
		if(list!=null&&list.size()>0){
			for(Object[] obj:list){
				DefectApply apply=new DefectApply();
				String instanceId=obj[0]==null?"":obj[0].toString();
				String grade=obj[1]==null?"":obj[1].toString();
				String describe=obj[2]==null?"":obj[2].toString();
				Timestamp defectTime=obj[3]==null?null:(Timestamp)obj[3];
				String state=obj[4]==null?"":obj[4].toString();
				String processName=obj[5]==null?"":obj[5].toString();
				String equipmentNumber=obj[6]==null?"":obj[6].toString();
				String equipmentState=obj[7]==null?"":obj[7].toString();
				String equipmentName=obj[8]==null?"":obj[8].toString();
				String equipmentUnit=obj[9]==null?"":obj[9].toString();
				String defectDepartment=obj[10]==null?"":obj[10].toString();
				String defectProfessional=obj[11]==null?"":obj[11].toString();
				String defectNumber=obj[12]==null?"":obj[12].toString();
				Timestamp updateTime=obj[14]==null?null:(Timestamp)obj[14];
				Timestamp createTime=obj[18]==null?null:(Timestamp)obj[18];
				apply.setInstanceId(instanceId);
				apply.setGrade(grade);
				apply.setDescribe(describe);
				apply.setDefectTime(defectTime);
				apply.setCreateTime(createTime);
				apply.setState(state);
				apply.setProcessName(processName);
				apply.setDefectNumber(defectNumber);
				apply.setEquipmentNumber(equipmentNumber);
				apply.setEquipmentState(equipmentState);
				apply.setEquipmentName(equipmentName);
				apply.setEquipmentUnit(equipmentUnit);
				apply.setDefectDepartment(defectDepartment);
				apply.setDefectProfessional(defectProfessional);
				apply.setIsDelay((Integer)obj[13]);
				apply.setUpdateTime(updateTime);
				apply.setProcessType((Integer)obj[15]);
				apply.setProcessId((Integer)obj[16]);
				apply.setType((Integer)obj[17]);
				apply.setCreateUserId((Integer)obj[19]);
				apply.setCreateGroupId((Integer)obj[20]);
				applyList.add(apply);
			}
		}
		return new PageImpl<DefectApply>(applyList,pageable,count);
	}
	
	
	public int waitProcessCount(Integer userId,String taskName){
		String countSql="select count(distinct a.instance_id) from tb_cbb_my_wait_process w,tb_defect_apply a "
				+ "where w.instance_id=a.instance_id and a.is_delete=0 ";
		if(userId!=null){
			countSql += " and w.processer_id="+userId+" ";
		}
		if(StringUtils.isNotBlank(taskName)){
			countSql+=" and w.task_name in ("+taskName+") ";
		}
		Object o=this.entityManager.createNativeQuery(countSql).getSingleResult();
		int count=0;
		if(o!=null){
			count=(Integer)o;
		}
		return count;
	}
	
	public List<Object[]> getFinishProcessIdList(SearchVo searchVo){
		String sql="select a.instance_id,p.processer_id from tb_cbb_my_processed p,tb_defect_apply a "
				+ " where p.instance_id=a.instance_id and a.is_delete=0 and p.task_name=3 and a.state=3 ";
		if(searchVo!=null){
			if(searchVo.getType()!=null){
				sql+=" and a.type="+searchVo.getType()+" ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				sql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				sql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				sql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				sql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				sql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				sql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				sql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
		}
		sql+="   order by p.end_time asc";
		List<Object[]> list=this.entityManager.createNativeQuery(sql).getResultList();
		return list;
	}
	
	
	public List<Object[]> getProcessTimeList(SearchVo searchVo){
		String sql="select a.instance_id,p.processer_id,p.end_time,p.task_name,p.approveResult from tb_cbb_my_processed p,tb_defect_apply a  where p.instance_id=a.instance_id and a.is_delete=0 ";
		if(searchVo!=null){
			if(searchVo.getType()!=null){
				sql+=" and a.type="+searchVo.getType()+" ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectDepartment())){
				sql+=" and a.defect_Department='"+searchVo.getDefectDepartment()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getDefectProfessional())){
				sql+=" and a.defect_Professional='"+searchVo.getDefectProfessional()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentState())){
				sql+=" and a.equipment_State='"+searchVo.getEquipmentState()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getEquipmentUnit())){
				sql+=" and a.equipment_Unit='"+searchVo.getEquipmentUnit()+"' ";
			}
			if(StringUtils.isNotBlank(searchVo.getSearchName())){
				sql+=" and a.equipment_Name like '%"+searchVo.getSearchName()+"%' ";
			}
			if(StringUtils.isNotBlank(searchVo.getStartTime())){
				sql += " and a.create_time>='"+searchVo.getStartTime()+"'";
			}
			if(StringUtils.isNotBlank(searchVo.getEndTime())){
				sql += " and a.create_time<='"+searchVo.getEndTime()+"'";
			}
		}
		sql+="  ORDER BY p.end_time asc";
		List<Object[]> list=this.entityManager.createNativeQuery(sql).getResultList();
		return list;
	}
	
	/**
	 * 报表
	 * @Title: getReport   
	 * @param startTime
	 * @param endTime
	 * @param equipmentUnit
	 * @param defectDepartment
	 * @return
	 */
	public Map<String, Object> getReport(String startTime, String endTime, String equipmentUnit, String defectDepartment,String defectProfessional) {
		Map<String, Object> map=new HashMap<String,Object>();
		String sql="select "+
			"isnull(sum(case when (state=0 or state=1 or state=5 or state=6 or state=7) then 1 else 0 end),0) as defecting, "+
			"isnull(sum(case when (state=2 or state=8) then 1 else 0 end),0) as accepting, "+
			"isnull(sum(case when (state=3 or state=4) then 1 else 0 end),0) as defected, "+
			"isnull(sum(case when grade='1' then 1 else 0 end),0) as grade1, "+
			"isnull(sum(case when grade='2' then 1 else 0 end),0) as grade2, "+
			"isnull(sum(case when grade='3' then 1 else 0 end),0) as grade3, "+
			"isnull(sum(case when (state=3 or state=4) and defect_time>=real_defect_time then 1 else 0 end),0) as inTimeDefect, "+
			"isnull(sum(case when (state=3 or state=4) and defect_time<real_defect_time then 1 else 0 end),0) as outTimeDefect, "+
			"isnull(sum(case when state <>3 and state <> 4 and defect_time>=GETDATE() then 1 else 0 end),0) as inTimeunDefect, "+
			"isnull(sum(case when state <>3 and state <> 4 and defect_time<GETDATE() then 1 else 0 end),0) as outTimeunDefect "+
			"from tb_defect_apply where is_delete=0 ";
		if(StringUtils.isNotBlank(startTime)){
			sql+=" and create_time>= '"+startTime+"'";
		}
		if(StringUtils.isNotBlank(endTime)){
			sql+=" and create_time<= '"+endTime+"'";
		}
		if(StringUtils.isNotBlank(equipmentUnit)){
			sql+=" and equipment_unit='"+equipmentUnit+"'";
		}
		if(StringUtils.isNotBlank(defectDepartment)){
			sql+=" and defect_department='"+defectDepartment+"'";
		}
		if(StringUtils.isNotBlank(defectProfessional)){
			sql+=" and defect_professional='"+defectProfessional+"'";
		}
		List<Map<String,Object>> list=jdbcTemplate.queryForList(sql);
		if(list!=null&&list.size()>0){
			map=list.get(0);
		}
		sql="select month(create_time) as month,"+
			"isnull(sum(case when grade='1' then 1 else 0 end),0) as grade1,"+ 
			"isnull(sum(case when grade='2' then 1 else 0 end),0) as grade2, "+
			"isnull(sum(case when grade='3' then 1 else 0 end),0) as grade3 "+
			" from tb_defect_apply where create_time> convert(VARCHAR(20),year(getdate()))+'-01-01' and create_time<GETDATE() and is_delete=0 group by month(create_time)";
		List<Map<String,Object>> listMonth=jdbcTemplate.queryForList(sql);
		Integer[] grade1=new Integer[12];
		Integer[] grade2=new Integer[12];
		Integer[] grade3=new Integer[12];
		for(int i=0;i<12;i++){
			boolean flag=true;
			if(listMonth!=null&&listMonth.size()>0){
				for(Map<String,Object> mapM:listMonth){
					Object obj=mapM.get("month");
					if(obj!=null&&obj.toString().equals(i+1+"")){
						grade1[i]=Integer.parseInt(mapM.get("grade1").toString());
						grade2[i]=Integer.parseInt(mapM.get("grade2").toString());
						grade3[i]=Integer.parseInt(mapM.get("grade3").toString());
						flag=false;
						break;
					}
				}
			}
			if(flag){
				grade1[i]=0;
				grade2[i]=0;
				grade3[i]=0;
			}
		}
		map.put("grade1Ary", grade1);
		map.put("grade2Ary", grade2);
		map.put("grade3Ary", grade3);
		return map;
	}
	
	
	
	/**
	 * 报表
	 * @Title: getReport   
	 * @param startTime
	 * @param endTime
	 * @param equipmentUnit
	 * @param defectDepartment
	 * @return
	 */
	public Map<String, Object> getReport_new(String startTime, String endTime, String equipmentUnit, String defectDepartment,String defectProfessional) {
		Map<String, Object> map=new HashMap<String,Object>();
		String sql="select "+
				"isnull(sum(case when (state=0 or state=6) then 1 else 0 end),0) as waitAccept," +
				"isnull(sum(case when (state=1 or state=7) then 1 else 0 end),0) as waitDefect," +
				"isnull(sum(case when (state=2 or state=8) then 1 else 0 end),0) as waitCheck, " +
				"isnull(sum(case when (state=3) then 1 else 0 end),0) as waitFile," +
				"isnull(sum(case when (state=4) then 1 else 0 end),0) as hasFile," +
				"isnull(sum(case when (state=9) then 1 else 0 end),0) as hasDelete,	" +
				"isnull(sum(case when ((state=1 or state=7) and is_delay=1 ) then 1 else 0 end),0) as waitDefectWaitDelay,"+
				"isnull(sum(case when ((state=1 or state=7) and is_delay=2 ) then 1 else 0 end),0) as waitDefectDelaying,"+
				"isnull(sum(case when ((state=3 or state=4) and is_delay=2 ) then 1 else 0 end),0) as hasDefectDelaying,"+
				"isnull(sum(case when grade='1' then 1 else 0 end),0) as grade1, " +
				"isnull(sum(case when grade='2' then 1 else 0 end),0) as grade2, " +
				"isnull(sum(case when grade='3' then 1 else 0 end),0) as grade3 " +
				"from tb_defect_apply where is_delete=0 ";
		if(StringUtils.isNotBlank(startTime)){
			sql+=" and create_time>= '"+startTime+"'";
		}
		if(StringUtils.isNotBlank(endTime)){
			sql+=" and create_time<= '"+endTime+"'";
		}
		if(StringUtils.isNotBlank(equipmentUnit)){
			sql+=" and equipment_unit='"+equipmentUnit+"'";
		}
		if(StringUtils.isNotBlank(defectDepartment)){
			sql+=" and defect_department='"+defectDepartment+"'";
		}
		if(StringUtils.isNotBlank(defectProfessional)){
			sql+=" and defect_professional='"+defectProfessional+"'";
		}
		List<Map<String,Object>> list=jdbcTemplate.queryForList(sql);
		if(list!=null&&list.size()>0){
			map=list.get(0);
		}
		sql="select month(create_time) as month,"+
			"isnull(sum(case when grade='1' then 1 else 0 end),0) as grade1,"+ 
			"isnull(sum(case when grade='2' then 1 else 0 end),0) as grade2, "+
			"isnull(sum(case when grade='3' then 1 else 0 end),0) as grade3 "+
			" from tb_defect_apply where create_time> convert(VARCHAR(20),year(getdate()))+'-01-01' and create_time<GETDATE() and is_delete=0 group by month(create_time)";
		List<Map<String,Object>> listMonth=jdbcTemplate.queryForList(sql);
		Integer[] grade1=new Integer[12];
		Integer[] grade2=new Integer[12];
		Integer[] grade3=new Integer[12];
		for(int i=0;i<12;i++){
			boolean flag=true;
			if(listMonth!=null&&listMonth.size()>0){
				for(Map<String,Object> mapM:listMonth){
					Object obj=mapM.get("month");
					if(obj!=null&&obj.toString().equals(i+1+"")){
						grade1[i]=Integer.parseInt(mapM.get("grade1").toString());
						grade2[i]=Integer.parseInt(mapM.get("grade2").toString());
						grade3[i]=Integer.parseInt(mapM.get("grade3").toString());
						flag=false;
						break;
					}
				}
			}
			if(flag){
				grade1[i]=0;
				grade2[i]=0;
				grade3[i]=0;
			}
		}
		map.put("grade1Ary", grade1);
		map.put("grade2Ary", grade2);
		map.put("grade3Ary", grade3);
		return map;
	}
	/**
	 * 
	 * @Title: getDefectNumber   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return
	 */
	public String getDefectNumber() {
		String sql="select max(defect_number) as defectNumber from tb_defect_apply where defect_number like  CONVERT(varchar(50),getdate(),112)+'%' and defect_number is not null";
		List<Map<String,Object>> list=jdbcTemplate.queryForList(sql);
		if(list!=null&&list.size()>0){
			Map<String,Object> map=list.get(0);
			if(map.get("defectNumber")!=null){
				return map.get("defectNumber").toString();
			}
		}
		return null;
	}
	/**
	 * 请求作废次数
	 * @Title: getUnApprove   
	 * @param userId
	 * @param companyId
	 * @param instanceId
	 * @return
	 */
	public int getUnApprove(Integer userId, Integer companyId, String instanceId) {
		String sql="select count(*) as count from tb_cbb_my_processed where instance_id='"+instanceId+"' and processer_id="+userId+" and task_name=2 and approveResult=0 and company_id="+companyId;
		List<Map<String,Object>> list=jdbcTemplate.queryForList(sql);
		if(list!=null&&list.size()>0){
			Map<String,Object> map=list.get(0);
			if(map.get("count")!=null){
				return Integer.parseInt(map.get("count").toString());
			}
		}
		return 0;
	}
	/**
	 * 获取待处理的数量
	 * @Title: getWaitHandle   
	 * @param userId
	 * @param companyId
	 * @return
	 */
	public Map<String, Object> getWaitHandle(Integer groupId,Integer userId, Integer companyId,String beginTime,String endTime) {
		Map<String, Object> map=new HashMap<String,Object>();
		String sql="select "+
				"ISNULL(sum(case when wait2.task_name=-1 then 1 else 0 end),0) as dispatchCount,"+
				"ISNULL(sum(case when wait2.task_name=2 then 1 else 0 end),0) as approveCount,"+
				"ISNULL(sum(case when wait2.task_name=3 then 1 else 0 end),0) as defectCount,"+
				"ISNULL(sum(case when wait2.task_name=4 then 1 else 0 end),0) as acceptCount,"+
				"ISNULL(sum(case when wait2.task_name=5 then 1 else 0 end),0) as archiveCount ,"+
				"ISNULL(sum(case when wait2.task_name=7 then 1 else 0 end),0) as delayCount "+
				"from (select * from tb_cbb_my_wait_process wait where wait.my_started_id in (select max(wait1.my_started_id) from tb_cbb_my_wait_process wait1 group by wait1.instance_id )) wait2 where wait2.company_id="+companyId+" and wait2.module_code in ('defect','delayDefect') ";
		if(groupId!=null){
			sql += " and wait2.processer_id in (select u.user_id from tb_user_info u inner join tb_group_info g on u.group_id=g.group_id where u.is_delete=0 and g.group_id="+groupId+" or g.path like '"+groupId+",%' )";
		}else{
			if(userId!=null){
				sql += " and wait2.processer_id="+userId;
			}
		}
		sql += " and wait2.instance_id in (select instance_id from tb_defect_apply where company_id="+companyId+" and is_delete=0";
		if(StringUtils.isNoneBlank(beginTime)){
			sql+=" and update_time>='"+beginTime+"'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and update_time<='"+endTime+"'";
		}
		sql+=")";
		Map<String, Object> res=jdbcTemplate.queryForMap(sql);
		if(res!=null){
			map=res;
		}
		
		if(userId!=null){
			sql="select count(*) as applyCount from tb_defect_apply where company_id="+companyId+" and create_user_id="+userId+" and is_delete=0 and state=5 ";
			if(StringUtils.isNoneBlank(beginTime)){
				sql+=" and update_time>='"+beginTime+"'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				sql+=" and update_time<='"+endTime+"'";
			}
			int applyCount=jdbcTemplate.queryForInt(sql);
			map.put("applyCount", applyCount);
			
			//单独处理 待处理数据
			sql = "select count(*) as defectCount from tb_cbb_my_wait_process where company_id="+companyId+" and module_code='defect' and task_name=3  and processer_id="+userId;
			sql += " and instance_id in (select instance_id from tb_defect_apply where company_id="+companyId+" and is_delete=0";
			if(StringUtils.isNoneBlank(beginTime)){
				sql+=" and update_time>='"+beginTime+"'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				sql+=" and update_time<='"+endTime+"'";
			}
			sql+=")";
			int defectCount=jdbcTemplate.queryForInt(sql);
			map.put("defectCount", defectCount);
		}
		
		//单独处理 待批准数据
		sql = "select count(*) as delayCount from tb_cbb_my_wait_process where company_id="+companyId+" and module_code='defectDelay' and task_name=7  and processer_id="+userId;
		sql += " and instance_id in (select instance_id from tb_defect_apply where company_id="+companyId+" and is_delete=0";
		if(StringUtils.isNoneBlank(beginTime)){
			sql+=" and update_time>='"+beginTime+"'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and update_time<='"+endTime+"'";
		}
		sql+=")";
		int delayCount=jdbcTemplate.queryForInt(sql);
		map.put("delayCount", delayCount);
		
		return map;
	}
	public Map<String, Object> getWaitHandle_All(Integer companyId) {
		String sql = "select ISNULL(SUM (CASE WHEN apply.state =-1 THEN 1 ELSE 0 END ),0) AS dispatchCount,"
				+ "ISNULL(SUM (CASE WHEN apply.state =0 or apply.state =6 THEN 1 ELSE 0 END),0) AS approveCount,"
				+ "ISNULL(SUM (CASE WHEN apply.state =1 or apply.state =7 or apply.state = 10 or apply.state =11 or apply.state =12 THEN 1 ELSE 0 END),0) AS defectCount,"
				+ "ISNULL(SUM (CASE WHEN apply.state =2  THEN 1 ELSE 0 END),0) AS acceptCount"
				+ " from tb_defect_apply apply where apply.company_id="+companyId+" and apply.is_delete=0";
		Map<String, Object> res=jdbcTemplate.queryForMap(sql);
		return res;
	}
	
	public Integer findWaitCheckNum(Integer userId,Integer groupId,Integer companyId){
		String sql = "SELECT count(*) FROM tb_cbb_my_wait_process w,tb_defect_apply a WHERE w.instance_id = a.instance_id AND a.is_delete = 0 AND w.processer_id IN (SELECT u.user_id from tb_user_info u INNER JOIN tb_group_info g ON u.group_id = g.group_id WHERE u.is_delete = 0 AND g.group_id = "+groupId+" OR g.path LIKE '"+groupId+",%' ) AND w.task_name IN ('4')";
		return jdbcTemplate.queryForInt(sql);
	}
	
}
