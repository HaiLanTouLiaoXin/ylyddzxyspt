package cn.com.qytx.hemei.event.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.event.dao.EventDao;
import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:接口实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
@Service("eventService")
@Transactional
public class EventImpl extends BaseServiceImpl<Event> implements IEvent{

	@Resource(name="eventDao")
	private EventDao eventDao;

	@Override
	public Page<Event> findEventPage(Pageable pageInfo, UserInfo userInfo,
			Event event) {
		return eventDao.findEventPage(pageInfo, userInfo,
				event);
	}

	@Override
	public List<Event> findEventList(Event event) {
		return eventDao.findEventList(event);
	}

	@Override
	public List<UserInfo> findUserListByLoginName(String loginName,
			Integer companyId) {
		return eventDao.findUserListByLoginName(loginName,companyId);
	}
	@Override
	public Event findModel(Integer eventType) {
		// TODO Auto-generated method stub
		return eventDao.findModel(eventType);
	}

	@Override
	public Event findByName(String name) {
		// TODO Auto-generated method stub
		return eventDao.findByName(name);
	}
	
	
	

}
