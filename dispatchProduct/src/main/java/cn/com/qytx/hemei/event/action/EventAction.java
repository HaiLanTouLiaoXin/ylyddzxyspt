package cn.com.qytx.hemei.event.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.utils.DateUtils;

public class EventAction extends BaseActionSupport{
	private static final long serialVersionUID = 821099292714160046L;

	@Resource(name="eventService")
	private IEvent eventService;
	
	@Resource(name="groupService")
	private IGroup groupService;
	
	private Event event;
	
	private String eventIds;
	
	
	private String loginName;//用户工号
	
	/**
	 * 功能：事件列表
	 * @return
	 */
	public String eventList(){
		try{
			UserInfo userInfo = this.getLoginUser();
			List<Event> eventList = eventService.findEventList(event);
			List<GroupInfo> groupList = groupService.getGroupList(userInfo.getCompanyId(), 1);
			Map<Integer,String> groupMap = new HashMap<Integer, String>();
			if(groupList!=null&&groupList.size()>0){
				for(GroupInfo groupInfo:groupList){
					groupMap.put(groupInfo.getGroupId(), groupInfo.getGroupName());
				}
			}
			if(eventList!=null&&eventList.size()>0){
				for(Event event:eventList){
					Integer groupId = event.getGroupId();
					if(groupMap.containsKey(groupId)){
						event.setGroupName(groupMap.get(groupId));
					}
				}
			}
			Gson gson = new Gson();
			String gsonStr = gson.toJson(eventList);
			ajax(gsonStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 功能：用户列表
	 * @return
	 */
	public String userList(){
		try{
			UserInfo userInfo = this.getLoginUser();
			List<UserInfo> userList = eventService.findUserListByLoginName(loginName,userInfo.getCompanyId());
			List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
			if(userList!=null&&!userList.isEmpty()){
				for(UserInfo user : userList){
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("groupName", user.getGroupName());
					map.put("userId", user.getUserId());
					map.put("loginName", user.getLoginName());
					map.put("userName", user.getUserName());
					map.put("groupId", user.getGroupId());
					mapList.add(map);
				}
			}
			Gson gson = new Gson();
			String gsonStr = gson.toJson(mapList);
			ajax(gsonStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * 功能：事件列表分页
	 * @return
	 */
	public String eventPage(){
		try{
			Page<Event> pageInfo = null;
		    Sort sort = new Sort(new Sort.Order(Direction.DESC, "createTime"));
		    UserInfo userInfo = this.getLoginUser();
		    pageInfo = eventService.findEventPage(getPageable(sort), userInfo, event);
		    List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		    List<Event> list = pageInfo.getContent();
		    if (list != null && !list.isEmpty()) {
		    	Map<Integer,String> groupMap = new HashMap<Integer, String>(); 
		    	List<GroupInfo> groupList = groupService.getGroupList(userInfo.getCompanyId(), 1);
		    	for(GroupInfo groupInfo:groupList){
		    		groupMap.put(groupInfo.getGroupId(), groupInfo.getGroupName());
		    	}
		    	  int index = pageInfo.getNumber() * pageInfo.getSize() + 1;
	              for (Event tempEvnet : list)
	              {
	            	  Map<String, Object> map = new HashMap<String, Object>();
	            	  map.put("no", index);
	            	  map.put("eventName", tempEvnet.getEventName());
	            	  map.put("urgencyLevelName", getUrgencylevelName(tempEvnet.getUrgencyLevel()));
	            	  map.put("completeTimeType",tempEvnet.getCompleteTimeType());
	            	 // map.put("completeTimeTypeStr", getCompleteTimeTypeStr(tempEvnet.getCompleteTimeType()));
	            	  map.put("completeTime", DateUtils.date2LongStr(tempEvnet.getCompleteTime()));
	            	  String repairName = "";
	            	  if(groupMap.containsKey(tempEvnet.getGroupId())){
	            		  repairName = groupMap.get(tempEvnet.getGroupId());
	            	  }
	            	  map.put("repairName", repairName);
	            	  map.put("workHour", tempEvnet.getWorkHour());
	            	  map.put("eventId", tempEvnet.getId());
	            	  mapList.add(map);
	            	  index++;
	              }
		    }
		    Map<String, Object> jsonMap = new HashMap<String, Object>();
		    jsonMap.put("aaData", mapList);
		    this.ajaxPage(pageInfo, mapList);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String getUrgencylevelName(Integer urgencyLevel){
		String str = "紧急";
		if(urgencyLevel==1){
			str = "紧急";
		}else if(urgencyLevel==2){
			str = "一般";
		}else if(urgencyLevel==3){
			str = "普通";
		}else{
			str = "紧急";
		}
		return str;
	}
	
	public String getCompleteTimeTypeStr(Integer completeTimeType){
		// 1 (一小时 ) 2 (12小时)   3 (1天)  4 (3天)
		String str = "1小时";
		if(completeTimeType!=null){
			if(completeTimeType==1){
				str = "1小时";
			}else if(completeTimeType==2){
				str = "12小时";
			}else if(completeTimeType==3){
				str = "1天";
			}else if(completeTimeType==3){
				str = "3天";
			}else{
				str = "1小时";
			}
		}
		
		return str;
	}
	
	/**
	 * 新增或修改
	 */
	public void addOrUpdate(){
		UserInfo user =getLoginUser();
		if(user!=null){
			try {
				if(event.getId()!=null){//修改
					Event oldEvent = eventService.findOne(event.getId());
					Event e=eventService.findByName(event.getEventName());
					if( event.getId()!=e.getId() && event.getEventName().equals(e.getEventName())){
						ajax(2);//标题存在
						return;
					}
					
					
					oldEvent.setEventType(event.getEventType());
					oldEvent.setEventName(event.getEventName());
					oldEvent.setUrgencyLevel(event.getUrgencyLevel());
					oldEvent.setCompleteTime(event.getCompleteTime());
					oldEvent.setGroupId(event.getGroupId());
					oldEvent.setUpdateTime(new Timestamp(System.currentTimeMillis()));
					oldEvent.setUpdateUserId(user.getUserId());
					oldEvent.setWorkHour(event.getWorkHour());
					eventService.saveOrUpdate(oldEvent);
				}else{
					Event e=eventService.findByName(event.getEventName());
					if(e!=null){
						ajax(2);//标题存在
						return;
					}
					event.setCompanyId(user.getCompanyId());
					event.setUpdateTime(new Timestamp(System.currentTimeMillis()));
					event.setUpdateUserId(user.getUserId());
					event.setCreateTime(new Timestamp(System.currentTimeMillis()));
					event.setCreateUserId(user.getUserId());
					event.setIsDelete(0);
					event.setCompleteTimeType(1);//暂时没用到
					eventService.saveOrUpdate(event);
				}
				ajax(1);
			} catch (Exception e) {
				e.getStackTrace();
				ajax(0);
			}
		}
	}
	
/**
 * 获得详情
 */
	public void findView(){
		UserInfo user=getLoginUser();
		if(user!=null){
			Event e = eventService.findOne(event.getId());
			Map<String,Object> map =new HashMap<String, Object>();
			map.put("eventName", e.getEventName());
			map.put("urgencyLevel",e.getUrgencyLevel() );
			map.put("completeTime",DateUtils.date2LongStr(e.getCompleteTime()) );
			map.put("groupId",e.getGroupId());
			map.put("eventType",e.getEventType());
			map.put("workHour",e.getWorkHour());
			Gson json =new Gson();
			ajax(json.toJson(map));
		}
	}
	

/**
 * 删除事件	
 */
	public void deleteEvent(){
		if (StringUtils.isNotEmpty(eventIds))
        {
            if (eventIds.endsWith(","))
            {
            	eventIds = eventIds.substring(0, eventIds.length() - 1);
            }
            if (eventIds.startsWith(","))
            {
            	eventIds = eventIds.substring(1, eventIds.length());
            }
            String [] array=eventIds.split(",");
            List<Integer> ids = new ArrayList<Integer>();
            if(array!=null && array.length>0){
            	for(int i=0;i<array.length;i++){
            		ids.add(Integer.valueOf(array[i]));
            	}
            }
            eventService.deleteByIds( ids,false);
            ajax("success");
	}
  }
	/**
	 * 导出列表
	 * @return
	 */
	public String exportEvent(){
		HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		UserInfo user =getLoginUser();
			try {
				if(user!=null){
					this.setIDisplayStart(0);
					this.setIDisplayLength(200000);
					Page<Event> pageInfo = null;
				    Sort sort = new Sort(new Sort.Order(Direction.DESC, "createTime"));
				    UserInfo userInfo = this.getLoginUser();
				    String eventName=URLDecoder.decode(event.getEventName(),"utf-8");
				    event.setEventName(eventName);
				    pageInfo = eventService.findEventPage(getPageable(sort), userInfo, event);
				    List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
				    List<Event> list = pageInfo.getContent();
				    if (list != null && !list.isEmpty()) {
				    	Map<Integer,String> groupMap = new HashMap<Integer, String>(); 
				    	List<GroupInfo> groupList = groupService.getGroupList(userInfo.getCompanyId(), 1);
				    	for(GroupInfo groupInfo:groupList){
				    		groupMap.put(groupInfo.getGroupId(), groupInfo.getGroupName());
				    	}
				    	  int index = pageInfo.getNumber() * pageInfo.getSize() + 1;
			              for (Event tempEvnet : list)
			              {
			            	  Map<String, Object> map = new HashMap<String, Object>();
			            	  map.put("no", index);
			            	  map.put("eventName", tempEvnet.getEventName());
			            	  map.put("urgencyLevelName", getUrgencylevelName(tempEvnet.getUrgencyLevel()));
			            	  map.put("completeTimeType",tempEvnet.getCompleteTimeType());
			            	  map.put("completeTimeTypeStr", getCompleteTimeTypeStr(tempEvnet.getCompleteTimeType()));
			            	  map.put("completeTime", DateUtils.date2LongStr(tempEvnet.getCompleteTime()));
			            	  String repairName = "";
			            	  if(groupMap.containsKey(tempEvnet.getGroupId())){
			            		  repairName = groupMap.get(tempEvnet.getGroupId());
			            	  }
			            	  map.put("repairName", repairName);
			            	  map.put("workHour", tempEvnet.getWorkHour());
			            	  map.put("eventId", tempEvnet.getId());
			            	  mapList.add(map);
			            	  index++;
			              }
				    }
				 String fileName = URLEncoder.encode("事件列表.xls", "UTF-8");
				 // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), mapList, getExportKeyList());
	            exportExcel.export();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("事件标题");
        headList.add("紧急程度");
        headList.add("期望完成时间");
        headList.add("维修部门");
        headList.add("维修工时");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("eventName");
        headList.add("urgencyLevelName");
        headList.add("completeTime");
        headList.add("repairName");
        headList.add("workHour");
		return headList;
    }
	
	
	
	
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}


	public String getEventIds() {
		return eventIds;
	}


	public void setEventIds(String eventIds) {
		this.eventIds = eventIds;
	}


	public String getLoginName() {
		return loginName;
	}


	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
    
}