package cn.com.qytx.hemei.defect.service;

import java.util.List;
import java.util.Map;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.platform.base.service.BaseService;

public interface ISelectPeople extends BaseService<SelectPeople> {
	 /**
	    * 通过userId 和InstanceId 查找记录
	    * @param userId
	    * @param instanceId
	    * @return
	    */
		public SelectPeople findByPeopleIdAndInstanceId(Integer peopleId ,String instanceId);

		/**
		 * 通过流程Id 查询
		 * @return
		 */
		public List<SelectPeople> findList(String instanceId);
		
		public Map<String,Integer> getPeopleMap(Integer companyId);
}
