/**
 * 
 */
package cn.com.qytx.hemei.location.service;

import java.util.List;

import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月20日
 * 修改日期: 2017年4月20日
 * 修改列表: 
 */
public interface IGroupLocation extends BaseService<GroupLocation> {
    
	/**
	 * 根据部门ID 查询
	 * @param groupId
	 * @return
	 */
	public List<GroupLocation> findList(Integer groupId);
	
}
