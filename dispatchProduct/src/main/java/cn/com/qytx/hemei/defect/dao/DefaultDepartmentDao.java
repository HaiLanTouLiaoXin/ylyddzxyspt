/**
 * 
 */
package cn.com.qytx.hemei.defect.dao;

import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefaultDepartment;
import cn.com.qytx.platform.base.dao.BaseDao;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月20日
 * 修改日期: 2017年5月20日
 * 修改列表: 
 */
@Repository
public class DefaultDepartmentDao extends BaseDao<DefaultDepartment, Integer> {

	/**
	 * 根据类型查询默认部门
	 * @param type
	 * @return
	 */
	public DefaultDepartment findModel(Integer type){
		String hql =" type="+type;
		return super.findOne(hql);
	}
}
