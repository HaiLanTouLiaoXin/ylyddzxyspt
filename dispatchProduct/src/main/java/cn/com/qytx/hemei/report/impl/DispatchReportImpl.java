package cn.com.qytx.hemei.report.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.report.dao.DispatchReportDao;
import cn.com.qytx.hemei.report.service.IDispatchReport;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service
@Transactional
public class DispatchReportImpl extends BaseServiceImpl<DefectApply> implements IDispatchReport{

	@Autowired
	private DispatchReportDao dispatchReportDao;
	
	
	@Override
	public List<Object[]> findList(String beginTime, String endTime,
			String employeeName) {
		// TODO Auto-generated method stub
		return dispatchReportDao.findList(beginTime,endTime,employeeName);
	}

}
