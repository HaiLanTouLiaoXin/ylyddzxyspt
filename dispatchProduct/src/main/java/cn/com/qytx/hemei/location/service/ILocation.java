package cn.com.qytx.hemei.location.service;

import java.util.List;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:位置接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2017年4月10日
 * 修改日期: 2017年4月10日
 * 修改列表: 
 */
public interface ILocation extends BaseService<Location>{

	
	/**
	 * 功能：获取位置列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<Location> findLocationList(Integer companyId);
	
	
	/**
	 * 功能：获取地点树列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<TreeNode> getTreeLocationList(String path,Integer companyId,Integer isOpen);
	
	
	public List<TreeNode> getTreeLocationList_Select(String path,Integer companyId);
	
	/**
	 * 在指定ID下面是否有相同的地点名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameLocationName(Integer parentId,String groupName,int companyId);
	
	/**
	 * 功能：判断是否存在子地点
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildLocation(Integer id,int companyId);
	
	/**
	 * 功能：通过Id传获取地点列表
	 * @param Ids
	 * @return
	 */
	public List<Location> getLocationListByIds(String Ids);
	
	/**
	 * 功能：通过Id 获取 全路径  ， 地点列表
	 * @param Ids
	 * @param companyId
	 * @return
	 */
	public List<Location> getLocationPathListByIds(String Ids,Integer companyId);
	 
	
	
}
