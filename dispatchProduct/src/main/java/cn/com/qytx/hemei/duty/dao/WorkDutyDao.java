package cn.com.qytx.hemei.duty.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 
 * 功能:值班dao类 版本: 1.0 开发人员: 徐长江 创建日期: 2016年5月16日 修改日期: 2016年5月16日 修改列表:
 */
@Component
public class WorkDutyDao extends BaseDao<WorkDuty, Integer> {
	/**
	 * 描述含义
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 * 功能：根据值班日期得到值班信息
	 * @param workDate
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public List<WorkDuty> getWorkDutyByWorkDate(Date workDate)
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String hql=" 1=1 ";
		if(workDate!=null){
			hql+=" and dutyDate='"+sdf.format(workDate)+"'";
		}
		return	super.findAll(hql);
	}
	
	public void deleteWorkDuty(Date dutyDate, String dutyPost) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String hql="delete WorkDuty where dutyDate= '"+sdf.format(dutyDate)+"' and dutyPost=?";
		super.executeQuery(hql,dutyPost);
		
	}

	public WorkDuty getWorkDutyByUserId(Date workDate, Integer userId) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String hql=" createUser.userId= "+userId;
		if(workDate!=null){
			hql+=" and dutyDate='"+sdf.format(workDate)+"'";
		}
		return super.findOne(hql);
	}
}
