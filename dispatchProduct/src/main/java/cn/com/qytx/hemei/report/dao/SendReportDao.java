/**
 * 
 */
package cn.com.qytx.hemei.report.dao;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.dao.BaseDao;
/**
 * 功能: 维修报表
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月15日
 * 修改日期: 2017年5月15日
 * 修改列表: 
 */
@Repository
public class SendReportDao extends BaseDao<DefectApply, Integer> implements Serializable {
     /**
	 * 
	 */
	private static final long serialVersionUID = -2074647602619810124L;
	 /**
     * 查询后勤班组工作量
     * @return
     */
	 public Map<Integer,String> findListMap(String beginTime,String endTime,Integer type,String instanceIds){
		 String sql="SELECT SUM( CASE WHEN a.task_name =2 and a.approveResult=1 THEN 1 ELSE 0 END)as receive, SUM( CASE WHEN a.task_name =3 and a.approveResult=1 THEN 1 ELSE 0 END)as finshNum,a.group_id, SUM( CASE WHEN a.task_name =3 and a.approveResult=1 THEN a.equipment_number ELSE 0 END)as finshNum ";
		 sql+=" FROM (SELECT g.group_id,e.task_name,e.approveResult,e.equipment_number FROM View_repair_group g ";
		 sql+=" left JOIN [View_repair_event] e ON g.user_id = e.processer_id where e.type="+type+"  AND g.extension=2";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		if(StringUtils.isNoneBlank(instanceIds)){
			sql+=" and e.instance_id in("+instanceIds+")";
		}
		 sql+=" ) a GROUP BY group_id";
		 Query query = super.entityManager.createNativeQuery(sql);
		 List<Object[]> list=query.getResultList();
		 Map<Integer,String> map =new HashMap<Integer, String>();
		 if(list!=null && list.size()>0){
			 for(Object[] obj:list){
				 Integer groupId=(Integer)obj[2];
				 Integer receive=obj[0]==null?0:(Integer)obj[0];
				 Integer finshNum=obj[1]==null?0:(Integer)obj[1];
				 Integer equipmentNum=obj[3]==null?0:(Integer)obj[3];
				 map.put(groupId,receive+"-"+finshNum+"-"+equipmentNum);
			 }
		 }
		 return map;
	 }
	
	 
	 /**
      * 查询后勤班组应接工量
      * @return
      */
	 public List<Object[]> findTakeWorkList(String beginTime,String endTime,Integer type){
		 String sql="SELECT e.instance_id,g.group_id,g.group_name FROM tb_group_info g ";
		 sql+=" left JOIN [View_repair_event] e ON e.next_processer_id=g.group_id where e.type="+type+" AND g.extension=2 and ((e.task_name =1 and e.approveResult=-1) or (e.task_name =0 and e.approveResult=4))";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		 Query query = super.entityManager.createNativeQuery(sql);
		 return query.getResultList();
	 }
	 
	 
	 
	 
	 
	 /**
	 * 员工工作量统计
	 * @return
	 */
	public List<Object[]> findStaffList(String beginTime,String endTime,String userName,String departmentName,Integer type){
	    String sql="select c.group_name,c.user_name,sum(CASE WHEN c.task_name=2 and c.approveResult=1 THEN 1 ELSE 0 END)as receive,sum(CASE WHEN c.task_name=3 and c.approveResult=1 THEN 1 ELSE 0 END)as finsh";
	    sql+=",sum(isnull(c.time_long,0)) as timeLong,sum(isnull(workHour,0))as workHour,sum(CASE WHEN c.task_name=3 and c.approveResult=1 THEN c.equipment_number ELSE 0 END) as equipmentNumber from(SELECT * FROM View_repair_group a";
	    sql+=" LEFT JOIN (SELECT v.*, ISNULL(e.work_hour, 0) workHour FROM	[View_repair_event] v";
	    sql+=" LEFT JOIN tb_event e ON v.equipment_name = e.event_name WHERE v.type="+type;
	    if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and v.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and v.end_time <='"+endTime+" 23:59:59'";
		}
	    sql+=" ) b ON a.user_id = b.processer_id WHERE a.extension=2 and a.group_id=1357 ";
	    sql+=" )c  where 1=1";
	    if(StringUtils.isNoneBlank(userName)){
	    	sql+=" and c.user_name like '%"+userName+"%'";
	    }
	    if(StringUtils.isNoneBlank(departmentName)){
	    	sql+=" and c.group_name like '%"+departmentName+"%'";
	    }
		sql+="  GROUP BY group_name,user_name ";
		//接工量  和  未完工量 无法一起比较。       
		//如果接工量 可以 理解为 指派给员工处理，则可以比较。  算法需要调整 需要关联 next_processer_id and task_name=2 and approveResult=1
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	 
	 /**
	  *医务科室上报统计
	  * @return
	  */
	public List<Object[]> findMedicalDepartmenList(String beginTime,String endTime,String departmentName,Integer type){
		String  sql="SELECT	a.group_name, SUM (CASE WHEN a.task_name = 1 and a.approveResult=-1 THEN 1 ELSE 0 END)as report,";
		sql+="SUM (CASE WHEN a.task_name = 4 and a.approveResult=1 THEN 1 ELSE 0 END ) as finshNum,sum(isnull(a.time_long,0)) as timeLong,sum(isnull(a.work_hour,0)) as workTime";
		sql+=" FROM(SELECT e.task_name,g.group_name,e.time_long,e.work_hour,e.approveResult FROM View_repair_group g";
		sql+=" LEFT JOIN (SELECT	r.*, e.work_hour  FROM [View_repair_event] r LEFT JOIN tb_event e ON e.event_name = r.equipment_name  WHERE r.type="+type;
		if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and r.end_time >='"+beginTime+" 00:00:00'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and r.end_time <='"+endTime+" 23:59:59'";
		}
		sql+=" 	) e ON g.user_id = e.create_user_id WHERE g.extension=3) a where 1=1 ";
		if(StringUtils.isNoneBlank(departmentName)){
			sql+=" and a.group_name like '%"+departmentName+"%'";
		}
		sql+=" GROUP  BY group_name";
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	
	/**
	 * 满意度
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param  userName 员工姓名
	 *                  @param type
	 */
	public List<Object[]> findSatisfaction(String beginTime,String endTime,String userName,Integer type){

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT g.user_name,g.group_name,a.fcmy,a.my,a.yb,a.bmy,a.user_id FROM (" +
				"SELECT a.user_id,SUM (CASE WHEN a.task_name = 3 AND a.approveResult = 1 AND a.achievement = 1 THEN 1 ELSE 0 END ) AS fcmy," +
				"SUM (CASE WHEN a.task_name = 3 AND a.approveResult = 1 AND a.achievement = 2 THEN 1 ELSE 0 END) AS my," +
				"SUM (CASE WHEN a.task_name = 3 AND a.approveResult = 1 AND a.achievement = 3 THEN 1 ELSE 0 END) AS yb," +
				"SUM (CASE WHEN a.task_name = 3 AND a.approveResult = 1 AND a.achievement = 4 THEN 1 ELSE 0 END) AS bmy");
		sql.append(" FROM ( SELECT g.user_id, e.task_name,e.equipment_name,g.user_name,e.achievement,e.approveResult,e.time_long FROM View_repair_group g INNER JOIN [View_repair_event] e ON g.user_id = e.processer_id WHERE g.group_id=1357  ");
		if(type!=null){
			sql.append(" and e.type = "+type+" ");
		}
		if(StringUtils.isNoneBlank(beginTime)){
			sql.append(" and e.end_time >='"+beginTime+" 00:00:00'");
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql.append(" and e.end_time <='"+endTime+" 23:59:59'");
		}
		if(StringUtils.isNoneBlank(userName)){
			sql.append(" and g.user_name like '%"+userName+"%'");
		}
		sql.append(" ) a GROUP BY user_id ) a LEFT JOIN View_repair_group g on a.user_id = g.user_id");
		Query query = super.entityManager.createNativeQuery(sql.toString());
		return query.getResultList();
	}
}
