package cn.com.qytx.hemei.cardispatch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.cardispatch.dao.DriverDao;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.hemei.cardispatch.service.IDriver;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
@Service
@Transactional
public class DriverImpl extends BaseServiceImpl<Driver> implements IDriver{

	@Autowired
	DriverDao  driverDao;
	@Override
	public Page<Driver> findDriver(Pageable pageable, Integer companyId) {
		// TODO Auto-generated method stub
		return driverDao.findDriver(pageable, companyId);
	}

	/**
	 * 查询司机
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	public List<Driver>  findDriverName(Integer companyId,String serachKey){
		return driverDao.findDriverName(companyId, serachKey);
	}

}
