package cn.com.qytx.hemei.defect.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.platform.base.service.BaseService;

public interface ISelectGoods extends BaseService<SelectGoods> {
	 /**
	    * 通过gooid 和InstanceId 查找记录
	    * @param goodId
	    * @param instanceId
	    * @return
	    */
		public SelectGoods findByGoodIdAndInstanceId(Integer goodId ,String instanceId);

		/**
		 * 通过流程Id 查询
		 * @return
		 */
		public List<SelectGoods> findList(String instanceId);
}
