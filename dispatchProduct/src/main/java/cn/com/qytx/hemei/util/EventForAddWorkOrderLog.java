package cn.com.qytx.hemei.util;

import org.springframework.context.ApplicationEvent;

import cn.com.qytx.hemei.defect.domain.WorkOrderLog;

public class EventForAddWorkOrderLog extends ApplicationEvent{

	public EventForAddWorkOrderLog(WorkOrderLog source) {
		super(source);
	}

	private static final long serialVersionUID = 1L;

}
