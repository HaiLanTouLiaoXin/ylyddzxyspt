package cn.com.qytx.hemei.event.service;


import java.util.List;

import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:事件接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2017年4月10日
 * 修改日期: 2017年4月10日
 * 修改列表: 
 */
public interface IEvent extends BaseService<Event>{

	public Page<Event> findEventPage(Pageable pageInfo,UserInfo userInfo, Event event);
	
	
	public List<Event> findEventList(Event event);
	
	/**
	 * 根据事件类型id 查询事件是否存在
	 * @param eventType
	 * @return
	 */
		public Event findModel( Integer eventType);
	public List<UserInfo> findUserListByLoginName(String loginName,Integer companyId);
	public Event findByName( String name);
}