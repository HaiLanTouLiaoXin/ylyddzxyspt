package cn.com.qytx.hemei.duty.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;
/**
 * 
 * 功能:值班接口
 * 版本: 1.0
 * 开发人员: 徐长江
 * 创建日期: 2016年5月16日
 * 修改日期: 2016年5月16日
 * 修改列表:
 */
public interface IWorkDutyService extends BaseService<WorkDuty>,Serializable{
	/**
	 * 
	 * 功能：根据值班日期得到值班信息
	 * @param workDate
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public List<WorkDuty> getWorkDutyByWorkDate(Date workDate);
	/**
	 * 
	 * @Title: deleteWorkDuty   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param dutyDate
	 */
	public void deleteWorkDuty(Date dutyDate,String dutyPost);
	
	
	/**
	 * 功能：通过userId获取某天是否值班
	 * @param workDate
	 * @param userId
	 * @return
	 */
	public WorkDuty getWorkDutyByUserId(Date workDate,Integer userId);
		
}