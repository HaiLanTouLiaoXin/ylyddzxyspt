/**
 * 
 */
package cn.com.qytx.hemei.location.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.platform.base.dao.BaseDao;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月19日
 * 修改日期: 2017年4月19日
 * 修改列表: 
 */
@Repository
public class GroupLocationDao extends BaseDao<GroupLocation, Integer> {
     
	/**
	 * 根据部门ID 查询
	 * @param groupId
	 * @return
	 */
	public List<GroupLocation> findList(Integer groupId){
		String hql = " groupId ="+groupId;
		return super.findAll(hql);
	}
	
	
	public GroupLocation findModel(Integer locationId){
		String hql = " locationId ="+locationId;
		return super.findOne(hql);
	}
	
}
