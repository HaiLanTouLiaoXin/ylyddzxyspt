package cn.com.qytx.hemei.util;

public class WorkOrderType {

	/**
	 * 工单添加
	 */
	public static Integer WORK_ORDER_ADD = 1;
	
	/**
	 * 工单修改
	 */
	public static Integer WORK_ORDER_UPDATE = 2;
	
	/**
	 * 工单作废
	 */
	public static Integer WORK_ORDER_DRAFT = 3;
	
	
	/**
	 * 工单调度
	 */
	public static Integer WORK_ORDER_DISPATCH = 4;
	
	/**
	 * 工单接收
	 */
	public static Integer WORK_ORDER_ACCEPT = 5;
	
	/**
	 * 工单转交
	 */
	public static Integer WORK_ORDER_TURN = 6;
	
	/**
	 * 工单处理
	 */
	public static Integer WORK_ORDER_DEFECT = 7;
	
	/**
	 * 工单申请延期
	 */
	public static Integer WORK_ORDER_APPLY_DELAY = 8;
	
	/**
	 * 工单 延期审批
	 */
	public static Integer WORK_ORDER_APPROVE_DELAY = 9;
	
	/**
	 * 工单验收
	 */
	public static Integer WORK_ORDER_CHECK_ACCEPT = 10;
	
	/**
	 * 工单 物资
	 */
	public static Integer WORK_ORDER_GOODS = 11;
	
	/**
	 * 工单 参与人
	 */
	public static Integer WORK_ORDER_PEOPLE = 12;
	
	/**
	 * 工单 设备
	 */
	public static Integer WORK_ORDER_EQUIPMENT = 13;
	
	
	
}
