package cn.com.qytx.hemei.cardispatch.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

/**
 * 功能：司机实体
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
@Entity
@Table(name="tb_dispatch_driver")
public class Driver extends BaseDomain implements java.io.Serializable{

	private static final long serialVersionUID = 8418952366110467728L;
	
	
	@Id
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="user_name")
	private String userName;
	@Transient
	private String shortUserName;
	
	/**
	 * 手机号
	 */
	@Column(name="phone")
	private String phone;
	
	/**
	 * 司机状态（ 1空闲 2 待发车 3 待回场（已发车） ）
	 */
	@Column(name="status")
	private Integer status;
	
	@Transient
	private String statuStr;
	
	
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete=0;
	
	/**
	 * 修改人
	 */
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 创建日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	@Transient
	private Integer no;//
	

	public Integer getUserId() {
		return userId;
	}

	public String getPhone() {
		return phone;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatuStr() {
		return statuStr;
	}

	public void setStatuStr(String statuStr) {
		this.statuStr = statuStr;
	}

	public String getShortUserName() {
		return shortUserName;
	}

	public void setShortUserName(String shortUserName) {
		this.shortUserName = shortUserName;
	}
  
	
	
	
}
