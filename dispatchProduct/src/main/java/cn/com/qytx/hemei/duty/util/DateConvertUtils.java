package cn.com.qytx.hemei.duty.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class DateConvertUtils {
	/**
	 * 产生日期
	 */
	public static List<Timestamp> createDate(String dateString) {
		List<Timestamp> timeList = new ArrayList<Timestamp>();
		// 定义分隔符
		String decollator = "######";
		if (StringUtils.isNotBlank(dateString)) {
			// 分割日期
			dateString = dateString.replaceAll("[,，、]", decollator);
			String[] dateArr = dateString.split(decollator);
			for (String singleDate : dateArr) {
				if (StringUtils.isNotBlank(singleDate)) {
					// 年月日处理
					// 替换掉日
					singleDate = singleDate.replaceAll("日", "");
					singleDate = singleDate.replaceAll("月", "-");
					singleDate = singleDate.replaceAll("年", "-");
					singleDate = singleDate.replaceAll("[/]", "-");
					String[] singleDateArr = singleDate.split("-");
					if (singleDateArr != null && singleDateArr.length == 3) {
						int year = 0;
						int month = 0;
						int day = 0;
						String yearStr = singleDateArr[0];
						String monthStr = singleDateArr[1];
						String dayStr = singleDateArr[2];
						// 判断日期正确
						boolean isRightYear = false;
						boolean isRightMonth = false;
						boolean isRightDay = false;
						// 判断年
						if (StringUtils.isNotBlank(yearStr) && NumberUtils.isDigits(yearStr)) {
							year = Integer.parseInt(yearStr);
							if (year > 2000 && year <= 9999) {
								isRightYear = true;
							}
						}
						// 判断月
						if (StringUtils.isNotBlank(monthStr) && NumberUtils.isDigits(monthStr)) {
							month = Integer.parseInt(monthStr);
							if (month >= 1 && month <= 12) {
								isRightMonth = true;
							}
						}
						// 判断日
						if (StringUtils.isNotBlank(dayStr) && NumberUtils.isDigits(dayStr)) {
							day = Integer.parseInt(dayStr);
							if (isRightYear && isRightMonth) {
								Calendar calendar = Calendar.getInstance();
								calendar.set(Calendar.YEAR, year);
								calendar.set(Calendar.MONTH, month - 1);
								int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
								if (day >= 1 && month <= lastDay) {
									isRightDay = true;
								}
							}
						}
						// 假如正确,装换成日期
						if (isRightYear && isRightMonth && isRightDay) {
							Calendar calendar = Calendar.getInstance();
							calendar.set(Calendar.YEAR, year);
							calendar.set(Calendar.MONTH, month - 1);
							calendar.set(Calendar.DAY_OF_MONTH, day);
							timeList.add(new Timestamp(calendar.getTimeInMillis()));
						}
					}
				}
			}
		}
		return timeList;
	}

	/**
	 * 产生日期
	 */
	public static Date createDate1(String dateString) {
		if (StringUtils.isNotBlank(dateString)) {
			// 年月日处理
			// 替换掉日
			dateString = dateString.replaceAll("日", "");
			dateString = dateString.replaceAll("月", "-");
			dateString = dateString.replaceAll("年", "-");
			dateString = dateString.replaceAll("[/]", "-");
			String[] singleDateArr = dateString.split("-");
			if (singleDateArr != null && singleDateArr.length == 3) {
				int year = 0;
				int month = 0;
				int day = 0;
				String yearStr = singleDateArr[0];
				String monthStr = singleDateArr[1];
				String dayStr = singleDateArr[2];
				// 判断日期正确
				boolean isRightYear = false;
				boolean isRightMonth = false;
				boolean isRightDay = false;
				// 判断年
				if (StringUtils.isNotBlank(yearStr) && NumberUtils.isDigits(yearStr)) {
					year = Integer.parseInt(yearStr);
					if (year > 2000 && year <= 9999) {
						isRightYear = true;
					}
				}
				// 判断月
				if (StringUtils.isNotBlank(monthStr) && NumberUtils.isDigits(monthStr)) {
					month = Integer.parseInt(monthStr);
					if (month >= 1 && month <= 12) {
						isRightMonth = true;
					}
				}
				// 判断日
				if (StringUtils.isNotBlank(dayStr) && NumberUtils.isDigits(dayStr)) {
					day = Integer.parseInt(dayStr);
					if (isRightYear && isRightMonth) {
						Calendar calendar = Calendar.getInstance();
						calendar.set(Calendar.YEAR, year);
						calendar.set(Calendar.MONTH, month - 1);
						int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
						if (day >= 1 && month <= lastDay) {
							isRightDay = true;
						}
					}
				}
				// 假如正确,装换成日期
				if (isRightYear && isRightMonth && isRightDay) {
					Calendar calendar = Calendar.getInstance();
					calendar.set(Calendar.YEAR, year);
					calendar.set(Calendar.MONTH, month - 1);
					calendar.set(Calendar.DAY_OF_MONTH, day);
					return new Date(calendar.getTimeInMillis());
				}
			}
		}
		return null;
	}
}
