package cn.com.qytx.hemei.duty.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.duty.dao.WorkDutyDao;
import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.hemei.duty.service.IWorkDutyService;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
/**
 * 值班接口实现类
 * 功能:
 * 版本: 1.0
 * 开发人员: 徐长江
 * 创建日期: 2016年5月16日
 * 修改日期: 2016年5月16日
 * 修改列表:
 */
@Service
@Transactional
public class WorkDutyImpl extends BaseServiceImpl<WorkDuty> implements IWorkDutyService,Serializable {
	/** 值班dao */
	@Autowired
	WorkDutyDao workDutyDao;
	
	/**
	 * 
	 * 功能：根据值班日期得到值班信息
	 * @param workDate
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public List<WorkDuty> getWorkDutyByWorkDate(Date workDate){
		return workDutyDao.getWorkDutyByWorkDate(workDate);
	}

	@Override
	public void deleteWorkDuty(Date dutyDate, String dutyPost) {
		workDutyDao.deleteWorkDuty(dutyDate,dutyPost);
	}

	@Override
	public WorkDuty getWorkDutyByUserId(Date workDate,Integer userId) {
		return workDutyDao.getWorkDutyByUserId(workDate,userId);
	}
	
	
}