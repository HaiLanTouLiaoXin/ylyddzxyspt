package cn.com.qytx.hemei.defect.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.hemei.defect.service.ISelectPeople;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;

public class SelectPeopleWapAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 908348374683536753L;

	
    private String instanceId;
	
	private Integer id;
	
	@Autowired
	private ISelectPeople selectPeopleService;
	
	@Autowired
	private IGroup groupService;
	
	@Autowired
	private IUser userService;
	
	private String userIds;//选择人员Id串
	
	private Integer userId;
	
	
	
	/**
	 * 保存参与人
	 */
	public  void saveSelectPeople(){
		
		try {
			if(userId!=null){
				UserInfo user=userService.findOne(userId);
				List<SelectPeople> list = selectPeopleService.findList(instanceId);
				List<Integer> haveUserIdList = new ArrayList<Integer>();
				if(list!=null&&list.size()>0){
					for(SelectPeople sp:list){
						haveUserIdList.add(sp.getUserInfo().getUserId());
					}
				}
				if(StringUtils.isNoneBlank(userIds)){
					String userArr[] = userIds.split(",");
					for(String userIdStr:userArr){
						Integer userId = Integer.valueOf(userIdStr);
						if(!haveUserIdList.contains(userId)){
							SelectPeople sp = new SelectPeople();
							sp.setCompanyId(user.getCompanyId());
							sp.setCreateTime(new Timestamp(System.currentTimeMillis()));
							sp.setCreateUserId(user.getUserId());
							sp.setInstanceId(instanceId);
							sp.setUserInfo(userService.findOne(userId));
							selectPeopleService.saveOrUpdate(sp);
						}
					}
				}
				    ajax("100||操作成功");
				}else{
					ajax("101||参数缺少");
				}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}

	public void findSelectPeopleList(){
		try {
			if(userId!=null){
				UserInfo user=userService.findOne(userId);
				Map<String,Object> jsonMap =new HashMap<String, Object>(); 
				if(user!=null){
					List<SelectPeople> list = selectPeopleService.findList(instanceId);
					List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
					if(list!=null && list.size()>0){
						List<GroupInfo> groupList = groupService.getGroupListByeExtension(user.getCompanyId(), 1,"1,2",null);
						Map<Integer,String> groupMap = new HashMap<Integer, String>();
						if(groupList!=null&& groupList.size()>0){
							for(GroupInfo gi:groupList){
								groupMap.put(gi.getGroupId(), gi.getGroupName());
							}
						}
						for(SelectPeople sp:list){
							Map<String,Object> map = new HashMap<String, Object>();
							UserInfo people = sp.getUserInfo();
							map.put("id", sp.getId());
							map.put("userName",StringUtils.isNoneBlank(people.getUserName())?people.getUserName():"--");//姓名
							map.put("workNo",StringUtils.isNoneBlank(people.getWorkNo())?people.getWorkNo():"--");//工号
							map.put("groupName",groupMap.containsKey(people.getGroupId())?groupMap.get(people.getGroupId()):"--");
							map.put("phone",StringUtils.isNoneBlank(people.getPhone())?people.getPhone():"--");
							map.put("peopleId",people.getUserId());
							listMap.add(map);
						}
					}
					Gson gson = new Gson();
					jsonMap.put("aaData", listMap);
					ajax("100||"+gson.toJson(jsonMap));
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
	   }
	}
	
	/**
	 * 删除已选择的参与人员
	 */
	public void deleteSelectPeople(){
		
		try {
			if(userId!=null){
				UserInfo user = userService.findOne(userId);
				if(user!=null){
					selectPeopleService.delete(id,true);
					ajax("100||操作成功");
				}
			}else{
				ajax("101||参数缺少");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}
	
	
	
	
}
