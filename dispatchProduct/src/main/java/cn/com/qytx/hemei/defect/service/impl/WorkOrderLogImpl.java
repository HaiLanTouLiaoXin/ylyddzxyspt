package cn.com.qytx.hemei.defect.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.dao.WorkOrderLogDao;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IWorkOrderLog;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service("workOrderLogService")
@Transactional
public class WorkOrderLogImpl extends BaseServiceImpl<WorkOrderLog> implements IWorkOrderLog {

	@Resource(name="workOrderLogDao")
	private WorkOrderLogDao workOrderLogDao;
	
	@Override
	public List<WorkOrderLog> findWorkOrderLogListByInstanceId(String instanceId) {
		return workOrderLogDao.findWorkOrderLogListByInstanceId(instanceId);
	}
	

	

	
}
