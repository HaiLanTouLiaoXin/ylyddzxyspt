package cn.com.qytx.hemei.cardispatch.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.CarOrderVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

@Repository
public class CarOrderDao extends BaseDao<CarOrder,Integer> {
	
	
	/**
	 * 查询车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo){
		String hql=" isDelete=0";
		if(carOrderVo!=null){
			if(carOrderVo.getReferralDepartment()!=null){
				hql+=" and referralDepartment="+carOrderVo.getReferralDepartment();
			}
			if(carOrderVo.getBusinessType()!=null){
				hql+=" and businessType="+carOrderVo.getBusinessType();
			}
			if(carOrderVo.getStatus()!=null){
				hql+=" and status ="+carOrderVo.getStatus();
			}
			if(carOrderVo.getOrderType()!=null){
				hql+=" and orderType ="+carOrderVo.getOrderType();
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCompany())){//
				hql+=" and company  like '%"+carOrderVo.getCompany()+"%'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCreateTime_begin())){
				hql+=" and createTime >='"+carOrderVo.getCreateTime_begin()+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCreateTime_end())){
				hql+=" and createTime <='"+carOrderVo.getCreateTime_end()+" 23:59:59'";
			}
		}
		return super.findAll(hql, pageable);
		
	}
	
	
	
	
	/**
	 * 查询待发车车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findMyWaitCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo){
		String hql=" isDelete=0";
		if(carOrderVo!=null){
			if(carOrderVo.getStatus()!=null){
				if(carOrderVo.getStatus()==5){//我的任务 查询已派遣和已发车
					hql+=" and status in (2,3,7)";
				}else{
					hql+=" and status ="+carOrderVo.getStatus();
				}
			}
		}
		if(userId!=null){
			hql+=" and instanceId in (select instanceId from MyWaitProcess where moduleCode='carDispatch' and processerId="+userId+")" ;
		}
		
		
		return super.findAll(hql, pageable);
		
	}
	
	
	/**
	 * 查询已办车车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findMyProcessedCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo){
		String hql=" isDelete=0";
		if(userId!=null){
			hql+=" and instanceId in (select  instanceId from MyProcessed where moduleCode='carDispatch' and processerId="+userId+")" ;
		}
		if(carOrderVo!=null){
			if(carOrderVo.getBusinessType()!=null){
				hql+=" and businessType="+carOrderVo.getBusinessType();
			}
			if(carOrderVo.getCarType()!=null){
				hql+=" and carType="+carOrderVo.getCarType();
			}
			if(StringUtils.isNoneBlank(carOrderVo.getArriveLocation())){
				hql+=" and arriveLocation like'%"+carOrderVo.getArriveLocation()+"%'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getStartTime_begin())){
				hql+=" and actualStartTime >='"+carOrderVo.getStartTime_begin()+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getStartTime_end())){
				hql+=" and actualStartTime <='"+carOrderVo.getStartTime_end()+" 23:59:59'";
			}
			
			if(StringUtils.isNoneBlank(carOrderVo.getCarNo())){
				hql+=" and car.carNo like '%"+carOrderVo.getCarNo()+"%'";
			}
			
			if(StringUtils.isNoneBlank(carOrderVo.getDriver())){
				hql+=" and driver.userId in( select userId from UserInfo u where u.userName like '%"+carOrderVo.getDriver()+"%')";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getReturnTime_bengin())){
				hql+=" and backFactoryTime >='"+carOrderVo.getReturnTime_bengin()+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getReturnTime_end())){
				hql+=" and backFactoryTime <='"+carOrderVo.getReturnTime_end()+" 23:59:59'";
			}
			if(carOrderVo.getStatus()!=null){
				hql+=" and status ="+carOrderVo.getStatus();
			}
			if(StringUtils.isNoneBlank(carOrderVo.getOrderType())){
				hql+=" and orderType ="+carOrderVo.getOrderType();
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCompany())){//
				hql+=" and company  like '%"+carOrderVo.getCompany()+"%'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCreateTime_begin())){
				hql+=" and createTime >='"+carOrderVo.getCreateTime_begin()+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(carOrderVo.getCreateTime_end())){
				hql+=" and createTime <='"+carOrderVo.getCreateTime_end()+" 23:59:59'";
			}
		}
		
		
		
		
		
		return super.findAll(hql, pageable);
	}
	
	
	
	/**
	 * 功能：获取当前的工单编号
	 * @return
	 */
	public String getCarInstanceId() {
		String sql="select max(instance_id) as instanceId from tb_dispatch_car_order where instance_id like  CONVERT(varchar(50),getdate(),112)+'%' and instance_id is not null";
		List<Object> list = super.entityManager.createNativeQuery(sql).getResultList();
		if(list!=null&&list.size()>0){
			Object obj=list.get(0);
			if(obj!=null){
				return obj.toString();
			}
		}
		return null;
	}
	
	
	/**
	 * 查询详情
	 * @Title: findByInstanceId   
	 * @param instanceId
	 * @return
	 */
	public CarOrder findByInstanceId(String instanceId) {
		String hql="isDelete=0 and instanceId=?";
		List<CarOrder> list=super.findAll(hql, instanceId);
		if(list!=null&&list.size()>0){
			return list.get(0);
	    }
		return null;
	}




	public Map<String, Integer> getCarOrderWaitNum(Integer companyId,
			Integer userId,Integer groupId) {
		Map<String,Integer> map = new HashMap<String, Integer>();
		String sql = "select isnull(sum(case when (wait.task_name=1) then 1 else 0 end),0) as waitDispatch,isnull(sum(case when (wait.task_name=2) then 1 else 0 end),0) as waitStartCar from  tb_cbb_my_wait_process wait "
				+ "where wait.module_code='carDispatch' and wait.company_id="+companyId+" and wait.processer_id="+userId;
		String waitConfirmSql = "select isnull(sum(case when (carOrder.status>1 and carOrder.is_accept=0) then 1 else 0 end),0) as waitConfirm  from tb_dispatch_car_order carOrder where carOrder.referral_department in (select g.group_id from tb_group_info g where g.group_id="+groupId+" or path like '"+groupId+",%' and g.is_delete=0 and g.company_id="+companyId+");";
		String confirmFactorySql = "select isnull(sum(case when ( carOrder.confirm_factory=0) then 1 else 0 end),0) as waitConfirmFactory  from tb_dispatch_car_order carOrder where carOrder.referral_department in (select g.group_id from tb_group_info g where g.group_id="+groupId+" or path like '"+groupId+",%' and g.is_delete=0 and g.company_id="+companyId+")";
		List<Object[]> list = super.entityManager.createNativeQuery(sql).getResultList();
		Integer waitConfirmNum = (Integer)super.entityManager.createNativeQuery(waitConfirmSql).getSingleResult();
		Integer confirmFactoryNum = (Integer)super.entityManager.createNativeQuery(confirmFactorySql).getSingleResult();
		map.put("waitConfirm", 0);
		map.put("confirmFactoryNum", 0);
		if(list!=null&&!list.isEmpty()){
			Object obj[] = list.get(0);
			map.put("waitDispatch", (Integer)obj[0]);
			map.put("waitStartCar", (Integer)obj[1]);
		}else{
			map.put("waitDispatch", 0);
			map.put("waitStartCar", 0);
		}
		if(waitConfirmNum!=null){
			map.put("waitConfirm", waitConfirmNum);
		}
		if(confirmFactoryNum!=null){
			map.put("confirmFactoryNum", confirmFactoryNum);
		}
		
		return map;
	}	
	/**
	 * 查询所车正在执行的工单数
	 * @return
	 */
	public Map<Integer,Integer> getCarOrderNum(Integer companyId){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql = "SELECT count(*),o.car_id FROM tb_dispatch_car_order o INNER JOIN tb_dispatch_car c ON o.car_id = c.id";
		sql+=" where c.is_delete=0 AND o.status in(2,3)";
		if(companyId!=null){
			sql+=" and o.company_id="+companyId;
		} 
		sql+=" GROUP BY o.car_id";
		List<Object[]> list = super.entityManager.createNativeQuery(sql).getResultList();
		if(list!=null&&!list.isEmpty()){
			for(Object[] obj:list){
				map.put((Integer)obj[1], (Integer)obj[0]);
			}
		}
		return map;
		
	}
	/**
	 * 查询司机正在执行的工单数
	 * @return
	 */
	public Map<Integer,Integer> getDriverOrderNum(Integer companyId){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		String sql = "SELECT count(*),o.driver_id FROM tb_dispatch_car_order o INNER JOIN tb_dispatch_driver d ON o.driver_id = d.user_id";
		sql+=" where d.is_delete=0 AND o.status in(2,3)";
		if(companyId!=null){
			sql+=" and o.company_id="+companyId;
		} 
		sql+=" GROUP BY o.driver_id";
		List<Object[]> list = super.entityManager.createNativeQuery(sql).getResultList();
		if(list!=null&&!list.isEmpty()){
			for(Object[] obj:list){
				map.put((Integer)obj[1], (Integer)obj[0]);
			}
		}
		return map;
		
	}
	
}
