package cn.com.qytx.hemei.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;


import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import cn.com.qytx.cbb.redpoint.utils.HttpPostUtil;
import cn.com.qytx.cbb.sso.server.cache.SSOCache;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.hemei.util.proxy.SinglePointLoginProxy;
import cn.com.qytx.hemei.util.proxy.TokenResult;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.CompanyInfo;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.ModuleInfo;
import cn.com.qytx.platform.org.domain.RoleInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.ICompany;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IModule;
import cn.com.qytx.platform.org.service.IRole;
import cn.com.qytx.platform.org.service.IUser;

public class OutLoginAction extends BaseActionSupport{
	private static final long serialVersionUID = 8259068093105785602L;

	@Resource
	private IUser userService;
	
    @Resource(name = "moduleService")
    private IModule moduleService;
    
    @Resource(name = "roleService")
    private IRole roleService;
    
    @Resource(name = "companyService")
    private ICompany companyService;
    
    @Resource
    private IGroup groupService;
    
    @Resource(name="eventService")
	private IEvent eventService;
    
    private String loginName;//工号
    
    private String phone;//手机号
    
    private UserInfo eventUser;
    
    private String requestType;//eventAdd 调度上报 ; waitDispatch 待调度;waitAccept 待接收;allEvent 全部事件
    
    private String applyPhone;//来电号码
    
    private String loginType; //eventAdd 事件上报  eventSearch 事件查询
    
    /**
	 * 功能：登录
	 * @return
	 */
	public String login(){
		try {
			//解析URL路径
			if(getRequest().getSession().getAttribute("adminUser")!=null){
				UserInfo adminUser = (UserInfo) getRequest().getSession().getAttribute("adminUser");
				if(adminUser.getLoginName()!=null && getRequest().getSession().getAttribute("moduleList")!=null){
					//List<UserInfo> eventUserList = userService.findUsersByLoginNames(phone);
					UserInfo userInfo = this.getLoginUser();
					List<UserInfo> eventUserList = eventService.findUserListByLoginName(phone,userInfo.getCompanyId());
					if(eventUserList!=null && eventUserList.size()>0){
						eventUser = eventUserList.get(0);
					}
					getRequest().getSession().setAttribute("dispatchWorkNo", loginName);
					return getResultByRequestType(requestType);
				}
			}
			List<UserInfo> userList = userService.findUsersByLoginNames(loginName);
			UserInfo user = null;
			if(userList!=null && userList.size()>0){
				user = userList.get(0);
			}
			if(user!=null){
				doLogin(user);
				List<UserInfo> eventUserList = eventService.findUserListByLoginName(phone,user.getCompanyId());
				if(eventUserList!=null && eventUserList.size()>0){
					eventUser = eventUserList.get(0);
				}
				/*UserDetails userDetails=userDetailsService.loadUserByUsername(loginName);
	        	PreAuthenticatedAuthenticationToken authentication = 
	        	new PreAuthenticatedAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
	        	authentication.setDetails(new WebAuthenticationDetails(request));
	        	SecurityContextHolder.getContext().setAuthentication(authentication);*/
			}else{
				return "error";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return getResultByRequestType(requestType);
	}
	
	
	public String loginSmp(){
		try{
			UserInfo userInfo = userService.getUserInfoByLoginName(1, loginName);
			if(userInfo!=null){
				Gson gson = new Gson();
				Integer groupId = userInfo.getGroupId();
				GroupInfo groupInfo = groupService.findOne(groupId);
				String path = groupInfo.getPath();
				String pathName = "";
				if(path.contains(",")){
					String pathArr[] = path.split(",");
					for(String tempPath:pathArr){
						GroupInfo tempGroup = groupService.findOne(Integer.valueOf(tempPath));
						pathName += tempGroup.getGroupName()+"/";
					}
					if(pathName.endsWith("/")){
						pathName = pathName.substring(0, pathName.length()-1);
					}
				}else{
					pathName = groupInfo.getGroupName();
				}
				
				Map<String,Object> pParamMap=new HashMap<String,Object>();
				if("eventAdd".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListAccept3.view?view");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				if("eventSearch".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				/*待调度*/
				if("waitDispatch".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view?view&status=01");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				/*待接收*/
				if("waitAccept".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view?view&status=02");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				/*待完工*/
				if("waitComplete".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view?view&status=03,05");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				/*待确认完工*/
				if("waitConfirmComplete".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view?view&status=06");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				/*待评论*/
				if("waitComment".equals(loginType)){
					pParamMap.put("view", "com.bonawise.smp.view.maintainListSearch.view?view&status=07");//pParamMap中的view的值，即是要打开的画面的URL地址
				}
				String gsonStr = "";
				if(!"waitNum".equals(loginType)){
					TokenResult pResult=
							SinglePointLoginProxy.requireToken("http://117.159.26.230:6060/smp", 
					                        "8B98839ED21ECED2",loginName, userInfo.getUserName(),pathName, pParamMap); 
							System.out.println(pResult.toString()+"/"+pResult.getMessage());
							/*TokenResult pResult=
									SinglePointLoginProxy.requireToken("http://zzzxtest.yyhq365.cn", 
											                           "8B98839ED21ECED2",loginName, userInfo.getUserName(),pathName, null); */
								System.out.println(pResult.toString());
								gsonStr = gson.toJson(pResult);
				}else{
					Map<String,Object> paramMap = new HashMap<String, Object>();
					paramMap.put("userId", loginName);
					gsonStr = HttpPostUtil.doPost("http://117.159.26.230:6060/smp/app_maintainController.do?getTaskAmount", paramMap);
				}
				ajax(gsonStr);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 功能：存放session
	 * @param user
	 */
	private void doLogin(UserInfo user){
		/**--------------初始化菜单 start----------------------*/
		String roleIdArr="";
        List<RoleInfo> roleList =roleService.getRoleByUser(user.getUserId()); //根据人员Id获取角色列表
        if(roleList!=null)
        {
            roleIdArr= getRoleIds(roleList);
        }
		List<ModuleInfo> moduleList = null;
        if(user.getIsDefault()!=null&&user.getIsDefault() == 0 ){
        	moduleList = moduleService.findAll();
        }else{
        	moduleList =moduleService.getModuleByRole(roleIdArr);//获取模块列表
        }
        getRequest().getSession().setAttribute("moduleList",moduleList);//把模块列表存放到session
        /**--------------初始化菜单 end----------------------*/
        
        CompanyInfo companyInfo=companyService.findOne(user.getCompanyId());
        HttpSession session = getRequest().getSession();
        session.setAttribute("companyInfo",companyInfo);//把单位列表存放到session
        session.setAttribute("adminUser",user);//把用户信息存放到session
        session.setAttribute("dispatchWorkNo", loginName);
        Integer groupId = user.getGroupId();
    	if(groupId!=null){
	    	GroupInfo gInfo = groupService.findOne(groupId);
	    	List<GroupInfo> subgrouplist = groupService.getSubGroupList(groupId);
	    	List<RoleInfo> roleList1 =roleService.getRoleByUser(user.getUserId());
	    	session.setAttribute("usergroup", gInfo);
	    	session.setAttribute("groupInfo", gInfo);
	    	session.setAttribute("usersubgroups", subgrouplist);
	    	session.setAttribute("userroles", roleList1);
    	}
    	//SSO单点登录服务启动
    	ssoLogin(user);
	}
    private void ssoLogin(UserInfo user) {
    	BufferedReader br = null;
		try {
			 String url = getRequest().getScheme() + "://"
			            + getRequest().getServerName() + ":" + getRequest().getServerPort()
			            +getRequest().getContextPath()+"/sso/login" ;
			URL u = new URL(url);
			HttpURLConnection uConnection = (HttpURLConnection) u.openConnection();
			uConnection.setDoOutput(true);
			uConnection.setUseCaches(false);
			uConnection.connect();
			InputStream is = uConnection.getInputStream();
			br = new BufferedReader(new InputStreamReader(is, "utf8"));
			StringBuilder sb = new StringBuilder();
			while (br.read() != -1) {
				sb.append(br.readLine());
			}
			String content = new String(sb);
			br.close();
			br = null;
			System.out.print("单点登录服务启动"+content);
			String token = UUID.randomUUID().toString();
			SSOCache.getInstance().store(token, user);
			getRequest().setAttribute("token", token);
			getRequest().getSession().setAttribute("token", token);
			
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	/**
     * 获取角色id列表
     * @param roleList
     * @return
     */
    private String getRoleIds(final List<RoleInfo> roleList)
    {
        StringBuffer roleIdArr= new StringBuffer();
        String result = "";
        for(RoleInfo role:roleList)
        {
            roleIdArr.append(role.getRoleId()+",");
        }
        if(roleIdArr.toString().endsWith(","))
        {
            result=roleIdArr.substring(0,roleIdArr.length()-1);
        }
        return result;
    }
    
    
    public static String getResultByRequestType(String requestType){
    	//eventAdd 调度上报 ; waitDispatch 待调度;waitAccept 待接收;allEvent 全部事件
    	/*String result = "eventAdd";
    	if("waitDispatch".equals(requestType)){
    		result = "waitDispatch";
    	}else if("waitAccept".equals(requestType)){
    		result = "waitAccept";
    	}else if("allEvent".equals(requestType)){
    		result = "allEvent";
    	}else{
    		result = "eventAdd";
    	}*/
    	return requestType;
    	
    }


	public String getLoginName() {
		return loginName;
	}


	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public UserInfo getEventUser() {
		return eventUser;
	}


	public void setEventUser(UserInfo eventUser) {
		this.eventUser = eventUser;
	}


	public String getRequestType() {
		return requestType;
	}


	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public String getApplyPhone() {
		return applyPhone;
	}


	public void setApplyPhone(String applyPhone) {
		this.applyPhone = applyPhone;
	}


	public String getLoginType() {
		return loginType;
	}


	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	
	
}
