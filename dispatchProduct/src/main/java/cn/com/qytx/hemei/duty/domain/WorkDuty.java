package cn.com.qytx.hemei.duty.domain;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 
 * 功能:值班实体类
 * 版本: 1.0
 * 开发人员: 徐长江
 * 创建日期: 2016年5月16日
 * 修改日期: 2016年5月16日
 * 修改列表:
 */
@Entity
@Table(name="tb_work_duty")
public class WorkDuty implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	/**ID 自增长  */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	@Expose
	private int id;
	/**创建时间*/
	@Column(name="create_time")
	@Expose
	private Timestamp createTime;
	/**创建人id ,值班人员*/
	@JoinColumn(name="create_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo  createUser;
	/**公司id*/
	@Column(name="company_id")
	private Integer companyId;
	/**值班时间*/
	@Column(name="duty_date")
	@Expose
	private Date  dutyDate;
	/**值班岗位*/
	@Column(name="duty_post")
	@Expose
	private String dutyPost;
	/**值班人员*/
	@Column(name="duty_persons")
	@Expose
	private String dutyPersons;
	/**值班电话*/
	@Column(name="duty_phone")
	@Expose
	private String dutyPhone;
	
	@Transient
	@Expose
	private Integer createUserId;
	
	@Transient
	@Expose
	private Integer isOwn;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	public UserInfo getCreateUser() {
		return createUser;
	}
	public void setCreateUser(UserInfo createUser) {
		this.createUser = createUser;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Date getDutyDate() {
		return dutyDate;
	}
	public void setDutyDate(Date dutyDate) {
		this.dutyDate = dutyDate;
	}
	public String getDutyPost() {
		return dutyPost;
	}
	public void setDutyPost(String dutyPost) {
		this.dutyPost = dutyPost;
	}
	public String getDutyPersons() {
		return dutyPersons;
	}
	public void setDutyPersons(String dutyPersons) {
		this.dutyPersons = dutyPersons;
	}
	public String getDutyPhone() {
		return dutyPhone;
	}
	public void setDutyPhone(String dutyPhone) {
		this.dutyPhone = dutyPhone;
	}
	public Integer getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}
	public Integer getIsOwn() {
		return isOwn;
	}
	public void setIsOwn(Integer isOwn) {
		this.isOwn = isOwn;
	}
	
}
