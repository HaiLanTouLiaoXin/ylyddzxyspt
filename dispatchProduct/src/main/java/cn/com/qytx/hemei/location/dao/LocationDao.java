package cn.com.qytx.hemei.location.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;

/**
 * 功能:位置持久层 版本: 1.0 开发人员: panbo 创建日期: 2016年8月2日 修改日期: 2016年8月2日 修改列表:
 */
@Repository("locationDao")
public class LocationDao extends BaseDao<Location, Integer> {

	public List<Location> findLocationList(Integer companyId) {
		String hql = "isDelete=0 and companyId=?";
		return super.findAll(hql, new Sort(Direction.ASC, "orderIndex"),
				companyId);
	}

	/**
	 * 在指定ID下面是否有相同的地点名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameLocationName(Integer parentId,String locationName,int companyId){
		String sql="parentId=? and locationName=?  and isDelete=0  and companyId = ?";
		Object object=super.findOne(sql, parentId,locationName,companyId);
		if(object==null){
			return false;
		}else{
			return true;
		}
	}



	/**
	 * 功能：判断是否存在子地点
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildLocation(Integer id, int companyId) {
		String hql = "isDelete=0 and parentId=? and companyId=?";
		List list = super.findAll(hql, id,companyId);
		if(list!=null&&list.size()>0){
			return true;
		}
		return false;
	}

	public List<Location> getLocationListByIds(String ids) {
		String hql = "1=0";
		if(StringUtils.isNotBlank(ids)){
			if(ids.startsWith(",")){
				ids = ids.substring(1);
			}
			if(ids.endsWith(",")){
				ids = ids.substring(0,ids.length()-1);
			}
			hql += "or id in("+ids+")";
		}
		return super.findAll(hql,new Sort(Direction.ASC,"id"));
	}
	
	 /**
     * 功能：获取知识库完整路径
     * @param knowledgeTypeId 知识库类型
     * @return String
     */
    public String getLocationPathNameById(int locationId)
    {
        String hql = "isDelete=0 and id = ?";
        Location location = (Location) super.findOne(hql, locationId);
        String res = "";

        if (null != location)
        {
        	String path = location.getPath();
        	if(path.startsWith(",")){
        		path = path.substring(1);
        	}
        	if(path.endsWith(",")){
        		path = path.substring(0,path.length()-1);
        	}
            // 获取此部门的级联上级部门
            String hqlAll = "isDelete=0 and id in (" + path + ")";
            List<Location> knowledgeTypeList = super.findAll(hqlAll);

            // 组装级联结果信息
            String[] allPath = path.split(",");
            StringBuffer pathSb = new StringBuffer();
            for (String groupId : allPath)
            {
                Location temp = getLocationFromList(knowledgeTypeList,
                        Integer.parseInt(groupId));
                if (null != temp)
                {
                    pathSb.append(temp.getLocationName());
                }
            }
            res = pathSb.toString();
        }
        return res;
    }

    private Location getLocationFromList(List<Location> locationList,
            Integer groupId)
    {
        for (Location location : locationList)
        {
            if (location.getId().intValue() == groupId.intValue())
            {
                return location;
            }
        }
        return null;
    }
}
