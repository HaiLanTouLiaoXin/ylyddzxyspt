package cn.com.qytx.hemei.util.excel;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
/**
 * Excel 工具类
 * 黄普友
 */
public class ExcelUtils {

    /**
     * 生产模板文件
     * @param out 输出流
     * @param headers 模板表头信息
     * @throws IOException
     * @throws Exception
     */
    public static void createTemplate(OutputStream out,String[] headers)throws IOException
    {
            HSSFWorkbook wb = new HSSFWorkbook();//建立新HSSFWorkbook对象
            HSSFSheet sheet = wb.createSheet("Sheet1");//建立新的sheet对象
            HSSFRow row = sheet.createRow((short) 0);//建立新行
            if (headers != null) {
                HSSFCell[][] cells= new HSSFCell[1][headers.length];//二维数组
                for (int i = 0; i < headers.length; i++) {
                    cells[0][i] = row.createCell(i);
                    cells[0][i].setCellValue(headers[i]);
                }
                wb.write(out);
            }
    }
}
