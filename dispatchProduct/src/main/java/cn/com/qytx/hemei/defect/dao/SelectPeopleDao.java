package cn.com.qytx.hemei.defect.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.platform.base.dao.BaseDao;
@Repository
public class SelectPeopleDao extends BaseDao<SelectPeople, Integer> {
   /**
    * 通过peopleId 和InstanceId 查找记录
    * @param peopleId
    * @param instanceId
    * @return
    */
	public SelectPeople findByPeopleIdAndInstanceId(Integer peopleId ,String instanceId){
		String hql = " isDelete=0  and userInfo.userId="+peopleId+" and instanceId='"+instanceId+"'";
		return super.findOne(hql);
	}
	
	/**
	 * 通过流程Id 查询
	 * @return
	 */
	public List<SelectPeople> findList(String instanceId){
		String hql =" 1=1";
		if(StringUtils.isNoneBlank(instanceId)){
			hql += " and  instanceId='"+instanceId+"'";
		}
		return super.findAll(hql);
	}
	
	public Map<String,Integer> getPeopleMap(Integer companyId){
		String sql = "select instance_id,count(*) from tb_cbb_people_select group by instance_id ";
		Map<String,Integer> map = new HashMap<String, Integer>();
		List<Object[]> list = super.entityManager.createNativeQuery(sql).getResultList();
		if(list!=null&&!list.isEmpty()){
			for(Object[] obj:list){
				map.put((String)obj[0], (Integer)obj[1]);
			}
		}
		return map;
	}
	
}
