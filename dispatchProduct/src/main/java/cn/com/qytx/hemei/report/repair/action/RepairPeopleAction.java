/**
 * 
 */
package cn.com.qytx.hemei.report.repair.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.IRepairReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月12日
 * 修改日期: 2017年5月12日
 * 修改列表: 
 */
public class RepairPeopleAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1462193030254924389L;
	
	@Autowired
	private IRepairReport repairReportService;
	
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private String userName;
	private String departmentName;
	/**
	 * 后勤维修人员工作量统计
	 */
	public void findRepairPeopleList(){
		UserInfo user= getLoginUser();
		if(user!=null){
			try {
				userName=URLDecoder.decode(userName, "utf-8");
				departmentName=URLDecoder.decode(departmentName, "utf-8");
				List<Object[]> takeWorkList = repairReportService.findTakeWorkList(beginTime,endTime);
				String instanceIds="";
				if(takeWorkList!=null && takeWorkList.size()>0){
					for(Object[] obj:takeWorkList){
						instanceIds+="'"+(String)obj[0]+"',";
					}
					instanceIds=instanceIds.substring(0, instanceIds.length()-1);
				}
				List<Object[]> list = repairReportService.findRepairPeople(beginTime, endTime, userName, departmentName,instanceIds);
	    		List<Map<String,Object>> listMap= getMapList(list);
	    		ajax(listMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		
		if(list!=null && list.size()>0){
			int i=1;
			Integer receiveNum=0;
			Integer finishNum=0;
			Integer noFinishNum=0;
            Double workTimeSum=0.0;
            Double longTimeSum=0.0;
            Double time=0.0;
            DecimalFormat df =new DecimalFormat("0.00");
			for(Object[] obj:list){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("no", i);
				map.put("name", obj[1].toString());
				map.put("departmentName", obj[0].toString());
				Integer receive=Integer.valueOf(obj[2].toString());
				Integer noFinish=Integer.valueOf(obj[2].toString())-Integer.valueOf(obj[3].toString());
				if(receive==0){
					noFinish=0;
				}
				map.put("receiveNum", receive);//接收
				map.put("finishNum",Integer.valueOf(obj[3].toString()));
				
				map.put("noFinishNum",noFinish);
				map.put("workTimeSum", df.format(Double.valueOf(obj[5].toString())));//工时总计
				Double sumTime=Double.valueOf(obj[4].toString());
				map.put("longTimeSum", df.format(sumTime));//总耗时
				Double num=0.0;
				if(Integer.valueOf(obj[3].toString())!=0){
					 num= Double.valueOf(obj[4].toString())/Integer.valueOf(obj[3].toString());
					 map.put("avgTime", df.format(num));//平均时长
				}else{
					map.put("avgTime", 0);//平均时长
				}
				   
				
				listMap.add(map);
				receiveNum+=receive;
				finishNum+=Integer.valueOf(obj[3].toString());
				noFinishNum+=noFinish;
				workTimeSum+=Double.valueOf(obj[5].toString());
				longTimeSum+=sumTime;
				time+=num;
				i++;
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("no", "合计");
			map.put("name", "");
			map.put("departmentName", "");
			map.put("receiveNum", receiveNum);
			map.put("finishNum",receiveNum);
			map.put("noFinishNum",noFinishNum);
			map.put("workTimeSum",df.format(workTimeSum));
			map.put("longTimeSum",df.format(longTimeSum));
		    map.put("avgTime", df.format(time));//平均时长
			listMap.add(map);
		}
		return listMap;
	}
	
	
	 /**
     * 导出维修人员工作量
     */
    public  void exportPeopleWork(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				userName=URLDecoder.decode(userName, "utf-8");
				departmentName=URLDecoder.decode(departmentName, "utf-8");
				List<Object[]> takeWorkList = repairReportService.findTakeWorkList(beginTime,endTime);
				String instanceIds="";
				if(takeWorkList!=null && takeWorkList.size()>0){
					for(Object[] obj:takeWorkList){
						instanceIds+="'"+(String)obj[0]+"',";
					}
					instanceIds=instanceIds.substring(0, instanceIds.length()-1);
				}
				List<Object[]> list = repairReportService.findRepairPeople(beginTime, endTime, userName, departmentName,instanceIds);
	    		List<Map<String,Object>> listMap= getMapList(list);
				String fileName = URLEncoder.encode("维修人员工作量统计.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("维修人员工作量统计");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("姓名");
        headList.add("班组名称");
        headList.add("接工量");
        headList.add("完工量");
        headList.add("未完工量");
        headList.add("处理耗时");
        headList.add("平均处理时长");
        headList.add("工时总计");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("departmentName");
        headList.add("receiveNum");
        headList.add("finishNum");
        headList.add("noFinishNum");
        headList.add("longTimeSum");
        headList.add("avgTime");
        headList.add("workTimeSum");
        return headList;
    }
	
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	
	
	
	
	
	

}
