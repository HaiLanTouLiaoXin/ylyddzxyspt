package cn.com.qytx.hemei.cardispatch.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

/**
 * 功能：车辆表
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
@Entity
@Table(name="tb_dispatch_car")
public class Car extends BaseDomain implements java.io.Serializable{

	private static final long serialVersionUID = 8418952366110467728L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	/**
	 * 车牌号
	 */
	@Column(name="car_no")
	private String carNo;
	
	/**
	 * 车辆状态（1 空闲 2 待发车 3 待回场（已发车） ）
	 */
	@Column(name="status")
	private Integer status;
	
	@Transient
	private String statusStr;
	
	
	/**
	 * 车辆类型（1 转诊车 2 急诊车） 
	 */
	@Column(name="car_type")
	private Integer carType;
	
	/**
	 * 车辆品牌 
	 */
	@Column(name="car_brand")
	private String carBrand;
	
	/**
	 * 车辆型号 
	 */
	@Column(name="car_model")
	private String carModel;
	
	
	/**
	 * 车辆型手机号
	 */
	@Column(name="car_phone")
	private String carPhone;
	
	/**
	 * 历史任务数
	 */
	@Column(name="history_num")
	private Integer historyNum;
	
	
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete=0;
	
	/**
	 * 修改人
	 */
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	
	
	@Transient
	private Integer no;//序号
	
	/**
	 * 创建日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	/**
	 * 是否有担架  1 有  2 没有
	 */
	@Column(name="hava_stretcher")
	private Integer havaStretcher;
	
	/**
	 * 是否被占用  1 有  2 没有
	 */
	@Column(name="occupy")
	private Integer occupy;
	
	/**
	 * 座位数
	 */
	@Column(name="seat_num")
	private Integer seatNum;
	
	/**
	 * 剩余座位数
	 */
	@Column(name="rest_seatNum")
	private Integer restSeatNum;
	
	/**
	 * 发车时间
	 */
	@Column(name="departure_time")
	private Timestamp departureTime;

	public Integer getId() {
		return id;
	}

	public String getCarNo() {
		return carNo;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getCarType() {
		return carType;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public String getCarModel() {
		return carModel;
	}

	public Integer getHistoryNum() {
		return historyNum;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public void setHistoryNum(Integer historyNum) {
		this.historyNum = historyNum;
	}


	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getCarPhone() {
		return carPhone;
	}

	public void setCarPhone(String carPhone) {
		this.carPhone = carPhone;
	}

	public Integer getHavaStretcher() {
		return havaStretcher;
	}

	public void setHavaStretcher(Integer havaStretcher) {
		this.havaStretcher = havaStretcher;
	}

	public Integer getOccupy() {
		return occupy;
	}

	public void setOccupy(Integer occupy) {
		this.occupy = occupy;
	}

	public Integer getSeatNum() {
		return seatNum;
	}

	public void setSeatNum(Integer seatNum) {
		this.seatNum = seatNum;
	}

	public Integer getRestSeatNum() {
		return restSeatNum;
	}

	public void setRestSeatNum(Integer restSeatNum) {
		this.restSeatNum = restSeatNum;
	}

	public Timestamp getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Timestamp departureTime) {
		this.departureTime = departureTime;
	}
  
	
}
