package cn.com.qytx.hemei.cardispatch.dao;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.PageImpl;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 功能：司机持久层
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月11日
 * 修改日期：2017年9月11日	
 */
@Repository("driverDao")
public class DriverDao extends BaseDao<Driver,Integer> {
	
	/**
	 * 查询司机
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	public Page<Driver>  findDriver(Pageable pageable, Integer companyId){
		String sql=" SELECT  u.user_name,u.phone,d.status,u.user_id FROM tb_user_info u";
		sql+=" INNER JOIN tb_group_info g ON u.group_id = g.group_id";
		sql+=" LEFT JOIN tb_dispatch_driver d ON u.user_id=d.user_id and d.company_id="+companyId+" and d.is_delete=0";
		sql+=" WHERE g.branch = '1' AND g.is_delete = 0";
		if(companyId!=null){
			sql+=" and g.company_id="+companyId;
		}
		sql += " order by d.status asc";
		String countSql=" SELECT count(*) FROM tb_user_info u INNER JOIN tb_group_info g ON u.group_id = g.group_id";
		countSql+=" WHERE g.branch = '1' AND g.is_delete = 0";
		if(companyId!=null){
			countSql+=" and g.company_id="+companyId;
		}
		Object o=this.entityManager.createNativeQuery(countSql).getSingleResult();
		int count=0;
		if(o!=null){
			count=(Integer)o;
		}
		@SuppressWarnings("unchecked")
		List<Object[]> list=this.entityManager.createNativeQuery(sql).setFirstResult(pageable.getOffset())
			.setMaxResults(pageable.getPageSize()).getResultList();
		
		List<Driver> applyList=new ArrayList<Driver>();
		if(list!=null && list.size()>0){
			for(Object[] obj:list){
				Driver d=new Driver();
				String name=(String)obj[0];
				String phone=obj[1]!=null?(String)obj[1]:"--";
				Integer state=obj[2]==null?1:(Integer)obj[2];
				Integer userId=obj[3]==null?1:(Integer)obj[3];
				d.setUserName(name);
				d.setPhone(phone);
				d.setStatus(state);
				d.setUserId(userId);
				applyList.add(d);
			}
		}
		return new PageImpl<Driver>(applyList,pageable,count);
	} 
	
	
	/**
	 * 查询司机
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	public List<Driver>  findDriverName(Integer companyId,String serachKey){
		String sql=" SELECT  u.user_name,u.phone,d.status,u.user_id FROM tb_user_info u";
		sql+=" INNER JOIN tb_group_info g ON u.group_id = g.group_id";
		sql+=" LEFT JOIN tb_dispatch_driver d ON u.user_id=d.user_id and d.company_id="+companyId+" and d.is_delete=0";
		sql+=" WHERE g.branch = '1' AND g.is_delete = 0";
		if(companyId!=null){
			sql+=" and g.company_id="+companyId;
		}
		if(StringUtils.isNoneBlank(serachKey)){
			sql+=" AND u.user_name like '%"+serachKey+"%'";
		}
		sql += " order by d.status asc";
		@SuppressWarnings("unchecked")
		List<Object[]> list=this.entityManager.createNativeQuery(sql).getResultList();
		List<Driver> applyList=new ArrayList<Driver>();
		if(list!=null && list.size()>0){
			for(Object[] obj:list){
				Driver d=new Driver();
				String name=(String)obj[0];
				String phone=obj[1]!=null?(String)obj[1]:"--";
				Integer state=obj[2]==null?1:(Integer)obj[2];
				Integer userId=obj[3]==null?1:(Integer)obj[3];
				d.setUserName(name);
				d.setPhone(phone);
				d.setStatus(state);
				d.setUserId(userId);
				applyList.add(d);
			}
		}
		return applyList;
	} 
	
	
}
