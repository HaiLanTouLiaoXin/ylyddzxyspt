package cn.com.qytx.hemei.report.send.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.ISendReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月15日
 * 修改日期: 2017年5月15日
 * 修改列表: 
 */
public class SendStaffAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6930200586566799305L;

	@Autowired
	private ISendReport sendReportService;
	
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private Integer type;// 1:配送 2:归还
	private String userName;
	private String departmentName;
	/**
	 * 人员工作量统计
	 */
	public void findStaffSend(){
		UserInfo user= getLoginUser();
		if(user!=null){
			try {
				if(StringUtils.isNoneBlank(userName)){
					userName=URLDecoder.decode(userName, "utf-8");
				}
				if(StringUtils.isNoneBlank(departmentName)){
					departmentName=URLDecoder.decode(departmentName, "utf-8");
				}
				List<Object[]> list = sendReportService.findStaff(beginTime, endTime, userName, departmentName,type);
	    		List<Map<String,Object>> listMap= getMapList(list);
	    		ajax(listMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int i=1;
			Integer receiveNum=0;
			Integer finishNum=0;
			Integer noFinishNum=0;
            Double workTimeSum=0.0;
            Double longTimeSum=0.0;
            Double time=0.0;
            Integer equipmentNumberSum=0;
            DecimalFormat df =new DecimalFormat("0.00");
			for(Object[] obj:list){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("no", i);
				map.put("name", obj[1].toString());
				map.put("departmentName", obj[0].toString());
				map.put("receiveNum", Integer.valueOf(obj[2].toString()));//接收
				map.put("finishNum",Integer.valueOf(obj[3].toString()));
				Integer noFinish=Integer.valueOf(obj[2].toString())-Integer.valueOf(obj[3].toString());
				map.put("noFinishNum",noFinish<0?0:noFinish);
				map.put("workTimeSum", df.format(Double.valueOf(obj[5].toString())));//工时总计
				Double sumTime=Double.valueOf(obj[4].toString());
				map.put("longTimeSum", df.format(sumTime));//总耗时
				Double num=0.0;
				if(Integer.valueOf(obj[3].toString())!=0){
					 num= Double.valueOf(obj[4].toString())/Integer.valueOf(obj[3].toString());
					 map.put("avgTime", df.format(num));//平均时长
				}else{
					 map.put("avgTime", 0);//平均时长
				}
				Integer equipmentNumber = (Integer)obj[6];
				map.put("equipmentNumber",equipmentNumber );//设备数量
				listMap.add(map);
				receiveNum+=Integer.valueOf(obj[2].toString());
				finishNum+=Integer.valueOf(obj[3].toString());
				noFinishNum+=noFinish;
				workTimeSum+=Double.valueOf(obj[5].toString());
				longTimeSum+=sumTime;
				equipmentNumberSum+=equipmentNumber;
				time+=num;
				i++;
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("no", "合计");
			map.put("name", "");
			map.put("departmentName", "");
			map.put("receiveNum", receiveNum);
			map.put("finishNum",receiveNum);
			map.put("equipmentNumber",equipmentNumberSum);
			map.put("noFinishNum",noFinishNum);
			map.put("workTimeSum",df.format(workTimeSum));
			map.put("longTimeSum",df.format(longTimeSum));
		    map.put("avgTime", df.format(time));//平均时长
			listMap.add(map);
		}
		return listMap;
	}
	
	/**
	 * 获得图表数据
	 */
	public void findStaffChart(){
		UserInfo user = getLoginUser();
		if(user!=null){
			List<Object[]> list = sendReportService.findStaff(beginTime, endTime, userName, departmentName,type);
			List<String> xname=new ArrayList<String>();
			List<Integer> finish=new ArrayList<Integer>();
			List<Integer> nofinsh=new ArrayList<Integer>();
			List<Integer> sum=new ArrayList<Integer>();
			String[] completeName={"接工量","完工量","未完工量"};
			if(list!=null&& list.size()>0){
				for(Object[] obj:list){
					xname.add(obj[1].toString());
					finish.add(obj[3]==null?0:Integer.valueOf(obj[3].toString()));
					sum.add(obj[2]==null?0:Integer.valueOf(obj[2].toString()));
					Integer noFinish=0;
					if(obj[2]!=null&&obj[3]!=null){
						noFinish=Integer.valueOf(obj[2].toString())-Integer.valueOf(obj[3].toString());
					}
					nofinsh.add(noFinish<0?0:noFinish);
				}
			}
			String[] xnameArray= new String[xname.size()];
			xname.toArray(xnameArray);
			Integer[]finishArray= new Integer[finish.size()];
			finish.toArray(finishArray);
			Integer[]noFinishArray= new Integer[nofinsh.size()];
			nofinsh.toArray(noFinishArray);
			Integer[]sumArray= new Integer[sum.size()];
			sum.toArray(sumArray);
			Map<String,Object> map =new HashMap<String, Object>();
			map.put("name", xnameArray);
			map.put("completeName", completeName);
			map.put("finishArray", finishArray);
			map.put("noFinishArray", noFinishArray);
			map.put("sumArray", sumArray);
			ajax(map);
		}
	}
	
	 /**
     * 导出人员工作量
     */
    public  void export(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				String excelName = "";
				if(type==1){
					excelName = "员工配送工作量";
				}else if(type==2){
					excelName = "员工归还工作量";
				}
				userName=URLDecoder.decode(userName, "utf-8");
				departmentName=URLDecoder.decode(departmentName, "utf-8");
				List<Object[]> list = sendReportService.findStaff(beginTime, endTime, userName, departmentName,type);
	    		List<Map<String,Object>> listMap= getMapList(list);
				String fileName = URLEncoder.encode(excelName+".xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName(excelName);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        String  rowName = "";
		if(type==1){
			rowName = "配送";
		}else if(type==2){
			rowName = "归还";
		}
        headList.add("序号");
        headList.add("姓名");
        headList.add("班组名称");
        headList.add("接工量");
        headList.add("完工量");
        headList.add("配送设备量");
        headList.add("未完工量");
        headList.add(rowName+"总耗时");
        headList.add("平均"+rowName+"耗时");
       // headList.add("工时总计");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("departmentName");
        headList.add("receiveNum");
        headList.add("finishNum");
        headList.add("equipmentNumber");
        headList.add("noFinishNum");
        headList.add("longTimeSum");
        headList.add("avgTime");
        //headList.add("workTimeSum");
        return headList;
    }
	
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
}
